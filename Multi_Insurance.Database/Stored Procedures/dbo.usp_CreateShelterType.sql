SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateShelterType] 
	-- Add the parameters for the stored procedure here
	@id int = 0, 
	@branchCode smallint,
	@name varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Amparos As Target 
		Using (Select @id, @name, @branchCode) As Source(CodAmp, NomAmp, CodtipRam)
			On (Target.CodAmp = Source.CodAmp)
		When Matched Then
			Update Set nomAmp = Source.NomAmp,
					   CodtipRam = Source.CodtipRam
		When Not Matched Then
			Insert 
				(
					--CodAmp - this column value is auto-generated
					NomAmp,
					CodTipRam,
					FecUltMod
				)
				Values
				(
					-- CodAmp - int
					Source.NomAmp, -- NomAmp - varchar
					Source.CodtipRam, -- CodTipRam - smallint
					GetDate()-- FecUltMod - datetime
				)
	Output $action;
			
END



GO
