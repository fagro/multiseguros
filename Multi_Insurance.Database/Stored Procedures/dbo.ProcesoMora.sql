SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[ProcesoMora]
        	@CodPro  VARCHAR(15),  
	@FechaLim Datetime,
	@FechaEnv Datetime
AS  

DECLARE @CodProA CHAR(16), 
	@CodCer Float,
	@ConCer Float

set nocount on
  DECLARE Recibos1 CURSOR
  FOR SELECT CodPro, CodCer, ConCer
	FROM Recibos
	WHERE (CodTipEstRec < 7 Or CodTipEstRec >9) AND FecRec <= @FechaLim  AND FecEnvRec = @FechaEnv
	GROUP BY CodPro, CodCer, ConCer
	HAVING CodPro = @CodPro AND CodCer > 0  AND ConCer > 0 
	ORDER BY CodPro, CodCer, ConCer
  FOR Read Only
  OPEN Recibos1
  FETCH Recibos1 INTO @CodProa, @CodCer, @ConCer
  WHILE @@fETCH_STATUS = 0
  BEGIN
  
	Update Certificados   
   	set codtipestcer = 3, FecTerCer = @FechaEnv, FecUltMod = GETDATE(), CodTipMotCan = 6, Observaciones = 'Cancelado por mora'  
	where 	CodPro = @CodProa and CodCer = @CodCer and ConCer = @ConCer  

	Update Recibos  
       	set codtipestrec = 7, fecultmod = GETDATE()   
	where 	CodPro = @CodProa and CodCer = @CodCer and ConCer = @ConCer And FecRec >= '1/1/1990'  and  (codtipestrec < 7 or   codtipestrec > 9)

	insert into RecPolCan select * from dbo.Recibos where CodPro = @CodProa and CodCer = @CodCer and ConCer = @ConCer And FecRec >= '1/1/1990' 

	Delete Recibos  
	where 	CodPro = @CodProa and CodCer = @CodCer and ConCer = @ConCer And fecrec >= '1/1/1990'  

  FETCH Recibos1 INTO @CodProa, @CodCer, @ConCer
  END
  DEALLOCATE Recibos1
set nocount off

Select	*  FROM certificados (nolock)
where 	Codpro = @CodProa and  FecTerCer = @FechaEnv and CodTipMotCan = 6
order By CodPro, Codcer, Concer







GO
