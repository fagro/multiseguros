SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2012-08-13
-- Description:	Efectua devoluciones de los pagos realizados a los certificados
-- =============================================
CREATE PROCEDURE [dbo].[Usp_ARefundPayment] 

	-- Add the parameters for the stored procedure here
	@codigoCertificado float,
	@fechaDesde datetime = Null, 
	@fechaHasta datetime = Null,
	@codigoProducto varchar(25),
	@consecutivoCertificado int = null,
	@fechaDevolucion datetime = Null,
	@motivoDevolucion as smallint = null,
	@Activo as tinyint

AS

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	
		BEGIN TRANSACTION
	
			SET NOCOUNT ON;
			SET XACT_ABORT ON;


			--Declare statements for testing sp.
			
			--DECLARE @fechaDesde DateTime = '2012-05-30'
			--DECLARE @fechaHasta DateTime = '2012-10-30'
			--DECLARE @CodigoCertificado INT = 5808
			--DECLARE @CodigoProducto Varchar(25) = '2-102-495'
			--DECLARE @consecutivoCertificado int = 1
			--DECLARE @fechaDevolucion datetime = '2012-12-01'
			--DECLARE @motivoDevolucion as smallint = 7
			

if @Activo = 0
		begin 
			--Updates the state of an invoice in recpolcan TABLE with the proper state, in this case 8.
			UPDATE RecPolCan 
			SET codTipEstRec = 8,
				FecUltMod = GETDATE()			 
			WHERE CodCer = @codigoCertificado 
				AND FecRec BETWEEN @fechaDesde AND @fechaHasta
				AND CodPro = @codigoProducto
				AND ConCer  = @consecutivoCertificado
				
						--Save a log of the Refunds transactions.
			INSERT INTO Devoluciones ([CodPro],[CodCer],[ConCer],[FecDev],[NumCue],[ValDev],[CodTipMedPag],
									  [NumDocIde],[FecUltMod],[CodTipMotDev],[FecExpCer], [CodOfi])
			SELECT CodPro,
				   CodCer,
				   ConCer,
				   @fechaDevolucion as FecDev,
				   NumCue,
				   SUM(ValRec) as ValDev,
				   CodTipMedPag,
				   NumDocIde,
				   GETDATE() as FecUltMod,
				   @motivoDevolucion as CodTipMotDev,
				   FecExpCer,
				   CodOfi 				   
			FROM RecPolCan
			WHERE   CodPro = @codigoProducto
				AND CodCer = @codigoCertificado 
				AND ConCer = @consecutivoCertificado		
				AND FecRec BETWEEN @fechaDesde AND @fechaHasta	
			Group by CodPro, CodCer, ConCer, NumCue, CodTipMedPag, NumDocIde, FecExpCer, CodOfi	
							
		end
			else

			--Updates the state of an invoice in Recibos TABLE with the proper state, in this case 8.
			UPDATE Recibos 
			SET codTipEstRec = 8,
				FecUltMod = GETDATE()			 
			WHERE CodCer = @codigoCertificado 
				AND FecRec BETWEEN @fechaDesde AND @fechaHasta
				AND CodPro = @codigoProducto
				AND ConCer  = @consecutivoCertificado
				
			--Save a log of the Refunds transactions.
			INSERT INTO Devoluciones ([CodPro],[CodCer],[ConCer],[FecDev],[NumCue],[ValDev],[CodTipMedPag],
									  [NumDocIde],[FecUltMod],[CodTipMotDev],[FecExpCer], [CodOfi])
			SELECT CodPro,
				   CodCer,
				   ConCer,
				   @fechaDevolucion as FecDev,
				   NumCue,
				   SUM(ValRec) as ValDev,
				   CodTipMedPag,
				   NumDocIde,
				   GETDATE() as FecUltMod,
				   @motivoDevolucion as CodTipMotDev,
				   FecExpCer,
				   CodOfi 				   
			FROM Recibos
			WHERE   CodPro = @codigoProducto
				AND CodCer = @codigoCertificado 
				AND ConCer = @consecutivoCertificado		
				AND FecRec BETWEEN @fechaDesde AND @fechaHasta	
			Group by CodPro, CodCer, ConCer, NumCue, CodTipMedPag, NumDocIde, FecExpCer, CodOfi	
															   					
		COMMIT TRANSACTION

END TRY

BEGIN CATCH 
	IF @@TRANCOUNT <> 0 ROLLBACK TRANSACTION
	RAISERROR('ERROR no se pudo completar el stored procedures',10,1);

END CATCH







GO
