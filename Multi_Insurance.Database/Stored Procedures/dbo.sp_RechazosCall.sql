SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_rechazoscall '2-102-495', '08/01/2007','08/31/2007'
CREATE   PROCEDURE [dbo].[sp_RechazosCall]
@CodPro		nvarchar(20),
@FechaIni		smalldatetime,
@FechaFin		smalldatetime
AS

delete from tmpRechazosCall

insert into tmpRechazosCall
SELECT 
Recibos.CodPro, Recibos.FecEnvRec, Productos.NomPro, 
Certificados.CodOfi AS Oficina, Oficinas.NomOfi, Recibos.CodCer, 
Recibos.ConCer, Certificados.CodTipForPag, Certificados.CodTipMedPag, 
Certificados.CodTipOpc, Certificados.ValPriCer, Certificados.NumCue, 
Certificados.NumDocIde, 
ltrim(rtrim(Clientes.ApeCli)) + ' ' + ltrim(rtrim(Clientes.NomCli)) AS Nombre, 
Clientes.DirCli, Clientes.TelCli, Ciudades.NomCiu, Recibos.CodTipEstRec, 
Tipos_Estado_Recibo.NomTipEstRec, Recibos.MonOpc, Certificados.FecExpCer, 
Sum(Recibos.ValRec) AS Deuda, Count(Recibos.CodCer) AS Recibos
FROM (((Clientes RIGHT JOIN (Certificados INNER JOIN Productos 
ON Certificados.CodPro = Productos.CodPro) 
ON (Clientes.NumDocIde = Certificados.NumDocIde) 
AND (Clientes.CodTipDocIde = Certificados.CodTipDocIde)) 
INNER JOIN (Recibos INNER JOIN Tipos_Estado_Recibo 
ON Recibos.CodTipEstRec = Tipos_Estado_Recibo.CodTipEstRec) 
ON (Certificados.ConCer = Recibos.ConCer) 
AND (Certificados.CodCer = Recibos.CodCer) 
AND (Certificados.CodPro = Recibos.CodPro)) 
LEFT JOIN Ciudades ON Clientes.CodCiu = Ciudades.CodCiu) 
LEFT JOIN Oficinas ON Certificados.CodOfi = Oficinas.CodOfi
where Recibos.CodPro = @CodPro and 
Recibos.FecEnvRec >= @Fechaini and 
Recibos.FecEnvRec <= @FechaFin and 
Recibos.CodTipEstRec <>7 And 
Recibos.CodTipEstRec <>9
GROUP BY Recibos.CodPro, Recibos.FecEnvRec, Productos.NomPro, 
Certificados.CodOfi, Oficinas.NomOfi, Recibos.CodCer, Recibos.ConCer, 
Certificados.CodTipForPag, Certificados.CodTipMedPag, 
Certificados.CodTipOpc, Certificados.ValPriCer, Certificados.NumCue, 
Certificados.NumDocIde, ltrim(rtrim(Clientes.ApeCli)) + ' ' + ltrim(rtrim(Clientes.NomCli)), Clientes.DirCli, 
Clientes.TelCli, Ciudades.NomCiu, Recibos.CodTipEstRec, 
Tipos_Estado_Recibo.NomTipEstRec, Recibos.MonOpc, Certificados.FecExpCer

select * from tmpRechazosCall







GO
