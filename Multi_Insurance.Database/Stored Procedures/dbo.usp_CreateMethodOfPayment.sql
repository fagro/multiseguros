SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-13
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateMethodOfPayment] 
	-- Add the parameters for the stored procedure here
	@productCode varchar(15), 
	@methodOfPaymentCode smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Formas_Pago_Producto
	(
	    CodPro,
	    CodTipForPag,
	    PorRecForPag,
	    fecultmod
	)
	VALUES
	(
	    @productCode, -- CodPro - varchar
	    @methodOfPaymentCode, -- CodTipForPag - smallint
	    0.0, -- PorRecForPag - float
	    GETDATE() -- fecultmod - datetime
	)
END







GO
