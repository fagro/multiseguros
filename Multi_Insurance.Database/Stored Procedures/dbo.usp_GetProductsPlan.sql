SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-16
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProductsPlan]
	@productCode varchar(15) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	--declare @productCode varchar(15)
	--Set @productCode = 0
    -- Insert statements for procedure here
	If @productCode > 0

		SELECT DISTINCT [to].CodTipOpc AS productId,
			   [to].NomTipOpc AS planName
		FROM Tipos_Opcion [to]
			JOIN Opcion_Producto op
				ON op.CodTipOpc = [to].CodTipOpc
		WHERE [to].CodTipOpc = @productCode
	Else
		SELECT DISTINCT [to].CodTipOpc AS productId,
		   [to].NomTipOpc AS planName
		FROM Tipos_Opcion [to]
		
END









GO
