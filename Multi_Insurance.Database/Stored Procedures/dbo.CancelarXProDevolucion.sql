SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[CancelarXProDevolucion]
        	@CODPRO  VARCHAR(15),
 	@CODCER  INTEGER,
	 @CONCER  INTEGER,
              @OBSERVA  VARCHAR(100)
AS

DECLARE 
  @Codproa Varchar(15),
   @CodError int,
   @Observaciones Varchar(100),
   @MsgError Varchar(100)


select @CodError = 0
select @MSGERROR =  'Certificado Cancelado'

Select  @Observaciones = @OBSERVA

update certificados 
   set codtipestcer = 3, 
   fectercer = GETDATE(),
   fecultmod = GETDATE(),
   CodTIPMotCan = 9,
   Observaciones =  @Observaciones
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER

insert into recibos
select  CodPro, CodCer, ConCer, dateadd(day,1,FecRec) as fecrec, NumCue, ValRec, NumRecEnv,
MonOpc, FecIniPerCob, FecFinPerCob, CodTipMedPag, 8 as codtipestrec, CodTipForPag,
BanEnv,  convert(nvarchar(10),getdate(),101)   as FecEnvRec, NumDocIde, Altura, Ano, FecExpCer, CodOfi, getdate() as FecUltMod, unico,
CONSECUTIVO, NumCueAlterna
from Recibos 
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER And fecrec >= '1/1/1990' and codtipestrec = 9 

update recibos
       set codtipestrec = 7, fecultmod = GETDATE() 
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER And fecrec >= '1/1/1990' 
and codtipestrec < 8  or codtipestrec > 9 

Finalizar:
If @CodError <> 0
Begin
 Select @codproa = 'xx'
 Select @codcer=1
End

Select @CodError As CodError,
       Codpro,
       Codcer, 
       @MsgError As MsgError
       From Certificados
       Where CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = 1







GO
