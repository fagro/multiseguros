SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
/****** Object:  Stored Procedure dbo.MovDevol    Script Date: 12/11/2000 9:18:35 ******/
CREATE procedure [dbo].[MovDevol] @codpro char(15), @codcer int, @concer smallint as 
select Devoluciones.*, Tipos_Estados_Devoluciones.nomestdev, Tipos_Motivo_Devolucion.nomtipmotdev 
         from Devoluciones, Tipos_Estados_Devoluciones, Tipos_Motivo_Devolucion 
        where codpro = @codpro and codcer = @codcer and concer = @concer 
        and devoluciones.codestdev = Tipos_estados_devoluciones.codestdev 
        and devoluciones.CodTipMotDev = Tipos_Motivo_Devolucion.codtipmotdev







GO
