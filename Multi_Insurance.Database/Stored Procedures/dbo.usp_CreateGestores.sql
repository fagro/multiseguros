SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateGestores]
	-- Add the parameters for the stored procedure here
	@id smallint = 0,
	@name varchar(60), 
	@address varchar(120),
	@phone varchar(10),
	@invoice decimal(14,4)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Gestores As Target
		Using (Select @id, @name, @phone, @address, @invoice) 
			As Source(GestoresId, NomGes,TelGes,DirGes, FACTURA)
				On (Target.GestoresId = Source.GestoresId)
		When Matched Then
			Update set NomGes = Source.NomGes,
					   TelGes = Source.TelGes,
					   DirGes = Source.DirGes,
					   Factura = source.Factura
		When Not Matched Then

			Insert 
			(
				NomGes,
				DirGes,
				TelGes,
				FACTURA,
				LOGOTIPO,
				COMENTARIO,
				Nitr,
				Tipdev,
				notacredito
			)
			VALUES
			(
					-- NitGes - varchar
				@name, -- NomGes - varchar
				@address, -- DirGes - varchar
				@phone, -- TelGes - varchar
					 -- FecUltMod - datetime
				@invoice, -- FACTURA - float
				0x, -- LOGOTIPO - image
				'', -- COMENTARIO - text
				'', -- Nitr - varchar
				0, -- Tipdev - smallint
				0.0 -- notacredito - float
			)

	OutPut $ACTION;
END







GO
