SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[ActualizarCobro]
         @CodPro varchar(16),
         @FecEnvRec datetime,
         @MedPago1 smallint,
         @MedPago2 smallint,
         @MedPago3 smallint,
         @MedPago4 smallint,
         @TipoSeleccion Varchar(10),
         @Formato as varchar(5)  as
DECLARE  @CodProA CHAR(16), 
         @codcer float,  @concer smallint,
         @FechaRec datetime,@FecEnvRecA datetime, @FecUltMod datetime, @Estado as varchar(12), @EstadoT as varchar(12),
         @CodTipEstRec smallint, @CodTipMedPag smallint, @NumCue Varchar(20), @ContadorT as float, @Prima as Float,
         @FacAct as float

Set nocount on
  DECLARE FacturasP CURSOR
  FOR SELECT Codpro, Codcer, Concer, FecRec, FecEnvRec, FecUltMod, CodTipEstRec, CodTipMedPag, NumCue, ValRec FROM Recibos 
      where  CodPro = @CodPro and FecEnvRec = @FecEnvRec and (CodTipMedPag = @MedPago1 or CodTipMedPag = @MedPago2 or
      CodTipMedPag = @MedPago3 or CodTipMedPag = @MedPago4) order By Codpro, Codcer, Concer, Fecrec
  for update
 
  Select @FacAct = 0
  
OPEN FacturasP
  FETCH FacturasP INTO @CodProA, @CodCer, @ConCer, @FechaRec, @FecEnvRecA, @FecUltMod, @CodTipEstRec, @CodTipMedPag, @NumCue, @Prima
  WHILE @@fETCH_STATUS = 0
  BEGIN
     IF @TipoSeleccion = 'Cuenta'
	Begin
	    select @ContadorT = (Select Top 1 Contador from Bancaseg where Cuenta = @NumCue and Valor = @Prima And (Procesado ='' OR Procesado Is Null) Order by Contador)
             End
     else
             Begin
    	    Select @ContadorT = (Select Top 1 Contador from Bancaseg where Cert = @CodCer and Valor = @Prima And (Procesado ='' OR Procesado Is Null) Order by Contador)
             end
    if @ContadorT <> Null and @ContadorT > 0	
	Begin
	   Select @Estado = (Select Rechazo from bancaseg where Contador = @ContadorT)
	   Select @EstadoT = (Select CodTipEstRec From Eqrechazos Where CodFormato = @Formato And CodTipEstRecEq = @Estado)
          	    If @EstadoT <> Null
	      Begin	
	   	Update Bancaseg Set CodPro = @CodproA, Cert = @Codcer, FecRec = @FechaRec, Procesado = 'x' Where Contador = @ContadorT
		if @CodTipEstRec <> @EstadoT
		   Begin
		       Update Recibos set FecUltMod = getdate(), CodTipEstRec = @EstadoT Where Current Of FacturasP
		       Select @FacAct = @FacAct + 1	
                              end
                  end
	end
    FETCH FacturasP INTO @codproa, @codcer, @concer, @FechaRec, @FecEnvRecA, @Fecultmod, @CodTipEstRec, @CodTipMedPag, @NumCue, @Prima
  END
  DEALLOCATE FacturasP
set nocount off
print @FacAct







GO
