SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateProductPaymentPlans] 
	-- Add the parameters for the stored procedure here
	@planId smallint, 
	@valueOptions money,
	@MethodOfPaymentOptionsId smallint,
	@MeansOfPaymentOptionsId smallint,
	@productId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @productCode varchar(40)
	Set @productCode = (Select p.CodPro From dbo.Productos p Where p.IdProductos = @productId)
    -- Insert statements for procedure here
	Insert Into dbo.Opcion_Producto
	(
	    CodPro,
	    CodTipOpc,
	    MonOpc,
	    FecUltMod,
	    Dependiente,
	    ValidacionVinculado,
	    declaracionJurada
	)
	Values
	(
	    @productCode, -- CodPro - varchar
	    @planId, -- CodTipOpc - smallint
	    @valueOptions, -- MonOpc - float
	    GETDATE(), -- FecUltMod - datetime
	    0, -- Dependiente - bit
	    0, -- ValidacionVinculado - bit
	    '' -- declaracionJurada - varchar
	)

 
	Insert Into dbo.Medios_Pago_Producto
	(
	    CodPro,
	    CodTipMedPag,
	    FecUltMod,
	    CodTipDigCheq
	)
	Values
	(
	    @productCode, -- CodPro - varchar
	    @MeansOfPaymentOptionsId, -- CodTipMedPag - smallint
	    GETDATE(), -- FecUltMod - datetime
	    0 -- CodTipDigCheq - int
	)

	Insert Into dbo.Formas_Pago_Producto
	(
	    CodPro,
	    CodTipForPag,
	    PorRecForPag,
	    fecultmod
	)
	Values
	(
	    @productCode, -- CodPro - varchar
	    @MethodOfPaymentOptionsId, -- CodTipForPag - smallint
	    0.0, -- PorRecForPag - float
	    GETDATE() -- fecultmod - datetime
	)

END




GO
