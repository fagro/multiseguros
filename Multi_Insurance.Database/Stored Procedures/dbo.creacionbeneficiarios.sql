SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[creacionbeneficiarios]
	@CodPro varchar (15),
	@CodCer int,
	@ConCer smallint,
	@NomBene varchar(35),
	@ApeBene varchar(35),
	@PorBene float,
	@ParentBene varchar(2),
	@CodTipDocIde smallint,
	@NumDocIde varchar(16),
	@DirBene varchar(35),
	@TelBene varchar(29),
	@CodCiu int,
	@Email varchar(35) 
as
DECLARE  @Prueba char (25)
 
GRABAR:
IF NOT EXISTS(SELECT CodPro FROM BeneficiariosReal WHERE CodPro = @CodPro AND CodCer = @CodCer AND ConCer = @ConCer and CodTipDocIde = @CodTipDocIde and NumDocIde = @NumDocIde)
BEGIN
insert BeneficiariosReal 
       (
	CodPro,	CodCer,	ConCer,	NomBene,ApeBene,PorBene,ParentBene,
	CodTipDocIde,NumDocIde,DirBene,TelBene,CodCiu, Email 
        )
        VALUES 
        (
	@CodPro,@CodCer,@ConCer,@NomBene,@ApeBene,@PorBene,@ParentBene,
	@CodTipDocIde,@NumDocIde,@DirBene,@TelBene,@CodCiu,@Email 
        )
END
else
BEGIN
update BeneficiariosReal
       set
	CodPro  = @CodPro,CodCer  = @CodCer,
	ConCer  = @ConCer, NomBene  = @NomBene,
	ApeBene  = @ApeBene,	PorBene  = @PorBene,
	ParentBene  = @ParentBene, CodTipDocIde = @CodTipDocIde,
	NumDocIde  = @NumDocIde, DirBene  = @DirBene,
	TelBene = @TelBene, CodCiu  = @CodCiu, Email = @Email
       WHERE CodPro = @CodPro AND CodCer = @CodCer AND ConCer = @ConCer 
                      and CodTipDocIde = @CodTipDocIde and NumDocIde = @NumDocIde
end







GO
