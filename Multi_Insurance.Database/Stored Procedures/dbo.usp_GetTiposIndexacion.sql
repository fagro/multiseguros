SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTiposIndexacion] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0
		Select ti.TipoIndexacionId, ti.NomTipInd, ti.FecUltMod
		From dbo.Tipos_Indexacion ti
	Else
		Select ti.TipoIndexacionId, ti.NomTipInd, ti.FecUltMod
		From dbo.Tipos_Indexacion ti
		Where ti.TipoIndexacionId = @id
END




GO
