SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 24/07/2012
-- Description:	Activa un certificado que haya sido cancelado.
-- =============================================

CREATE PROCEDURE [dbo].[Usp_AActivateCertificate]

@CodigoCertificado AS FLOAT,
@CodigoProducto AS VARCHAR(20)

AS
	BEGIN TRY
    
		SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

		BEGIN TRANSACTION
        
				SET NOCOUNT ON;   
				SET XACT_ABORT ON;
				              
				--DECLARE @CodigoCertificado AS Int
				--DECLARE @CodigoProducto As VARCHAR(20)

				--SET @CodigoCertificado = 256789
				--SET @CodigoProducto = '2-102-495'


				--Toma los recibos cancelados del certificado.
				WITH #Tmp_RecibosActivar AS
				(
					SELECT * 
					FROM RecPolCan
					WHERE codcer = @CodigoCertificado
							AND CodPro = @CodigoProducto
				)

				--Regresa los recibos cancelados a activados.
				INSERT INTO Recibos
					SELECT *
					FROM #Tmp_RecibosActivar;


				--Elimina los recibos de RecPolCan
				DELETE RecPolCan
				WHERE CodCer = @CodigoCertificado
					AND CodPro =@CodigoProducto



				--Activa los recibos en estado cancelados (estado 7)
				UPDATE Recibos 
				SET CodTipEstRec = 6
				WHERE CodTipEstRec = 7
					AND CodCer = @CodigoCertificado
					AND CodPro = @CodigoProducto
				

				--Activa el certificado
				UPDATE dbo.Certificados
				SET CodTipEstCer = 1,
					CODTIPMOTCAN = NULL,
					observaciones = NULL,
					FecTerCer = NULL                  
				WHERE CodCer = @CodigoCertificado
					AND CodPro = @CodigoProducto
				              
		COMMIT TRANSACTION
        
	END TRY
  
	BEGIN CATCH
  
	IF @@TRANCOUNT <> 0  ROLLBACK TRANSACTION
		RAISERROR('Error en la ejecucion del Stored Procedure',10,1);
	END CATCH







GO
