SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.seleccionrecibos    Script Date: 24/07/2000 09:57:33 ******/
create procedure [dbo].[seleccionrecibos]  
  @codpro char(25),
         @feccorte datetime as
DECLARE @diaexp int, @diacorte INT, @MONOPC float, @codtipsexa int
select @diacorte = datepart(dd, @feccorte)
if @diacorte <=15 
begin
  SELECT T1.*, t2.fecnaccli, t2.codtipsex 
  FROM   Certificados as T1, Clientes as t2  
  WHERE  T1.CodPro = @codpro AND T1.IndCob <> 0 
         AND (T1.CodTipEstCer <> 3 AND T1.CodTipEstCer <> 6) 
         AND DATEPART(dd, fecexpcer) <= 15 
         AND (T1.CodTipdocide = T2.codtipdocide) 
         AND (T1.numdocide = T2.numdocide)
end
else
begin
  SELECT T1.*, t2.fecnaccli, t2.codtipsex 
  FROM   Certificados as T1, Clientes as t2  
  WHERE  T1.CodPro = @codpro AND T1.IndCob <> 0 
         AND (T1.CodTipEstCer <> 3 AND T1.CodTipEstCer <> 6) 
         AND DATEPART(dd, fecexpcer) > 15 
         AND (T1.CodTipdocide = T2.codtipdocide) 
         AND (T1.numdocide = T2.numdocide)
end







GO
