SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CANCELARMORA]  
        @CODPRO  VARCHAR(15),  
 @CODCER  INTEGER,  
 @CONCER  INTEGER  
AS  

update certificados   
   set codtipestcer = 3,   
   fectercer = GETDATE(),  
   fecultmod = GETDATE(),  
   CodTIPMotCan = 6,  
   Observaciones = 'Cancelado por mora'  
where 
	codpro = @CODPRO 
and 	codcer = @CODCER 
and 	concer = @CONCER  


update recibos  
       set codtipestrec = 7, 
	   fecultmod = GETDATE()   
where 
	codpro = @CODPRO 
and 	codcer = @CODCER 
and 	concer = @CONCER 
And 	fecrec >= '1/1/1990' 
and 	codtipestrec <> 9   

SELECT 
	* 
FROM 
	certificados (nolock)
where 
	codpro = @CODPRO 
and 	codcer = @CODCER 
and 	concer = @CONCER







GO
