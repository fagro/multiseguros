SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cerda, Victor
-- Create date: 2013-01-11
-- Description:	Reporte Cuadre Mensual Recaudos Polizas
-- =============================================
CREATE PROCEDURE [dbo].[Usp_MonthlyReconcilePolicies]	
	@ProductCode as varchar(15),
	@BeginDate as datetime,
	@EndDate as datetime
AS

	
	--Testing variables
	--Declare @Begindate as datetime
	--Declare @EndDate as datetime
	--Declare @ProductCode as varchar(15)
	
	--Set @Begindate = '2012-12-01'
	--Set @EndDate = '2012-12-31'
	--Set @ProductCode = '0'
	
	
	--Variables Internas--------	
	Declare @TaxValue as money;
	Declare @BankPercentage as money;
	----------------------------
	-----------------------------------------------------------
	Set @TaxValue = (Select a.Taxes/100 as TaxValue
						From TaxesConfiguration a
						Where a.EffectiveDate in (Select max(r.EffectiveDate)
													From TaxesConfiguration r						
													Where r.[Status] > 0
													and r.EffectiveDate <= @EndDate));
													
	Set @BankPercentage = (Select DoubleData/100 as BankPercentage 
							  From ParametersControl 
							  Where (Case when @ProductCode not like  '0'  then ProductCode else '0' end) = @ProductCode 		   
								and keyword = 'BankPercentage' 
								and [Status] = 1)													
	-----------------------------------------------------------			
	
	If object_id('tempdb..#Temp_Recibos') is not null
	Begin
		Truncate Table #Temp_Recibos
		Drop Table #Temp_Recibos
	End;

		Select  a.FecEnvRec,
				a.CodTipForPag, 		
				b.CodTipOpc,		
				sum(a.ValRec) as ValorRecaudo,
				sum(a.MonOpc) as ValorAsegurado,
				count(a.CodCer) as CantidadRecibos	
		into #Temp_Recibos		
		From Recibos a
		Inner join Certificados b
			on  b.CodPro = a.CodPro
			and b.CodCer = a.CodCer
			and b.ConCer = a.ConCer	
		Where a.FecEnvRec between @Begindate and @EndDate	
		  and (Case when @ProductCode not like  '0'  then a.CodPro else '0' end) = @ProductCode 		   
		  and a.CodTipEstRec = 9		
		Group by a.FecEnvRec,a.CodTipForPag,b.CodTipOpc

			union
			
		Select  a.FecEnvRec,
				a.CodTipForPag, 				
				b.CodTipOpc,				
				sum(a.ValRec) as ValorRecaudo,
				sum(a.MonOpc) as ValorAsegurado,
				count(a.CodCer) as CantidadRecibos	
		From RecPolCan a
		Inner join Certificados b
			on b.CodPro = a.CodPro
			and b.CodCer = a.CodCer
			and b.ConCer = a.ConCer
		Where a.FecEnvRec between @Begindate and @EndDate	
		  and (Case when @ProductCode not like  '0'  then a.CodPro else '0' end) = @ProductCode 		   
		  and a.CodTipEstRec = 9		
		Group by a.FecEnvRec,a.CodTipForPag,b.CodTipOpc
																															
	
	If object_id('tempdb..#Temp_FinalNumbers') is not null
	Begin
		Truncate Table #Temp_FinalNumbers
		Drop Table #Temp_FinalNumbers
	End;
	
		Select	a.FecEnvRec,				
				d.NomTipForPag,				
				c.NomTipOpc,				
				sum(a.ValorRecaudo) as ValorRecaudo,
				sum(a.ValorAsegurado) as ValorAsegurado,
				sum(a.CantidadRecibos) as CantidadRecibos				
		Into #Temp_FinalNumbers		   
		From #Temp_Recibos a
		Inner join Tipos_Opcion c
			on c.CodTipOpc = a.CodTipOpc	
		Inner join Tipos_Forma_Pago d
			on d.CodTipForPag = a.CodTipForPag
		Group by a.FecEnvRec, a.CodTipForPag, d.NomTipForPag, a.CodTipOpc, c.NomTipOpc
		
	-------		
		Select  a.*,
				a.ValorRecaudo - Round((a.ValorRecaudo/(1 + @TaxValue))*@BankPercentage,2) as ProduccionSeguro,
				Round((a.ValorRecaudo/(1 + @TaxValue))*@BankPercentage,2) as ProduccionBanco							  
		From #Temp_FinalNumbers a







GO
