SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create     procedure [dbo].[sp_Reporte_Oficiales]
@FechaIni	smalldatetime,
@FechaFin	smalldatetime,
@CodPro		nvarchar(20)

AS

Truncate Table dbo.tmpOficiales

SELECT 		c.CodPro, 
		c.NumDocIdeEmp, 
		p.NomPro, 
		c.CodOfi, 
		o.NomOfi, 
		ISNULL(rtrim(ltrim(e.ApeEmp)), '') + ' ' + rtrim(lTrim(e.NomEmp)) AS Nombre,
		c.FecExpCer,
		CASE
		WHEN c.CodTipestcer = 1 THEN 1 ELSE 0
		END AS CertNuevos, 
		CASE
		WHEN c.CodTipestcer = 1 THEN c.ValPriCer ELSE 0
		END AS PrimasN, 
		CASE
		WHEN c.CodTipestcer = 1 THEN c.valpricer*c.codtipforpag ELSE 0
		END AS AnualN, 
		CASE
		WHEN c.CodTipestcer = 3 or c.CodTipestcer = 6 THEN 1 ELSE 0
		END AS CertCancelados, 
		CASE
		WHEN c.CodTipestcer = 3 or c.CodTipestcer = 6 THEN c.ValPriCer ELSE 0
		END AS PrimasC, 
		CASE
		WHEN c.CodTipestcer = 3 or c.CodTipestcer = 6 THEN c.valpricer*c.codtipforpag ELSE 0
		END AS AnualC
into 		#tmp
FROM		dbo.Certificados c
INNER JOIN 	dbo.Productos p
ON		c.CodPro = p.CodPro
LEFT JOIN	dbo.Oficinas o
ON		c.CodOfi = o.CodOfi
LEFT JOIN	dbo.Empleados e
ON		c.NumDocIdeEmp = e.NumDocIde
where 		c.codpro = @CodPro and
		c.fecexpcer >= @FechaIni and
		c.fecexpcer <= @FechaFin


insert into 	dbo.tmpOficiales
select 		CodPro, 
		NumDocIdeEmp, 
		NomPro, 
		CodOfi, 
		NomOfi,
		nombre,
		sum(certnuevos) as CertNuevos,
		primasn,
		anualn,
		sum(certcancelados) as CertCancelados,
		primasc,
		anualc
from 		#tmp
GROUP BY 	CodPro, 
		NumDocIdeEmp, 
		NomPro, 
		CodOfi, 
		NomOfi, 
		nombre, 
		primasn,
		anualn,
		primasc,
		anualc

select * from dbo.tmpOficiales







GO
