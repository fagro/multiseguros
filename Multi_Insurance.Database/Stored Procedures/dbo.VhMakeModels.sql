SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[VhMakeModels]
@VhMakeCode varchar(20)
As

Select * 
from VehicleMakeModel
where MakeModel   is not null 
    and  Makecode  = @VhMakeCode 
     And ISNULL (Deprecated,0)=0
Order by Description







GO
