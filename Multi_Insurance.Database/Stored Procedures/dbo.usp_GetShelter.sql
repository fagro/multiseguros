SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShelter] 
	-- Add the parameters for the stored procedure here
	@Id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @Id = 0
		Select a.CodAmp, NomAmp, CodTipRam, FecUltMod
		From dbo.Amparos a
	Else
		Select a.CodAmp, NomAmp, CodTipRam, FecUltMod
		From dbo.Amparos a
		Where CodAmp = @Id		
  
END







GO
