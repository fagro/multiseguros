SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.creacionlinea    Script Date: 11/07/2000 14:29:21 ******/
CREATE procedure [dbo].[creacionlinea]  
  @codcer int,
   @nomCLI char(25),
  @apecli char(25),
  @codtipdocide smallint, 
  @numdocide varchar(16),
  @expedida varchar(19),
  @codtipsex varchar(1),
  @fecnaccli CHAR(10),
  @coddep int, 
  @codciu int, 
  @dircli varchar(40),
  @telcli varchar(10),
         @telofi varchar(10),
         @ramo   int,
  @codtipopc smallint,
  @codtipforpag smallint,
  @vrhogarinmueble float,
  @vrhogarcontenido float,
  @codofi varchar(3),
  @codtipmedpag smallint,
  @numcue varchar(9),
         @prima float,
  @codtipdocideemp smallint,
  @numdocideemp char(12),
  @codtipdocideben1 smallint,
  @numdocideben1 char(12),
         @nomben1 char(47),
         @parenben1 char(15),
         @porcentajeben1 smallint,
  @codtipdocideben2 smallint,
  @numdocideben2 char(12),
         @nomben2 char(47),
         @parenben2 char(15),
         @porcentajeben2 smallint,
  @codtipdocideben3 smallint,
  @numdocideben3 char(12),
         @nomben3 char(47),
         @parenben3 char(15),
         @porcentajeben3 smallint,
  @codtipdocideben4 smallint,
  @numdocideben4 char(12),
         @nomben4 char(47),
         @parenben4 char(15),
         @porcentajeben4 smallint
         as
declare 
  @sexor smallint,
  @codpro varchar(16),
         @error smallint,
         @dia smallint,
  @mes smallint,
  @nmes smallint,
  @ano int,
         @fecharec datetime,
  @fechafinpercob datetime,
  @coderror smallint
if @codtipsex = 'm' or @codtipsex = 'M'
   select @sexor = 2
else
   select @sexor = 1
if @ramo = 16 
  select @codpro = '160280-1'
if @ramo = 24 
  select @codpro = '100.000-5/98'
select @coderror = 0
IF NOT EXISTS(SELECT * FROM CERTIFICADOS WHERE CODPRO = @CODPRO AND CODCER = @CODCER AND CONCER = 1)
  BEGIN
  INSERT CERTIFICADOS
         (
   CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP,
   NUMDOCIDEEMP, CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE,
   VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, MONOPC, indcob, 
   VALCONTENIDO, DIRINM
  )
  VALUES 
  (
   @CODPRO, @CODCER, 1, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFI, @CODTIPDOCIDEEMP,
   @NUMDOCIDEEMP, 1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE,
   @prima, 0, GETDATE(), GETDATE(), GETDATE(), @vrhogarinmueble, 1, 
   @vrhogarcontenido, @dircli
  )
  end
else
begin
  select @coderror=1
  goto finalizar
end
IF NOT EXISTS(SELECT * FROM CLIENTES WHERE CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
  BEGIN
  insert clientes 
       (
        codtipdocide, numdocide, 
        nomcli, apecli,
        fecnaccli, dircli,
        telcli, codciu,
        codtipsex, fecultmod, dirrec, telrec
        )
        VALUES 
        (
        @CODTIPDOCIDE, @NUMDOCIDE, 
        @NOMCLI, @APECLI,
        @FECNACCLI, @DIRCLI,
        @TELCLI, @CODCIU,
        @sexor, GETDATE(), @dircli, @telofi)
  END
else
  begin
  update clientes 
       set
        codtipdocide = @codtipdocide,
        numdocide = @numdocide,
        nomcli = @nomcli,
        apecli = @apecli,
        fecnaccli = @fecnaccli,
        dircli = @dircli,
        telcli = @telcli,
        codciu = @codciu,
        codtipsex = @sexor,
        fecultmod = getdate(),
        dirrec = @dircli,
        telrec = @telcli
  END
if not exists(select codpro from recibos where codpro = @CODPRO and codcer = @CODCER and concer =1)
begin
   select @dia = datepart(dd, getdate())
   select @mes =datepart(mm, getdate())
   select @ano=datepart(yy, getdate())
   if  @dia <=15 
     select @dia = 15
   else
     select @dia= 30
   select @fecharec = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   SELECT @NMES = @MES+12/@CODTIPFORPAG
   IF @NMES > 12 
   BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
   end
   else 
     select @mes =@nmes
   select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, FECENVREC, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPRO, @CODCER, 1, @FECHAREC, @NUMCUE, @prima, 0, @vrhogarinmueble, @FECHAREC, 
   @fechafinpercob, @CODTIPMEDPAG, 9, @codtipforpag, getdate(), @numdocide, GETDATE(), @codofi, GETDATE(), 1)
 end
FINALIZAR:
if @coderror <>0
begin
 select @codpro = 'xx'
 select @codcer=1
end
SELECT @CODERROR,
       CERTIFICADOS.CODCER, 
       CERTIFICADOS.VALPRICER
       FROM CERTIFICADOs
       WHERE CERTIFICADOS.CODPRO = @CODPRO 
       AND CERTIFICADOS.CODCER = @CODCER







GO
