SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cerda, Victor
-- Create date: 2013-11-05
-- Description:	Actualizacion de las Oficinas de los Certificados segun Empleado
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateCertificatesOffice]
	@CodigoProducto as varchar(15),
	@CodigoEmpleado as varchar(15), 
	@FechaCambio as Date,
	@CodigoOficina as varchar(15)
AS

BEGIN
	Update Certificados
	Set CodOfi = @CodigoOficina
	From Certificados 
	Where CodPro = @CodigoProducto
	and NumDocIdeEmp = @CodigoEmpleado
	and FecExpCer >= @FechaCambio

	Update Recibos
	Set CodOfi = @CodigoOficina
	From Certificados a 
	Inner Join Recibos b
		on a.CodCer = b.CodCer
	Where a.CodPro = @CodigoProducto
	and a.NumDocIdeEmp = @CodigoEmpleado
	and a.FecExpCer >= @FechaCambio
END







GO
