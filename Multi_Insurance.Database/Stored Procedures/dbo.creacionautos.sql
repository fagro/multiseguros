SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[creacionautos]
	 @CodPro char(15),
	 @CodCer int,
	 @ConCer smallint,
              @Tipo char(50),
	 @Marca char (50),
	 @Modelo char (50),
	 @Chasis char (20),
	 @Serie char (50),
	 @Caract char (50),
	 @Codfasecolda char (15),
	 @Motor char (20),
	 @CodServicio int,
	 @CodEstado int,
	 @Color Char (25),
 	 @Ano int,
	 @Registro char (10),
	 @FechaVencimiento datetime,
	 @Cazador smallint
as
DECLARE  @Prueba char (25)
 
GRABAR:
IF NOT EXISTS(SELECT Registro FROM Vehiculos1 
WHERE CodPro = @CodPro AND CodCer = @CodCer AND ConCer = @ConCer)
BEGIN
insert Vehiculos1 
       ( CodPro, CodCer, ConCer, Tipo, Marca, Modelo,Chasis,Serie,
	 Caract, Codfasecolda, Motor, CodServicio, CodEstado,
	 Color, Ano, Registro, FechaVencimiento, Cazador
        )
        VALUES 
        ( @CodPro,	 @CodCer, @ConCer, @Tipo,@Marca, @Modelo, @Chasis,@Serie, 
          @Caract, @Codfasecolda, @Motor, @CodServicio, @CodEstado,
	 @Color, @Ano, @Registro, @FechaVencimiento, @Cazador
        )
END
else
BEGIN
update Vehiculos1
       set
	 CodPro  = @CodPro, CodCer  = @CodCer, ConCer  = @ConCer, Tipo = @Tipo,
	 Marca = @Marca,Modelo = @Modelo, Chasis  = @Chasis,	 Serie  = @Serie,
	 Caract = @Caract,Codfasecolda = @Codfasecolda, Motor = @Motor,
	 CodServicio = @CodServicio, CodEstado  = @CodEstado,	 Color  = @Color,
 	 Ano = @Ano, Registro = @Registro, FechaVencimiento = @FechaVencimiento,
         Cazador	  = @Cazador
       WHERE CodPro = @CodPro AND CodCer = @CodCer AND ConCer = @ConCer
end







GO
