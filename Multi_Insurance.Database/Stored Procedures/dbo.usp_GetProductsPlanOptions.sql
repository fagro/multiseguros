SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProductsPlanOptions] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0

		Select op.CodTipOpc, op.NomTipOpc, op.FecUltMod, op.InfoAdicional 
		From Tipos_Opcion op
	Else
		Select op.CodTipOpc, op.NomTipOpc, op.FecUltMod, op.InfoAdicional 
		From Tipos_Opcion op
		Where op.CodTipOpc = @id
END


GO
