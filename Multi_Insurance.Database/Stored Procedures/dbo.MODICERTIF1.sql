SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
/****** Object:  Stored Procedure dbo.MODICERTIF1    Script Date: 9/15/99 12:36:17 PM ******/
CREATE procedure [dbo].[MODICERTIF1] 	
         @CODPRO char(25),
	 @CODCER NUMERIC,
	 @CONCER int, 
	 @MONOPC NUMERIC,
	 @VALPRICER NUMERIC,
         @NUMCUE CHAR(25) as
update 	Certificados 
	set MonOpc = @MONOPC, 
	ValPriCer = @VALPRICER, 
        FecUltMod = GETDATE(),
        NumCue = @NUMCUE
where 	CodPro = @CODPRO 
	and CodCer = @CODCER 
	and ConCer = @CONCER
SELECT  * FROM Certificados 
where 	CodPro = @CODPRO and CodCer = @CODCER and ConCer = @CONCER







GO
