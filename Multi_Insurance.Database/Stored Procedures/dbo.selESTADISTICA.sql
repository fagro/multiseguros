SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.selESTADISTICA    Script Date: 24/07/2000 09:57:33 ******/
create procedure [dbo].[selESTADISTICA]
         @codpro varchar(16),
  @fecini datetime,
  @fecfin datetime
         as
SELECT Productos.CodPro, Productos.NomPro,  
       Oficinas.NitPat, Certificados.CodOfi, 
       Oficinas.NomOfi,  Count(Certificados.CodCer) AS CuentaDeCodCer,
       Sum(Opcion_Producto.MonOpc) AS SumaDeMonOpc 
FROM  Productos, Oficinas, Certificados, Opcion_Producto
WHERE (CERTIFICADOS.CodPro<>'0001'
       AND Certificados.FecExpCer>=@FECINI
       And Certificados.FecExpCer<=@FECFIN)
       AND (Productos.NitPatPro = Oficinas.NitPat
       AND Certificados.CodTipOpc = Opcion_Producto.CodTipOpc
       AND Certificados.CodPro = Opcion_Producto.CodPro
       AND Oficinas.CodOfi = Certificados.CodOfi)
GROUP BY Productos.CodPro, Productos.NomPro, Oficinas.NitPat, 
         Certificados.CodOfi, Oficinas.NomOfi







GO
