SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.selren    Script Date: 24/07/2000 09:57:33 ******/
CREATE procedure [dbo].[selren]
         @codpro varchar(16),
  @fecini datetime,
  @fecfin datetime
         as
SELECT Certificados.CodOfi, Certificados.CodCer, Certificados.NumDocIde,
       Certificados.ValPriCer, Certificados.NumCue,DATEAdd (yy,datediff(mm, Certificados.FecExpCer, getdate())/12, Certificados.FecExpCer) as FecExpCer,
/*       datename (dd, FecExpCer) + ' de ' + datename (mm, FecExpCer) + ' de ' + datename (yy, FecExpCer) as FecExpCer1,*/
       Certificados.FecDigCer, Certificados.MonOpc, 
       Nomcli + ' ' + ApeCli + ' ' + Ape2Cli AS NombreCliente, 
       Clientes.DirCli, Clientes.TelCli, Certificados.CodPro, 
       Productos.NomPro, Certificados.CodTipEstCer, Certificados.FecUltMod,
       datename (dd, Certificados.FecTerCer) + ' de ' + datename (mm, Certificados.FecTerCer) + ' de ' + datename (yy, Certificados.FecTerCer) AS FecTerCer1, 
       Certificados.CodTipOpc, Ciudades.Nomciu

FROM   Certificados
INNER JOIN clientes

	on Certificados.NumDocIde = Clientes.NumDocIde
       AND Certificados.CodTipDocIde = Clientes.CodTipDocIde
       
INNER JOIN Opcion_Producto
	on	Certificados.CodPro = Opcion_Producto.CodPro
        AND Certificados.CodTipOpc = Opcion_Producto.CodTipOpc
        
INNER JOIN Productos
	on Certificados.CodPro = Productos.CodPro
	
LEFT JOIN Ciudades 
	on clientes.codciu = ciudades.codciu

WHERE ((Certificados.CodPro=@codpro) and (Certificados.Fecexpcer<@fecini 
        And datepart(mm,Certificados.Fecexpcer) >= datepart(mm, @fecini)
        And datepart(mm,Certificados.Fecexpcer) <= datepart(mm, @fecfin)
        And datepart(dd,Certificados.Fecexpcer) >= datepart(dd, @fecini)
        And datepart(dd,Certificados.Fecexpcer) <= datepart(dd, @fecfin)
        and Certificados.codtipestcer = 1)) 
        
ORDER BY Certificados.CodOfi







GO
