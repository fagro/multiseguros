SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-22
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetRepresentative] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0
		SELECT g.GestoresId, g.NomGes AS name, g.DirGes AS [Address], g.TelGes AS PhoneNumber,
				g.FecUltMod AS Modified, g.FACTURA AS invoice
		From Gestores g
	else
		SELECT g.GestoresId, g.NomGes AS name, g.DirGes AS [Address], g.TelGes AS PhoneNumber,
				g.FecUltMod AS Modified, g.FACTURA AS invoice
		From Gestores g
		Where g.GestoresId = @id
END







GO
