SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[CreaC]
 @nomCLI char(25),
  @apecli char(25),
  @codtipdocide smallint, 
  @numdocide varchar(16),
  @codtipsex smallint,
  @fecnaccli CHAR(10),
  @codciu int, 
  @nomciu char(25),
  @telcli varchar(10),
  @dircli varchar(60),
  @codtipmedpag smallint,
  @numcue varchar(20),
  @codpro varchar(16),
  @codcer int,
  @concer smallint,
  @codtipopc smallint,
  @codtipforpag smallint,
  @vrhogarinmueble float,
  @vrhogarcontenido float,
  @codtipdocideemp smallint,
  @numdocideemp char(12),
  @codofi char(10),
  @nomofi char(25),
  @dirrec char (60),
  @telrec char (15),
  @Fecexpcer CHAR(10),
  @CODTIPOCUPACION INT,
  @USUARIO CHAR(25),
  @CODTIPESTCIVIL INT,
  @NOHIJOS INT,
  @email char(50),
  @telmovil char(15)
 As
DECLARE @VALPRICER float, @VALPRICERA float,  @VALrenta float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
        @CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
        @VALBANCAsEGUROS INT, @VALAsEGURADORA INT, @IVA INT, @CodError INT,
        @LIMMONOPC float, @VALAsEGACT int, @EDAD INT, @edadmin int, @recargo float,
        @edadmax int, @forliqprima int, @valAsegopcion float,
        @CARACTERES CHAR(16),
        @factor float (10),
        @adicionedad float,
        @codcerc char(6),
        @codcern int, 
        @valpricerc char(10),
        @DIA INT,
        @MES INT,
        @ANO INT,
        @FECHAREC CHAR(10),
        @FECHAFINPERCOB CHAR(10),
        @nmes int,
        @ramo int,
        @VALOR1 FLOAT,
        @VALOR2 FLOAT, @pripagban float,
        @estprirec int,
        @periodoinc int, 
        @NITAsEG CHAR(16),
        @beneedu int,
        @Numcer  int,
        @CertCli int,
        @MsgError CHAR(100) ,
        @FechaInicial datetime,   @Fechafinal datetime,  
        @FechaExpedicion datetime, @FechaNacimiento datetime, @FechaRecibo datetime
Select @FechaExpedicion = CONVERT(DATETIME,@Fecexpcer,101)
Select @FechaNacimiento = CONVERT(DATETIME,@Fecnaccli,101)
Select @CODOFIA = CONVERT(CHAR(4), @CODOFI)
Select @CodError = 0
Select @MsgError = 'TRANSACCION EXITOSA'
Select @VALAsEGACT = 0
Select @CODTIPSEXa = @codtipsex
Select @CodError = 0
Select @CODPROA = @CODPRO

If @NumDocIdeEmp = Null
 Begin
     Select @CodError = 7
     Select @MsgError = 'Usuario Desconectado. Por favor vuelva a Ingresar a la Web.'
    End
  
  print @MsgError







GO
