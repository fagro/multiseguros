SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Cuadre Mensual General.

create procedure [dbo].[sp_cuadremensualgen]
@FechaDesde 	smalldatetime,
@FechaHasta	smalldatetime
as
SELECT 	DISTINCT Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, Certificados.MonOpc, 
case 	when DateDiff("m",fecnaccli,fecenvrec)/12 < 30 then '18-29'	
	when DateDiff("m",fecnaccli,fecenvrec)/12 < 45 then '30-44'
	when DateDiff("m",fecnaccli,fecenvrec)/12 < 55 then '45-54'
	else '55-Mas' end as Edad,
	Recibos.ValRec, Recibos.CodTipForPag, Tipos_Forma_Pago.NomTipForPag, 
	Tipos_Opcion.NomTipOpc, Recibos.CodTipEstRec, Count(Recibos.CodCer) AS Cobrados, 
	Sum(Recibos.ValRec) AS RecaudoTotal, Sum(Recibos.MonOpc) AS ValorAsegurado, 
	Sum(([valrec]/(1+[piva]/100))) AS RecaudoNeto, Sum(([valrec]/(1+[piva]/100))*8/100) AS RER, 
	Sum(([valrec]/(1+[piva]/100))*45/100) AS Aseguradora, Sum(([valrec]/(1+[piva]/100))*47/100) AS Patrocinador
FROM ((((Recibos INNER JOIN Tipos_Forma_Pago ON Recibos.CodTipForPag = Tipos_Forma_Pago.CodTipForPag) 
	INNER JOIN Certificados ON (Recibos.CodPro = Certificados.CodPro) AND 
	(Recibos.ConCer = Certificados.ConCer) AND (Recibos.CodCer = Certificados.CodCer)) 
	INNER JOIN Productos ON Recibos.CodPro = Productos.CodPro) LEFT JOIN 
	Tipos_Opcion ON Certificados.CodTipOpc = Tipos_Opcion.CodTipOpc) 
	INNER JOIN Clientes ON (Certificados.NumDocIde = Clientes.NumDocIde) AND 
	(Certificados.CodTipDocIde = Clientes.CodTipDocIde)
GROUP BY Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, Certificados.MonOpc, 
	case 	when DateDiff("m",fecnaccli,fecenvrec)/12 < 30 then '18-29'	
	when DateDiff("m",fecnaccli,fecenvrec)/12 < 45 then '30-44'
	when DateDiff("m",fecnaccli,fecenvrec)/12 < 55 then '45-54'
	else '55-Mas' end, 
	Recibos.ValRec, Recibos.CodTipForPag, Tipos_Forma_Pago.NomTipForPag, 
	Tipos_Opcion.NomTipOpc, Recibos.CodTipEstRec
HAVING (((Recibos.FecEnvRec)>=@fechadesde And (Recibos.FecEnvRec)<=@fechahasta) AND ((Recibos.CodTipEstRec)=9))
--sp_cuadremensualgen '20080101','20080101'







GO
