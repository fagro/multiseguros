SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.selbien    Script Date: 24/07/2000 09:57:33 ******/
CREATE procedure [dbo].[selbien]
         @codpro varchar(16),
  @fecini datetime,
  @fecfin datetime
         as
SELECT Certificados.CodOfi, Certificados.CodCer, Certificados.NumDocIde,
       Certificados.ValPriCer, Certificados.NumCue, Certificados.FecExpCer,
       datename (dd, FecExpCer) + ' de ' + datename (mm, FecExpCer) + ' de ' + datename (yy, FecExpCer) as FecExpCer1,
       Certificados.FecDigCer, Opcion_Producto.MonOpc, 
       Nomcli + ' ' + ApeCli + ' ' + Ape2Cli AS NombreCliente, 
       Clientes.DirCli, Clientes.TelCli, Certificados.CodPro, 
       Productos.NomPro, Certificados.CodTipEstCer, Certificados.FecUltMod,
       datename (dd, Certificados.FecTerCer) + ' de ' + datename (mm, Certificados.FecTerCer) + ' de ' + datename (yy, Certificados.FecTerCer) AS FecTerCer1, 
       Certificados.CodTipOpc, Ciudades.Nomciu

FROM   Certificados 

INNER JOIN clientes 
	on Certificados.CodTipDocIde = Clientes.CodTipDocIde
	and Certificados.NumDocIde = Clientes.NumDocIde
	
INNER JOIN Opcion_Producto 
	on Certificados.CodTipOpc = Opcion_Producto.CodTipOpc
	and Certificados.CodPro = Opcion_Producto.CodPro
	
INNER JOIN Productos 
	on Certificados.CodPro = Productos.CodPro
	
LEFT JOIN Ciudades 
	on clientes.codciu = ciudades.codciu

WHERE	Certificados.CodPro=@codpro 
and		Certificados.FecDigCer>=@fecini 
And		Certificados.FecDigCer<@fecfin

ORDER BY Certificados.CodOfi







GO
