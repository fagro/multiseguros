SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-17
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Usp_CreatePremiumUnder] 
	-- Add the parameters for the stored procedure here
	@productId int,
	@productCode varchar(15) = '',
	@shelterCode smallint,
	@productPlan smallint,
	@methodOfPayment smallint,
	@ageFrom smallint,
	@ageTo smallint,
	@premiumUnderValue decimal(12,4)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Set @productCode = (Select p.CodPro  From dbo.Productos p Where p.IdProductos = @productId)
    
	-- Insert statements for procedure here
	INSERT INTO Primas_Amparo
	(
	    CodPro,
	    CodAmp,
	    CodTipOpc,
	    CodTipForPag,
	    EdaDes,
	    EdaHas,
	    PorPriAmp,
	    FecUltMod,
		productID
	)
	VALUES
	(
	    @productCode, -- CodPro - varchar
	    @shelterCode, -- CodAmp - varchar
	    @productPlan, -- CodTipOpc - smallint
	    @methodOfPayment, -- CodTipForPag - float
	    @ageFrom, -- EdaDes - smallint
	    @ageTo, -- EdaHas - smallint
	    @premiumUnderValue, -- PorPriAmp - float
	    GetDate(), -- FecUltMod - datetime
		@productId
	)	

	SELECT SCOPE_IDENTITY()
END







GO
