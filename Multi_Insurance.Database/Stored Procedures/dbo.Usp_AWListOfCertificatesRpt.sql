SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=============================================
 --Author:		Victor Cerda
 --Create date: 29/06/2012
 --Description:	Reporte Listado de Certificados 
 --=============================================

CREATE PROCEDURE [dbo].[Usp_AWListOfCertificatesRpt]
	@ProductCode as Varchar(15),
	@BeginDate as Datetime,
	@EndDate as Datetime,	
	@OfficeCode as Varchar(15),
	@CertificateStatus as int,
	@User as varchar(16),
	@PaymentStatus as int
AS



	/* Parámetros de Prueba */
	--Declare @ProductCode as Varchar(15)
	--Declare @BeginDate as Datetime
	--Declare @EndDate as Datetime
	--Declare @OfficeCode as Varchar(15)	
	--Declare @CertificateStatus as int
	--Declare @User as varchar(16)
	--Declare @PaymentStatus as int

	--Set @ProductCode = '2-102-495'
	--Set @BeginDate = '2012-08-01'
	--Set @EndDate = '2012-08-29'
	--Set @OfficeCode = '0'
	--Set @CertificateStatus = 0
	--Set @User = 'vcerda'
	--Set @PaymentStatus = 0    /* 0 = Todos, 1 = Pago, 2 = Pendiente */


--Variables Interna
Declare @ExecuteUser as int;
Declare @sql as varchar(1000);
Declare @sql1 as varchar(1000);
--

Set @ExecuteUser = (Select ExecutiveId From ExecutiveCommissions Where ExecutiveWindowsUser = @User and ExecutivePosition = 3)

------- CERTIFICADOS ACTIVOS Y CANCELADOS DENTRO DE RANGO DE FECHA --------------------------------

	If object_id('tempdb..#Temp_Certificados') is not null
	Begin
		Truncate Table #Temp_Certificados
		Drop Table #Temp_Certificados
	End;

	Select A.CodCer as CodigoCertificado,
		   A.CodOfi as CodigoOficina,		   
		   ltrim(rtrim(C.NomCli)) + ' ' + ltrim(rtrim(C.ApeCli)) as NombreCliente, 		    
		   C.FecNacCli as FechaNacimiento,
		   C.TelCli as TelefonoCliente,
		   A.NumDocIde as Identificacion,
		   TFP.NomTipForPag as FormadePago,
		   TMP.NomTipMedPag as MediodePago,
		   A.NumCue as NumerodeCuenta,
		   A.ValPriCer as PrimaMensual,
		   A.ValPriCer * TFP.CodTipForPag as PrimaAnual,
		   A.FecExpCer as FechaExpedicion,
		   A.MonOpc as ValorAsegurado		   		   
	Into #Temp_Certificados
	From Certificados A
		Left join Clientes C
			on A.NumDocIde = C.NumDocIde
		Left join Tipos_Forma_Pago TFP
			on A.CodTipForPag = TFP.CodTipForPag
		Left join Tipos_Medio_Pago TMP
			on A.CodTipMedPag = TMP.CodTipMedPag
	Where A.FecExpCer between @BeginDate and @EndDate	
	and   A.CodPro = @ProductCode 	
	and   (Case when @OfficeCode not like '0' then A.CodOfi else '0' end) = @OfficeCode
	and   (Case when @CertificateStatus >  0  then A.CodTipEstCer else 0 end) = @CertificateStatus
	Group by A.CodCer, A.CodOfi, C.NomCli, C.ApeCli, C.FecNacCli, C.TelCli, A.NumDocIde, TFP.NomTipForPag, 
		   TMP.NomTipMedPag, A.NumCue, A.ValPriCer,TFP.CodTipForPag, A.FecExpCer, A.MonOpc 

Declare @WhereFilter  varchar(100)= (case when @ExecuteUser is not null then 'Where B.ExecutiveWindowsUser = ' + CHAR(39) + @User + CHAR(39) else '' end);

Set @sql =		   
		   'Select A.*,
				   O.NomOfi as NombreOficina							  
		    From #Temp_Certificados A
			Left join Oficinas O
				on A.CodigoOficina = O.CodOfi
	   		Left join ExecutiveCommissions B
				on O.CodProm = B.ExecutiveId
			<WHEREFILTER>'
			
set @sql = replace(@sql,'<WHEREFILTER>', @WhereFilter);		

----------------------------------------------------------------------------------------------------------------------
If object_id('tempdb..#Temp_CertificadosFinal') is not null
Begin
	Truncate Table #Temp_CertificadosFinal
	Drop Table #Temp_CertificadosFinal
End;

Create Table #Temp_CertificadosFinal
(
		   CodigoCertificado int,
		   CodigoOficina varchar(15),		   
		   NombreCliente varchar(100), 		    
		   FechaNacimiento datetime,
		   TelefonoCliente varchar(20),
		   Identificacion varchar(16),
		   FormadePago varchar(20),
		   MediodePago varchar(20),
		   NumerodeCuenta varchar(20),
		   PrimaMensual money,
		   PrimaAnual money,
		   FechaExpedicion datetime,
		   ValorAsegurado money,
		   NombreOficina varchar(100)	
);

Insert into #Temp_CertificadosFinal
Exec (@sql);
		   
create index ix_prod on #Temp_CertificadosFinal(CodigoCertificado);		   
		
----------------------------------------------
Declare @PayStatusFilter varchar(100)= Case when @PaymentStatus = 0 then '' else (case when @PaymentStatus = 1 then 'and A.CodTipEstRec = 9' else 'and A.CodTipEstRec <> 9' end) end;
Declare @Pago varchar(5) = 'Pago'
Declare @Pendiente varchar(10) = 'Pendiente'
Declare @PrimerPago varchar(100) = '(Case when A.CodTipEstRec = 9 then' + CHAR(39) +  @Pago + CHAR(39) + 'else' + CHAR(39) + @Pendiente + CHAR(39) + 'end) as PrimerPago'

Set @sql1 =		
   'Select B.*,
		   <PRIMERPAGO>
	From Recibos A (nolock)
		Inner join #Temp_CertificadosFinal B
			on A.CodCer = B.CodigoCertificado			
	Where NumRecEnv = 0
	<PAYSTATUSFILTER>
union 	
	Select B.*,
		   <PRIMERPAGO>
	From RecPolCan A (nolock)
		Inner join #Temp_CertificadosFinal B
			on A.CodCer = B.CodigoCertificado			
	Where NumRecEnv = 0
	<PAYSTATUSFILTER>'
	
	Set @sql1 = replace(@sql1,'<PRIMERPAGO>',@PrimerPago); 
	Set @sql1 = replace(@sql1,'<PAYSTATUSFILTER>', @PayStatusFilter);		
	Exec (@sql1);







GO
