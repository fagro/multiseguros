SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[CancelarXDevolucion]
        	@CODPRO  VARCHAR(15),
 	@CODCER  INTEGER,
	 @CONCER  INTEGER
AS

DECLARE 
  @Codproa Varchar(15),
   @CodError int,
   @MsgError Varchar(100)


select @CodError = 0
select @MSGERROR =  'Certificado Cancelado'

update certificados 
   set codtipestcer = 3, 
   fectercer = GETDATE(),
   fecultmod = GETDATE(),
   CodTIPMotCan = 9,
   Observaciones = 'Cancelado por Devolucion'
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER

update recibos
       set codtipestrec = 8, fecultmod = GETDATE() 
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER And fecrec >= '1/1/1990' and codtipestrec = 9 

update recibos
       set codtipestrec = 7, fecultmod = GETDATE() 
where codpro = @CODPRO and codcer = @CODCER and concer = @CONCER And fecrec >= '1/1/1990' 
and codtipestrec < 8  or codtipestrec > 9 

Finalizar:
If @CodError <> 0
Begin
 Select @codproa = 'xx'
 Select @codcer=1
End

Select @CodError As CodError,
       Codpro,
       Codcer, 
       @MsgError As MsgError
       From Certificados
       Where CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = 1







GO
