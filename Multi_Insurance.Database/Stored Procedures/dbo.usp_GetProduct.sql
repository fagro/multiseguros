SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-12
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProduct] 
	-- Add the parameters for the stored procedure here
	@ProductId int = 0 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT p.IdProductos, p.CodPro, p.NomPro, p.CodTipRam, p.NitComSeg, p.NitGes, p.NitPatPro,
			p.CodTipInd, p.PorIndPro, p.CodTipMon, p.PorComPro, p.porcompror, p.IndProCon, 
			p.IndAmpSel, p.IndPriFijTas, p.IndInfBie, p.FecCorPro, p.FecIniVen, p.FecFinCer, 
			p.FecUltMod, p.IndMts, p.CodTipMer, p.DesPro, p.PriPagBan, p.IVA, p.honocobn, 
			p.honocobr, p.medadm, p.medadf, p.CODPROALTER, p.expedir, p.PORINCENT, p.fechaincentivo, 
			p.codprocesos, p.beneedu, p.beneedu1, p.PORINCENTR, p.ramo, p.plan1, p.poliza, p.CapPrima, 
			p.RangosValSeg, p.ContPro, p.piva
	FROM Productos p
	WHERE p.IdProductos <> 0 and (p.IdProductos = @ProductId or p.IdProductos is not null)

END







GO
