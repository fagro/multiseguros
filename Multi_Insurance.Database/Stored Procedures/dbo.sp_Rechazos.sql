SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   procedure [dbo].[sp_Rechazos]
@CodPro		nvarchar(20),
@FechaIni	smalldatetime,
@FechaFin	smalldatetime

as
--sp_rechazos '2-102-495', '08/01/2007','08/31/2007'
delete from tmpRechazos
insert into tmpRechazos
SELECT Recibos.CodPro, Recibos.CodCer, Recibos.ConCer, 
Recibos.CodTipEstRec, Certificados.CodTipForPag, 
Certificados.CodTipMedPag, Certificados.CodTipOpc, 
Certificados.NumCue, Recibos.ValRec, Recibos.FecEnvRec AS Minimo, 
Certificados.CodOfi, Oficinas.NomOfi, Certificados.NumDocIde, 
lTrim(rtrim(clientes.ApeCli)) + ' ' + lTrim(rtrim(clientes.NomCli)) AS Nombre, 
Clientes.DirCli, Clientes.TelCli, Clientes.FecNacCli, 
Recibos.FecIniPerCob, Recibos.FecFinPerCob, 
Tipos_Estado_Recibo.NomTipEstRec, Productos.NomPro, 
Recibos.MonOpc, Certificados.FecExpCer, 
case when DateDiff(year,certificados.fecexpcer,fecrec)< 1 then
(case when recibos.codtipestrec = 9 then recibos.valrec * porincent /100 else 0 end)
else 0 end as tipopo1
FROM ((Clientes INNER JOIN (Certificados 
INNER JOIN Productos ON Certificados.CodPro = Productos.CodPro) 
ON (Clientes.NumDocIde = Certificados.NumDocIde) AND 
(Clientes.CodTipDocIde = Certificados.CodTipDocIde)) 
INNER JOIN (Recibos INNER JOIN Tipos_Estado_Recibo ON 
Recibos.CodTipEstRec = Tipos_Estado_Recibo.CodTipEstRec) ON 
(Certificados.ConCer = Recibos.ConCer) AND 
(Certificados.CodCer = Recibos.CodCer) AND 
(Certificados.CodPro = Recibos.CodPro)) LEFT JOIN Oficinas ON 
Certificados.CodOfi = Oficinas.CodOfi
WHERE (((Recibos.CodPro)= @CodPro) AND 
((Recibos.FecEnvRec)>= @FechaIni And 
(Recibos.FecEnvRec)<= @FechaFin))

select * from tmpRechazos







GO
