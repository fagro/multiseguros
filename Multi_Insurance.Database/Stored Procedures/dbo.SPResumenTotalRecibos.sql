SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[SPResumenTotalRecibos]
@codpro varchar(20)
as 
--********	 ResCertif	********
SELECT a.CodPro, a.CodCer, a.ConCer, a.NumCue, a.ValPriCer, a.MonOpc, 
Tipos_Estados_Certificado.NomTipEstCer, b.NumDocIde, b.NomCli, 
b.ApeCli, b.FecNacCli, cast(DateDiff(month,[fecnaccli],getdate())/12 as int) AS Edad, 
CASE
   WHEN a.CodTipmedpag = 1 THEN 'Cuenta Ahorros'
   WHEN a.CodTipmedpag = 2 THEN 'Prestamos'
   WHEN a.CodTipmedpag = 3 THEN 'Tarjeta de Crédito'
   WHEN a.CodTipmedpag = 4 THEN 'XF por TC'
END AS MedioPago
into #tmpResCertif
FROM (Certificados a INNER JOIN Clientes b
ON (a.CodTipDocIde = b.CodTipDocIde) AND (a.NumDocIde = b.NumDocIde)) 
INNER JOIN Tipos_Estados_Certificado 
ON a.CodTipEstCer = Tipos_Estados_Certificado.CodTipEstCer
where a.codpro=@codpro
GROUP BY a.CodPro, a.CodCer, a.ConCer, a.NumCue, a.ValPriCer, a.MonOpc, 
Tipos_Estados_Certificado.NomTipEstCer, b.NumDocIde, b.NomCli, b.ApeCli, b.FecNacCli, 
cast(DateDiff(month,[fecnaccli],getdate())/12 as int), a.CodTipmedpag

--*********	ResRecibos	********
SELECT a.CodPro, a.CodCer, a.ConCer, 
sum(case when CodTipEstRec = 9 then 1 else 0 end) as Pagos,
sum(case when CodTipEstRec = 9 then valrec else 0 end) as [Prima Pagos],
sum(case when CodTipEstRec = 8 then 1 else 0 end) as Devol,
sum(case when CodTipEstRec = 8 then valrec else 0 end) as PrimaDevol,
sum(case when CodTipEstRec = 7 then 1 else 0 end) as Cancelados,
sum(case when CodTipEstRec = 7 then valrec else 0 end) as PrimaCancel,
sum(case when CodTipEstRec < 7 Or CodTipEstRec>9 then 1 else 0 end) as Otros,
sum(case when CodTipEstRec < 7 Or CodTipEstRec>9 then valrec else 0 end) as PrimaOtros
into #tmpResRecibos
FROM Recibos a  where codpro=@codpro
GROUP BY a.CodPro, a.CodCer, a.ConCer


SELECT a.*, b.Pagos, b.[Prima Pagos], b.Devol, b.PrimaDevol, b.Cancelados, 
b.PrimaCancel, b.Otros, b.PrimaOtros
FROM #tmpResCertif a INNER JOIN #tmpResRecibos b
ON (a.ConCer = b.ConCer) AND (a.CodCer = b.CodCer) AND 
(a.CodPro = @CodPro)







GO
