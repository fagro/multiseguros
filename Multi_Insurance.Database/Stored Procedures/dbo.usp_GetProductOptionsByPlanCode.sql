SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-04-02
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProductOptionsByPlanCode] 
	-- Add the parameters for the stored procedure here
	@planCode varchar(5),
	@productCode varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT op.CodTipOpc AS PlanCode, op.MonOpc AS PremiumValue 
	FROM Opcion_Producto op
	WHERE op.CodTipOpc = @planCode 
			AND
		 op.CodPro = @productCode
END







GO
