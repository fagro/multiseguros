SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Fagro Vizcaino Salcedo>
-- Create date: <18/06/2012>
-- Description:	<Buscar clientes por ID donde ID es Cédula>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AWSearchClientByIdentification]

@ID as varchar(11)
AS
BEGIN

	SET NOCOUNT ON; 

	SELECT *
	FROM Clientes
	WHERE NumDocIde = @ID
	
END







GO
