SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Envio a la aseguradora
create procedure [dbo].[SP_EnvioAseguradora]
@FechaDesde	smalldatetime,
@FechaHasta	smalldatetime
as
SELECT 	Certificados.CodPro, Productos.NomPro, Certificados.CodCer, 
	Certificados.ConCer, Certificados.CodTipOpc, Certificados.NumCue, 
	Certificados.ValPriCer, Recibos.FecRec AS Corte, Certificados.NumDocIde, 
	ltrim(rTrim(ApeCli)) + ' ' + ltrim(rTrim(NomCli)) AS Nombre, Clientes.DirCli, Clientes.TelCli, 
	Clientes.FecNacCli, cast((DateDiff(month,[fecnaccli],[fecenvrec]))/12 as int) AS Edad, 
	Opcion_Producto.MonOpc
FROM 	Clientes INNER JOIN Certificados ON Clientes.CodTipDocIde = Certificados.CodTipDocIde AND 
	Clientes.NumDocIde = Certificados.NumDocIde INNER JOIN 
	Productos ON Certificados.CodPro = Productos.CodPro INNER JOIN 
	Opcion_Producto ON Certificados.CodTipOpc = Opcion_Producto.CodTipOpc AND 
	Productos.CodPro = Opcion_Producto.CodPro INNER JOIN 
	Recibos ON Productos.CodPro = Recibos.CodPro AND Certificados.CodCer = Recibos.CodCer AND 
	Certificados.ConCer = Recibos.ConCer
WHERE 	Recibos.FecRec >=@fechadesde AND 
	Recibos.FecRec <=@fechahasta AND 
	Certificados.CodTipEstCer =1
--SP_EnvioAseguradora '20080101'







GO
