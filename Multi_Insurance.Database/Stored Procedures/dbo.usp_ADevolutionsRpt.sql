SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Victor Cerda
-- Create date: 2013-02-04
-- Description:	Reporte Devoluciones
-- =============================================
CREATE PROCEDURE [dbo].[usp_ADevolutionsRpt]
	 @ProductCode as Varchar(15),
	 @BeginDate as Datetime,
	 @EndDate as Datetime,
	 @OfficeCode as varchar(15),
	 @MotivoDev as smallint
AS
	
		
	--	Parámetros de Pruebas --
	--Declare @ProductCode as Varchar(15)
	--Declare @BeginDate as Datetime
	--Declare @EndDate as Datetime
	--Declare @OfficeCode as varchar(15) 	
	--Declare @MotivoDev as smallint
  
	--Set @ProductCode = '2-102-495'
	--Set @BeginDate = '2013-01-01'
	--Set @EndDate = '2013-01-31'
	--Set @OfficeCode = '013'
	--Set @MotivoDev = 6


	Select a.CodCer,
		   a.NumCue,
		   a.ValDev,
		   a.NumDocIde,
		   ltrim(rtrim(C.NomCli)) + ' ' + ltrim(rtrim(C.ApeCli)) as NombreCliente,
		   a.CodOfi,
	   	   o.NomOfi,
	   	   d.NomTipMotdev
	From Devoluciones a (nolock)
		Inner join Oficinas o
			on a.CodOfi = o.CodOfi	
		Inner join Tipos_Motivo_Devolucion d
			on a.CodTipMotDev = d.CodTipMotdev			
		Inner join Clientes c
			on a.NumDocIde = c.NumDocIde			
	Where a.FecDev between @BeginDate and @EndDate
	and   (Case when @ProductCode not like  '0'  then a.CodPro else '0' end) = @ProductCode 
	and	  (Case when @OfficeCode  not like  '0'  then a.CodOfi else '0' end) = @OfficeCode
	and   (Case when @MotivoDev  <> 0  then a.CodTipMotDev else 0 end) = @MotivoDev







GO
