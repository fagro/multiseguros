SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SPEstRecMes]
    @FechaProceso datetime ,
    @ProducPro Varchar(15) 
AS

DECLARE 
  @CodproT Varchar(15), @AnoEstT Int, @MesEstT Int,@ZonaT Varchar(15), @NomZonaT Varchar(30)
  ,@ResponsableT Varchar(30)
  ,@CodOfiT Varchar(15),@NomOfiT Varchar(30), @ComEmpT VARCHAR(16),@NomEmpT Varchar(30)
  ,@CertRecMesT INT ,@ValRecMesT Float, @ValAsegMesT Float,@ATmp Int,@MTmp Int
  ,@CP Varchar(15),@AIni Int,@MIni Int,@Ofi Varchar(15), @CerRecM Float,@Aact Int,@Mact Int
  ,@MetaT Int,@MetaAsegT Int,@MetaRecaudoT Float,@ZonaAsegT Varchar(15)
  ,@CerAnual Int,@RecAnual Float,@VrAsegAnual Float,@CertMen Int,@VrAsegMen Float,@VrRecMen Float
  ,@CumpVtaM Float,@CumpRecM Float,@CumpVAsegM Float,@CumpVtaA Float,@CumpRecA Float,@CumpVAsegA Float

SELECT @AIni = datepart(yy, @fechaProceso)
SELECT @MIni = datepart(mm, @fechaProceso)
SELECT @MAct = datepart(mm, getdate())
SELECT @AAct = datepart(yy, getdate())

IF @MIni <= 6
   BEGIN
      SELECT @MIni = (@MIni + 12) - 6
      SELECT @AIni = @AIni - 1
   end
else 
     SELECT @MIni = (@MIni - 6)


DELETE EstRecEmpMes
DELETE EstRecEmpMesConcurso

DELETE EstadisticasCump where Codpro = @ProducPro And AnoEst > @AIni
DELETE EstadisticasCump where Codpro = @ProducPro And AnoEst = @AIni and MesEst >= @MIni

INSERT EstRecEmpMes
SELECT CodPro, AE AS AnoEst, ME as MesEst, Zona, NomZona, Responsable, CodOfi, NomOfi, CodEmp, NomEmp,
               Count(CodCer) as CertRecMes, sum(ValRec) as ValRecMes,sum(MonOpc) as ValAsegMes
FROM   VrecMensualEmp
GROUP BY CodPro, AE, ME, Zona, NomZona, Responsable, CodOfi, NomOfi, CodEmp, NomEmp
HAVING Count(CodCer) >= 10

DECLARE EstRec CURSOR FOR
SELECT CodPro, AE AS AnoEst, ME as MesEst, Zona, NomZona, Responsable, CodOfi, NomOfi, CodEmp, NomEmp,
               Count(CodCer) as CertRecMes, sum(ValRec) as ValRecMes,sum(MonOpc) as ValAsegMes
FROM  VrecMensualEmp
GROUP BY CodPro, AE, ME, Zona, NomZona, Responsable, CodOfi, NomOfi, CodEmp, NomEmp
HAVING Count(CodCer) >= 10
ORDER BY CodPro, AE, ME, Zona, CodEmp

OPEN EstRec
FETCH NEXT FROM EstRec
INTO   @CodproT, @AnoEstT, @MesEstT ,@ZonaT , @NomZonaT, @ResponsableT 
           ,@CodOfiT ,@NomOfiT , @ComEmpT ,@NomEmpT ,@CertRecMesT ,@ValRecMesT , @ValAsegMesT 

WHILE @@FETCH_STATUS = 0
	BEGIN
	SELECT @CerRecM = (SELECT CertRecMes FROM EstRecEmpMesConcurso WHERE Codpro = @CodproT and AnoEst = @AnoEstT and MesEst = @MesEstT and Zona = @ZonaT)
	IF @CerRecM = Null  or @CerRecM < @CertRecMesT
	   BEGIN
	        DELETE EstRecEmpMesConcurso WHERE Codpro = @CodproT and AnoEst = @AnoEstT 
                     	and  MesEst = @MesEstT and Zona = @ZonaT
	        INSERT EstRecEmpMesConcurso (Codpro, AnoEst, MesEst, Zona, NomZona, Responsable 
		,CodOfi ,NomOfi ,CodEmp ,NomEmp ,CertRecMes ,ValRecMes ,ValAsegMes)
	        VALUES (@CodproT, @AnoEstT, @MesEstT ,@ZonaT , @NomZonaT, @ResponsableT 
	               ,@CodOfiT ,@NomOfiT , @ComEmpT ,@NomEmpT ,@CertRecMesT ,@ValRecMesT , @ValAsegMesT)
	   END
	FETCH NEXT FROM EstRec
		INTO @CodproT, @AnoEstT, @MesEstT ,@ZonaT , @NomZonaT, @ResponsableT 
		    ,@CodOfiT ,@NomOfiT , @ComEmpT ,@NomEmpT ,@CertRecMesT ,@ValRecMesT , @ValAsegMesT 
END

CLOSE EstRec

DEALLOCATE EstRec

SELECT @CodProT = @ProducPro

DECLARE cOfi CURSOR FOR
SELECT CodOfi, Meta, MetaAseg, MetaRecaudo, Zona, ZonaAseg
FROM  Oficinas
WHERE CodOfi <> '0'

OPEN cOfi
FETCH NEXT FROM cOfi
INTO   @CodOfiT, @MetaT, @MetaAsegT, @MetaRecaudoT, @ZonaT, @ZonaAsegT

WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @AIni = datepart(yy, @fechaProceso)
		SELECT @MIni = datepart(mm, @fechaProceso)
		IF @MIni <= 6
			BEGIN
				SELECT @MIni = (@MIni + 12) - 6
				SELECT @AIni = @AIni - 1
			END
		ELSE
			BEGIN
				SELECT @MIni = (@MIni - 6)
			END

		WHILE (@AIni <= @AAct) or ((@AIni = @AAct AND @MIni <= @MAct))
		    BEGIN	
			SELECT @MTmp = (@MIni - 1)
			IF @Mtmp = 0 
				BEGIN
					SELECT @MTmp = 12
					SELECT @ATmp = @AIni - 1
				END
			ELSE
				BEGIN
					SELECT @ATmp = @AIni
				END

			SELECT @CertMen = (SELECT Cert from EstCertificados where Codpro = @CodproT and Codofi = @CodOfiT And AE = @AIni and ME = @MIni)
			SELECT @VrRecMen = (SELECT VRec from EstRecaudo where Codpro = @CodproT and Codofi = @CodOfiT And AR = @AIni and MR = @MIni)

			IF @CertMen = Null OR @CertMen = 0
			   BEGIN
				SELECT @CertMen = 0
				SELECT @VrAsegMen = 0
				SELECT @CumpVtaM = 0
				SELECT @CumpVAsegM = 0
			   END
			ELSE
			   BEGIN
				IF @MetaT = 0 or @MetaT = null
				   BEGIN
					SELECT @CumpVtaM = 0
				   END
				ELSE
				   BEGIN
					SELECT @CumpVtaM = ((@CertMen * 100) / @MetaT)
				   END
				SELECT @CumpVAsegM = 0
		           		SELECT @VrAsegMen = (SELECT VrAseg from EstCertificados where Codpro = @CodproT and Codofi = @CodOfiT And AE = @AIni and ME = @MIni)
				IF @VrAsegMen = Null or @VrAsegMen = 0
				    BEGIN
					SELECT @VrAsegMen = 0
					SELECT @CumpVAsegM = 0
				    END
				ELSE
				    BEGIN
					IF @MetaAsegT = 0
					   BEGIN
						SELECT @CumpVAsegM = 0
					   END
					ELSE
					   BEGIN
						SELECT @CumpVAsegM = ((@VrAsegMen * 100) / @MetaAsegT)
					   END
				    END
			   END

			
	                        IF @MTmp = 12
	                           BEGIN
				SELECT @CerAnual = 0
				SELECT @VrAsegAnual = 0
				SELECT @RecAnual = 0
	                           END
	                        ELSE
	                           BEGIN
		                    SELECT @CerAnual = (SELECT VentasCerAnual from EstadisticasCump where Codpro = @CodproT and Codofi = @CodOfiT And AnoEst = @ATmp and MesEst = @MTmp)
			       SELECT @VrAsegAnual = (SELECT ValAsegAnual from EstadisticasCump where Codpro = @CodproT and Codofi = @CodOfiT And AnoEst = @ATmp and MesEst = @MTmp)
			       SELECT @RecAnual = (SELECT ValRecaudoAnual from EstadisticasCump where Codpro = @CodproT and Codofi = @CodOfiT And AnoEst = @ATmp and MesEst = @MTmp)
	                           END
			IF @CerAnual = Null or @CerAnual = 0
			    BEGIN
				SELECT @CerAnual = 0	
				SELECT @CerAnual = @CerAnual + @CertMen
				IF @CerAnual = 0
				   BEGIN
					SELECT @CumpVtaA = 0
				   END
				ELSE
				   BEGIN
					IF @MetaT = 0 or @MetaT = null
					   BEGIN
						SELECT @CumpVtaA = 0
					   END
					ELSE
					   BEGIN
						SELECT @CumpVtaA = (@CerAnual * 100) / (@MetaT * @MIni)
					   END
				  END
			    END
			ELSE
			    BEGIN
				SELECT @CerAnual = @CerAnual + @CertMen
				IF @MetaT = 0 or @MetaT = null
				   BEGIN
					SELECT @CumpVtaA = 0
				   END
				ELSE
				   BEGIN
					SELECT @CumpVtaA = (@CerAnual * 100) / (@MetaT * @MIni)
				   END
			    END

			IF @VrAsegAnual = Null or @VrAsegAnual = 0
			    BEGIN
				SELECT @VrAsegAnual = @VrAsegMen
				IF @VrAsegAnual = 0 or @MetaAsegT = 0
				     BEGIN
					SELECT @CumpVAsegA = 0
				     END
				ELSE
				     BEGIN
					SELECT @CumpVAsegA = (@VrAsegAnual * 100) / (@MetaAsegT * @MIni)
				     END
			    END
			ELSE
			   BEGIN
				SELECT @VrAsegAnual = @VrAsegAnual + @VrAsegMen
				IF @MetaAsegT > 0
				  BEGIN
					SELECT @CumpVAsegA = (@VrAsegAnual * 100) / (@MetaAsegT * @MIni)
				  END
				ELSE
				  BEGIN
					SELECT @CumpVAsegA = 0
				  END
			   END

			IF @VrRecMen = Null or @VrRecMen = 0 
		   	   BEGIN
				SELECT @VrRecMen = 0
				SELECT @CumpRecM = 0
		   	   END
			ELSE
			   BEGIN
				IF @MetaRecaudoT = 0
				   BEGIN
					SELECT @CumpRecM = 0
				   END
				ELSE
				   BEGIN
					SELECT @CumpRecM = (@VrRecMen * 100) / @MetaRecaudoT
				   END
			   END

			IF @RecAnual = Null or @RecAnual = 0 
			    BEGIN
				SELECT @RecAnual = @VrRecMen
				IF @RecAnual = 0 or @MetaRecaudoT = 0
				    BEGIN
					SELECT @CumpRecA = 0
				    END
				ELSE
				    BEGIN
					SELECT @CumpRecA = (@RecAnual * 100) / (@MetaRecaudoT * @MIni)
				    END
			    END
			ELSE
			    BEGIN
				SELECT @RecAnual = @RecAnual + @VrRecMen
				IF @MetaRecaudoT = 0
				   BEGIN
					SELECT @CumpRecA = 0
				   END
				ELSE
				   BEGIN	
					SELECT @CumpRecA = (@RecAnual * 100) / (@MetaRecaudoT * @MIni)
				   END
			    END

	       		INSERT EstadisticasCump (Codpro, AnoEst, MesEst, CodOfi, MetaCerMen, MetaValAsegMes, MetaRecaudoMes 
				,MetaCerAnual ,MetaRecaudoAnual ,MetaValAsegAnual ,Zona ,ZonaAseg ,VentasCerMen
				,CumpVtasMes,ValAsegMes,CumpValAsegMes,ValRecaudoMes,CumpRecaudoMes,ValRecaudoAnual
				,CumpRecaudoAnual,ValAsegAnual,VentasCerAnual,CumpVtasAnual,CumpValAsegAnual)
			VALUES (@CodproT, @AIni, @MIni, @CodOfiT, @MetaT, @MetaAsegT, @MetaRecaudoT
				,(@MetaT * @MIni),(@MetaRecaudoT * @MIni),(@MetaAsegT * @MIni), @ZonaT, @ZonaAsegT, @CertMen
				,@CumpVtaM, @VrAsegMen, @CumpVAsegM, @VrRecMen, @CumpRecM, @RecAnual 
				,@CumpRecA, @VrAsegAnual,@CerAnual, @CumpVtaA, @CumpVAsegA)

			SELECT @MIni = @MIni + 1
			IF @MIni > 12
				BEGIN
					SELECT @MIni = 1
					SELECT @AIni = @AIni + 1	
				END
		END
	FETCH NEXT FROM cOfi
	INTO   @CodOfiT, @MetaT, @MetaAsegT, @MetaRecaudoT, @ZonaT, @ZonaAsegT
END

CLOSE cOfi

DEALLOCATE cOfi







GO
