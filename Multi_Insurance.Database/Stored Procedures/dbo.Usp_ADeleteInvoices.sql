SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 24/07/2012
-- Description:	Elimina recibos masivos siempre y cuando no esten pagados.
-- =============================================
CREATE PROCEDURE [dbo].[Usp_ADeleteInvoices]

@fechaRecibo AS DATETIME,
@codigoProducto AS VARCHAR(20),
@numeroCuenta AS VARCHAR(20)
	
AS
	BEGIN TRY
    
		SET NOCOUNT ON;   
		SET XACT_ABORT ON;

		BEGIN TRANSACTION

		--Verifica que no exista ningun recibo cobrado.
		IF NOT EXISTS(SELECT CodTipEstRec FROM dbo.Recibos WHERE CodTipEstRec = 9 AND NumCue = @numeroCuenta AND FecRec = @fechaRecibo AND CodPro = @codigoProducto)
		BEGIN
        
			DELETE dbo.Recibos
			WHERE CodTipEstRec NOT IN (9,1)
				AND NumCue = @numeroCuenta
				AND CodPro = @codigoProducto
				AND FecRec = @fechaRecibo			      
			COMMIT
		END 	      
		
	END TRY    

	BEGIN CATCH
		IF	 @@TRANCOUNT > 0 ROLLBACK
		RAISERROR('La transacion no pudo ser completa',16,1);      
	END CATCH







GO
