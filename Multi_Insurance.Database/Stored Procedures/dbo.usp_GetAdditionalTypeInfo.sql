SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAdditionalTypeInfo] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0
		Select idTipInfAdi, tia.CodTipInfAdi, tia.NomTipInfAdi, tia.FecUltMod 
		From dbo.Tipos_Info_Adicional tia
	Else
		Select idTipInfAdi, tia.CodTipInfAdi, tia.NomTipInfAdi, tia.FecUltMod 
		From dbo.Tipos_Info_Adicional tia
		Where tia.CodTipInfAdi = @id
END




GO
