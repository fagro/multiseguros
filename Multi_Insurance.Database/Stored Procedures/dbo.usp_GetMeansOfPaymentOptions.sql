SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-13
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMeansOfPaymentOptions] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0

		SELECT tmp.NomTipMedPag As MeansOfPaymentOptionName,
				tmp.CodTipMedPag as MeansOfPaymenOptiontCode,
				FecUltMod As LastModification
		FROM Tipos_Medio_Pago tmp
		
		

	Else
		SELECT tmp.NomTipMedPag As MeansOfPaymentOptionName,
				tmp.CodTipMedPag as MeansOfPaymenOptiontCode,
				FecUltMod As LastModification
		FROM Tipos_Medio_Pago tmp
		Where CodTipMedPag = @id	

END




GO
