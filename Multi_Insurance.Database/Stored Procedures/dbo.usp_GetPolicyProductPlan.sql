SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPolicyProductPlan] 
	-- Add the parameters for the stored procedure here
	@productId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

        -- Insert statements for procedure here

		SELECT DISTINCT [to].CodTipOpc As productId,
			   [to].NomTipOpc AS planName
		FROM Tipos_Opcion [to]
			JOIN Opcion_Producto op
				ON op.CodTipOpc = [to].CodTipOpc
		
	
END



GO
