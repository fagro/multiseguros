SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[AgregarCliTar]
  @codpro varchar(16),
  @codcer int,
  @NomCli char(30),
  @apecli char(30),
  @codtipdocide smallint, 
  @numdocide varchar(16),
  @codtipsex smallint,
  @fecnaccli datetime,
  @coddep int, 
  @codciu int, 
  @dircli varchar(60),
  @telcli varchar(20),
  @telofi varchar(20),
  @codtipopc smallint,
  @codtipforpag smallint,
  @vrasegurado float,
  @vrcontenido float,
  @codofi varchar(3),
  @codtipmedpag smallint,
  @numcue varchar(16),
  @prima float,
  @FecEnvRec datetime,
  @FecProceso datetime
 
as
declare 
  @sexor smallint,
  @codpror varchar(16),
  @error smallint,
  @dia smallint,
  @mes smallint,
  @nmes smallint,
  @ano int,
  @fecharec datetime,
  @fechafinpercob datetime,
  @coderror smallint

select @sexor =  @codtipsex
select @coderror = 0

IF NOT EXISTS(SELECT * FROM CERTIFICADOS WHERE CODPRO = @CODPRO AND CODCER = @CODCER AND CONCER = 1)
  BEGIN
  INSERT CERTIFICADOS
         (
   CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP,
   NUMDOCIDEEMP, CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE,
   VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, MONOPC, indcob, VALCONTENIDO, DIRINM
  )
  VALUES 
  (
   @CODPRO, @CODCER, 1, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFI, 1,
   1, 1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE,
   @prima, 0,  @FecProceso , GETDATE(), GETDATE(), @vrasegurado, 1, @vrcontenido, @dircli
  )
  end
else
begin
  select @coderror=1
  goto finalizar
end

IF NOT EXISTS(SELECT * FROM CLIENTES WHERE CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
  BEGIN
  insert clientes 
       (
        codtipdocide, numdocide, nomcli, apecli, fecnaccli, dircli,telcli, codciu,
        codtipsex, fecultmod, dirrec, telrec,CodtipOcu,NoHijos,CodTipEstCivil )
        VALUES 
        (
        @CODTIPDOCIDE, @NUMDOCIDE,  @NOMCLI, @APECLI, @FECNACCLI, @DIRCLI, @TELCLI, @CODCIU,
        @sexor, GETDATE(), @dircli, @telofi,0,0,0)
  END

if not exists(select codpro from recibos where codpro = @CODPRO and codcer = @CODCER and concer =1)
begin
   select @dia = datepart(dd, @FecProceso )
   select @mes =datepart(mm, @FecProceso )
   select @ano=datepart(yy, @FecProceso )
   if  @dia <=15 
     select @dia = 15
   else
     select @dia= 30
    if @Mes = 2 and @dia= 30
        select @Dia = 28
   select @fecharec = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))
  
  SELECT @NMES = @MES+12/@CODTIPFORPAG
   IF @NMES > 12 
   BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
   end
   else 
     select @mes = @nmes
   If @Mes = 2 and @DIA = 30
         SELECT  @DIA = 28
   If @Mes <> 2 and @DIA = 28
         SELECT  @DIA = 30
 
   select @FECHAFINPERCOB = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))

   INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, FECENVREC, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPRO, @CODCER, 1, @FECHAREC, @NUMCUE, @prima, 0, @vrasegurado, @FECHAREC, 
   @fechafinpercob, @CODTIPMEDPAG, 6, @codtipforpag, @FecEnvRec, @numdocide, @FecProceso, @codofi, GETDATE(), 1)
 end


FINALIZAR:
if @coderror <>0
begin
 select @codpro = 'xx'
 select @codcer=1
end
SELECT @CODERROR,
       CERTIFICADOS.CODCER, 
       CERTIFICADOS.VALPRICER
       FROM CERTIFICADOs
       WHERE CERTIFICADOS.CODPRO = @CODPRO 
       AND CERTIFICADOS.CODCER = @CODCER







GO
