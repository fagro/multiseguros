SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2012-09-25
-- Description:	Obtiene la informacion del vehículo por número de cotización(Hecho con motivo feria de vehículos).
-- =============================================

CREATE PROCEDURE [dbo].[Usp_AWGetVehicleDataBySummaryNumber]

@numeroCotizacion AS int

As 

BEGIN
		
		--DECLARE @numeroCotizacion As Int
		--DECLARE @codigoProducto As VARCHAR(20)

		--SET @codigoProducto = '2-105-102'
		--SET @numeroCotizacion = 208535

		SET NOCOUNT ON;

		SELECT	t.NumCotizacion AS NumeroCotizacion, 
				vq.MakeDescription AS Marca, 
				v.Description AS Modelo, 
				t.Ano AS Año, 
				t.Chassis AS Chasis, 
				t.Registro, 
				tuso.Descripcion AS Uso, 
				vt.Descripcion AS TipoVehiculo,
				cc.Prima_Total As PrimaTotal

		FROM	CotizacionMasivos.dbo.TbVehVehiculos AS t 
					INNER JOIN	SegurosMasivos_2008r2.dbo.VehicleMakeModel AS v 
						ON v.MakeCode = t.Marca 
						AND v.MakeModel = t.Modelo 

					INNER JOIN CotizacionMasivos.dbo.TbVehCotizacion cc
						ON  cc.NumCotizacion = t.NumCotizacion

					INNER JOIN	SegurosMasivos_2008r2.dbo.VQVehicleMake AS vq 
						ON v.MakeCode = vq.VehicleMake 

					INNER JOIN	CotizacionMasivos.dbo.TbVehUso_Vehiculo AS tuso 
						ON tuso.Codigo = t.Codigo_Uso 

					INNER JOIN	CotizacionMasivos.dbo.View_VehicleTypes AS vt 
						ON t.Tipo_Vehiculo = vt.Codigo 

						AND vt.Estado = 1 AND vt.Ley = 0
		WHERE        (t.NumCotizacion = @numeroCotizacion)


END







GO
