SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cerda , Victor
-- Create date: 28/05/2012
-- Description:	Inserta un nuevo registro de empleado o modifica uno existente.
-- =============================================

CREATE PROCEDURE [dbo].[usp_CreateUpdateEmployee] 
	@EmployeeId as varchar(16),
	@OfficeCode as varchar(15),
	@EmployeeName as varchar(40),
	@LastModifiedDate as Datetime,
	@PayMethod as float,
	@AccountNumber as varchar(25)
AS
/*
	Declare @EmployeeId as varchar(16)
	Declare @OfficeCode as varchar(15)
	Declare @EmployeeName as varchar(40)
	Declare @LastModifiedDate as Datetime
	Declare @PayMethod as float
	Declare @AccountNumber as varchar(25)
	
	Set @Employeeid = 8888
	Set @OfficeCode = 580
	Set @EmployeeName = 'Victor Cerda'
	Set @LastModifiedDate = '2012-06-08'
	set @PayMethod = 4
	Set @AccountNUmber = ''
*/
BEGIN	
	MERGE Empleados as E
	USING (Select @EmployeeId,@OfficeCode,@EmployeeName,@LastModifiedDate, @PayMethod, @AccountNumber)
			 As Source (NumDocIde,CodOfi,NomEmp,FecUltMod,CodTipMedPag,NumCue) 
			on E.NumDocIde = @EmployeeId
	when Matched then 
		Update 		 
			Set NomEmp = @EmployeeName,
				CodOfi = @OfficeCode,
				FecUltMod = @LastModifiedDate,
				CodTipMedPag = @PayMethod,				
				NumCue = @AccountNumber			
	when not matched then
		Insert (CodTipDocIde,NumDocIde,CodOfi,NomEmp,FecUltMod,CodTipMedPag,NumCue) 
			Values (0,@EmployeeId,@OfficeCode,@EmployeeName,@LastModifiedDate,@PayMethod,@AccountNumber)	
		
		OUTPUT $action ;	
END







GO
