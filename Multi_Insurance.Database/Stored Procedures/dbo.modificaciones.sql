SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.modificaciones    Script Date: 24/07/2000 09:57:33 ******/
CREATE procedure [dbo].[modificaciones] @codpro char(15), @codcer int, 
             @concer smallint, @codtipdocide smallint, @numdocide varchar(16), 
             @nomcli varchar(30), @apecli varchar(30), @dir1cli varchar(35), 
             @tel1cli varchar(25), @dir2cli varchar(35), @tel2cli varchar(25), 
             @codciu int, @fecnaccli datetime, @codtipsex smallint, @codofi varchar(15), 
             @codtipforpag smallint, @codtipmedpag smallint,@numcue varchar(20), 
             @codtipopc smallint as
declare @codtipsexa smallint
update clientes set nomcli = @nomcli, apecli = @apecli, ape2cli = '', 
            dircli = @dir1cli, telcli = @tel1cli, codciu = @codciu, 
             fecnaccli = @fecnaccli, codtipsex= @codtipsexa, dirrec = @dir2cli, telrec = @tel2cli 
             where codtipdocide = @codtipdocide and numdocide = @numdocide 
update certificados set CodtipForPag = @CodTipForPag, CodTipMedPag = @CodTipMedPag, 
              NumCue = @NumCue, CodTipOpc = @CodTipOpc 
               where CodPro = @CodPro and CodCer = @CodCer and ConCer = @Concer
Update Recibos set CodTipForPag = @Codtipforpag, CodTipMedPag = @CodtipMedPag, NumCue = @NumCue 
                where Codpro = @CodPro and Codcer = @CodCer and ConCer = @Concer 
                 and CodTipEstRec <> 9 and CodTipEstRec <> 7







GO
