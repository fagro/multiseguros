SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.carguerec    Script Date: 11/07/2000 14:29:20 ******/
CREATE PROCEDURE [dbo].[carguerec]
 @CodPro varchar(10),
 @CodCer float,
 @ConCer Smallint,
 @FecRec datetime,
 @NumCue varchar(20),
 @ValRec Float,
 @NumRecEnv smallint,
 @MonOpc float,
 @FecIniPerCob datetime,
 @FecFinPerCob dateTime,
 @CodTipMedPag smallint,
 @CodTipEstRec smallint,
 @CodTipForPag smallint,
 @BanEnv Bit,
 @FecEnvrec DateTime,
 @NumDocIde varchar(16),
 @Altura smallint,
 @ano smallint,
 @FecExpcer datetime,
 @CodOfi varchar(15),
 @FecUltmod datetime
 AS
insert into Recibos (
 CodPro,
 CodCer,
 ConCer,
 FecRec,
 NumCue,
 ValRec,
 NumRecEnv,
 MonOpc,
 FecIniPerCob,
 FecFinPerCob,
 CodTipMedPag,
 CodTipEstRec,
 CodTipForPag,
 BanEnv,
 FecEnvrec,
 NumDocIde,
 Altura,
 ano,
 FecExpcer,
 CodOfi,
 FecUltmod
)
values (
 @CodPro,
 @CodCer,
 @ConCer,
 @FecRec,
 @NumCue,
 @ValRec,
 @NumRecEnv,
 @MonOpc,
 @FecIniPerCob,
 @FecFinPerCob,
 @CodTipMedPag,
 @CodTipEstRec,
 @CodTipForPag,
 @BanEnv,
 @FecEnvrec,
 @NumDocIde,
 @Altura,
 @ano,
 @FecExpcer,
 @CodOfi,
 @FecUltmod
)







GO
