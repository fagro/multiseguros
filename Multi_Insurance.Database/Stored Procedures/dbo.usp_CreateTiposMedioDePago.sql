SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTiposMedioDePago] 
	-- Add the parameters for the stored procedure here
	@Name varchar(50),
	@id smallint = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Merge dbo.Tipos_Medio_Pago As Target
		Using (Select @id, @Name ) As Source( CodTipMedPag, NomTipMedPag)
			On (Target.CodTipMedPag = Source.CodTipMedPag)
		When Matched Then
			Update Set NomTipMedPag = Source.NomTipMedPag
		When Not Matched Then
    -- Insert statements for procedure here
			Insert
			(
	   				NomTipMedPag
			)
			Values
			(
	   				Source.NomTipMedPag -- NomTipMedPag - varchar
			)

	Output $action;
END






GO
