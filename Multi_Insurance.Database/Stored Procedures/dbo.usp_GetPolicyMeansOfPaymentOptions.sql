SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPolicyMeansOfPaymentOptions] 
	-- Add the parameters for the stored procedure here
	@productId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT tmp.NomTipMedPag As MeansOfPaymentOptionName,
				tmp.CodTipMedPag as MeansOfPaymenOptiontCode,
				mpp.FecUltMod As LastModification
		FROM Tipos_Medio_Pago tmp
			Join dbo.Medios_Pago_Producto mpp
				On mpp.CodTipMedPag = tmp.CodTipMedPag
		Where mpp.CodPro = (Select mpp.CodPro From dbo.Productos p Where p.IdProductos = @productId)
END


GO
