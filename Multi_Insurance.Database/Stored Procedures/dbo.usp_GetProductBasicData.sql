SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProductBasicData] 
	-- Add the parameters for the stored procedure here
	@Id int = 0 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0
		Select IdProductos, NomPro, CodPro, NitComSeg, NitPatPro,CodTipMon, CodTipRam,NitGes,
				 CodTipInd, CodTipMer, IndProCon, IndAmpSel, iva, expedir, FecCorPro,FecIniVen, PorComPro, porcompror,
				 honocobn, honocobr, medadm,  medadf, PORINCENT, PORINCENTR, FecUltMod
		From dbo.Productos p
	Else

		Select IdProductos, NomPro, CodPro, NitComSeg, NitPatPro,CodTipMon, CodTipRam,NitGes,
				 CodTipInd, CodTipMer, IndProCon, IndAmpSel, iva, expedir, FecCorPro,FecIniVen, PorComPro, porcompror,
				 honocobn, honocobr, medadm,  medadf, PORINCENT, PORINCENTR, FecUltMod
		From dbo.Productos p
		Where p.IdProductos = @Id
		
END



GO
