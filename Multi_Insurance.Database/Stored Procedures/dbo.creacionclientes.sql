SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE procedure [dbo].[creacionclientes]
	@CodTipDocIde smallint,
	@NumDocIde varchar (16),
	@NomCli varchar (30),
	@ApeCli varchar (30),
	@Ape2Cli varchar (30),
	@FecNacCli VARCHAR(10),
	@DirCli varchar (60),
	@TelCli varchar (25),
	@CodCiu int,
	@CodTipOcu smallint,
	@CodTipSex smallint,
	@ExaMed SMALLINT,
	@FecUltMod VARCHAR(10),
	@DIRREC varchar (60),
	@TELREC varchar (20),
	@email varchar (35)
as
DECLARE  @Prueba char (25)
 
GRABAR:
IF NOT EXISTS(SELECT NumDocIde FROM Clientes WHERE CodTipDocIde = @CodTipDocIde AND NumDocIde = @NumDocIde)
BEGIN
insert Clientes 
       (
	CodTipDocIde,	NumDocIde,NomCli,ApeCli,Ape2Cli,FecNacCli,DirCli,TelCli,CodCiu,
	CodTipOcu,CodTipSex,ExaMed,FecUltMod,DIRREC,TELREC, email
        )
        VALUES 
        (
	@CodTipDocIde,@NumDocIde,@NomCli,@ApeCli,@Ape2Cli,@FecNacCli,	@DirCli,	@TelCli,@CodCiu,
	@CodTipOcu,@CodTipSex,@ExaMed,@FecUltMod,@DIRREC,@TELREC,@email
        )
END
else
BEGIN
update Clientes
       set
	CodTipDocIde = @CodTipDocIde,
	NumDocIde = @NumDocIde,
	NomCli = @NomCli,
	ApeCli = @ApeCli,
	Ape2Cli = @Ape2Cli,
	FecNacCli = @FecNacCli,
	DirCli = @DirCli,
	TelCli = @TelCli,
	CodCiu =  @CodCiu,
	CodTipOcu = @CodTipOcu,
	CodTipSex = @CodTipSex,
	ExaMed = @ExaMed,
	FecUltMod = @FecUltMod,
	DIRREC = @DIRREC,
	TELREC = @TELREC,
	email = @email
        WHERE CodTipDocIde = @CodTipDocIde AND NumDocIde = @NumDocIde
end







GO
