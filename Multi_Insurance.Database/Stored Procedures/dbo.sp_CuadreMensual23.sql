SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE    PROCEDURE [dbo].[sp_CuadreMensual23]
@CodPro		nvarchar(20),
@FecEnvRecIni	smalldatetime,
@FecEnvRecFin	smalldatetime,
@porcompro		float,
@aonrell		float
AS
delete from tmpCuadreMensual

SELECT DISTINCT Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, 
Recibos.CodTipForPag, Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, 
Recibos.CodTipEstRec, Count(Recibos.CodCer) AS CuentaDeCodCer, 
Sum(-Recibos.ValRec) AS SumaDeValRec, 
Sum(Recibos.MonOpc) AS SumaDeMonOpc, 
Edad = case 
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=18 and 		
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=29) then '18-29'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=30 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=44) then '30-44'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=45 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=49) then '45-49'
	else '50-70' end,

---sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))<=365 then
--((valrec-((Recibos.MonOpc/1000)*0.1575))*0.08)*1.16 else 0 end) as AonN,
---Sum(Case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))>365 And Abs(DateDiff(day,recibos.fecexpcer,fecrec)) < 731 then
--((valrec-((Recibos.MonOpc/1000)*0.1575))*0.08)*1.16 else 0 end) AS AonR, 
---Sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec)) > 730 then 
--((valrec-((Recibos.MonOpc/1000)*0.1575))*0.08)*1.16 else 0 end) AS AonRT, 
---Sum(((Recibos.MonOpc/1000)*0.1575)*1.16) as Valor1,
---sum(((valrec-((Recibos.MonOpc/1000)*0.1575))*0.92)*1.16) as Valor2

	-sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))<=365 then
		(valrec*(@aonrell/100))*(1.16)
		else 0 end) as AonN,

	-Sum(Case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))>365 then
		(valrec*(@aonrell/100))*(1.16)
		else 0 end) AS AonR, 

	-Sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec)) > 730 then 
		(valrec*(@aonrell/100))*(1.16) else 0 end) AS AonRT, 
	
	-Sum((Recibos.MonOpc/1000)*0.1575)*(1.16) as Valor1,

	-sum((valrec)*(0.7625))*(1.16) as Valor2

into #x1
FROM Recibos INNER JOIN Productos ON Recibos.CodPro = Productos.CodPro 
INNER JOIN Tipos_Forma_Pago ON Recibos.CodTipForPag = Tipos_Forma_Pago.CodTipForPag 
INNER JOIN Certificados ON Recibos.ConCer = Certificados.ConCer 
AND Recibos.CodCer = Certificados.CodCer AND Recibos.CodPro = Certificados.CodPro 
INNER JOIN Tipos_Opcion ON Certificados.CodTipOpc = Tipos_Opcion.CodTipOpc
inner join clientes on recibos.NumDocIde = Clientes.NumDocIde
GROUP BY Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, Recibos.CodTipForPag, 
Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, Recibos.CodTipEstRec,
case 
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=18 and 		
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=29) then '18-29'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=30 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=44) then '30-44'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=45 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=49) then '45-49'
	else '50-70' end

HAVING Recibos.CodPro = @CodPro
AND Recibos.FecEnvRec >= @FecEnvRecIni
And Recibos.FecEnvRec <= @FecEnvRecFin
AND Recibos.CodTipEstRec = 8

--***********************************************************************

SELECT DISTINCT Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, 
Recibos.CodTipForPag, Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, 
Recibos.CodTipEstRec, Count(Recibos.CodCer) AS CuentaDeCodCer, 
Sum(Recibos.ValRec) AS SumaDeValRec,
Sum(Recibos.MonOpc) AS SumaDeMonOpc, 
Edad = case 
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=18 and 		
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=29) then '18-29'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=30 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=44) then '30-44'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=45 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=49) then '45-49'
	else '50-70' end,

--sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))<=365 then
--(valrec-((Recibos.MonOpc/1000)*0.1575))*0.08 else 0 end) as AonN,
--Sum(Case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))>365 And Abs(DateDiff(day,recibos.fecexpcer,fecrec)) < 731 then
--(valrec-((Recibos.MonOpc/1000)*0.1575))*0.08 else 0 end) AS AonR, 
--Sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec)) > 730 then 
--(valrec-((Recibos.MonOpc/1000)*0.1575))*0.08 else 0 end) AS AonRT, 
--Sum((Recibos.MonOpc/1000)*0.1575) as Valor1,
--sum((valrec-((Recibos.MonOpc/1000)*0.1575))*0.92) as Valor2

	sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))<=365 then
		((valrec)*(@aonrell/100))
		else 0 end) as AonN,

	Sum(Case when Abs(DateDiff(day,recibos.fecexpcer,fecrec))>365 then
		((valrec)*(@aonrell/100)) else 0 end) AS AonR, 

	Sum(case when Abs(DateDiff(day,recibos.fecexpcer,fecrec)) > 730 then 
		((valrec)*(@aonrell/100)) else 0 end) AS AonRT, 	

	Sum((Recibos.MonOpc/1000)*0.1575) as Valor1,

	sum((valrec)*0.7625) as Valor2

into #x2
FROM Recibos INNER JOIN Productos ON Recibos.CodPro = Productos.CodPro 
INNER JOIN Tipos_Forma_Pago ON Recibos.CodTipForPag = Tipos_Forma_Pago.CodTipForPag 
INNER JOIN Certificados ON Recibos.ConCer = Certificados.ConCer 
AND Recibos.CodCer = Certificados.CodCer AND Recibos.CodPro = Certificados.CodPro 
INNER JOIN Tipos_Opcion ON Certificados.CodTipOpc = Tipos_Opcion.CodTipOpc
inner join clientes on recibos.NumDocIde = Clientes.NumDocIde
GROUP BY Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, Recibos.CodTipForPag, 
Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, Recibos.CodTipEstRec,
case 
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=18 and 		
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=29) then '18-29'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=30 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=44) then '30-44'
	when (cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int)>=45 and 
 	cast(datediff(year,clientes.FecNacCli,recibos.fecenvrec) as int) <=49) then '45-49'
	else '50-70' end

HAVING Recibos.CodPro = @CodPro
AND Recibos.FecEnvRec >= @FecEnvRecIni
And Recibos.FecEnvRec <= @FecEnvRecFin
AND Recibos.CodTipEstRec = 9


--***************************************************************

insert into tmpCuadreMensual
select * from #x1

insert into tmpCuadreMensual
select * from #x2

select * from tmpCuadreMensual
order by FecEnvRec,codtipestrec desc,NomTipForPag







GO
