SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CreaDeclaracion] 
	@CodPro VarChar(16),
	@CodCer float,
	@ConCer smallint,
	@Peso varchar (50),
	@LugarTrabajo varchar (150),
	@Enfermedad varchar (2),
	@DesEnfermedad varchar (150),
	@Tratamiento varchar (2),
	@DesTratamiento varchar (150),
	@Operacion varchar (2),
	@DesOperacion varchar (150),
	@Observacion varchar (2),
	@DesObservacion varchar (150),
	@Radiografia varchar (2),
	@FechaRadiografia varchar (10),
	@Corazon varchar (2),
	@DesCorazon varchar (150),
	@Medico varchar (150),
	@Medicamento varchar (150),
	@Defecto varchar (2),
	@DesDefecto varchar (150),
	@MedicoPar varchar (150),
	@Transfusion varchar (2),
	@FechaTransfusion varchar (10)   

as

DECLARE  
  @CodError int,
  @Msgerror varchar(40),
  @Numb INT
  
Select @CodError = 0

Select @Numb =  (Select count(Codcer) from dbo.declaracion where Codpro = @CodPro and Codcer = @Codcer and Concer = @Concer)
if @Numb = 0
   Begin 
      Insert into dbo.declaracion 
             (CodPro, CodCer, ConCer, Peso, LugarTrabajo, Enfermedad, DesEnfermedad, Tratamiento, DesTratamiento, Operacion, DesOperacion,
				Observacion, DesObservacion, Radiografia, FechaRadiografia, Corazon, DesCorazon, Medico, Medicamento, Defecto, DesDefecto,
				MedicoPar, Transfusion, FechaTransfusion, FecUltMod)
      values 
             (@CodPro, @CodCer, @ConCer, @Peso, @LugarTrabajo, @Enfermedad, @DesEnfermedad, @Tratamiento, @DesTratamiento, @Operacion,
				@DesOperacion,	@Observacion, @DesObservacion, @Radiografia, @FechaRadiografia, @Corazon, @DesCorazon, @Medico, @Medicamento,
				@Defecto, @DesDefecto, @MedicoPar, @Transfusion, @FechaTransfusion, GETDATE())

      select @MSGERROR = 'Declaración de Salud Guardada'
   end
else
	BEGIN
		Update	dbo.declaracion 
		Set		Peso =	@Peso,
				LugarTrabajo = @LugarTrabajo,
				Enfermedad = @Enfermedad,
				DesEnfermedad = @DesEnfermedad,
				Tratamiento = @Tratamiento,
				DesTratamiento = @DesTratamiento,
				Operacion = @Operacion,
				DesOperacion = @DesOperacion,
				Observacion = @Observacion,
				DesObservacion = @DesObservacion,
				Radiografia = @Radiografia,
				FechaRadiografia = @FechaRadiografia,
				Corazon = @Corazon,
				DesCorazon = @DesCorazon,
				Medico = @Medico,
				Medicamento = @Medicamento,
				Defecto = @Defecto,
				DesDefecto = @DesDefecto,
				MedicoPar = @MedicoPar,
				Transfusion = @Transfusion,
				FechaTransfusion = @FechaTransfusion,
				FecUltMod = GETDATE() 
		where	CodPro = @CodPro and 
				CodCer = @CodCer and 
				ConCer = @ConCer 
 
		Select @MSGERROR = 'Declaración de Salud Actualizada'
	end	

terminar:
if @coderror <> 0
begin
 Select @Codpro = 'xx'
 Select @Codcer=1
 Select @Concer=1
 Select @Enfermedad = 'N'
end
Select	@CODERROR AS CODERROR,
		CodPro, 
		CodCer,
		@MSGERROR as msgerror
FROM	dbo.declaracion
WHERE	CodPro = @CodPro and 
		CodCer = @CodCer and 
		ConCer = @ConCer







GO
