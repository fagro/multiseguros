SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[Sp_IngresoPolizasold]
	@nomCLI char(25),
	 @apecli char(25),
	 @codtipdocide smallint, 
	 @numdocide varchar(16),
	 @codtipsex smallint,
	 @fecnaccli CHAR(10),
	 @codciu int,	
	 @nomciu char(25),
	 @telcli varchar(10),
	 @dircli varchar(60),
	 @codtipmedpag smallint,
	 @numcue varchar(25),
	 @codpro varchar(16),
	 @codcer int,
	 @concer smallint,
	 @codtipopc smallint,
	 @codtipforpag smallint,
	 @vrhogarinmueble float,
	 @vrhogarcontenido float,
	 @codtipdocideemp smallint,
	 @numdocideemp char(12),
	 @codofi char(10),
	 @nomofi char(25),
         @dirrec char (60),
         @telrec char (15),
         @fecexpcer CHAR(10),
         @CODTIPOCUPACION INT,
	 @USUARIO CHAR(25),
         @CODTIPESTCIVIL INT,
         @NOHIJOS INT as
DECLARE @VALPRICER float, @VALPRICERA float,  @VALrenta float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
        @CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
        @VALBANCASEGUROS INT, @VALASEGURADORA INT, @IVA INT, @CODERROR INT,
        @LIMMONOPC float, @VALASEGACT int, @EDAD INT, @edadmin int, @recargo float,
        @edadmax int, @forliqprima int, @valasegopcion float, @NumCer int, @CertCli int,
        @CARACTERES CHAR(16),@factor float (10),@adicionedad float,@codcerc char(6),@codcern int, 
        @valpricerc char(10),
        @DIA INT,@MES INT,@ANO INT,@FECHAREC CHAR(10),@FECHAFINPERCOB CHAR(10),@nmes int,
        @ramo int,
        @VALOR1 FLOAT,@VALOR2 FLOAT,@pripagban float,
	@estprirec int,
	@periodoinc int, 
        @NITASEG CHAR(16),
        @beneedu int,
        @MSGERROR CHAR(100)  

SELECT @CODOFIA=CONVERT(CHAR(4), @CODOFI)
SELECT @CODERROR=0
SELECT @MSGERROR = 'TRASACCION EXITOSA'
SELECT @VALASEGACT=0
SELECT @CODTIPSEXa=@codtipsex
SELECT @CODERROR=0
SELECT @CODPROA =@CODPRO
select @forliqprima = -1
select @forliqprima = (select indprifijtas from productos where codpro = @codpro)
select @ramo = (select codtipram from productos where codpro = @codpro)
select @beneedu = (select beneedu from productos where codpro = @codpro)
select @pripagban = (select pripagban from productos where codpro = @codpro)
select @estprirec = 6  
if @pripagban = 1
  select @estprirec = 9
select @edadmin = 0
select @edadmax = 9999
select @edadmin = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 1)
select @edadmax = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 2)
SELECT @EDAD = abs(DATEDIFF(dd, @fecnaccli, @fecexpcer))/365.25
if @FECEXPCER > GETDATE()
begin
  select @coderror = 2  
  SELECT @MSGERROR = 'Fecha Expedicion Mayor a la actual'
  goto finalizar
end
if @edad < @edadmin
begin
  select @coderror =3  
  SELECT @MSGERROR = 'Cliente No Alcanza Edad Minima Requerida ' + convert(varchar(19), @edad) + ' ' + convert(varchar(10), @edadmin)
  goto finalizar
end
if @edad > @edadmax
begin
  select @coderror =4
  SELECT @MSGERROR = 'Cliente Supera Edad Maxima Permitida'
  goto finalizar
end
select @adicionedad =0
if @codtipsex = 2
  select @adicionedad = (select medadm from productos where codpro = @codproa)
else
  select @adicionedad = (select medadf from productos where codpro = @codproa)
select @edad = @edad+@adicionedad

SELECT @NITASEG = (SELECT NITCOMSEG FROM PRODUCTOS WHERE CODPRO = @CODPRO)
SELECT @VALASEGACT = @VALASEGACT + (SELECT SUM(OPCION_PRODUCTO.MONOPC) FROM CERTIFICADOS, opcion_producto, PRODUCTOS WHERE ((PRODUCTOS.NITCOMSEG = @NITASEG) AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER <> 3) and (CERTIFICADOS.codpro = PRODUCTOS.codpro AND certificados.codpro = opcion_producto.codpro and certificados.CODTIPOPC = opcion_producto.CODTIPOPC))
if @ramo= 1
  BEGIN
  select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
  SELECT @VALOR1 = @vrhogarinmueble
  SELECT @VALOR2 = @vrhogarcontenido
  END
else
  BEGIN
  select @valasegopcion =(SELECT OPCION_PRODUCTO.MONOPC FROM OPCION_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
  SELECT @VALOR1 = @valasegopcion
  SELECT @VALOR2 = 0
  END
SELECT @VALASEGACT = @VALASEGACT + @valasegopcion
SELECT @LIMMONOPC = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 4)
if @limmonopc = null
  select @limmonopc =999999999
IF @LIMMONOPC < @VALASEGACT
BEGIN
  SELECT @CODERROR = 1
  SELECT @MSGERROR = 'Cliente Supera el Maximo Valor Asegurado Permitido'
  GOTO FINALIZAR
END

/*SELECT @Numcer = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 7)
if @Numcer = null
  select @Numcer = 1
SELECT @CertCli =  (SELECT count(Codcer) FROM CERTIFICADOS, PRODUCTOS WHERE ((PRODUCTOS.NITCOMSEG = @NITASEG) AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER = 1) and (CERTIFICADOS.codpro = PRODUCTOS.codpro))
IF @Numcer < @CertCli
BEGIN
  SELECT @CODERROR = 6
  SELECT @MSGERROR = "Cliente Supera el Maximo Numero de Certificados"
  GOTO FINALIZAR
END*/

if @forliqprima = 0
begin
  if @codtipsex = 1
   begin
     select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
   end
  else
   Begin
     select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
   end
  end
else
BEGIN 
  SELECT @RECARGO= (SELECT PORRECFORPAG FROM fORMAS_pAGO_pRODUCTO WHERE CODPRO = @CODPRO AND CODTIPFORPAG = @CODTIPFORPAG)
  if @recargo = null
    SELECT @RECARGO =0
  select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas > @edad)
    end
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = 0 AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)    end
  if @factor = null
  begin
    SELECT @CODERROR = 5
    SELECT @MSGERROR = 'No Existe Prima Para la Opción Seleccionada'
    GOTO FINALIZAR
  end 
  select @Valpricer= round((@factor * @valasegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)
END
print @valpricer

GRABAR:
select @incentivo = @valpricer * (select porincent from productos where codpro = @codproa)/100
IF NOT EXISTS(SELECT * FROM CLIENTES WHERE CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
  BEGIN
  insert clientes 
       (codtipdocide, numdocide, nomcli, apecli, fecnaccli, dircli, telcli, codciu,
        codtipsex, fecultmod, dirrec, telrec, codtipocu, CODTIPESTCIVIL, NOHIJOS)
        VALUES 
        (@CODTIPDOCIDE, @NUMDOCIDE, @NOMCLI, @APECLI, @FECNACCLI, @DIRCLI, @TELCLI, @CODCIU,
        @CODTIPSEXA, GETDATE(), @dirrec, @telrec, @CODTIPOCUPACION, @CODTIPESTCIVIL, @NOHIJOS)
  END
else
   BEGIN
       Update clientes 
       set codtipdocide = @CODTIPDOCIDE, numdocide = @NUMDOCIDE, nomcli = @NOMCLI, apecli = @APECLI,
       fecnaccli = @FECNACCLI, dircli = @DIRCLI, telcli = @TELCLI, codciu = @CODCIU,
       codtipsex = @CODTIPSEXA, fecultmod = GETDATE(), dirrec = @dirrec, telrec = @telrec, codtipocu = @CODTIPOCUPACION, CODTIPESTCIVIL = @CODTIPESTCIVIL, NOHIJOS = @NOHIJOS
       where  CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE         
   END
IF NOT EXISTS(SELECT * FROM CERTIFICADOS WHERE CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = @CONCER)
 BEGIN
  if @codcer = 0    begin
    select @codcern = (SELECT ContPro FROM Productos WHERE CODPRO = @CODPROA)
    select @codcer=@codcern
    select @codcern= @codcer+1
    update Productos set ContPro = @codcern where  CODPRO = @CODPROA
  end
  INSERT CERTIFICADOS (CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP, NUMDOCIDEEMP, CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE, VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, MONOPC, indcob, VALCONTENIDO, DIRINM, USUARIO)
  VALUES (@CODPROA, @CODCER, @concer, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFIA, @CODTIPDOCIDEEMP, @NUMDOCIDEEMP, 1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE, @VALPRICER, @NUMMET, @fecexpcer, GETDATE(), GETDATE(), @VALOR1, 1, @VALOR2, @DIRREC, @USUARIO)
 END
else
 begin
  SELECT @CODERROR = 6
  SELECT @MSGERROR = 'Certificado ya Existe...'
  GOTO FINALIZAR
 end
select @dia = datepart(dd, @fecexpcer)
select @mes =datepart(mm, @fecexpcer)
select @ano=datepart(yy, @fecexpcer)
if  @dia <=15 
     select @dia = 15
else
   BEGIN
      select @dia= 30
      IF @MES = 2 AND @DIA =30
        SELECT  @DIA = 28
   END  
Select @fecharec = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
SELECT @NMES = @MES+12/@CODTIPFORPAG
IF @NMES > 12 
   BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
   end
else 
   select @mes =@nmes
   IF @MES = 2 AND @DIA =30
     SELECT  @DIA = 28
   select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   IF  @DIA =28
     SELECT @DIA = 30
if not exists(select codpro from recibos where codpro = @CODPROA and codcer = @CODCER and concer =1 AND FECREC >= @FECHAREC)
  begin
   INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPROA, @CODCER, 1, @FECHAREC, @NUMCUE, @VALPRICER, 0, @VALOR1, @FECHAREC,    @fechafinpercob, @CODTIPMEDPAG, @estprirec, @codtipforpag, @numdocide, @fecexpcer, @codofia, GETDATE(), 1)
  end
else
  begin
    SELECT @CODERROR = 7
    SELECT @MSGERROR = 'Existen Recibos Previos para este certificado'
    GOTO FINALIZAR
  end
select @periodoinc = 12/@codtipforpag
while @fecharec <= getdate()
   begin
     select @dia = datepart(dd, @fechafinpercob)
     select @mes =datepart(mm, @fechafinpercob)
     select @ano=datepart(yy, @fechafinpercob)
     if  @dia <=15 
       select @dia = 15
     else
       select @dia= 30
     SELECT @NMES = @MES+12/@CODTIPFORPAG
     IF @NMES > 12 
     BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
     end
     else       select @mes =@nmes
     select @FECHArec = @fechafinpercob
     IF @fecharec > getdate()
        BREAK
     IF @MES = 2 AND @DIA =30
       SELECT  @DIA = 28
     select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
     IF  @DIA =28
       SELECT @DIA = 30
     INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
     fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
     VALUES (@CODPROA, @CODCER, 1, @FECHAREC, @NUMCUE, @VALPRICER, 0, @VALOR1, @FECHAREC, 
     @fechafinpercob, @CODTIPMEDPAG, 6, @codtipforpag, @numdocide, @fecexpcer, @codofia, GETDATE(), 1)
end

FINALIZAR:
if @coderror <>0
begin
 select @codproa = 'xx'
 select @codcer=1
end
SELECT @CODERROR AS CODERROR,
       CERTIFICADOS.CODCER, 
       EMPLEADOS.CODTIPMEDPAG,
       EMPLEADOS.NUMCUE,
       CERTIFICADOS.VALPRICER,
       @INCENTIVO, 
       @VALBANCASEGUROS,
       @VALASEGURADORA,
       @IVA, @EDAD,
       @MSGERROR as msgerror
FROM CERTIFICADOS
LEFT JOIN EMPLEADOS
	ON CERTIFICADOS.CODPRO = @CODPROA 
       AND CERTIFICADOS.CODCER = @CODCER
       AND CERTIFICADOS.CONCER = @CONCER  
       AND CERTIFICADOS.CODTIPDOCIDEEMP = EMPLEADOS.CODTIPDOCIDE 
       AND CERTIFICADOS.NUMDOCIDEEMP = EMPLEADOS.NUMDOCIDE







GO
