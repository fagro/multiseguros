SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBranchType] 
	-- Add the parameters for the stored procedure here
	@Id smallint = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @id = 0 
		Select tr.TiposRamosId, tr.CodTipRam, tr.NomTipRam, tr.FecUltMod
		From dbo.Tipos_Ramos tr
	Else
		Select tr.TiposRamosId, tr.CodTipRam, tr.NomTipRam, tr.FecUltMod
		From dbo.Tipos_Ramos tr
		Where tr.TiposRamosId = @Id

END




GO
