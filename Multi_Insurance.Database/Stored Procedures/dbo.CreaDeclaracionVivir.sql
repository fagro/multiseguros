SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[CreaDeclaracionVivir] 
	@CodPro VarChar(16),
	@CodCer float,
	@ConCer smallint,
	@Peso varchar (50),
	@Estatura varchar (50),
	@DerechoIzquierdo varchar (10),
	@Sector varchar (150),
	@correo varchar (50),
	@Enfermedades varchar (2),
	@Consultar varchar (2),
	@Deficiencia varchar (2),
	@Embarazada varchar (2),
	@ExpliqueEmbarazada varchar (150),
	@Fuma varchar (2),
	@ExpliqueFuma varchar (150),
	@Consume varchar (2),
	@ExpliqueConsume varchar (150),
	@PadecidonoAnterior varchar (2),
	@Diagnostico varchar (150),
	@NombreHospital varchar (150),
	@Fecha varchar (10)
		
as

DECLARE  
  @CodError int,
  @Msgerror varchar(40),
  @Numb INT
  
Select @CodError = 0

Select @Numb =  (Select count(Codcer) from dbo.declaracion where Codpro = @CodPro and Codcer = @Codcer and Concer = @Concer)
if @Numb = 0
   Begin 
      Insert into dbo.declaracionVivir
             (CodPro, CodCer, ConCer, Peso, Estatura, DerechoIzquierdo, Sector, correo, Enfermedades, Consultar, Deficiencia, Embarazada,
				ExpliqueEmbarazada, Fuma, ExpliqueFuma, Consume, ExpliqueConsume, PadecidonoAnterior, Diagnostico, NombreHospital, Fecha, FecUltMod)
      values 
             (@CodPro, @CodCer, @ConCer, @Peso, @Estatura, @DerechoIzquierdo, @Sector, @correo, @Enfermedades, @Consultar,
				@Deficiencia,	@Embarazada, @ExpliqueEmbarazada, @Fuma, @ExpliqueFuma, @Consume, @ExpliqueConsume, @PadecidonoAnterior, @Diagnostico,
				@NombreHospital, @Fecha, GETDATE())

      select @MSGERROR = 'Declaración de Salud Guardada'
   end
else
	BEGIN
		Update	dbo.declaracionVivir 
		Set		Peso =	@Peso,
				Estatura = @Estatura,
				DerechoIzquierdo = @DerechoIzquierdo,
				Sector = @Sector,
				correo = @correo,
				Enfermedades = @Enfermedades,
				Consultar = @Consultar,
				Deficiencia = @Deficiencia,
				Embarazada = @Embarazada,
				ExpliqueEmbarazada = @ExpliqueEmbarazada,
				Fuma = @Fuma,
				ExpliqueFuma = @ExpliqueFuma,
				Consume = @Consume,
				ExpliqueConsume = @ExpliqueConsume,
				PadecidonoAnterior = @PadecidonoAnterior,
				Diagnostico = @Diagnostico,
				NombreHospital = @NombreHospital,
				Fecha = @Fecha,
				FecUltMod = GETDATE() 
		where	CodPro = @CodPro and 
				CodCer = @CodCer and 
				ConCer = @ConCer 
 
		Select @MSGERROR = 'Declaración de Salud Actualizada'
	end	

terminar:
if @coderror <> 0
begin
 Select @Codpro = 'xx'
 Select @Codcer=1
 Select @Concer=1
 Select @Enfermedades = 'N'
end
Select	@CODERROR AS CODERROR,
		CodPro, 
		CodCer,
		@MSGERROR as msgerror
FROM	dbo.declaracion
WHERE	CodPro = @CodPro and 
		CodCer = @CodCer and 
		ConCer = @ConCer







GO
