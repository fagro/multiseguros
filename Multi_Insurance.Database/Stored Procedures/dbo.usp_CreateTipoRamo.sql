SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTipoRamo] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0,
	@Code smallint, 
	@Name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Tipos_Ramos As Target
		Using (Select @id, @Code, @Name) As Source(TiposRamosId, codtipRam, NomTipRam)
			On (Target.TiposRamosId = Source.TiposRamosId)
		When Matched Then
			Update Set CodTipRam = Source.codtipRam,
						NomTipRam = Source.nomtipRam
		When Not Matched Then
			Insert 
			(
				CodTipRam,
				NomTipRam
			)
			VALUES
			(
				Source.codtipRam, -- CodTipRam - smallint
				Source.NomTipRam -- NomTipRam - varchar
			)

	Output $ACTION;
END








GO
