SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPolicyMethodOfPayment] 
	-- Add the parameters for the stored procedure here
	@productId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select tfp.CodTipForPag As MethodOfpaymentOptionCode, 
				tfp.NomTipForPag As MethodOfPaymentOptionName, 
				tfp.DivTipForPag As MethodOfPaymentOptionDivisor,
				tfp.FecUltMod as LastModified
		FROM Tipos_Forma_Pago tfp
			Join dbo.Formas_Pago_Producto fpp
				On fpp.CodTipForPag = tfp.CodTipForPag
		--Where fpp.CodPro = (Select codpro from dbo.Productos p Where p.IdProductos = @productId)
END


GO
