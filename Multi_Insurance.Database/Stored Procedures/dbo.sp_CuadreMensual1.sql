SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_CuadreMensual1 'VC510','04/01/2008','04/17/2008','04/01/2008','04/17/2008',3
CREATE    PROCEDURE [dbo].[sp_CuadreMensual1]
@CodProd		nvarchar(20),
@FecExpCerIni		smalldatetime,
@FecExpCerFin		smalldatetime,
@FecEnvRecIni		smalldatetime,
@FecEnvRecFin	smalldatetime

AS
delete from tmpCuadreMensual1
SELECT DISTINCT Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, 
	Recibos.CodTipForPag, Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, 
	Recibos.CodTipEstRec, Count(Recibos.CodCer) AS CuentaDeCodCer,
	sum(case when Recibos.CodTipEstRec = 8 then -Recibos.ValRec 
	else Recibos.ValRec end) as SumaDeValRec, Sum(Recibos.MonOpc) AS SumaDeMonOpc
into #xxx
FROM   Recibos INNER JOIN Productos ON Recibos.CodPro = Productos.CodPro 
	INNER JOIN Tipos_Forma_Pago ON Recibos.CodTipForPag = Tipos_Forma_Pago.CodTipForPag 
	INNER JOIN Certificados INNER JOIN Tipos_Opcion ON Certificados.CodTipOpc = Tipos_Opcion.CodTipOpc 
	ON Recibos.ConCer = Certificados.ConCer AND Recibos.CodCer = Certificados.CodCer 
	AND Recibos.CodPro = Certificados.CodPro
WHERE 	Recibos.FecExpCer >=  @FecExpCerIni
	And Recibos.FecExpCer <= @FecExpCerFin
	AND Recibos.FecEnvRec >= @FecEnvRecIni
	And Recibos.FecEnvRec <= @FecEnvRecFin
	AND Recibos.CodTipEstRec in (8,9)
	and Recibos.CodPro= @CodProd
GROUP BY Recibos.CodPro, Productos.NomPro, Recibos.FecEnvRec, Recibos.CodTipForPag, 
	Tipos_Forma_Pago.NomTipForPag, Tipos_Opcion.NomTipOpc, Recibos.CodTipEstRec

insert into tmpCuadreMensual1
select * from #xxx 

select * from tmpCuadreMensual1







GO
