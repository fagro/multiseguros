SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Victor Cerda
-- Create date: 30/05/2012
-- Description:	Inserta un nuevo o actualiza un existente usuario.
-- =============================================

CREATE PROCEDURE [dbo].[usp_CreateUpdateUser]	
	@UserId as varchar(15),
	@Username as varchar(40),
	@ProfileCode as varchar(15),
	@UserPassword as varchar(15),
	@LastModifiedDate as datetime	
AS


	--Declare @UserId as varchar(15)
	--Declare @Username as varchar(40)
	--Declare @ProfileCode as varchar(15)
	--Declare @UserPassword as varchar(15)
	--Declare @LastModifiedDate as datetime	
	
	--Set @UserId = '5555'
	--Set @Username = 'Victor Rafael Cerda'
	--Set @ProfileCode = 'Usuarios'
	--Set @UserPassword = '5555'
	--Set @LastModifiedDate = '2012-06-08'
	
BEGIN
	MERGE Usuarios as U
	USING (Select @UserId,@UserName,@ProfileCode,@UserPassword,@LastModifiedDate)
				As Source (CodUsu,NomUsu,CodPer,ClaUsu,FecUltMod) 
			On U.CodUsu = @UserId
	WHEN Matched then
			Update
				Set NomUsu = @UserName,
					CodPer = @ProfileCode,
					ClaUsu = @UserPassword,
					FecUltMod = @LastModifiedDate
	WHEN Not Matched then
		Insert (CodUsu,NomUsu,CodPer,ClaUsu,FecUltMod) 
			Values (@UserId,@UserName,@ProfileCode,@UserPassword,@LastModifiedDate)
	
	OUTPUT $action;					
END







GO
