SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateAdditionalTypeInfo] 
	-- Add the parameters for the stored procedure here
	@id int = 0,
	@code smallint,  
	@name varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Tipos_Info_Adicional As target 
		Using (Select @id, @code, @name) As Source(idTipInfAdi, CodTipInfAdi, NomTipInfAdi) 
			On (Target.idTipInfAdi = Source.idTipInfAdi)
		When Matched Then
			Update Set CodTipInfAdi = Source.CodTipInfAdi,
						NomTipInfAdi = Source.nomTipInfAdi
		When Not Matched Then
			Insert
			(
				CodTipInfAdi,
				NomTipInfAdi,
				FecUltMod
			)
			Values
			(
				Source.CodTipInfAdi, -- CodTipInfAdi - smallint
				Source.NomTipInfAdi, -- NomTipInfAdi - varchar
				GetDate() -- FecUltMod - datetime
			)
	Output $action;	
	
END



GO
