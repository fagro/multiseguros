SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2013/06/20
-- Description:	Genera el reporte para asistencia funeraria de importe al sisme
-- =============================================
CREATE PROCEDURE [dbo].[Usp_rpt_AsistenciaFunerariaImportarSisme] 

	-- Add the parameters for the stored procedure here
	@beginEdit datetime, 
	@endEdit datetime

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

    -- Insert statements for procedure here
--Para extraer los clientes.
SELECT		TOP 50 '2-109-0009834' As No_Poliza
			,c.FecExpCer As [Fecha_Movimiento]   
			,c.FecExpCer  [Fecha_Efectividad]
			,1 AS [Tipo_Movimiento]
			,c.CodCer AS [Codigo_Asegurado]
			,'0' AS [Codigo_Beneficiario]
			,''AS [Parantesco]
			,'5' AS [Codigo_Sucursal]
			,'1' AS [Codigo_Oficina]
			,LTRIM(RTRIM(cli.NomCli)) [Nombre_Asegurado]
			,LTRIM(RTRIM(cli.ApeCli))+' '+LTRIM(RTRIM(COALESCE(cli.Ape2Cli,''))) [Apellido_Asegurado]
			,'' AS [Nombre_Beneficiario]
			,c.NumDocIde [Cedula]
			,0 [Porciento_Beneficiario]
			,cli.FecNacCli [Fecha_Nacimiento]
			,--Determina cuantos meses han pasado desde la ultima vez que se cumplio años
				CASE
 					WHEN
						datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,cli.FecNacCli,SYSDATETIME()),cli.FecNacCli),SYSDATETIME()) >= 6 AND
						datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,cli.FecNacCli,SYSDATETIME()),cli.FecNacCli),SYSDATETIME()) < 12
						THEN 
						(DATEDIFF(DAY,cli.FecNacCli, SYSDATETIME()) / 365) + 1
						
					WHEN 
						datediff(MONTH,dateadd(YEAR,-1, DATEADD(YEAR,DATEDIFF(YEAR,cli.FecNacCli,SYSDATETIME()),cli.FecNacCli)), sysdatetime()) < 12 AND
						datediff(MONTH,dateadd(YEAR,-1, DATEADD(YEAR,DATEDIFF(YEAR,cli.FecNacCli,SYSDATETIME()),cli.FecNacCli)), sysdatetime()) >= 6		
						THEN
						(DATEDIFF(DAY,cli.FecNacCli, SYSDATETIME()) / 365) + 1
					WHEN 
						datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,cli.FecNacCli,SYSDATETIME()),cli.FecNacCli),SYSDATETIME()) = 0
						THEN
						(DATEDIFF(YEAR,cli.FecNacCli, SYSDATETIME()))
						
					ELSE 
						(DATEDIFF(DAY,cli.FecNacCli, SYSDATETIME()) / 365)	
				
				END [Edad]
			, CASE cli.CodTipSex 
					WHEN 1 THEN 'F' 
					WHEN 2 THEN 'M' 
			  END AS [Sexo]
			,c.monOpc AS [Valor_Asegurado]
	
FROM	Certificados AS c
		JOIN Clientes As cli ON c.NumDocIde = cli.NumDocIde
		JOIN BeneficiariosReal As b ON c.CodCer = b.CodCer
		JOIN PARENTESCOS As p ON p.CODPARENT = b.ParentBene
		
--WHERE  
--		c.FecExpCer BETWEEN @beginEdit AND @endEdit
--		AND
--		c.CodPro = '2-109-0009834'
--		AND 
--		c.CodTipEstCer = 1

UNION 

--Para extraer los beneficiarios.
SELECT		TOP 50 '2-109-0009834' As No_Poliza
			,c.FecExpCer As [Fecha_Movimiento]   
			,c.FecExpCer  [Fecha_Efectividad]
			,1 AS [Tipo_Movimiento]
			,c.CodCer AS [Codigo_Asegurado]
			,b.Consecutivo AS [Codigo_Beneficiario]
			,CASE p.CODPARENT 
				WHEN 'Esposo (a)' THEN 'C'
				WHEN 'Madre' THEN 'M'
				WHEN 'Padre' THEN 'P'
				WHEN 'Hijo (a)' THEN 'H'
				WHEN 'Sobrino (a)' THEN 'S'
				WHEN 'Hermano (a)' THEN 'HE'
				ELSE 
					'O'
			END AS [Parantesco]
			,'5' AS [Codigo_Sucursal]
			,'1' AS [Codigo_Oficina]
			,LTRIM(RTRIM(cli.NomCli)) [Nombre_Asegurado]
			,LTRIM(RTRIM(cli.ApeCli))+' '+LTRIM(RTRIM(COALESCE(cli.Ape2Cli,''))) [Apellido_Asegurado]
			,LTRIM(RTRIM(b.NomBene)) AS [Nombre_Beneficiario]
			,c.NumDocIde [Cedula]
			,b.PorBene [Porciento_Beneficiario]
			,NULL AS [Fecha_Nacimiento]
			,0 [Edad]
			,'0' AS [Sexo]
			,c.monOpc AS [Valor_Asegurado]
	
FROM	Certificados AS c
		JOIN BeneficiariosReal As b ON c.CodCer = b.CodCer
		JOIN PARENTESCOS As p ON p.CODPARENT = b.ParentBene
		JOIN Clientes As cli ON c.NumDocIde = cli.NumDocIde
		
--WHERE  
--		c.FecExpCer BETWEEN @beginEdit AND @endEdit
--		AND
--		c.CodPro = '2-109-0009834'
--		AND 
--		c.CodTipEstCer = 1


--Select * From PARENTESCOS			
--Select top 100 * From BeneficiariosReal
END

--Declare @fecha as date = '1958-07-25'

--			Select 
--			CASE
-- 			 WHEN
--					datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,@fecha,SYSDATETIME()),@fecha),SYSDATETIME()) >= 6 AND
--					datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,@fecha,SYSDATETIME()),@fecha),SYSDATETIME()) < 12
--					THEN 
--					(DATEDIFF(DAY,@fecha, SYSDATETIME()) / 365) + 1
					
--				WHEN 
--					datediff(MONTH,dateadd(YEAR,-1, DATEADD(YEAR,DATEDIFF(YEAR,@fecha,SYSDATETIME()),@fecha)), sysdatetime()) < 12 AND
--					datediff(MONTH,dateadd(YEAR,-1, DATEADD(YEAR,DATEDIFF(YEAR,@fecha,SYSDATETIME()),@fecha)), sysdatetime()) >= 6		
--					THEN
--					(DATEDIFF(DAY,@fecha, SYSDATETIME()) / 365) + 1
--				WHEN 
--						datediff(MONTH,DATEADD(YEAR,DATEDIFF(YEAR,@fecha,SYSDATETIME()),@fecha),SYSDATETIME()) = 0
--						THEN
--						(DATEDIFF(YEAR,@fecha, SYSDATETIME()))	
					
--				ELSE 
--					(DATEDIFF(DAY,@fecha, SYSDATETIME()) / 365)	
				
--			 END [Edad]







GO
