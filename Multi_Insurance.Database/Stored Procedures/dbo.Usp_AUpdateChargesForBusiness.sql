SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 24/07/2012
-- Description:	Elimina recibos masivos siempre y cuando no esten pagados.
	
CREATE PROCEDURE [dbo].[Usp_AUpdateChargesForBusiness]

	AS
		BEGIN
		SET NOCOUNT ON;   
		SET XACT_ABORT ON;
		
			UPDATE Certificados SET CodHabilPago = 'X' 
			FROM certificados AS Cer Join salterno.VrecibosUnion  as Rec ON (Cer.ConCer = Rec.ConCer) AND (Cer.CodCer = Rec.CodCer) AND (Cer.CodPro = Rec.CodPro) 
			WHERE (((Cer.CodHabilPago) <> 'X' OR (Cer.CodHabilPago) IS NULL) AND ((Rec.CodTipEstRec)= 9))
		END







GO
