SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-12
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateProductBasicData] 
	-- Add the parameters for the stored procedure here
	@id int = 0,
	@Name varchar(50), 
	@productCode varchar(40),
	@companyId smallint,
	@patrocinador smallint = 1,
	@currency smallint,
	@branchId smallint,
	@representative smallint,
	@typeIndexId smallint,
	@MarketingType smallint = 1,
	@contributive bit,
	@optativeShelter bit,
	@Itbis bit,
	@emit bit,
	@cutDay varchar(15),
	@initSaleDay varchar(10),
	@NewPromotorCommission decimal(12,2),
	@RenewPromotorCommission decimal(12,2),
	@NewBankCommission decimal(12,2),
	@RenewBankCommission decimal(12,2),
	@MenDiscount decimal(12,2),
	@WomenDiscount decimal(12,2),
	@NewSalerIncentive decimal(12,0),
	@RenewSalerIncentive decimal(12,0)

				
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Merge dbo.Productos As Target
		Using (Select @id, @Name, @productCode,@companyId, @patrocinador, @currency, @branchId,@representative,
					  @typeIndexId, @MarketingType, @contributive, @optativeShelter, @Itbis,
					  @emit, @cutDay, @initSaleDay, @NewPromotorCommission, @RenewPromotorCommission,
					  @NewBankCommission, @RenewBankCommission, @MenDiscount, @WomenDiscount, @NewSalerIncentive, @RenewSalerIncentive
				) 
		as Source
				(IdProductos, NomPro, CodPro, NitComSeg, NitPatPro,CodTipMon, CodTipRam,NitGes,
				 CodTipInd, CodTipMer, IndProCon, IndAmpSel, iva, expedir, FecCorPro,FecIniVen, PorComPro, porcompror,
				 honocobn, honocobr, medadm,  medadf, PORINCENT, PORINCENTR
				 )
			On (Target.IdProductos = Source.IdProductos)
		When Matched Then
			Update set NomPro = Source.NomPro,
					   CodPro = Source.CodPro,
					   NitComSeg = Source.NitComSeg,
					   NitPatPro = Source.NitPatPro,
					   CodTipMon = Source.CodTipMon, 
					   CodTipRam = Source.CodTipRam,
					   NitGes = Source.NitGes,	  
					   CodTipInd = Source.CodTipInd, 
					   CodTipMer = Source.CodTipMer, 
					   IndProCon = Source.IndProCon, 
					   IndAmpSel = Source.IndAmpSel, 
					   iva = Source.iva,
					   expedir = Source.expedir, 
					   FecCorPro = Source.FecCorPro, 
					   FecIniVen = Source.FecIniVen, 
					   PorComPro = Source.PorComPro, 
					   porcompror = Source.porcompror, 
					   honocobn = Source.honocobn,	
					   honocobr = Source.honocobr,
					   medadm = Source.medadm,	  
					   medadf = Source.medadf,
					   PORINCENT = Source.PORINCENT,
					   PORINCENTR = Source.PORINCENTR

		When Not Matched Then
			-- Insert statements for procedure here
			INSERT 
			(
				CodPro,
				NomPro,
				CodTipRam,
				NitComSeg,
				NitGes,
				NitPatPro,
				CodTipInd,
				PorIndPro,
				CodTipMon,
				PorComPro,
				porcompror,
				IndProCon,
				IndAmpSel,
				IndPriFijTas,
				IndInfBie,
				FecCorPro,
				FecIniVen,
				FecFinCer,
				FecUltMod,
				IndMts,
				CodTipMer,
				DesPro,
				PriPagBan,
				IVA,
				honocobn,
				honocobr,
				medadm,
				medadf,
				CODPROALTER,
				expedir,
				PORINCENT,
				fechaincentivo,
				codprocesos,
				beneedu,
				beneedu1,
				PORINCENTR,
				ramo,
				plan1,
				poliza,
				CapPrima,
				RangosValSeg,
				ContPro,
				piva
			)
			VALUES
			(
				Source.CodPro, -- CodPro - varchar
				Source.NomPro, -- NomPro - varchar
				Source.CodTipRam , -- CodTipRam - smallint
				Source.NitComSeg, -- NitComSeg - varchar
				Source.NitGes, -- NitGes - varchar
				Source.NitPatPro, -- NitPatPro - varchar
				Source.CodTipInd, -- CodTipInd - smallint
				0.0, -- PorIndPro - float
				Source.CodTipMon, -- CodTipMon - smallint
				Source.PorComPro, -- PorComPro - float
				Source.porcompror, -- porcompror - float
				Source.IndProCon, -- IndProCon - bit
				Source.IndAmpSel, -- IndAmpSel - bit
				0, -- IndPriFijTas - bit
				0, -- IndInfBie - bit
				Source.FecCorPro, -- FecCorPro - varchar
				Source.FecIniVen, -- FecIniVen - datetime
				Null, -- FecFinCer - datetime
				GetDate(), -- FecUltMod - datetime
				0, -- IndMts - bit
				Source.CodTipMer, -- CodTipMer - smallint
				0, -- DesPro - smallint
				0, -- PriPagBan - bit
				Source.iva, -- IVA - bit
				Source.honocobn, -- honocobn - float
				Source.honocobr, -- honocobr - float
				Source.medadm, -- medadm - float
				Source.medadf, -- medadf - float
				0.0, -- CODPROALTER - float
				Source.expedir, -- expedir - bit
				Source.PORINCENT, -- PORINCENT - numeric
				NUll, -- fechaincentivo - datetime
				0, -- codprocesos - int
				0, -- beneedu - int
				0, -- beneedu1 - int
				Source.PORINCENTR, -- PORINCENTR - numeric
				'', -- ramo - varchar
				'', -- plan1 - varchar
				'', -- poliza - varchar
				0, -- CapPrima - bit
				0, -- RangosValSeg - bit
				0.0, -- ContPro - float
				0 -- piva - int
			)


	Output $ACTION;	

END


GO
