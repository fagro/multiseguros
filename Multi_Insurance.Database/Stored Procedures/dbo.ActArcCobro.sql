SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[ActArcCobro]
as
declare 
   @CodProa char(16),
   @FechaPro Char(10),
   @DIA INT,
   @MES INT,
   @ANO INT,
   @FechaPar Char(10)

   SELECT @CodProa ='2-102-495'

   Select @FechaPar = (Select Valor From Parametros Where Parametro = 'FechaPro')
   
   select @dia = datepart(dd, @FechaPar)
   select @mes =datepart(mm, @FechaPar)
   select @ano=datepart(yy, @FechaPar)
   select @FechaPro = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   
   update recibos set FecEnvRec =  @FechaPro where codpro = @codproa and CodTipEstRec<>9 and CodTipEstRec <>7







GO
