SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-16
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateProductOption] 
	-- Add the parameters for the stored procedure here
	@ProductCode varchar(15),
	@planCode smallint,
	@monOpc decimal(12,4),
	@Dependant bit = 0,
	@isVehicle bit = 0,
	@declaration varchar(800)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO Opcion_Producto
    (
        CodPro,
        CodTipOpc,
        MonOpc,
        FecUltMod,
        Dependiente,
        ValidacionVinculado,
		declaracionJurada
	
    )
    VALUES
    (
        @ProductCode, -- CodPro - varchar
        @planCode, -- CodTipOpc - smallint
        @monOpc, -- MonOpc - float
        GETDATE(), -- FecUltMod - datetime
        @Dependant, -- Dependiente - bit
        @isVehicle, -- ValidacionVinculado - bit
		@declaration
    )
	
END







GO
