SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-23
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetTiposMoneda] 
	-- Add the parameters for the stored procedure here
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If	@id = 0
		Select tm.TiposMonedaId, tm.NomTipMon, tm.simbolo, tm.FecUltMod 
		From dbo.Tipos_Moneda tm		
	Else
		Select tm.TiposMonedaId, tm.NomTipMon, tm.simbolo, tm.FecUltMod 
		From dbo.Tipos_Moneda tm
		Where tm.TiposMonedaId = @id
END







GO
