SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_CancelacionPorMotivo '04/01/2008','04/17/2008'
CREATE   procedure [dbo].[sp_CancelacionPorMotivo]
@Fecha1	as smalldatetime,
@Fecha2 as smalldatetime,
@codpro   nvarchar(20)
as

delete from tmpCancelacionxMotivo

insert into tmpCancelacionxMotivo
SELECT c.NomPro, 
Year(FecTerCer) AS [AT], 
Month(FecTerCer) AS MT, 
a.nomtipmotcan AS Motivo,
sum(case when -datediff(month,fectercer,fecexpcer) <1 then 1 else 0 end) as [0],
sum(case when -datediff(month,fectercer,fecexpcer) =1 then 1 else 0 end) as [1],
sum(case when -datediff(month,fectercer,fecexpcer) =2 then 1 else 0 end) as [2],
sum(case when -datediff(month,fectercer,fecexpcer) =3 then 1 else 0 end) as [3],
sum(case when -datediff(month,fectercer,fecexpcer) =4 then 1 else 0 end) as [4],
sum(case when -datediff(month,fectercer,fecexpcer) =5 then 1 else 0 end) as [5],
sum(case when -datediff(month,fectercer,fecexpcer) =6 then 1 else 0 end) as [6],
sum(case when -datediff(month,fectercer,fecexpcer) >6 then 1 else 0 end) as [>6],
Sum(b.ValPriCer) AS Primas, 
Sum(Codtipforpag * valpricer) AS PAnual
FROM Tipos_Motivo_Cancelacion a
RIGHT JOIN Certificados b
ON a.codtipmotcan = b.CODTIPMOTCAN
INNER JOIN Productos c ON b.CodPro = c.CodPro
WHERE (b.CodTipEstCer =3 Or b.CodTipEstCer=6) 
AND (b.FecTerCer >= @Fecha1 And b.FecTerCer < dateadd(day,1,@Fecha2)) and b.codpro=@codpro
GROUP BY c.NomPro, Year([FecTerCer]), Month([FecTerCer]), 
a.nomtipmotcan

select * from tmpCancelacionxMotivo

select convert(smalldatetime,getdate(),112) as fecha







GO
