SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateShelterAndConditions] 
	-- Add the parameters for the stored procedure here
	@productId int,
	@shelterValue money,
	@shelterId smallint,
	@additionalInfoValue decimal(10,2),
	@additionalTypeId smallint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @productCode varchar(40)
	Set @productCode = (Select p.CodPro From dbo.Productos p Where p.IdProductos = @productId)

    -- Insert statements for procedure here
	Insert Into dbo.Info_Adic_Producto
	(
	    CodPro,
	    CodTipInfAdi,
	    ValInfAdi,
	    FecUltMod
	)
	Values
	(
	    @productCode, -- CodPro - varchar
	    @additionalTypeId, -- CodTipInfAdi - smallint
	    @additionalInfoValue, -- ValInfAdi - float
	    GETDATE() -- FecUltMod - datetime
	)
	
	
	Insert dbo.Amparos_Producto
	(
	    CodPro,
	    CodAmp,
	    PorValAmp,
	    IndAmpPri,
	    FecUltMod
	)
	Values
	(
	    @productCode, -- CodPro - varchar
	    @shelterId, -- CodAmp - varchar
	    @shelterValue, -- PorValAmp - float
	    0, -- IndAmpPri - bit
	    GETDATE() -- FecUltMod - datetime
	)
END






GO
