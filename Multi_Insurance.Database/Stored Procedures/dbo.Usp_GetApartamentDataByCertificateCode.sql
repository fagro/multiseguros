SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2012/09/04
-- Description:	Carga el reporte de vivienda Seguro con los datos del certificado
-- =============================================
CREATE PROCEDURE [dbo].[Usp_GetApartamentDataByCertificateCode] 
	-- Add the parameters for the stored procedure here
	@numeroCotizacion int = 0, 
	@codigoProducto varchar(16) =null
AS

--Declare @numeroCotizacion as int
--Declare @codigoProducto as varchar(16)

--Set @numeroCotizacion = 43
--Set @codigoProducto = '2-112-159'

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	RTRIM(LTRIM(cli.NomCli))+' ' +RTRIM(LTRIM(cli.ApeCli)) as Nombre, --+' '+ LTRIM(RTRIM(cli.Ape2Cli)) As Nombre,
			cli.NumDocIde As Cedula,
			cli.TelCli As Telefono,
			ciu.NomCiu As NombreCiudad,
			tmp.NomTipMedPag As MedioDePago,
			c.NumCue As Cuenta,
			cli.DirCli As Direccion,
			vtp.NombreTipoOpcion As [PlanSeleccionado],
			vtp.NombreTipoFormaDePago As FormaDePago,
			c.ValPriCer As ValorCuota,
			c.MonOpc As ValorPrimaAnula,
			co.TotalAsegurado ValorAseguradoTotal,
			c.CodOfi As Oficina,
			c.NumDocIdeEmp As CodigoEmpleado,
			co.ValorEdificio As ValorEdificio,
			co.ValorMobiliario As valorMobiliario,
			co_ciu.NomCiu +', '+co.Sector+', '+co.Calle+' '+co.Casa As DireccionVivienda,
			ces.Nombre As NombreCesionario,
			ces.Valor As ValorCesionario,
			c.FecIniVig As FechaInicioVigencia,
			c.FecFinVig As FechaFinVigencia,
			c.CodCer As Certificados,
			c.codPro As Producto
			
	FROM Certificados As c

			INNER JOIN Clientes As cli
				ON c.NumDocIde = cli.NumDocIde

			INNER JOIN CotizacionVivienda As co
				ON c.CodCer = co.NumeroCotizacion

			INNER JOIN Ciudades As ciu
				ON ciu.CodCiu = cli.CodCiu

			INNER JOIN Tipos_Medio_Pago As tmp
				ON tmp.CodTipMedPag = c.CodTipMedPag

			INNER JOIN View_WOpcionProducto As vtp
				ON vtp.CodigoProducto = c.CodPro

			INNER JOIN ciudades As co_ciu
				ON co_ciu.CodCiu = co.CodigoCiudad

			LEFT JOIN Cesionarios As ces
				ON ces.NumeroCotizacion = co.NumeroCotizacion
					AND ces.CodigoProducto = @codigoProducto

WHERE c.CodPro = @codigoProducto
	AND c.CodCer = @numeroCotizacion
	
END







GO
