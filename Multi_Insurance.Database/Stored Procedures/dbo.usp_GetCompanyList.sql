SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCompanyList]
@id smallint = 0 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	--DECLARE @id smallint = 0

    IF @id = 0 -- Insert statements for procedure here
		SELECT cs.NitComSeg, cs.NomComSeg, cs.DirComSeg,
			   cs.TelComSeg, cs.FaxComSeg, cs.Porciento, 
			   cs.RNC,CONTACTO
		FROM Companias_Seguros cs
	ELSE
		SELECT cs.NitComSeg, cs.NomComSeg, cs.DirComSeg,
			   cs.TelComSeg, cs.FaxComSeg, cs.Porciento, 
			   cs.RNC, cs.CONTACTO
		FROM Companias_Seguros cs
		WHERE cs.NitComSeg = @id
END







GO
