SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Victor Cerda
-- Create date: 2012-08-17
-- Description:	Comisiones Ejecutivos de Ventas
-- =============================================

CREATE PROCEDURE [dbo].[Usp_AExecutiveCommissions]
	@ProductCode as Varchar(15),
	@BeginDate datetime, 
	@EndDate datetime,
	@Executive as varchar(16)	
AS

		--	Parámetros de Pruebas --
		--Declare @ProductCode as Varchar(15)
		--Declare @BeginDate as Datetime
		--Declare @EndDate as Datetime
		--Declare @Executive as varchar(16)	 	
	  
		--Set @ProductCode = '2-102-495'
		--Set @BeginDate = '2012-12-01'
		--Set @EndDate = '2012-12-31'
		--Set @Executive = '0'


BEGIN
		
--------- RECIBOS COBRADOS DE CERTIFICADOS ACTIVOS Y CANCELADOS -------------------------------------

	If object_id('tempdb..#Temp_FinalConcourse') is not null
	Begin
		Truncate Table #Temp_FinalConcourse
		Drop Table #Temp_FinalConcourse
	End;
		
	Select Count(A.CodCer) as CertificadosCobrados, 
		   Sum(A.ValRec) as ValorRecibo,
		   D.CodProm as CodigoEjecutivo,
		   E.ExecutiveName as NombreEjecutivo,			  
		   A.CodOfi as CodigoOficina,
		   D.NomOfi as Oficina		  
	Into #Temp_FinalConcourse		   
	From Recibos A (nolock)
		Inner join Oficinas D
			on A.CodOfi = D.CodOfi
		Left join ExecutiveCommissions E
			on D.CodProm = E.ExecutiveId
	Where A.CodTipEstRec = 9
	and A.FecEnvRec between @BeginDate and @EndDate
	and   (Case when @ProductCode not like  '0'  then A.CodPro else '0' end) = @ProductCode 
	and	  (Case when @Executive not like  '0'  then D.CodProm else '0' end) = @Executive		
	Group by D.CodProm, E.ExecutiveName,A.CodOfi, D.NomOfi
--
	Insert into #Temp_FinalConcourse		   
	Select Count(A.CodCer) as CertificadosCobrados, 
		   Sum(A.ValRec) as ValorRecibo,
		   D.CodProm as CodigoEjecutivo,
		   E.ExecutiveName as NombreEjecutivo,
		   A.CodOfi as CodigoOficina,
		   D.NomOfi as Oficina		   	   
	From RecPolCan A (nolock)
		Inner join Oficinas D
			on A.CodOfi = D.CodOfi
		Left join ExecutiveCommissions E
			on D.CodProm = E.ExecutiveId
	Where A.CodTipEstRec = 9
	and A.FecEnvRec between @BeginDate and @EndDate	
	and   (Case when @ProductCode not like  '0'  then A.CodPro else '0' end) = @ProductCode 
	and	  (Case when @Executive not like  '0'  then D.CodProm else '0' end) = @Executive		
	Group by D.CodProm, E.ExecutiveName,A.CodOfi, D.NomOfi
	
	
	If object_id('tempdb..#Temp_FinalNumbers') is not null
	Begin
		Truncate Table #Temp_FinalNumbers
		Drop Table #Temp_FinalNumbers
	End;
	
	Select Sum(CertificadosCobrados) as CertificadosCobrados, 
		   Sum(ValorRecibo) as ValorRecibo,
		   CodigoEjecutivo,
		   NombreEjecutivo,		     		   
		   CodigoOficina,
		   Oficina		   	   
	Into #Temp_FinalNumbers		   
	From #Temp_FinalConcourse	
	Group by CodigoEjecutivo, NombreEjecutivo,CodigoOficina, Oficina	
	
-------- DEVOLUCION DE PRIMA POR RECLAMOS DURANTE RANGO DE FECHA -----------------------------------

	If object_id('tempdb..#Temp_RecibosDev') is not null
	Begin
		Truncate Table #Temp_RecibosDev
		Drop Table #Temp_RecibosDev
	End;

	Select Count(A.CodCer) as CertificadosConDevolucion, 
		   Sum(A.ValDev) as ValorReciboDev,
		   D.CodProm as CodigoEjecutivo,
		   E.ExecutiveName as NombreEjecutivo,   
		   A.CodOfi as CodigoOficina,
		   D.NomOfi as Oficina
	into #Temp_RecibosDev		   		   	   
	From Devoluciones A (nolock)
		Inner join Oficinas D
			on A.CodOfi = D.CodOfi
		Left join ExecutiveCommissions E
			on D.CodProm = E.ExecutiveId
	Where A.FecDev between @BeginDate and @EndDate	
	and   (Case when @ProductCode not like  '0'  then A.CodPro else '0' end) = @ProductCode 
	and	  (Case when @Executive not like  '0'  then D.CodProm else '0' end) = @Executive		
	Group by D.CodProm, E.ExecutiveName,A.CodOfi, D.NomOfi--, F.NomZona 	
		
	--Variable Interna---------	
	Declare @TaxValue as money;
	Declare @ManagerComissionPercent as money;
	-----------------------------------------------------------
	Set @TaxValue = (Select a.Taxes/100 as TaxValue
						From TaxesConfiguration a
						Where a.EffectiveDate in (Select max(r.EffectiveDate)
													From TaxesConfiguration r						
													Where r.[Status] > 0
													and r.EffectiveDate <= @EndDate));																																			
																								
------------------	CALCULOS --------------------------------------------------------------
		
	If object_id('tempdb..#Temp_Calculos') is not null
	Begin
		Truncate Table #Temp_Calculos
		Drop Table #Temp_Calculos
	End;
	
	Select  Sum(A.CertificadosCobrados) as CertificadosCobrados,			
			Sum(A.ValorRecibo) as PrimaBruta,
			isnull(Round(Sum(A.ValorRecibo) / (1 + @TaxValue),2),0) as PrimaNeta,
			isnull(Round(((Sum(A.ValorRecibo) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as Comision, 
			isnull(Sum(X.ValorReciboDev),0) as PrimaBrutaDev,
			isnull(Round(Sum(X.ValorReciboDev) / (1 + @TaxValue),2),0) as PrimaNetaDev,
			isnull(Round(((Sum(X.ValorReciboDev) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as ValorDevolucionComision, 
			isnull(Round(((Sum(A.ValorRecibo) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) - isnull(Round(((Sum(X.ValorReciboDev) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as MontoAComisionar,
			A.CodigoEjecutivo,			
			A.NombreEjecutivo,			
			A.CodigoOficina,
			A.Oficina		
	Into #Temp_Calculos				
	From #Temp_FinalNumbers A	
	Left join #Temp_RecibosDev X
		on A.CodigoEjecutivo = X.CodigoEjecutivo
		and A.CodigoOficina = X.CodigoOficina 
	Inner join ExecutiveCommissions C
		on A.CodigoEjecutivo = C.ExecutiveId		
	Group by C.CommissionPercentage,A.NombreEjecutivo,A.CodigoEjecutivo,A.CodigoOficina,A.Oficina
	
	Insert into #Temp_Calculos
	Select Sum(CertificadosCobrados) as CertificadosCobrados,
		   Sum(A.PrimaBruta) as PrimaBruta,
		   Sum(A.PrimaNeta) as PrimaNeta,
     	   isnull(Round(((Sum(A.PrimaBruta) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as Comision, 
		   isnull(Sum(X.ValorReciboDev),0) as PrimaBrutaDev,
		   isnull(Round(Sum(X.ValorReciboDev) / (1 + @TaxValue),2),0) as PrimaNetaDev,
		   isnull(Round(((Sum(X.ValorReciboDev) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as ValorDevolucionComision, 
		   isnull(Round(((Sum(A.PrimaBruta) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) - isnull(Round(((Sum(X.ValorReciboDev) / (1 + @TaxValue)) * (C.CommissionPercentage/100)),2),0) as MontoAComisionar,
		   C.ExecutiveId as CodigoEjecutivo,
		   C.ExecutiveName as NombreEjecutivo,			
			'000' as CodigoOficina,
			'Todas' as Oficina		
	From #Temp_Calculos A
	Left join #Temp_RecibosDev X
		on A.CodigoEjecutivo = X.CodigoEjecutivo
		and A.CodigoOficina = X.CodigoOficina						
	Inner join ExecutiveCommissions C
		on C.ExecutivePosition <> 3
		and C.ExecutivePosition is not null
	Group by C.CommissionPercentage,C.ExecutiveId,C.ExecutiveName
	
	Select *
	From #Temp_Calculos
END







GO
