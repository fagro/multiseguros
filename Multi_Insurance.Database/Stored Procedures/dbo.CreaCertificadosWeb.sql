SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[CreaCertificadosWeb]
 @nomCLI char(25),
  @apecli char(25),
  @codtipdocide smallint, 
  @numdocide varchar(16),
  @codtipsex smallint,
  @fecnaccli CHAR(10),
  @codciu int, 
  @nomciu char(25),
  @telcli varchar(10),
  @dircli varchar(60),
  @codtipmedpag smallint,
  @numcue varchar(20),
  @codpro varchar(16),
  @codcer int,
  @concer smallint,
  @codtipopc smallint,
  @codtipforpag smallint,
  @vrhogarinmueble float,
  @vrhogarcontenido float,
  @codtipdocideemp smallint,
  @numdocideemp char(12),
  @codofi char(10),
  @nomofi char(25),
  @dirrec char (60),
  @telrec char (15),
  @Fecexpcer CHAR(10),
  @CODTIPOCUPACION INT,
  @USUARIO CHAR(25),
  @CODTIPESTCIVIL INT,
  @NOHIJOS INT,
  @email char(50),
  @telmovil char(15),
  @FechaInicioVigencia DATETIME,
  @FechaFinVigencia DATETIME,
  @numeroCotizacion as int,
  @valorAseguradoOtrosProductos as FLOAT,
  @nombreCesionario AS VARCHAR(50) = NULL,
  @valorCesionario AS FLOAT = NULL
  
 As
DECLARE @VALPRICER float, @VALPRICERA float,  @VALrenta float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
		@CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
		@VALBANCAsEGUROS INT, @VALAsEGURADORA INT, @IVA INT, @CodError INT,
		@LIMMONOPC float, @VALAsEGACT int, @EDAD INT, @edadmin int, @recargo float,
		@edadmax int, @forliqprima int, @valAsegopcion float,
		@CARACTERES CHAR(16),
		@factor float (10),
		@adicionedad float,
		@codcerc char(6),
		@codcern int, 
		@valpricerc char(10),
		@DIA INT,
		@MES INT,
		@ANO INT,
		@FECHAREC CHAR(10),
		@FECHAFINPERCOB CHAR(10),
		@nmes int,
		@ramo int,
		@VALOR1 FLOAT,
		@VALOR2 FLOAT, @pripagban float,
		@estprirec int,
		@periodoinc int, 
		@NITAsEG CHAR(16),
		@beneedu int,
		@Numcer  int,
		@CertCli int,
		@MsgError CHAR(100) ,
		@FechaInicial datetime,
		@Fechafinal datetime,  
		@FechaExpedicion datetime, 
		@FechaNacimiento datetime, 
		@FechaRecibo datetime
   
Select @FechaExpedicion = CONVERT(DATETIME,@Fecexpcer,101)
Select @FechaNacimiento = CONVERT(DATETIME,@Fecnaccli,101)
Select @CODOFIA = CONVERT(CHAR(4), @CODOFI)
Select @CodError = 0
Select @MsgError = 'TRANSACCION EXITOSA'
Select @VALAsEGACT = 0
Select @CODTIPSEXa = @codtipsex
Select @CodError = 0
Select @CODPROA = @CODPRO

If @NumDocIdeEmp = Null
 Begin
	 Select @CodError = 7
	 Select @MsgError = 'Usuario Desconectado. Por favor vuelva a Ingresar a la Web.'
	 Goto Finalizar
  End

If @NumDocIdeEmp = ''
 Begin
	 Select @CodError = 7
	 Select @MsgError = 'Usuario Desconectado. Por favor vuelva a Ingresar a la Web.'
	 Goto Finalizar
  End

If @codtipforpag = 0  
 Begin
	 Select @CodError = 8
	 Select @MsgError = 'No existe Forma de Pago. Por favor revizar la cuenta y la forma de pago.'
	 Goto Finalizar
  End


If EXISTS (Select * From Empleados Where CODTIPDOCIDE = @CODTIPDOCIDEEMP AND NUMDOCIDE = @NUMDOCIDEEMP)
  Select @CodError = 0
Else
Begin
  Select @CodError = 7
  Select @MsgError = 'El Usuario no esta creado en Empleados'
  Goto Finalizar
End

Select @USUARIO =  CONVERT(CHAR(8),@numdocideemp)
Select @forliqprima = -1
Select @forliqprima = (Select indprIfijtAs From productos Where codpro = @codpro)
Select @ramo = (Select codtipram From productos Where codpro = @codpro)
Select @beneedu = (Select beneedu From productos Where codpro = @codpro)

Select @Pripagban = (Select pripagban From productos Where codpro = @codpro)
Select @estprirec = 6  
If @pripagban = 1
  Select @estprirec = 9
Select @edadmin = 0
Select @edadmax = 9999
Select @edadmin = (Select valinfadi From info_adic_producto Where codpro = @codpro and codtipinfadi = 1)
Select @edadmax = (Select valinfadi From info_adic_producto Where codpro = @codpro and codtipinfadi = 2)
Select @EDAD = abs(DATEDIfF(dd, @FechaNacimiento, @FechaExpedicion))/365.25
If @FechaExpedicion > GETDATE()
Begin
  Select @CodError = 2  
  Select @MsgError = 'Fecha de Expedición Errada'
  Goto Finalizar
End
If @edad < @edadmin
Begin
  Select @CodError =3  
  Select @MsgError = 'Cliente No Alcanza Edad Minima Requerida'
  Goto Finalizar
End
If @edad > @edadmax
Begin
  Select @CodError = 4
  Select @MsgError = 'Cliente Supera Edad Maxima Permitida'
  Goto Finalizar
End
Select @adicionedad = 0
If @codtipsex = 0
  Select @adicionedad = (Select medadm From productos Where codpro = @codproa)
Else
   Select @adicionedad = (Select medadf From productos Where codpro = @codproa)
Select @edad = @edad + @adicionedad
Select @NITAsEG = (Select NITCOMSEG From PRODUCTOS Where CODPRO = @CODPRO)
Select @Numcer = (Select VALINFADI From INFO_ADIC_PRODUCTO Where CODPRO = @CODPROA AND CODTIPINFADI = 7)
If @Numcer = null
  Select @Numcer = 1
Select @CertCli =  (Select count(Codcer) From Certificados Where NUMDOCIDE = @NUMDOCIDE AND CODTIPESTCER = 1 AND CodPro = @codpro)
If @CertCli = null
   Select @CertCli = 0
If @CertCli >= @Numcer
Begin
   Select @CodError = 6
   Select @MsgError = 'Cliente Supera el Maximo Numero de Certificados'
   Goto Finalizar
End
Select @VALAsEGACT = @VALAsEGACT + (Select SUM(OPCION_PRODUCTO.MONOPC) From Certificados, opcion_producto, PRODUCTOS Where ((PRODUCTOS.NITCOMSEG = @NITAsEG) AND NUMDOCIDE = @NUMDOCIDE AND Certificados.CODTIPESTCER <> 3) and (Certificados.codpro = PRODUCTOS.codpro AND Certificados.codpro = opcion_producto.codpro and Certificados.CODTIPOPC = opcion_producto.CODTIPOPC))
If @ramo= 1
  Begin
	Select @valAsegopcion = @vrhogarinmueble + @vrhogarcontenido
	Select @VALOR1 = @vrhogarinmueble
	Select @VALOR2 = @vrhogarcontenido
  End
Else
  Begin
	Select @valAsegopcion = (Select OPCION_PRODUCTO.MONOPC From OPCION_PRODUCTO Where CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
	Select @VALOR1 = @valAsegopcion
	Select @VALOR2 = 0
  End
Select @VALAsEGACT = @VALAsEGACT + @valAsegopcion
Select @LIMMONOPC = 99999999
Select @LIMMONOPC = (Select VALINFADI From INFO_ADIC_PRODUCTO Where CODPRO = @CODPROA AND CODTIPINFADI = 4)
If @limmonopc = null
  Select @limmonopc = 999999999
If @LIMMONOPC < @VALAsEGACT
Begin
  Select @CodError = 1
  Select @MsgError = 'Cliente Supera el Maximo Valor Asegurado Permitido'
  Goto Finalizar
End
If @forliqprima = 0
Begin
 /* If @codtipsex = 1
  Begin
	Select @caracteres = rtrim(@codproa) + 'f'
	Select @VALPRICER = (Select PORPRIAMP From PRIMAs_AMPARO Where CODPRO = @caracteres AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
	If @valpricer = null
	Begin
		Select @VALPRICER = (Select PORPRIAMP From PRIMAs_AMPARO Where CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
	End
  End
  Else*/
	Select @VALPRICER = (Select PORPRIAMP From PRIMAs_AMPARO Where CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahAs >= @edad)
   If @VALPRICER = null
  Begin
	Select @CodError = 5
	 Select @MsgError = 'No existe Prima para esta opción'
	Goto Finalizar
  End 
End
Else
Begin 
  Select @RECARGO = 0
  Select @factor = (Select PORPRIAMP From PRIMAs_AMPARO Where CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahAs > @edad)
   If @factor = null
  Begin
	Select @CodError = 5
	Select @MsgError = 'No existe Prima para esta opción'
	Goto Finalizar
  End 
  Select @valpricer= round((@factor*@valAsegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)
End

GRABAR:
Select @incentivo = @valpricer * (Select porincent From productos Where codpro = @codproa)/100
If NOT EXISTS(Select * From Clientes Where CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
Begin
insert Clientes (
				codtipdocide, numdocide, nomcli, apecli, fecnaccli, dircli, telcli, codciu, 
				 codtipsex, fecultmod, dirrec, telrec, codtipocu, CODTIPESTCIVIL, NOHIJOS, email, telmovil
				 )
		VALUES 
		(
			@CODTIPDOCIDE, @NUMDOCIDE, @NOMCLI, @APECLI, @FechaNacimiento, @DIRCLI,  @TELCLI, @CODCIU,
			@CODTIPSEXA, GETDATE(), @dirrec, @telrec, @CODTIPOCUPACION, @CODTIPESTCIVIL, @NOHIJOS, @email, @telmovil
		)
End
Else
Begin
update Clientes 
	   set codtipdocide = @CODTIPDOCIDE, numdocide = @NUMDOCIDE, nomcli = @NOMCLI, apecli = @APECLI,
		   fecnaccli = @FechaNacimiento, dircli = @DIRCLI, telcli = @TELCLI, codciu = @CODCIU,
		   codtipsex = @CODTIPSEXA, fecultmod = GETDATE(), dirrec = @dirrec, telrec = @telrec, 
		   codtipocu = @CODTIPOCUPACION, CODTIPESTCIVIL = @CODTIPESTCIVIL, NOHIJOS = @NOHIJOS, email = @email, telmovil = @telmovil
Where      CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE         
End

If NOT EXISTS(Select * From Certificados Where CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = @CONCER)
Begin
  If @codcer = 0
   Begin
	Select @codcern = (Select ContPro From Productos Where CODPRO = @CODPROA)
	 Select @codcer=@codcern
	 Select @codcern= @codcer+1
	 update Productos set ContPro = @codcern Where  CODPRO = @CODPROA
	/*Select @CODCERC = (Select VALOR From PARAMETROS Where PARAMETRO = @codpro)
	Select @codcer = convert(int,@codcerc)
	Select @codcern = @codcer+1
	Select @codcerc = convert(char(6), @codcern)
	update parametros set valor = @codcerc Where parametro = @codproa*/
   End
----------------------------------------------------------------------------------------------------------------------------------------------
--Validacion para vehículo.
----------------------------------------------------------------------------------------------------------------------------------------------
   
IF EXISTS (SELECT p.NomPro  FROM Productos p WHERE p.CodPro = @codpro AND p.NomPro like '%Vehículos%') 
	
	BEGIN
		SET @VALPRICER = @valorAseguradoOtrosProductos;
		SET @VALOR1 = (SELECT Valor_Vehiculo FROM CotizacionMasivos.dbo.TbVehVehiculos WHERE NumCotizacion = @numeroCotizacion)
		SET @CODCER = @numeroCotizacion

		If @VALPRICER = 0 OR @VALOR1 <= 0
			BEGIN
				Select @CodError = 10
				Select @MsgError = 'No existe Valor de cuota de pago, favor cotizar vehículo'
				Goto Finalizar
			END
	END

-----------------------------------------------------------------------------------------------------------------------------------------------
--Validacion vivienda
-----------------------------------------------------------------------------------------------------------------------------------------------
			IF  EXISTS (SELECT p.NomPro  FROM Productos p WHERE p.CodPro = @codpro AND p.NomPro like '%vivienda%') 
	
				BEGIN
					SET @VALPRICER = @valorAseguradoOtrosProductos;
					SET @VALOR1 = (SELECT TotalAsegurado FROM CotizacionVivienda WHERE NumeroCotizacion = @numeroCotizacion)
					SET @CODCER = @numeroCotizacion

					If @VALPRICER = 0 OR @VALOR1 <= 0
						BEGIN
							Select @CodError = 10
							Select @MsgError = 'No existe Valor de cuota de pago, favor cotizar vivienda'
							Goto Finalizar
						END
				END	
--------------------------------------------------------------------------------------------------------------------------------------
--Graba el certificado
--------------------------------------------------------------------------------------------------------------------------------------
  INSERT Certificados (CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP, NUMDOCIDEEMP, CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE, VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, MONOPC, indcob, VALCONTENIDO, DIRINM, USUARIO, FecIniVig, FecFinVig)
  VALUES (@CODPROA, @CODCER, @concer, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFIA, @CODTIPDOCIDEEMP, @NUMDOCIDEEMP, 1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE, @VALPRICER, @NUMMET, @FechaExpedicion, GETDATE(), GETDATE(), @VALOR1, 1, @VALOR2, @DIRREC, @USUARIO, @FechaInicioVigencia, @FechaFinVigencia)
  if @@error<>0 
  begin
	 Select @CodError = 9
	 Select @MsgError = 'Error al ingresar Poliza No se ingreso la poliza.'
	 Goto Finalizar
  end
END
 
Select @dia = datepart(dd, @FechaExpedicion)
Select @mes = datepart(mm, @FechaExpedicion)
Select @ano = datepart(yy, @FechaExpedicion)
If  @dia <= 15 
	 Select @dia = 15
   Else
	 Begin
	  Select @dia= 30
	  If @MES = 2 AND @DIA = 30
		Select  @DIA = 28
	End 
Select @fecharec = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))
Select  @FechaRecibo = CONVERT(DATETIME,@fecharec ,101)	
   /*Select @fecharec = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))*/
   /*Select @fecharec = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))*/
Select @NMES = @MES+12/@CODTIPFORPAG
If @NMES > 12 
   Begin
	  Select @MES = @nmes-12
	  Select @ano = @ano+1
   End
Else 
   Select @mes = @nmes
If @MES = 2 AND @DIA = 30
	 Select  @DIA = 28
Select @FECHAFINPERCOB = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))
Select  @FechaFinal = CONVERT(DATETIME,@FECHAFINPERCOB  ,101)
If  @DIA = 28
	 Select @DIA = 30

-------------------------------------------------------------------------------------------------------------------------------------
--Inclusion de cesionarios vivienda segura.
-------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS  (SELECT p.NomPro  FROM Productos p WHERE p.CodPro = @codpro AND  p.NomPro like '%Vivienda%')
	
	BEGIN
	 
		IF @nombreCesionario IS NOT NULL AND LEN(@nombreCesionario) > 0
			BEGIN
				INSERT INTO dbo.Cesionarios
							( Nombre, CodigoCertificado, Valor, CodigoProducto, NumeroCotizacion )
				VALUES		( @nombreCesionario, @numeroCotizacion, @valorCesionario, @codpro, @numeroCotizacion) 
			END		
	END

----------------------------------------------------------------------------------------------------------------------------------------
--Creacion de datos de valor de la prima y valor asegurado Vehículos.
----------------------------------------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT p.NomPro  FROM Productos p WHERE p.CodPro = @codpro AND p.NomPro like '%Vehículo%') 
	
	BEGIN
		SET @VALPRICER = @valorAseguradoOtrosProductos;
		SET @VALOR1 = (SELECT Valor_Vehiculo FROM CotizacionMasivos.dbo.TbVehVehiculos WHERE NumCotizacion = @numeroCotizacion)
		If @VALPRICER = 0 OR @VALOR1 <= 0
			BEGIN
				Select @CodError = 10
				Select @MsgError = 'No existe Valor de cuota de pago, favor cotizar vehículo'
				Goto Finalizar
			END	 
	END
------------------------------------------------------------------------------------------------------------------------------------
--Creacion de datos de valor de la prima y valor asegurado vivienda.
------------------------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT p.NomPro  FROM Productos p WHERE p.CodPro = @codpro AND p.NomPro like '%vivienda%') 
	
	BEGIN

		SET @VALPRICER = @valorAseguradoOtrosProductos;
		SET @VALOR1 = (SELECT TotalAsegurado FROM CotizacionVivienda WHERE NumeroCotizacion = @numeroCotizacion)
		SET @CODCER = @numeroCotizacion

		If @VALPRICER = 0 OR @VALOR1 <= 0
			BEGIN
				Select @CodError = 10
				Select @MsgError = 'No existe Valor de cuota de pago, favor cotizar vivienda'
				Goto Finalizar
			END
	END

-----------------------------------------------------------------------------------------------------------------------------------
--Creacion de Recibos
-----------------------------------------------------------------------------------------------------------------------------------

INSERT Recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPROA, @CODCER, 1, @FechaREcibo, @NUMCUE, @VALPRICER, 0, @VALOR1, @FechaRecibo,
				   @fechafinal, @CODTIPMEDPAG, @estprirec, @codtipforpag, @numdocide, @FechaExpedicion, @codofia, GETDATE(), 1)
   
Finalizar:
If @CodError <> 0
Begin
 Select @codproa = 'xx'
 Select @codcer=1
End
SELECT @CodError As CodError,
	   Certificados.CODCER, 
	   Empleados.CODTIPMEDPAG,
	   Empleados.NUMCUE,
	   Certificados.VALPRICER,
	   @INCENTIVO, 
	   @VALBANCAsEGUROS,
	   @VALAsEGURADORA,
	   @IVA, @EDAD,
	   @MsgError As MsgError
From Certificados
	INNER JOIN Empleados
		ON Certificados.CODPRO = @CODPROA 
		AND Certificados.CODCER = @CODCER 
		AND Certificados.CONCER = @CONCER  
		AND Certificados.CODTIPDOCIDEEMP = Empleados.CODTIPDOCIDE 
		AND Certificados.NUMDOCIDEEMP = Empleados.NUMDOCIDE







GO
