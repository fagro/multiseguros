SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.selren2    Script Date: 24/07/2000 09:57:33 ******/
create procedure [dbo].[selren2]
         @codpro varchar(16),
  @fecini datetime,
  @fecfin datetime
         as
delete cartasbien
insert into cartasbien
SELECT Certificados.CodOfi, Certificados.CodCer, Certificados.NumDocIde,
       Certificados.ValPriCer, Certificados.NumCue, Certificados.FecExpCer,
       datename (dd, FecExpCer) + ' de ' + datename (mm, FecExpCer) + ' de ' + datename (yy, FecExpCer) as FecExpCer1,
       Certificados.FecDigCer, Certificados.CodPro, 
       Certificados.CodTipEstCer, Certificados.FecUltMod,
       datename (dd, Certificados.FecTerCer) + ' de ' + datename (mm, Certificados.FecTerCer) + ' de ' + datename (yy, Certificados.FecTerCer) AS FecTerCer1, 
       Certificados.CodTipOpc
FROM   Certificados
WHERE ((Certificados.CodPro=@codpro) and (Certificados.Fecexpcer<@fecini 
        And datepart(mm,Certificados.Fecexpcer) >= datepart(mm, @fecini)
        And datepart(mm,Certificados.Fecexpcer) <= datepart(mm, @fecfin)
        And datepart(dd,Certificados.Fecexpcer) >= datepart(dd, @fecini)
        And datepart(dd,Certificados.Fecexpcer) <= datepart(dd, @fecfin)
        and Certificados.codtipestcer = 1))
/*ORDER BY Certificados.CodOfi*/
select "finalizado"







GO
