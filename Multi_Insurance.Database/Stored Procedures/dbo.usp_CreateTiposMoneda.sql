SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-23
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTiposMoneda] 
	-- Add the parameters for the stored procedure here
	@id smallint,
	@name varchar(25),
	@symbol varchar(4)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Merge dbo.Tipos_Moneda As Target
		Using (Select @id, @name, @symbol) As Source(TiposMonedaId, NomTipMon, simbolo)
			On (Target.TiposMonedaId = Source.TiposMonedaId)

		When Matched Then
			Update Set NomTipMon = Source.NomTipMon,
					   Simbolo = Source.simbolo

		When Not Matched then
    -- Insert statements for procedure here
			Insert 
			(
				NomTipMon,
				simbolo
			)
			Values
			(
				 -- CodTipMon - smallint
				@name, -- NomTipMon - varchar
				@symbol --Simbolo - varchar
				-- FecUltMod - datetime
			)
	Output $ACTION;

END 


Select tm.	TiposMonedaId, tm.NomTipMon, tm.simbolo, tm.FecUltMod From dbo.Tipos_Moneda tm




GO
