SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.selpersis2    Script Date: 24/07/2000 09:57:33 ******/
create procedure [dbo].[selpersis2]
         @CodPro varchar(16),
  @fecini datetime,
  @fecfin datetime,
  @feccance datetime
         as
SELECT Certificados.CodPro, Certificados.CodOfi, Certificados.CodTipForPag, 
       Count(Certificados.CodCer) AS CuentaDeCodCer, 
       Sum(Certificados.MonOpc) AS SumaDeMonOpc, 
       Sum(ValPriCer*CodTipForPag) AS PRIMAANUAL 
FROM  Certificados /*, Opcion_Producto*/
WHERE (Certificados.CodPro = @CodPro
       and Certificados.FecExpCer>=@fecini
       And Certificados.FecExpCer<=@fecfin
       AND (Certificados.FecTerCer>=@feccance
            Or Certificados.FecTerCer = Null))
/*       and (Certificados.CodTipOpc = Opcion_Producto.CodTipOpc*/
/*       AND Certificados.CodPro = Opcion_Producto.CodPro)*/
GROUP BY Certificados.CodPro, Certificados.CodOfi, Certificados.CodTipForPag







GO
