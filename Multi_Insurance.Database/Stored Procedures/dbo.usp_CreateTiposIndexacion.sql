SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-23
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTiposIndexacion] 
	-- Add the parameters for the stored procedure here
	@name varchar(25),
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Merge dbo.Tipos_Indexacion As Target 
		Using (Select @id, @name) As Source(TipoIndexacionId,NomTipInd )
			On (Target.TipoIndexacionId = Source.TipoIndexacionId)

		When Matched Then
			Update Set NomTipInd = Source.NomTipInd
		When Not Matched Then
			Insert 
			(
				NomTipInd
			)
			Values
			(
				@name -- NomTipInd - varchar
			)
	Output $ACTION;
END





GO
