SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CreaBenefWeb] 
  @CodPro VarChar(16),
  @CodCer float,
  @ConCer smallint,
  @NomBene VarChar(35),
  @ApeBene VarChar(35),
  @PorBene Float,
  @ParentBene Varchar(2)
as
DECLARE  
  @valortotal float, 
  @valoreste float,
  @valornuevo float,
  @CodError int,
  @Msgerror varchar(30),
  @NumB int, 
  @existe int
Select @valortotal = (select sum(convert(int,PorBene)) From Beneficiariosreal where CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer)   
if @valortotal = null
  begin
     set @Numb = 1
  end
else
  begin
      SELECT @Numb =  (select count(Codcer) from BeneficiariosReal where Codpro = @CodPro and Codcer = @Codcer and Concer = @Concer) + 1
  end
Select @valoreste = (select sum(convert(int,PorBene))  From Beneficiariosreal where CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer and NomBene = @NomBene and ApeBene = @ApeBene)  + 0
select @existe = 1
if @valoreste > 0
   begin
      select @valortotal = @valortotal - @valoreste
   end
else
  begin
    select @existe = 0
  end
select @valornuevo = @valortotal + @porbene
select @CodError = 0
select @MSGERROR = 'Beneficiario Actualizado'
if @ValorNuevo > 100
begin
   select @CodError = 1
   select @MSGERROR = 'Supera el 100%'
   goto terminar
end
if @porbene <= 0 and @existe = 1
  begin
  delete BeneficiariosReal where CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer and NomBene = @NomBene and ApeBene = @ApeBene
   select @CodError = 2
   select @MSGERROR = 'Beneficiario Eliminado'
   goto terminar
  end
if @Valoreste > 0
begin
  if @porbene <= 0
    delete BeneficiariosReal where CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer and NomBene = @NomBene and ApeBene = @ApeBene
  else
    Update BeneficiariosReal Set PorBene = @PorBene, ParentBene = @ParentBene where CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer and NomBene = @NomBene and ApeBene = @ApeBene
end
else
begin
  Insert into Beneficiariosreal 
  (CodPro, CodCer, ConCer, Nombene, ApeBene, PorBene, ParentBene,Consecutivo)
  values 
  (@CodPro, @CodCer, @ConCer, LTRIM(RTRIM(@NomBene)), LTRIM(RTRIM(@ApeBene)), @porbene, @ParentBene, @Numb)
end
Terminar:
if @coderror <> 0
begin
 select @Codpro = 'xx'
 select @Codcer=1
 select @Concer=1
 select @NomBene = 'N'
 select @ApeBene = 'A'
end
SELECT @CODERROR AS CODERROR,
       Nombene, 
       ApeBene,
       @MSGERROR as msgerror
       FROM BeneficiariosReal
       WHERE CodPro = @CodPro and CodCer = @CodCer and ConCer = @ConCer and NomBene = @NomBene and ApeBene = @ApeBene







GO
