SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[SpExec_SpEstRecMes]   
AS  
DECLARE   
 @ProducPro varchar(15),  
 @FechaProceso datetime  
  
SELECT   
 @ProducPro = '2-102-495',  
 @FechaProceso = Getdate()  
  
  
EXEC SPEstRecMes    
  @FechaProceso = @FechaProceso,
 @ProducPro = @ProducPro    

/*  
IF @@ERROR <> 0   
begin   
  EXEC MASTER..xp_startmail 'hcastillo@segbanreservas.com','El proceso de Generacion de Estadisticas Se Ejecuto Satisfactoriamente '  
end else   
begin   
  EXEC master..xp_startmail 'hcastillo@segbanreservas.com','El proceso de Generacion de Estadisticas Fallo '  
end  
*/







GO
