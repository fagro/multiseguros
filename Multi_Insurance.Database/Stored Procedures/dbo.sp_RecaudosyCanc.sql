SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_RecaudosyCanc '04/01/2008'
CREATE  procedure [dbo].[sp_RecaudosyCanc] 
@Fecha1 	smalldatetime
as

delete from tmpRecaudosyCanc
--Datos de Cancelados
SELECT Productos.NomPro, Month(FecTerCer) AS MC, Opcion_Producto.MonOpc, 
Count(Certificados.CodCer) AS Cancelados, Sum(Certificados.ValPriCer) AS Primas
into #tmpDatosCancelados
FROM Opcion_Producto 
INNER JOIN Productos 
ON Opcion_Producto.CodPro = Productos.CodPro
INNER JOIN Certificados 
ON Opcion_Producto.CodTipOpc = Certificados.CodTipOpc
AND Opcion_Producto.CodPro = Certificados.CodPro
WHERE (Certificados.CodTipEstCer = 3 Or Certificados.CodTipEstCer = 6) 
AND (Year(FecTerCer)=Year(@Fecha1))
GROUP BY Productos.NomPro, Month(FecTerCer), Opcion_Producto.MonOpc
ORDER BY Month(FecTerCer), Opcion_Producto.MonOpc


--Datos de Pago
SELECT Productos.NomPro, Month(FecEnvRec) AS MC, Opcion_Producto.MonOpc, 
Count(Recibos.CodCer) AS Certificados, Sum(Recibos.ValRec) AS Recaudo
into #tmpDatosPago
FROM Opcion_Producto 
INNER JOIN Productos 
ON Opcion_Producto.CodPro = Productos.CodPro 
LEFT JOIN Certificados 
ON Opcion_Producto.CodTipOpc = Certificados.CodTipOpc 
AND Opcion_Producto.CodPro = Certificados.CodPro 
LEFT JOIN Recibos ON Certificados.ConCer = Recibos.ConCer 
AND Certificados.CodCer = Recibos.CodCer AND Certificados.CodPro = Recibos.CodPro
WHERE Year(FecEnvRec)=Year(@Fecha1) AND Recibos.CodTipEstRec=9
GROUP BY Productos.NomPro, Month(FecEnvRec), Opcion_Producto.MonOpc


--Union Datos de cancelados y Datos de Pago
insert into tmpRecaudosyCanc
SELECT a.NomPro, a.MC, a.MonOpc,a.Certificados AS NoCert, 
a.Recaudo, b.Cancelados, b.Primas
FROM #tmpDatosPago a
LEFT JOIN #tmpDatosCancelados b 
ON (a.MonOpc = b.MonOpc) 
AND (a.MC = b.MC) 
AND (a.NomPro = b.NomPro)

select * from tmpRecaudosyCanc







GO
