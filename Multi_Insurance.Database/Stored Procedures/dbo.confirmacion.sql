SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.confirmacion    Script Date: 11/07/2000 14:29:20 ******/
CREATE procedure [dbo].[confirmacion]
 @codpro numeric, 
@codcer numeric,
 @valpricer numeric
as
declare 
   @codproa char(16)
IF @CODPRO = 1
  SELECT @CODPROA ='200.095-3'
else
  SELECT @CODPROA ='100.000-5/97'
update certificados set codtipestcer = 1 where codpro = @codproa and codcer = @codcer and concer = 1
update recibos set codtipestrec = 9 where codpro = @codproa and codcer = @codcer and concer = 1







GO
