SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.creselESTADISTICA    Script Date: 11/07/2000 14:29:21 ******/
CREATE procedure [dbo].[creselESTADISTICA]
/*       @codpro varchar(16),*/
/*  @fecini datetime*/
/*  @fecfin datetime*/
         as
declare @fechadesde char(10),
 @fechahasta char(10),
        @fecini datetime,
 @dia int,
        @mes int,
        @ano int
select @fecini = getdate()
select @dia = datepart(dd, @fecini)
select @mes = datepart(mm, @fecini)
select @ano = datepart(yy, @fecini)
if @mes = 1
  begin
    select @mes = 12
    select @ano = @ano - 1
  end
else
  select @mes = @mes - 1
select @dia = 31
if @mes = 2
  select @dia = 28
if @mes = 4 or @mes = 6 or @mes = 9 or @mes = 11
  select @dia = 30
select @fechadesde = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),1)) + '/' + ltrim(convert(char(4),@ano))
select @fechahasta = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))
delete estadisticaventas1 where fecha >= @fechadesde and fecha <= @fechahasta
insert into estadisticaventas1
SELECT @fechahasta,
       Certificados.CodPro,   
       Certificados.CodOfi, 
       Count(Certificados.CodCer) AS CuentaDeCodCer,
       Sum(Certificados.MonOpc) AS SumaDeMonOpc 
FROM  Certificados
WHERE (Certificados.FecExpCer>=@FEChadesde
       And Certificados.FecExpCer<=@Fechahasta) 
       /*and codpro <> "0001" and codpro <> "educativo"*/
GROUP BY Certificados.CodPro,
         Certificados.CodOfi
select @dia = datepart(dd, @fecini)
select @mes = datepart(mm, @fecini)
select @ano = datepart(yy, @fecini)
select @dia = 31
if @mes = 2
  select @dia = 28
if @mes = 4 or @mes = 6 or @mes = 9 or @mes = 11
  select @dia = 30
select @fechadesde = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),1)) + '/' + ltrim(convert(char(4),@ano))
select @fechahasta = ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(4),@ano))
/*print @fechadesde*/
/*print @fechahasta*/
delete estadisticaventas1 where fecha >= @fechadesde and fecha <= @fechahasta
insert into estadisticaventas1
SELECT @fechahasta,
       Certificados.CodPro,   
       Certificados.CodOfi, 
       Count(Certificados.CodCer) AS CuentaDeCodCer,
       Sum(Certificados.MonOpc) AS SumaDeMonOpc 
FROM  Certificados
WHERE (Certificados.FecExpCer>=@FEChadesde
       And Certificados.FecExpCer<=@Fechahasta) 
       /*and codpro <> "0001" and codpro <> "educativo"*/
GROUP BY Certificados.CodPro,
         Certificados.CodOfi







GO
