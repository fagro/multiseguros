SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.recibos1    Script Date: 24/07/2000 09:57:33 ******/
CREATE
procedure [dbo].[recibos1] (@codpro char(15), @codcer int, @concer smallint) as
SELECT
recibos.*, Tipos_Estado_recibo.nomtipestrec
FROM recibos, Tipos_Estado_recibo
WHERE codpro = @codpro
AND codcer = @codcer
AND concer = @concer
AND recibos.codtipestrec = Tipos_Estado_recibo.codtipestrec







GO
