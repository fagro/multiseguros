SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-20
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateMeansOfPayment] 
	-- Add the parameters for the stored procedure here
	@productCode varchar(15), 
	@meansOfPaymentCode smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO  Medios_Pago_Producto
	(
	    CodPro,
	    CodTipMedPag,
	    FecUltMod,
	    CodTipDigCheq
	)
	VALUES
	(
	    @productCode, -- CodPro - varchar
	    @meansOfPaymentCode, -- CodTipMedPag - smallint
	    GETDATE(), -- FecUltMod - datetime
	    0 -- CodTipDigCheq - int
	)
END







GO
