SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.reversar    Script Date: 24/07/2000 09:57:33 ******/
create procedure [dbo].[reversar] @CodPro numeric, @CodCer numeric
as 
declare @CodProC char(16)
if @CodPro = 1 
  select @CodProC = '160280-1'
if @CodPro = 2 
  select @CodProC = '100.000-5/98'
update certificados set codtipestcer = 3 where codpro = @CodProC AND CODCER = @CodCer AND CONCER = 1
delete recibos where codpro = @CodProC AND CODCER = @CodCer AND CONCER = 1







GO
