SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino, Salcedo Fagro
-- Create date: 2012/08/14
-- Description:	Graba una cotizacion de vivienda
-- =============================================
CREATE PROCEDURE [dbo].[Usp_SaveSummaryVivienda] 
	
	-- Add the parameters for the stored procedure here
	@valorEdificio money,
	@valorMobiliario money,
	@totalCotizacion money,
	@totalAsegurado money,
	@codciudad as int,
	@sector varchar(120),
	@calle varchar(50),
	@casa varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	--Asigna un numero de cotizacion que asegura la integridad de la misma.
	DECLARE @numeroCotizacion INT = (SELECT COUNT(ID) FROM CotizacionVivienda) + 1;
	

	INSERT INTO CotizacionVivienda
		(numeroCotizacion, ValorEdificio, ValorMobiliario, TotalCotizacion, TotalAsegurado, CodigoCiudad, Sector, Calle, Casa)
	VALUES
		(@numeroCotizacion, @valorEdificio, @valorMobiliario, @totalCotizacion, @totalAsegurado, @codciudad,@sector, @calle, @casa)
	
	RETURN @numeroCotizacion
END







GO
