SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Victor Cerda
-- Create date: 2013-05-22
-- Description:	Estadisticas Ejecutivos de Cuenta
-- =============================================
CREATE PROCEDURE [dbo].[Usp_rpt_ExecutiveStats] 
	-- Add the parameters for the stored procedure here
	@ProductCode varchar(15) = '0',
	@BeginDate datetime, 
	@EndDate datetime,	
	@ZoneCode varchar(10) = '0',
	@OfficeCode varchar(10) = '0',
	@User as varchar(16)
	
AS

--Declare @ProductCode as Varchar(15)
--Declare @BeginDate as Datetime
--Declare @EndDate as Datetime
--Declare @ZoneCode as Varchar(10)
--Declare @OfficeCode as Varchar(15)
--Declare @User as Varchar(16)	

--Set @ProductCode = '2-102-495'
--Set @BeginDate =   '2013-08-31'
--Set @EndDate =     '2013-09-27'
--Set @ZoneCode =    '0'
--Set @OfficeCode =  '0'
--Set @User =        'kbrito'

--Variables Interna
Declare @ExecuteUser as int;
Declare @sql as varchar(1000);

Set @ExecuteUser = (Select ExecutiveId From ExecutiveCommissions Where ExecutiveWindowsUser = @User and ExecutivePosition = 3)

	SET NOCOUNT ON;

	/* SELECT DE TODOS LOS CERTIFICADOS Y SUS RESPECTIVOS FILTROS */
  	If object_id('tempdb..#Temp_Certificados') is not null
	Begin
		Truncate Table #Temp_Certificados
		Drop Table #Temp_Certificados
	End;

	Select 
		   C.NOMZONA,
		   B.CodProm,		   
		   A.CodOfi,
		   B.NomOfi,
		   B.Categoria,
		   A.CodCer,
		   A.FecExpCer,
		   A.FecTerCer,
		   A.CodTipEstCer,
		   A.ValPriCer,
		   CASE WHEN DATEDIFF(d,@BeginDate,@EndDate) < 35 THEN  B.meta ELSE B.meta * (datediff(m,@BeginDate,@EndDate) + 1) END as MetaCertificados,
		   CASE WHEN DATEDIFF(d,@BeginDate,@EndDate) < 35 THEN B.metarecaudo ELSE B.metarecaudo * (datediff(m,@BeginDate,@EndDate) + 1) END as MetaRecaudo		   	   		   		   
	Into #Temp_Certificados
	From Certificados A
	Left Join Oficinas B
		on A.CodOfi = B.CodOfi	
	Left Join ZonasPatPro C
		on B.zona = C.CODZONA					
	Where A.codtipestcer in (1,3)
	and   (Case when @ProductCode not like  '0'  then A.CodPro else '0' end) = @ProductCode 
	and   (Case when @ZoneCode    not like  '0'  then B.Zona   else '0' end) = @ZoneCode
	and   (Case when @OfficeCode  not like  '0'  then A.CodOfi else '0' end) = @OfficeCode	
	
	Declare @WhereFilter  varchar(100)= (case when @ExecuteUser is not null then 'Where B.ExecutiveWindowsUser = ' + CHAR(39) + @User + CHAR(39) else '' end);

	Set @sql =		   
			   'Select A.*,
			           B.ExecutiveName							  
				From #Temp_Certificados A
				Left join Oficinas O
					on A.CodOfi = O.CodOfi
	   			Left join ExecutiveCommissions B
					on O.CodProm = B.ExecutiveId
				<WHEREFILTER>'
				
	set @sql = replace(@sql,'<WHEREFILTER>', @WhereFilter);		
			
	If object_id('tempdb..#Temp_CertificadosFinal') is not null
	Begin
		Truncate Table #Temp_CertificadosFinal
		Drop Table #Temp_CertificadosFinal
	End;

	Create Table #Temp_CertificadosFinal
	(
		   NomZona varchar(50),		   
		   CodProm varchar(5),		   
		   CodOfi varchar(5),
		   NomOfi varchar(50),
		   Categoria varchar(5),
		   CodCer float,
		   FecExpCer datetime,
		   FecTerCer datetime,
		   CodTipEstCer int,
		   ValPriCer float,
		   MetaCertificados float,
		   MetaRecaudo float,
		   ExecutiveName varchar(50)				   
	);

	Insert into #Temp_CertificadosFinal
	Exec (@sql);						
						
	/* CALCULO ESTADISTICAS DE LOS CERTIFICADOS  */ 		
	If object_id('tempdb..#Temp_CertificateStats') is not null
	Begin
		Truncate Table #Temp_CertificateStats
		Drop Table #Temp_CertificateStats
	End;
	
	Select  a.CodOfi,
			a.NomOfi,
			a.NomZona,
			a.ExecutiveName,
			a.Categoria,
			a.MetaCertificados,
		    a.MetaRecaudo,
			sum(Case Codtipestcer when 1 then 1		    else 0 end) as TotalCertificadosActivos,		
			sum(Case Codtipestcer when 1 then ValPriCer else 0 end) as PrimaTotalCertificadosActivos,
			sum(Case When datediff(day, a.FecExpCer, @EndDate) <= 365 and a.CodTipEstCer = 1 then 1           else 0 end) as CertificadosNuevosActivos,         /* Activos con menos de 1 año de ser expedido */
			sum(Case When datediff(day, a.FecExpCer, @EndDate) <= 365 and a.CodTipEstCer = 1 then a.ValPriCer else 0 end) as PrimaCertificadosNuevosActivos,
			sum(Case When datediff(day, a.FecExpCer, @EndDate) >  365 and a.CodTipEstCer = 1 then 1           else 0 end) as CertificadosRenovadosActivos,      /* Activos con mas de 1 año de ser expedido */		
			sum(Case When datediff(day, a.FecExpCer, @EndDate) >  365 and a.CodTipEstCer = 1 then a.ValPriCer else 0 end) as PrimaCertificadosRenovadosActivos,
			sum(Case when CodtipestCer = 3 and a.FecTerCer Between @BeginDate and @EndDate   then 1           else 0 end) as TotalCertCanceladosMes,
			sum(Case when CodtipestCer = 3 and a.FecTerCer Between @BeginDate and @EndDate   then a.ValPriCer else 0 end) as PrimaTotalCertCanceladosMes,
			sum(Case when CodtipestCer = 1 and a.FecExpCer Between @BeginDate and @EndDate   then 1           else 0 end) as CertNuevosMesActivos,
			sum(Case when CodtipestCer = 1 and a.FecExpCer Between @BeginDate and @EndDate   then a.ValPriCer else 0 end) as PrimaCertNuevosMesActivos,
			sum(Case when CodtipestCer = 3 and a.FecExpCer Between @BeginDate and @EndDate   then 1           else 0 end) as CertNuevosMesCancelados,
			sum(Case when CodtipestCer = 3 and a.FecExpCer Between @BeginDate and @EndDate   then a.ValPriCer else 0 end) as PrimaCertNuevosMesCancelados
	Into #Temp_CertificateStats		
	From #Temp_CertificadosFinal a
	Group by a.CodOfi, a.NomOfi, a.NomZona,	a.ExecutiveName, a.Categoria, a.MetaCertificados, a.MetaRecaudo
	
	/* COBROS TOTALES DEL MES */
	If object_id('tempdb..#Temp_CobrosMes') is not null
	Begin
		Truncate Table #Temp_CobrosMes
		Drop Table #Temp_CobrosMes
	End;
		
	Select A.CodOfi as CodigoOficina,
		   Count(distinct B.CodCer) as CertificadosCobrados, 
		   Sum(A.ValRec) as ValorRecibo,
		   Sum(Case when b.FecExpCer between @BeginDate and @EndDate then 1 else 0 end) as RecibosNuevosCobrados,
		   Sum(Case when b.FecExpCer between @BeginDate and @EndDate then a.ValRec else 0 end) as PrimaRecibosNuevosCobrados		   		   		  
	Into #Temp_CobrosMes		   
	From Recibos A (nolock)	
	Inner Join #Temp_Certificados B
		on A.CodCer = B.CodCer	
	Where A.CodTipEstRec = 9
	and A.FecEnvRec between @BeginDate and @EndDate
	Group by A.CodOfi	
	union 		
	Select A.CodOfi as CodigoOficina,
		   Count(distinct B.CodCer) as CertificadosCobrados, 
		   Sum(A.ValRec) as ValorRecibo,
		   Sum(Case when b.FecExpCer between @BeginDate and @EndDate then 1 else 0 end) as RecibosNuevosCobrados,
		   Sum(Case when b.FecExpCer between @BeginDate and @EndDate then a.ValRec else 0 end) as PrimaRecibosNuevosCobrados		   	
	From RecPolCan A (nolock)		
	Inner Join #Temp_Certificados B
		on A.CodCer = B.CodCer
	Where A.CodTipEstRec = 9	  
	and A.FecEnvRec between @BeginDate and @EndDate  
	Group by A.CodOfi



	/* CAMPOS FINALES PARA EL REPORTE */					
	Select  a.CodigoOficina,
			b.NomZona,
			b.NomOfi,
			b.ExecutiveName,
			b.Categoria,
			b.TotalCertificadosActivos,	
			b.PrimaTotalCertificadosActivos,
			b.CertificadosNuevosActivos,        
			b.PrimaCertificadosNuevosActivos,
			b.CertificadosRenovadosActivos,     
			b.PrimaCertificadosRenovadosActivos,
			b.TotalCertCanceladosMes,
			b.PrimaTotalCertCanceladosMes,
			b.CertNuevosMesActivos,
			b.PrimaCertNuevosMesActivos,
			b.CertNuevosMesCancelados,
			b.PrimaCertNuevosMesCancelados,			
			sum(a.CertificadosCobrados) as CertificadosCobrados, 
		    sum(a.ValorRecibo) as RecaudodelMes,
		    sum(a.RecibosNuevosCobrados) as RecibosNuevosCobrados,
		    sum(a.PrimaRecibosNuevosCobrados) as PrimaRecibosNuevosCobrados,
		    b.MetaCertificados,
		    b.MetaRecaudo,
		    round(((b.CertNuevosMesActivos * 100)/b.MetaCertificados),2) as PorcentajeCumplimientoMes
	From #Temp_CobrosMes a
	Inner join #Temp_CertificateStats b
		on a.CodigoOficina = b.CodOfi COLLATE SQL_Latin1_General_CP1_CI_AS
	Group by a.CodigoOficina, b.NomZona, b.NomOfi,b.ExecutiveName, b.Categoria, b.TotalCertificadosActivos, b.PrimaTotalCertificadosActivos, b.CertificadosNuevosActivos, b.PrimaCertificadosNuevosActivos,
	    	 b.CertificadosRenovadosActivos, b.PrimaCertificadosRenovadosActivos, b.TotalCertCanceladosMes,	b.PrimaTotalCertCanceladosMes, b.CertNuevosMesActivos,
	    	 b.PrimaCertNuevosMesActivos, b.CertNuevosMesCancelados, b.PrimaCertNuevosMesCancelados, b.MetaCertificados, b.MetaRecaudo







GO
