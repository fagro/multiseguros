SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[creacionbancaseguroslocamZ]
	@nomCLI char(25),
	 @apecli char(25),
	 @codtipdocide smallint, 
	 @numdocide varchar(16),
	 @codtipsex smallint,
	 @fecnaccli CHAR(10),
	 @codciu int,	
	 @nomciu char(25),
	 @telcli varchar(10),
	 @dircli varchar(40),
	 @codtipmedpag smallint,
	 @numcue varchar(25),
	 @codpro varchar(16),
	 @codcer int,
	 @concer smallint,
	 @codtipopc smallint,
	 @codtipforpag smallint,
	 @vrhogarinmueble float,
	 @vrhogarcontenido float,
	 @codtipdocideemp smallint,
	 @numdocideemp char(12),
	 @codofi char(10),
	 @nomofi char(25),
         @dirrec char (40),
         @telrec char (15),
         @fecexpcer CHAR(10),
         @CODTIPOCUPACION INT,
	 @USUARIO CHAR(25),
         @CODTIPESTCIVIL INT,
         @NOHIJOS INT,
         @REFERIDO CHAR(25) as
DECLARE @VALPRICER float, @VALPRICERA float,  @VALrenta float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
        @CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
        @VALBANCASEGUROS INT, @VALASEGURADORA INT, @IVA INT, @CODERROR INT,
        @LIMMONOPC float, @VALASEGACT int, @EDAD INT, @edadmin int, @recargo float,
        @edadmax int, @forliqprima int, @valasegopcion float, @NumCer int, @CertCli int,
        @CARACTERES CHAR(16),
        @factor float (10),
        @adicionedad float,
        @codcerc char(6),
        @codcern int, 
        @valpricerc char(10),
        @DIA INT,
        @MES INT,
        @ANO INT,
        @FECHAREC CHAR(10),
        @FECHAFINPERCOB CHAR(10),
        @nmes int,
        @ramo int,
        @VALOR1 FLOAT,
        @VALOR2 FLOAT,	@pripagban float,
        @estprirec int,
        @periodoinc int, 
        @NITASEG CHAR(16),
        @beneedu int,
        @MSGERROR CHAR(100)  
SELECT @CODOFIA=CONVERT(CHAR(4), @CODOFI)
SELECT @CODERROR=0
SELECT @MSGERROR = "TRASACCION EXITOSA"
SELECT @VALASEGACT=0
SELECT @CODTIPSEXa=@codtipsex
SELECT @CODERROR=0
SELECT @CODPROA =@CODPRO
select @forliqprima = -1
select @forliqprima = (select indprifijtas from productos where codpro = @codpro)
select @ramo = (select codtipram from productos where codpro = @codpro)
select @beneedu = (select beneedu from productos where codpro = @codpro)
select @pripagban = (select pripagban from productos where codpro = @codpro)
select @estprirec = 6  
if @pripagban = 1
  select @estprirec = 9
select @edadmin = 0
select @edadmax = 9999
select @edadmin = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 1)
select @edadmax = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 2)
SELECT @EDAD = abs(DATEDIFF(dd, @fecnaccli, @fecexpcer))/365.25
select @caracteres = convert(char(16), @edad)
select @caracteres = convert(char(16), @edadmin)
if @FECEXPCER > GETDATE()
begin
  select @coderror =3  
  SELECT @MSGERROR = "Cliente No Alcanza Edad Minima Requerida"
  goto finalizar
end
if @edad < @edadmin
begin
  select @coderror =3  
  SELECT @MSGERROR = "Cliente No Alcanza Edad Minima Requerida " + convert(varchar(19), @edad) + " " + convert(varchar(10), @edadmin)
  goto finalizar
end
if @edad > @edadmax
begin
  select @coderror =4
  SELECT @MSGERROR = "Cliente Supera Edad Maxima Permitida"
  goto finalizar
end
if @beneedu = 1
  BEGIN
  set nocount on
  SELECT  @valpricerA = 0
  DECLARE BENEFICIARIOS CURSOR
  FOR SELECT CURSOACTUALBENE, valrenta FROM BENEEDUCATIVO WHERE CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = @CONCER
  OPEN BENEFICIARIOS
  FETCH BENEFICIARIOS INTO @EDAD, @valrenta
  WHILE @@fETCH_STATUS = 0
  BEGIN
    select @adicionedad =0
    if @codtipsex = 2
       select @adicionedad = (select medadm from productos where codpro = @codproa)
    else
       select @adicionedad = (select medadf from productos where codpro = @codproa)
    select @edad = @edad+@adicionedad
    SELECT @NITASEG = (SELECT NITCOMSEG FROM PRODUCTOS WHERE CODPRO = @CODPRO)
    SELECT @VALASEGACT = @VALASEGACT + (SELECT SUM(OPCION_PRODUCTO.MONOPC) FROM CERTIFICADOS, opcion_producto, PRODUCTOS WHERE ((PRODUCTOS.NITCOMSEG = @NITASEG) AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER <> 3) and (CERTIFICADOS.codpro = PRODUCTOS.codpro AND certificados.codpro = opcion_producto.codpro and certificados.CODTIPOPC = opcion_producto.CODTIPOPC))
    if @ramo= 1
      BEGIN
        select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
        SELECT @VALOR1 = @vrhogarinmueble
        SELECT @VALOR2 = @vrhogarcontenido
      END
    else
      BEGIN
        select @valasegopcion =(SELECT OPCION_PRODUCTO.MONOPC FROM OPCION_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
        SELECT @VALOR1 = @valasegopcion
        SELECT @VALOR2 = 0
      END
    SELECT @VALASEGACT = @VALASEGACT + @valasegopcion
    SELECT @LIMMONOPC = 99999999
    SELECT @LIMMONOPC = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 4)
    if @limmonopc = null
      select @limmonopc =999999999
    IF @LIMMONOPC < @VALASEGACT
    BEGIN
      SELECT @CODERROR = 1
      SELECT @MSGERROR = "Cliente Supera el Maximo Valor Asegurado Permitido"
      GOTO FINALIZAR
    END
    if @forliqprima = 1
    begin
     if @codtipsex = 1
     begin
       select @caracteres = rtrim(@codproa) + 'f'
       select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @caracteres AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
       if @valpricer = null
       begin
         select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
       end
     end
    else
      select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
    END
   else
    BEGIN 
      SELECT @RECARGO= (SELECT PORRECFORPAG FROM fORMAS_pAGO_pRODUCTO WHERE CODPRO = @CODPRO AND CODTIPFORPAG = @CODTIPFORPAG)
      if @recargo = null
        SELECT @RECARGO =0
        select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas > @edad) * @codtipforpag
      if @factor = null
      begin
        select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
      end
      if @factor = null
      begin
        select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = 0 AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
      end
      if @factor = null
      begin
        SELECT @CODERROR = 5
        SELECT @MSGERROR = "No Existe Prima Para la Opción Seleccionada"
        GOTO FINALIZAR
      end 
      /*select @valpricer = round((@factor*@valasegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)*/
      select @valpricer = round((@factor*@valrenta/1000)* (1+@recargo/100)/@codtipforpag, 0)
    END
    SELECT @VALPRICERA = @VALPRICERA + @VALPRICER
    SELECT @CARACTERES = CONVERT(CHAR(16), @VALPRICERA)
    PRINT @CARACTERES
    FETCH BENEFICIARIOS INTO @EDAD, @valrenta
   END
   SELECT @VALPRICER = @VALPRICERA 
   DEALLOCATE BENEFICIARIOS
   set nocount OFF
   GOTO GRABAR
 END
/*  select @edad = (select cursoactualbene from beneeducativo where codpro = @codpro and codcer = @codcer and concer = @concer)*/
select @adicionedad =0
if @codtipsex = 0
  select @adicionedad = (select medadm from productos where codpro = @codproa)
else
   select @adicionedad = (select medadf from productos where codpro = @codproa)
select @edad = @edad+@adicionedad
SELECT @NITASEG = (SELECT NITCOMSEG FROM PRODUCTOS WHERE CODPRO = @CODPRO)
SELECT @VALASEGACT = @VALASEGACT + (SELECT SUM(OPCION_PRODUCTO.MONOPC) FROM CERTIFICADOS, opcion_producto, PRODUCTOS WHERE ((PRODUCTOS.NITCOMSEG = @NITASEG) AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER <> 3) and (CERTIFICADOS.codpro = PRODUCTOS.codpro AND certificados.codpro = opcion_producto.codpro and certificados.CODTIPOPC = opcion_producto.CODTIPOPC))
if @ramo= 1
  BEGIN
  select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
  SELECT @VALOR1 = @vrhogarinmueble
  SELECT @VALOR2 = @vrhogarcontenido
  END
else
  BEGIN
  select @valasegopcion =(SELECT OPCION_PRODUCTO.MONOPC FROM OPCION_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
  SELECT @VALOR1 = @valasegopcion
  SELECT @VALOR2 = 0
  END
SELECT @VALASEGACT = @VALASEGACT + @valasegopcion
SELECT @LIMMONOPC = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 4)
if @limmonopc = null
  select @limmonopc =999999999
IF @LIMMONOPC < @VALASEGACT
BEGIN
  SELECT @CODERROR = 1
  SELECT @MSGERROR = "Cliente Supera el Maximo Valor Asegurado Permitido"
   GOTO FINALIZAR
END
SELECT @Numcer = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 7)
if @Numcer = null
  select @Numcer = 1
SELECT @CertCli =  (SELECT count(Codcer) FROM CERTIFICADOS, PRODUCTOS WHERE ((PRODUCTOS.NITCOMSEG = @NITASEG) AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER = 1) and (CERTIFICADOS.codpro = PRODUCTOS.codpro))
IF @Numcer < @CertCli
BEGIN
  SELECT @CODERROR = 6
  SELECT @MSGERROR = "Cliente Supera el Maximo Numero de Certificados"
   GOTO FINALIZAR
END
if @forliqprima = 0
begin
  /*if @codtipsex = 1
  begin
    select @caracteres = rtrim(@codproa) + 'f'
     select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO =  @caracteres AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
     if @valpricer = null
    begin
         select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
     end
  end
  else*/
     select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
 end
else BEGIN 
  SELECT @RECARGO= (SELECT PORRECFORPAG FROM fORMAS_pAGO_pRODUCTO WHERE CODPRO = @CODPRO AND CODTIPFORPAG = @CODTIPFORPAG)
  if @recargo = null
    SELECT @RECARGO =0
  select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas > @edad)
    end
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = 0 AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)    end
  if @factor = null
  begin
    SELECT @CODERROR = 5
    SELECT @MSGERROR = "No Existe Prima Para la Opción Seleccionada"
    GOTO FINALIZAR
  end 
  select @valpricer= round((@factor*@valasegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)
END
GRABAR:
select @incentivo = @valpricer * (select porincent from productos where codpro = @codproa)/100
IF NOT EXISTS(SELECT * FROM CLIENTES WHERE CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
BEGIN
insert clientes 
       (
        codtipdocide, numdocide, nomcli, apecli,fecnaccli, dircli,telcli, codciu,
        codtipsex, fecultmod, dirrec, telrec, codtipocu, CODTIPESTCIVIL, NOHIJOS
        )
        VALUES 
        (
        @CODTIPDOCIDE, @NUMDOCIDE, @NOMCLI, @APECLI, @FECNACCLI, @DIRCLI,@TELCLI, @CODCIU,
        @CODTIPSEXA, GETDATE(), @dirrec, @telrec, @CODTIPOCUPACION, @CODTIPESTCIVIL, @NOHIJOS)
END
else
BEGIN
update clientes 
        set  codtipdocide = @CODTIPDOCIDE, numdocide = @NUMDOCIDE, nomcli = @NOMCLI, apecli = @APECLI,
        fecnaccli = @FECNACCLI, dircli = @DIRCLI, telcli = @TELCLI, codciu = @CODCIU,
        codtipsex = @CODTIPSEXA, fecultmod = GETDATE(), dirrec = @dirrec, telrec = @telrec, 
        codtipocu = @CODTIPOCUPACION, CODTIPESTCIVIL = @CODTIPESTCIVIL, NOHIJOS = @NOHIJOS
        where  CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE         
END
IF NOT EXISTS(SELECT * FROM CERTIFICADOS WHERE CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = @CONCER)
 BEGIN
  if @codcer = 0    begin
    select @codcern = (SELECT ContPro FROM Productos WHERE CODPRO = @CODPROA)
    select @codcer=@codcern
    select @codcern= @codcer+1
    /*select @codcerc= convert(char(6), @codcern)*/
    update Productos set ContPro = @codcern where  CODPRO = @CODPROA
  end
  INSERT CERTIFICADOS (CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP, NUMDOCIDEEMP, 
  CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE, VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, 
  MONOPC, indcob, VALCONTENIDO, DIRINM, USUARIO, NUMDOCIDEGES, CODTIPDOCIDEGES)
  VALUES (@CODPROA, @CODCER, @concer, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFIA, @CODTIPDOCIDEEMP, @NUMDOCIDEEMP,
  1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE, @VALPRICER, @NUMMET, @fecexpcer, GETDATE(), GETDATE(), 
   @VALOR1, 1, @VALOR2, @DIRREC, @USUARIO,0,@REFERIDO)
 END
else
 begin
  SELECT @CODERROR = 6
  SELECT @MSGERROR = "Certificado ya Existe..."
  GOTO FINALIZAR
 end
/*if not exists(select codpro from recibos where codpro = @CODPROA and codcer = @CODCER and concer =1 AND FECREC >=@FECEXPCER)*/
/*begin*/
   select @dia = datepart(dd, @fecexpcer)
   select @mes =datepart(mm, @fecexpcer)
   select @ano=datepart(yy, @fecexpcer)
   if  @dia <=15 
     select @dia = 15
   else
   BEGIN
      select @dia= 30
      IF @MES = 2 AND @DIA =30
        SELECT  @DIA = 28
   END  
   select @fecharec = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   SELECT @NMES = @MES+12/@CODTIPFORPAG
   IF @NMES > 12 
   BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
   end
   else 
   select @mes =@nmes
   IF @MES = 2 AND @DIA =30
     SELECT  @DIA = 28
   select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   IF  @DIA =28
     SELECT @DIA = 30
if not exists(select codpro from recibos where codpro = @CODPROA and codcer = @CODCER and concer =1 AND FECREC >= @FECHAREC)
begin
   INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPROA, @CODCER, 1, @FECHAREC, @NUMCUE, @VALPRICER, 0, @MONOPC, @FECHAREC,    @fechafinpercob, @CODTIPMEDPAG, @estprirec, @codtipforpag, @numdocide, @fecexpcer, @codofia, GETDATE(), 1)
end
else
begin
    SELECT @CODERROR = 7
    SELECT @MSGERROR = "Existen Recibos Previos para este certificado"
    GOTO FINALIZAR
end
   select @periodoinc = 12/@codtipforpag
   while @fecharec <= getdate()
   begin
     select @dia = datepart(dd, @fechafinpercob)
     select @mes =datepart(mm, @fechafinpercob)
     select @ano=datepart(yy, @fechafinpercob)
     if  @dia <=15 
       select @dia = 15
     else
       select @dia= 30
     SELECT @NMES = @MES+12/@CODTIPFORPAG
     IF @NMES > 12 
     BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
     end
     else       select @mes =@nmes
     select @FECHArec = @fechafinpercob
     IF @fecharec > getdate()
        BREAK
     IF @MES = 2 AND @DIA =30
       SELECT  @DIA = 28
     select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
     IF  @DIA =28
       SELECT @DIA = 30
     INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
     fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
     VALUES (@CODPROA, @CODCER, 1, @FECHAREC, @NUMCUE, @VALPRICER, 0, @MONOPC, @FECHAREC, 
     @fechafinpercob, @CODTIPMEDPAG, 6, @codtipforpag, @numdocide, @fecexpcer, @codofia, GETDATE(), 1)
   end
FINALIZAR:
if @coderror <>0
begin
 select @codproa = 'xx'
 select @codcer=1
end
SELECT @CODERROR AS CODERROR,
       c.CODCER, 
       e.CODTIPMEDPAG,
       e.NUMCUE,
       c.VALPRICER,
       @INCENTIVO, 
       @VALBANCASEGUROS,
       @VALASEGURADORA,
       @IVA, @EDAD,
       @MSGERROR as msgerror
       FROM Certificados c LEFT JOIN EMPLEADOS e
       on c.CODPRO = @CODPROA 
       AND c.CODCER = @CODCER AND c.CONCER = @CONCER  
       AND c.CODTIPDOCIDEEMP = e.CODTIPDOCIDE 
       AND c.NUMDOCIDEEMP = e.NUMDOCIDE







GO
