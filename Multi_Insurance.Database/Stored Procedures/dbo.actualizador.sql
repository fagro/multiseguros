SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.actualizador    Script Date: 11/07/2000 14:29:20 ******/
CREATE procedure [dbo].[actualizador]
  @codpro varchar(16),
         @fecenvrec datetime as
DECLARE  @CODPROa CHAR(16), 
         @codcer float,
         @concer smallint,
         @FECHAREC datetime,
         @fecenvreca datetime,
         @fecultmod datetime
set nocount on
  DECLARE recibos1 CURSOR
  FOR SELECT codpro, codcer, concer, fecrec, fecenvrec, fecultmod FROM recibos 
      where  codpro = @codpro and codtipestrec <> 9 and codtipestrec <> 7
  for update
  OPEN recibos1
  FETCH recibos1 INTO @codproa, @codcer, @concer, @fecharec, @fecenvreca, @fecultmod
  WHILE @@fETCH_STATUS = 0
  BEGIN
    update recibos set fecenvrec = @fecenvrec, fecultmod = @fecenvrec where current of recibos1
    FETCH recibos1 INTO @codproa, @codcer, @concer, @fecharec, @fecenvreca, @fecultmod
  END
  DEALLOCATE recibos1
set nocount off







GO
