SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SP_Actualizaciones] 
  @CodCer float
as

delete actualizaciones where codcer = @CodCer

insert into actualizaciones
SELECT	getdate(), null, codcer, numdocide, codtipestcer, monopc, valpricer, 0 
from	dbo.certificados a
where	codpro = '50-1003-5' and	
		codcer = @CodCer and 	
		not exists ( select * from dbo.Actualizaciones where codcer = a.codcer and numdocide = a.numdocide )







GO
