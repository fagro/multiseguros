SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTiposOpcion] 
	-- Add the parameters for the stored procedure here
	@Name varchar(50),
	@id smallint = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Tipos_Opcion As Target
		Using (Select @name, @id) As Source(nomTipOpc, codTipOpc) 
			On (Target.CodTipOpc = source.codTipOpc)

		When Matched Then
			Update Set nomTipOpc = Source.nomTipOpc
					
		When Not Matched Then
			
			Insert 
			(
				NomTipOpc  
			)
			Values
			(
				@Name -- NomTipOpc - varchar
			)

		Output $action;
End










GO
