SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014/02/11
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateInsuranceCompany] 
	-- Add the parameters for the stored procedure here
	@Id smallint = 0,
	@Name varchar(50), 
	@Phone varchar(10),
	@Address varchar(50),
	@Contact varchar(30),
	@rnc varchar(12),
	@percent smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Merge dbo.Companias_Seguros As target
		Using(Values (@Id, @Name, @Phone, @Address, @Contact, @rnc, @percent)) 
			As Source (NitComSeg, NomComSeg, TelComSeg,DirComSeg, CONTACTO,rnc,Porciento)
		On(target.NitComSeg = Source.NitComSeg)

	When Matched Then
		Update Set NomComSeg = Source.NomComSeg,
					DirComSeg = Source.DirComSeg,
					TelComSeg = Source.TelComSeg,
					CONTACTO = Source.CONTACTO,
					Porciento = Source.Porciento,
					Rnc = Source.Rnc
	When Not Matched Then
		
	Insert 
	(
	    NomComSeg,
	    DirComSeg,
	    TelComSeg,
	    FaxComSeg,
	    CONTACTO,
		Porciento,
		rnc
	)
	VALUES
	(
	    @Name, -- NomComSeg - varchar
	    @Address, -- DirComSeg - varchar
	    @Phone, -- TelComSeg - varchar
		NULL, --FaxComSeg varchar
	    @Contact, -- CONTACTO - varchar
		@percent,
		@rnc
	)
	Output $action;
END







GO
