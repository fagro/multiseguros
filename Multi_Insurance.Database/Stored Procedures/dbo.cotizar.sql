SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   procedure [dbo].[cotizar]
 @codtipsex smallint,
 @fechaNacimiento varchar(10),
 @codpro varchar(16),
 @codtipopc smallint,
 @codtipforpag smallint,
 @vrhogarinmueble float,
 @vrhogarcontenido float
 as
DECLARE @VALPRICER float, @VALPRICERA float,  
        @VALrenta float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
        @CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
        @VALBANCASEGUROS INT, @VALASEGURADORA INT, @IVA INT, @CODERROR INT,
        @LIMMONOPC float, @VALASEGACT int, @EDAD INT, @edadmin int, @recargo float,
        @edadmax int, @forliqprima int, @valasegopcion float,
        @CARACTERES CHAR(16), @msgerror CHAR(100),
        @factor float (10), @adicionedad float, @codcerc char(6), @codcern int, @valpricerc char(10),
        @DIA INT, @MES INT, @ANO INT,
        @FECHAREC CHAR(10),
        @FECHAFINPERCOB CHAR(10),
        @nmes int, @ramo int,
        @VALOR1 FLOAT, @VALOR2 FLOAT, 
        @pripagban float, @estprirec int, @periodoinc int, 
        @NITASEG CHAR(16), @beneedu int ,
        @fecnaccli datetime
select @VALPRICER = 0
SELECT  @fecnaccli = CONVERT(DATETIME, @fechaNacimiento, 101)
SELECT @CODERROR=0
SELECT @MSGERROR = 'Liquidacion Exitosa'
SELECT @VALASEGACT=0
SELECT @CODTIPSEXa=@codtipsex
SELECT @CODERROR=0
SELECT @CODPROA =@CODPRO
select @forliqprima = -1
select @forliqprima = (select indprifijtas from productos where codpro = @codpro)
select @ramo = (select codtipram from productos where codpro = @codpro)
select @beneedu = (select beneedu from productos where codpro = @codpro)
select @pripagban = (select pripagban from productos where codpro = @codpro)
select @edadmin = 0
select @edadmax = 9999
select @edadmin = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 1)
select @edadmax = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 2)
SELECT @EDAD = abs(DATEDIFF(dd, @fecnaccli, GETDATE()))/365.25
if @edad < @edadmin
begin
  select @coderror =3  
  SELECT @MSGERROR = 'Cliente No Alcanza Edad Minima Requerida'
  goto finalizar
end
if @edad > @edadmax
begin
  select @coderror =4
  SELECT @MSGERROR = 'Cliente Supera Edad Maxima Permitida'
  goto finalizar
end
select @adicionedad =0
if @codtipsex = 0
  select @adicionedad = (select medadm from productos where codpro = @codproa)
else
   select @adicionedad = (select medadf from productos where codpro = @codproa)
select @edad = @edad+@adicionedad
SELECT @NITASEG = (SELECT NITCOMSEG FROM PRODUCTOS WHERE CODPRO = @CODPRO)
SELECT @VALASEGACT = 0
if @ramo= 1
  BEGIN
  select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
  SELECT @VALOR1 = @vrhogarinmueble
  SELECT @VALOR2 = @vrhogarcontenido
  END
else
  BEGIN
  select @valasegopcion =(SELECT OPCION_PRODUCTO.MONOPC FROM OPCION_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
  SELECT @VALOR1 = @valasegopcion
  SELECT @VALOR2 = 0
  END
SELECT @VALASEGACT = @VALASEGACT + @valasegopcion
SELECT @LIMMONOPC = 99999999
SELECT @LIMMONOPC = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 4)
if @limmonopc = null
  select @limmonopc =999999999
IF @LIMMONOPC < @VALASEGACT
BEGIN
  SELECT @CODERROR = 1
  GOTO FINALIZAR
END
if @forliqprima = 0
begin
 /* if @codtipsex = 1
  begin
    select @caracteres = rtrim(@codproa) + 'f'
    print @caracteres
    select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @caracteres AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
    if @valpricer = null
    begin
        select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
    end
  end
  else*/
     select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas >= @edad)
  if @VALPRICER = null
  begin
    SELECT @CODERROR = 5
    SELECT @MSGERROR = 'No Existe Prima'
    GOTO FINALIZAR
  end 
    /*select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)*/
end
else
BEGIN 
    select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
    SELECT @VALOR1 = @vrhogarinmueble
    SELECT @VALOR2 = @vrhogarcontenido
  SELECT @RECARGO= (SELECT PORRECFORPAG FROM fORMAS_pAGO_pRODUCTO WHERE CODPRO = @CODPRO AND CODTIPFORPAG = @CODTIPFORPAG)
  if @recargo = null
    SELECT @RECARGO =0
  select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas > @edad)
    end
  select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 12 and edades <= @edad and edahas > @edad)
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG and edades <= @edad and edahas > @edad)
    end
  if @factor = null
    begin
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = 0 AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad) 
   end
  if @factor = null
  begin
    SELECT @CODERROR = 5
    SELECT @MSGERROR = 'No Existe Prima'
    GOTO FINALIZAR
  end 
  select @valpricer= round((@factor*@valasegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)
    
END
FINALIZAR:
if @valpricer = null
BEGIN
   SELECT @valpricer = 0
END
if @coderror <>0
begin
 select @codproa = 'xx'
end
SELECT @CODERROR as error,
       CONVERT(varchar(15),CONVERT(money, @valpricer), 1) as prima,
      @msgerror as Merror,
      convert(varchar(15), @FACTOR) as OPCION







GO
