SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_rechazosres '2-102-495', '08/01/2007','08/31/2007'
CREATE   procedure [dbo].[sp_RechazosRes]
@CodPro 	nvarchar(20),
@FechaDesde	smalldatetime,
@FechaHasta	smalldatetime
as

delete from tmprechazosres

insert into tmpRechazosRes
SELECT Recibos.CodPro, Productos.NomPro, Recibos.CodOfi, 
Oficinas.NomOfi, Recibos.CodTipEstRec, Tipos_Estado_Recibo.NomTipEstRec, 
Sum(Recibos.ValRec) AS Recaudos, Sum(Recibos.MonOpc) AS VrAseg, 
Count(Recibos.CodCer) AS Recibos
FROM ((Recibos INNER JOIN Tipos_Estado_Recibo ON 
Recibos.CodTipEstRec = Tipos_Estado_Recibo.CodTipEstRec) 
INNER JOIN Productos ON Recibos.CodPro = Productos.CodPro) 
INNER JOIN Oficinas ON Recibos.CodOfi = Oficinas.CodOfi
WHERE Recibos.FecEnvRec >= @fechadesde And 
Recibos.FecEnvRec <= @fechahasta
and Recibos.CodPro = @codpro AND 
Recibos.CodTipEstRec <> 9
GROUP BY Recibos.CodPro, Productos.NomPro, Recibos.CodOfi, 
Oficinas.NomOfi, Recibos.CodTipEstRec, Tipos_Estado_Recibo.NomTipEstRec

select * from tmpRechazosRes







GO
