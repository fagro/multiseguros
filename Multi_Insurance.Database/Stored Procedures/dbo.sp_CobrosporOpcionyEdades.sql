SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
--sp_CobrosporOpcionyEdades '04/01/2008','04/17/2008'

CREATE   PROCEDURE [dbo].[sp_CobrosporOpcionyEdades] 
@FechaIni 	smalldatetime,
@FechaFin	smalldatetime
AS

delete from tmpCobrosxOpcion
IF EXISTS (SELECT name FROM sysobjects WHERE  name = N'#xxx' AND type = 'U')
begin
    DROP TABLE #xxx
end
SELECT Certificados.CodPro, 
	Productos.NomPro AS Producto, 
	Opcion_Producto.MonOpc AS Opcion, 
	Recibos.FecEnvRec,
	case when datediff(year,fecnaccli,getdate())< 51 then 1 else 0 end as C1,
	case when datediff(year,fecnaccli,getdate())<51 then valrec else 0 end as R1,
	case when datediff(year,fecnaccli,getdate())<51 then 0 else 1 end as C2,
	case when datediff(year,fecnaccli,getdate())<51 then 0 else valrec end as R2
into #xxx
FROM Certificados LEFT JOIN Opcion_Producto 
	ON Certificados.CodTipOpc = Opcion_Producto.CodTipOpc 
	INNER JOIN Productos 
	ON Certificados.CodPro = Productos.CodPro 
	LEFT JOIN Clientes 
	ON Certificados.NumDocIde = Clientes.NumDocIde 
	INNER JOIN Recibos 
	ON Certificados.ConCer = Recibos.ConCer 
	AND Certificados.CodCer = Recibos.CodCer 
	AND Certificados.CodPro = Recibos.CodPro
WHERE Recibos.CodTipEstRec = 9 and Certificados.CodPro <> 'xx' 
	and Recibos.FecEnvRec >= @FechaIni
	And Recibos.FecEnvRec <= @FechaFin

insert into tmpCobrosxOpcion
select codpro, producto,opcion,FecEnvRec,sum(c1) as C1, sum(R1) as R1, sum(C2) as C2, Sum(R2) as R2
from #xxx
group by codpro,producto,opcion,FecEnvRec
ORDER BY Opcion, FecEnvRec

select * from tmpcobrosxopcion







GO
