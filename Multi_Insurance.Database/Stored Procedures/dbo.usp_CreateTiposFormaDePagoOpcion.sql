SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-02-03
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreateTiposFormaDePagoOpcion] 
	-- Add the parameters for the stored procedure here
	@PaymentMethodCode smallint,
	@Name varchar(50),
	@Div decimal(12,4)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Merge dbo.Tipos_Forma_Pago As Target
		Using (Select @PaymentMethodCode, @Name, @Div) As
		Source (CodTipForPag,  NomTipForPag, DivTipForPag)
			ON (Target.CodTipForPag = Source.CodTipForPag)

		When Matched Then
			Update Set CodTipForPag = Source.CodTipForPag,
					  NomTipForPag = Source.NomTipForPag,
					  DivTipForPag = Source.DivTipForPag
		When Not Matched Then 
			-- Insert statements for procedure here
			Insert
			(
				CodTipForPag,
				NomTipForPag,
				DivTipForPag
			 )
			Values
			(
				@PaymentMethodCode, -- CodTipForPag - smallint
				@Name, -- NomTipForPag - varchar
				@Div -- DivTipForPag - float
			)	
	Output $ACTION;

END







GO
