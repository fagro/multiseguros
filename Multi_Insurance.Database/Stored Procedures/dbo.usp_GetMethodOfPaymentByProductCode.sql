SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMethodOfPaymentByProductCode] 
	-- Add the parameters for the stored procedure here
	@productCode varchar(15) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @productCode Is Not Null 
		Select tfp.CodTipForPag as MethodOfpaymentOptionCode, tfp.NomTipForPag As MethodOfPaymentOptionName, tfp.DivTipForPag As MethodOfPaymentOptionDivisor, tfp.FecUltMod As LastModified
		FROM Tipos_Forma_Pago tfp	
		
	Else
		Select tfp.CodTipForPag As MethodOfpaymentOptionCode, 
				tfp.NomTipForPag As MethodOfPaymentOptionName, 
				tfp.DivTipForPag As MethodOfPaymentOptionDivisor,
				tfp.FecUltMod as LastModified
		FROM Tipos_Forma_Pago tfp	


END



Select * From dbo.Tipos_Forma_Pago tfp





GO
