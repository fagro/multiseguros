SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Victor Cerda
-- Create date: 30/05/2012
-- Description:	Elimina un registro de la tabla pasada como parametro
-- =============================================
CREATE PROCEDURE [dbo].[usp_DeleteAllWhere]
	@Table as sysname,
	@Field as varchar(45),
	@Parameter as varchar(45)
AS

BEGIN
SET NOCOUNT ON;

	Declare @cmd as nvarchar(max)
	Set @cmd = 'Delete '+ @Table + ' Where ' + @Field +' = '''+ @Parameter +''''
	Select @@ROWCOUNT
	exec sp_executesql @cmd

END







GO
