SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  Stored Procedure dbo.creacionbancaseguroslocab    Script Date: 11/07/2000 14:29:20 ******/
CREATE procedure [dbo].[creacionbancaseguroslocab] 	
	@nomCLI char(25),
	 @apecli char(25),
	 @codtipdocide smallint, 
	 @numdocide varchar(16),
	 @codtipsex smallint,
	 @fecnaccli CHAR(10),
	 @codciu int,	
	 @nomciu char(25),
	 @telcli varchar(10),
	 @dircli varchar(40),
	 @codtipmedpag smallint,
	 @numcue varchar(9),
	 @codpro varchar(16),
	 @codcer int,
	 @concer smallint,
	 @codtipopc smallint,
	 @codtipforpag smallint,
	 @vrhogarinmueble float,
	 @vrhogarcontenido float,
	 @codtipdocideemp smallint,
	 @numdocideemp char(12),
	 @codofi char(10),
	 @nomofi char(25),
         @dirrec char (40),
         @telrec char (15) as
DECLARE @VALPRICER float, @NUMMET INT, @MONOPC float, @codtipsexa int, 
        @CODPROA CHAR(16), @CODOFIA CHAR(4), @INCENTIVO INT,
        @VALBANCASEGUROS INT, @VALASEGURADORA INT, @IVA INT, @CODERROR INT,
        @LIMMONOPC float, @VALASEGACT int, @EDAD INT, @edadmin int, @recargo float,
        @edadmax int, @forliqprima int, @valasegopcion float,
        @CARACTERES CHAR(16),
        @factor float (10),
        @adicionedad float,
        @codcerc char(6),
        @codcern int, 
        @valpricerc char(10),
        @DIA INT,
        @MES INT,
        @ANO INT,
        @FECHAREC CHAR(10),
        @FECHAFINPERCOB CHAR(10),
        @nmes int,
        @ramo int,
        @VALOR1 FLOAT,
        @VALOR2 FLOAT  
SELECT @CODOFIA=CONVERT(CHAR(4), @CODOFI)
SELECT @CODERROR=0
SELECT @VALASEGACT=0
SELECT @CODTIPSEXa=@codtipsex
IF EXISTS (SELECT * FROM EMPLEADOS WHERE CODTIPDOCIDE = @CODTIPDOCIDEEMP AND NUMDOCIDE = @NUMDOCIDEEMP)
  SELECT @CODERROR=0
ELSE
BEGIN
  SELECT @CODERROR = 2
  GOTO FINALIZAR
END
SELECT @CODPROA =@CODPRO
select @forliqprima = -1
select @forliqprima = (select indprifijtas from productos where codpro = @codpro)
select @ramo = (select codtipram from productos where codpro = @codpro)
select @edadmin = 0
select @edadmax = 9999
select @edadmin = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 1)
select @edadmax = (select valinfadi from info_adic_producto where codpro = @codpro and codtipinfadi = 2)
SELECT @EDAD = abs(DATEDIFF(yy, @fecnaccli, getdate()))
if @edad < @edadmin
begin
  select @coderror =3  
  goto finalizar
end
if @edad > @edadmax
begin
  select @coderror =4
  goto finalizar
end
select @adicionedad =0
if @codtipsex = 2
  select @adicionedad = (select medadm from productos where codpro = @codproa)
else
   select @adicionedad = (select medadf from productos where codpro = @codproa)
select @edad = @edad+@adicionedad
SELECT @VALASEGACT = @VALASEGACT + (SELECT SUM(OPCION_PRODUCTO.MONOPC) FROM CERTIFICADOS, OPCION_PRODUCTO WHERE CERTIFICADOS.CODPRO = @CODPROA AND NUMDOCIDE =@NUMDOCIDE AND CERTIFICADOS.CODTIPESTCER <> 3 AND CERTIFICADOS.CODPRO = OPCION_PRODUCTO.CODPRO AND CERTIFICADOS.CODTIPOPC = OPCION_PRODUCTO.CODTIPOPC GROUP BY CERTIFICADOS.CODPRO,CERTIFICADOS.NUMDOCIDE)
if @ramo= 1
  BEGIN
  select @valasegopcion = @vrhogarinmueble + @vrhogarcontenido
  SELECT @VALOR1 = @vrhogarinmueble
  SELECT @VALOR2 = @vrhogarcontenido
  END
else
  BEGIN
  select @valasegopcion =(SELECT OPCION_PRODUCTO.MONOPC FROM OPCION_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC)
  SELECT @VALOR1 = @valasegopcion
  SELECT @VALOR2 = 0
  END
SELECT @VALASEGACT = @VALASEGACT + @valasegopcion
SELECT @LIMMONOPC = 99999999
SELECT @LIMMONOPC = (SELECT VALINFADI FROM INFO_ADIC_PRODUCTO WHERE CODPRO = @CODPROA AND CODTIPINFADI = 4)
if @limmonopc = null
  select @limmonopc =999999999
IF @LIMMONOPC < @VALASEGACT
BEGIN
  SELECT @CODERROR = 1
  GOTO FINALIZAR
END
if @forliqprima = 1
begin
  if @codtipsex = 1
  begin
    select @caracteres = rtrim(@codproa) + 'f'
    select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @caracteres AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
    if @valpricer = null
    begin
         select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
    end
  end
  else
    select @VALPRICER = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = @CODTIPFORPAG)
end
else
BEGIN 
  SELECT @RECARGO= (SELECT PORRECFORPAG FROM fORMAS_pAGO_pRODUCTO WHERE CODPRO = @CODPRO AND CODTIPFORPAG = @CODTIPFORPAG)
  if @recargo = null
    SELECT @RECARGO =0
  select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = @CODTIPOPC AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
  if @factor = null
    begin
/*    select @caracteres =convert(char(16), @codtipopc)*/
/*    print @caracteres*/
    select @factor = (SELECT PORPRIAMP FROM PRIMAS_AMPARO WHERE CODPRO = @CODPROA AND CODTIPOPC = 0 AND CODTIPFORPAG = 1 and edades <= @edad and edahas > @edad)
    end
  if @factor = null
  begin
    SELECT @CODERROR = 5
    GOTO FINALIZAR
  end 
  select @valpricer= round((@factor*@valasegopcion/1000)* (1+@recargo/100)/@codtipforpag, 0)
END
select @incentivo = @valpricer * (select porincent from productos where codpro = @codproa)/100
IF NOT EXISTS(SELECT * FROM CLIENTES WHERE CODTIPDOCIDE = @CODTIPDOCIDE AND NUMDOCIDE = @NUMDOCIDE)
BEGIN
insert clientes 
       (
        codtipdocide, numdocide, 
        nomcli, apecli,
        fecnaccli, dircli,
        telcli, codciu,
        codtipsex, fecultmod, dirrec, telrec
        )
        VALUES 
        (
        @CODTIPDOCIDE, @NUMDOCIDE, 
        @NOMCLI, @APECLI,
        @FECNACCLI, @DIRCLI,
        @TELCLI, @CODCIU,
        @CODTIPSEXA, GETDATE(), @dirrec, @telrec)
END
IF NOT EXISTS(SELECT * FROM CERTIFICADOS WHERE CODPRO = @CODPROA AND CODCER = @CODCER AND CONCER = @CONCER)
BEGIN
  if @codcer = 0
    begin
    select @CODCERC = (SELECT VALOR FROM PARAMETROS WHERE PARAMETRO = @codpro)
    select @codcer=convert(int,@codcerc)
    select @codcern= @codcer+1
    select @codcerc= convert(char(6), @codcern)
    update parametros set valor = @codcerc where parametro = @codpro
  end
  INSERT CERTIFICADOS (CODPRO, CODCER, CONCER, CODTIPDOCIDE, NUMDOCIDE, CODOFI, CODTIPDOCIDEEMP, NUMDOCIDEEMP, CODTIPESTCER, CODTIPFORPAG, CODTIPMEDPAG, CODTIPOPC, NUMCUE, VALPRICER, NUMMET, FECEXPCER, FECULTMOD, FECDIGCER, MONOPC, indcob, VALCONTENIDO, DIRINM)
  VALUES (@CODPROA, @CODCER, @concer, @CODTIPDOCIDE, @NUMDOCIDE, @CODOFIA, @CODTIPDOCIDEEMP, @NUMDOCIDEEMP, 1, @CODTIPFORPAG, @CODTIPMEDPAG, @CODTIPOPC, @NUMCUE, @VALPRICER, @NUMMET, GETDATE(), GETDATE(), GETDATE(), @VALOR1, 1, @VALOR2, @DIRREC)
END
if not exists(select codpro from recibos where codpro = @CODPROA and codcer = @CODCER and concer =1)
begin
   select @dia = datepart(dd, getdate())
   select @mes =datepart(mm, getdate())
   select @ano=datepart(yy, getdate())
   if  @dia <=15 
     select @dia = 15
   else
     select @dia= 30
   select @fecharec = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   SELECT @NMES = @MES+12/@CODTIPFORPAG
   IF @NMES > 12 
   BEGIN
      SELECT @MES = @nmes-12
      select @ano = @ano+1
   end
   else 
     select @mes =@nmes
   select @FECHAFINPERCOB = ltrim(convert(char(2),@dia)) + '/' + ltrim(convert(char(2),@mes)) + '/' + ltrim(convert(char(4),@ano))
   INSERT recibos (CODPRO, CODCER, CONCER, fecrec, NUMCUE, VALREC, NUMRECENV, MONOPC, FECINIPERCOB,
   fecfinpercob, CODTIPMEDPAG, CODTIPESTREC, CODTIPFORPAG, FECENVREC, NUMDOCIDE, FECEXPCER, CODOFI, FECULTMOD, banenv)
   VALUES (@CODPROA, @CODCER, 1, @FECHAREC, @NUMCUE, @VALPRICER, 0, @MONOPC, @FECHAREC, 
   @fechafinpercob, @CODTIPMEDPAG, 9, @codtipforpag, getdate(), @numdocide, GETDATE(), @codofia, GETDATE(), 1)
 end
FINALIZAR:
if @coderror <>0
begin
 select @codproa = 'xx'
 select @codcer=1
end
SELECT @CODERROR,
       c.CODCER, 
       e.CODTIPMEDPAG,
       e.NUMCUE,
       c.VALPRICER,
       @INCENTIVO, 
       @VALBANCASEGUROS,
       @VALASEGURADORA,
       @IVA
       FROM CERTIFICADOS c LEFT JOIN EMPLEADOS e
       on c.CodPro = @CODPROA 
       AND c.CODCER = @CODCER  
       AND c.CODTIPDOCIDEEMP = e.CODTIPDOCIDE 
       AND c.NUMDOCIDEEMP = e.NUMDOCIDE







GO
