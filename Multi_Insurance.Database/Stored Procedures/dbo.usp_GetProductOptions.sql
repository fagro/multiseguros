SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 2014-03-29
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetProductOptions] 
	-- Add the parameters for the stored procedure here
	@productCode varchar(15) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT op.CodTipOpc AS planCode, op.MonOpc AS PremiumValue 
	FROM Opcion_Producto op
	WHERE op.CodPro IS NOT NULL AND (op.CodPro = @productCode OR @productCode IS NULL)
END







GO
