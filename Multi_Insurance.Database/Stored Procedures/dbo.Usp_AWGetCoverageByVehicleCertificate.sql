SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Vizcaino Salcedo, Fagro
-- Create date: 24/07/2012
-- Description:	Obtiene todas las coberturas del vehiculo por código de certificado.
-- =============================================
CREATE PROCEDURE [dbo].[Usp_AWGetCoverageByVehicleCertificate]

@numeroCotizacion AS INT

AS
	
	BEGIN
		--DECLARE @numeroCotizacion AS INT
		--SET @numeroCotizacion = 208548
		SET NOCOUNT ON;

			SELECT  DISTINCT 
						CASE 
							WHEN tc.Codigo_Cobertura = 554
								THEN 
									'Aero Ambulancia - 8 pasajeros max'
								ELSE
									tc.Descripcion           
						END	AS DescripcionCobertura, 

						CASE 
							WHEN tc.Codigo_Cobertura IN (501,554,346,347,562,544)
								THEN 
									0
								ELSE
									cv.Suma_Asegurada 
                                
						END	As ValorLimite,
					 cv.Porciento_Coaseguro As PorcientoCoaseguro,

					 cv.Valor_Deducible As ValorDeducible,

					 tc.Orden, 

					tc.Descripcion_Deducible AS DescripcionDeducible
			
			FROM CotizacionMasivos.dbo.TbLeyCoberturas_Vehiculo As cv
					
					INNER JOIN CotizacionMasivos.dbo.TbVehCobertura As tc
						ON cv.Codigo_Cobertura = tc.Codigo_Cobertura

					INNER JOIN CotizacionMasivos.dbo.TbVehVehiculos as vv
						ON vv.NumCotizacion = @numeroCotizacion

					LEFT JOIN CotizacionMasivos.dbo.TbVehCobertura_Coaseguro As cc
						ON cc.CodigoPlan = vv.Codigo_Plan

			WHERE cv.NumCotizacion = @numeroCotizacion
			ORDER BY tc.Orden

	END







GO
