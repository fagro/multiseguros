SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fn_Prima_Cot_Aseg]  
(@codpro varchar(15), @Edad int, @Pcargada int, @Cobertura int)
RETURNS int
AS
BEGIN
   Declare @PCot int
   set @PCot = (select isNull(PCotizada,0) as PCotizada from Aseg_Param 
   where (codpro=@codpro and EdadMin <= @Edad and EdadMax >= @Edad) and 
   pcargada = @pcargada and cobertura = @cobertura)
   RETURN(@PCot)
END







GO
