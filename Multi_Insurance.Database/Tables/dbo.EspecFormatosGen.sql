CREATE TABLE [dbo].[EspecFormatosGen]
(
[CodFormato] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrdenFormato] [int] NOT NULL,
[CampoFormato] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongitudFormato] [int] NULL,
[AlineacionFormato] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RellenoFormato] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormatoFormato] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DecimalesFormato] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EspecFormatosGen] ADD CONSTRAINT [PK___3__12] PRIMARY KEY CLUSTERED  ([CodFormato], [OrdenFormato]) ON [PRIMARY]
GO
