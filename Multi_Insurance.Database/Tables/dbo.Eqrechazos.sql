CREATE TABLE [dbo].[Eqrechazos]
(
[CodFormato] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipEstRecEq] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipEstRec] [int] NULL,
[NomTipEstRec] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Eqrechazos] ADD CONSTRAINT [PK_Eqrechazos] PRIMARY KEY CLUSTERED  ([CodFormato], [CodTipEstRecEq]) ON [PRIMARY]
GO
