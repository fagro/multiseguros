CREATE TABLE [dbo].[Facturas]
(
[NumFac] [smallint] NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValFac] [float] NULL,
[FecFac] [datetime] NULL,
[CodTipEstFac] [smallint] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
