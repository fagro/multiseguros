CREATE TABLE [dbo].[Tipos_Info_Adicional]
(
[idTipInfAdi] [int] NOT NULL IDENTITY(1, 1),
[CodTipInfAdi] [smallint] NOT NULL,
[NomTipInfAdi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL CONSTRAINT [DF__Tipos_Inf__FecUl__503BEA1C] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Info_Adicional] ADD CONSTRAINT [PK_Tipos_Info_Adicional] PRIMARY KEY NONCLUSTERED  ([idTipInfAdi]) ON [PRIMARY]
GO
