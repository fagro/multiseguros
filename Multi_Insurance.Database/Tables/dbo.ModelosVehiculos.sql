CREATE TABLE [dbo].[ModelosVehiculos]
(
[IdMarca] [int] NOT NULL,
[NombreModelo] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdModelo] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModelosVehiculos] ADD CONSTRAINT [PK_ModelosVehiculos] PRIMARY KEY CLUSTERED  ([IdModelo]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModelosVehiculos] ADD CONSTRAINT [fk_IdMarca] FOREIGN KEY ([IdMarca]) REFERENCES [dbo].[MarcasVehiculos] ([IdMarca])
GO
