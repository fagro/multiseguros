CREATE TABLE [dbo].[Tipos_Sexos]
(
[CodTipSex] [smallint] NOT NULL,
[NomTipSex] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Sexos] ADD CONSTRAINT [PK_Tipos_Sexos] PRIMARY KEY NONCLUSTERED  ([CodTipSex]) ON [PRIMARY]
GO
