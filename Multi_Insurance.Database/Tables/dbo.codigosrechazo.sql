CREATE TABLE [dbo].[codigosrechazo]
(
[patrocinador] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[codrechazo] [float] NOT NULL,
[descrechazo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[equivale] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[codigosrechazo] ADD CONSTRAINT [PK_codigosrechazo] PRIMARY KEY NONCLUSTERED  ([patrocinador], [codrechazo]) ON [PRIMARY]
GO
