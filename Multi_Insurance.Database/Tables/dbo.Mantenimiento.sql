CREATE TABLE [dbo].[Mantenimiento]
(
[NomTab] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomCam] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesCam] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndRef] [bit] NOT NULL,
[NomTabRef] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomCamCodRef] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomCamNomRef] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mantenimiento] ADD CONSTRAINT [PK_Mantenimiento] PRIMARY KEY NONCLUSTERED  ([NomTab]) ON [PRIMARY]
GO
