CREATE TABLE [dbo].[Tipos_Ocupacion]
(
[CodTipOcu] [smallint] NOT NULL,
[NomTipOcu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorSobPri] [float] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Ocupacion] ADD CONSTRAINT [PK_Tipos_Ocupacion_1__13] PRIMARY KEY CLUSTERED  ([CodTipOcu]) ON [PRIMARY]
GO
