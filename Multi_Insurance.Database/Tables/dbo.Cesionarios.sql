CREATE TABLE [dbo].[Cesionarios]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoCertificado] [int] NOT NULL,
[Valor] [float] NULL,
[CodigoProducto] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumeroCotizacion] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cesionarios] ADD CONSTRAINT [PK__Cesionar__3214EC0709583120] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
