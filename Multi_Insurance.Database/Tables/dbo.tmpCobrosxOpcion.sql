CREATE TABLE [dbo].[tmpCobrosxOpcion]
(
[codpro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[producto] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[opcion] [float] NULL,
[FecEnvRec] [datetime] NULL,
[C1] [int] NULL,
[R1] [float] NULL,
[C2] [int] NULL,
[R2] [float] NULL
) ON [PRIMARY]
GO
