CREATE TABLE [dbo].[Tipos_Reportes]
(
[CodTipRep] [smallint] NOT NULL,
[NomTipRep] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomRepRep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomProCal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Reportes] ADD CONSTRAINT [PK_Tipos_Reportes] PRIMARY KEY NONCLUSTERED  ([CodTipRep]) ON [PRIMARY]
GO
