CREATE TABLE [dbo].[Tipos_Documento_Identidad]
(
[CodTipDocIde] [smallint] NOT NULL,
[NomTipDocIde] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Documento_Identidad] ADD CONSTRAINT [PK_Tipos_Documento_Identidad] PRIMARY KEY NONCLUSTERED  ([CodTipDocIde]) ON [PRIMARY]
GO
