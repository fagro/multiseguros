CREATE TABLE [dbo].[Por_Index_Prod]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[A_oPorInd] [smallint] NOT NULL,
[PorInd] [float] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Por_Index_Prod] ADD CONSTRAINT [primaria] PRIMARY KEY CLUSTERED  ([CodPro], [A_oPorInd]) ON [PRIMARY]
GO
