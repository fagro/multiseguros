CREATE TABLE [dbo].[Actualizaciones]
(
[FechaIngreso] [smalldatetime] NULL,
[FechaEnvio] [smalldatetime] NULL,
[CodCer] [float] NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Estatus] [int] NULL,
[MonOpc] [float] NULL,
[ValPriCer] [float] NULL,
[Enviar] [int] NULL
) ON [PRIMARY]
GO
