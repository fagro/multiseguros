CREATE TABLE [dbo].[Devoluciones]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[ConCer] [smallint] NOT NULL,
[FecDev] [datetime] NOT NULL,
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValDev] [float] NULL,
[CodTipMedPag] [smallint] NULL,
[CodEstDev] [smallint] NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL,
[Observaciones] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipMotDev] [smallint] NULL,
[FecEnvDev] [datetime] NULL,
[FecExpCer] [datetime] NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Devoluciones] ADD CONSTRAINT [PK_Devoluciones_1__11] PRIMARY KEY NONCLUSTERED  ([CodPro], [CodCer], [ConCer], [FecDev]) ON [PRIMARY]
GO
