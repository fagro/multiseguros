CREATE TABLE [dbo].[Algoritmos_Chequeo]
(
[NitPat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[CodAlgChe] [smallint] NOT NULL,
[AlgChe] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Algoritmos_Chequeo] ADD CONSTRAINT [PK_Algoritmos_Chequeo] PRIMARY KEY NONCLUSTERED  ([NitPat], [CodTipMedPag], [CodAlgChe]) ON [PRIMARY]
GO
