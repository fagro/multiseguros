CREATE TABLE [dbo].[Tipos_Opcion]
(
[CodTipOpc] [smallint] NOT NULL IDENTITY(1, 1),
[NomTipOpc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [DF_TiposOpcion_FecUltMod] DEFAULT (getdate()),
[InfoAdicional] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Opcion] ADD CONSTRAINT [PK__Tipos_Op__5C34BBF1BE86EC1F] PRIMARY KEY CLUSTERED  ([CodTipOpc]) ON [PRIMARY]
GO
