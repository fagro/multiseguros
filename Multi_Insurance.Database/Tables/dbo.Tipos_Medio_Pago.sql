CREATE TABLE [dbo].[Tipos_Medio_Pago]
(
[CodTipMedPag] [smallint] NOT NULL IDENTITY(1, 1),
[NomTipMedPag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL CONSTRAINT [df_tipoMedioPago_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Medio_Pago] ADD CONSTRAINT [PK_Tipos_Medio_Pago] PRIMARY KEY CLUSTERED  ([CodTipMedPag]) ON [PRIMARY]
GO
