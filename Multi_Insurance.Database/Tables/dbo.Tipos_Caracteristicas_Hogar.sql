CREATE TABLE [dbo].[Tipos_Caracteristicas_Hogar]
(
[CodTipCarHog] [smallint] NOT NULL,
[NomTipCarHog] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Caracteristicas_Hogar] ADD CONSTRAINT [PK_Tipos_Caracteristicas_Hogar] PRIMARY KEY NONCLUSTERED  ([CodTipCarHog]) ON [PRIMARY]
GO
