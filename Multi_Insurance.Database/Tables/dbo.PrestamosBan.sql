CREATE TABLE [dbo].[PrestamosBan]
(
[CEDULA] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREDITO] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIPO_CREDITO] [int] NULL,
[SALDO] [float] NULL
) ON [PRIMARY]
GO
