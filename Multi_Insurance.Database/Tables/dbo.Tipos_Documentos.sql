CREATE TABLE [dbo].[Tipos_Documentos]
(
[CodTipDocs] [smallint] NOT NULL,
[NomTipDocs] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Documentos] ADD CONSTRAINT [PK_Tipos_Documentos] PRIMARY KEY CLUSTERED  ([CodTipDocs]) ON [PRIMARY]
GO
