CREATE TABLE [dbo].[ExecutivePositions]
(
[PositionId] [int] NOT NULL,
[PositionDescription] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateModified] [datetime] NULL,
[Status] [tinyint] NULL,
[UserId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExecutivePositions] ADD CONSTRAINT [PK_ExecutivePositions] PRIMARY KEY CLUSTERED  ([PositionId]) ON [PRIMARY]
GO
