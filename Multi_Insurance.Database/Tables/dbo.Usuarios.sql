CREATE TABLE [dbo].[Usuarios]
(
[CodUsu] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomUsu] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodPer] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClaUsu] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Usuarios] ADD CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED  ([CodUsu]) ON [PRIMARY]
GO
