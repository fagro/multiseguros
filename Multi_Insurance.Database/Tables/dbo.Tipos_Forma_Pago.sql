CREATE TABLE [dbo].[Tipos_Forma_Pago]
(
[CodTipForPag] [smallint] NOT NULL,
[NomTipForPag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DivTipForPag] [float] NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [df_tiposFormaPago_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Forma_Pago] ADD CONSTRAINT [PK__Tipos_Fo__BC5A5C6A0A011E26] PRIMARY KEY CLUSTERED  ([CodTipForPag]) ON [PRIMARY]
GO
