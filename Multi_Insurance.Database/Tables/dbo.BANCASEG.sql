CREATE TABLE [dbo].[BANCASEG]
(
[RAMO] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[encontrado] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CERT] [int] NULL,
[CUENTA] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[valor] [int] NULL,
[RECHAZO] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[procesado] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[codpro] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fecrec] [datetime] NULL,
[CODIGOUNICO] [binary] (8) NULL,
[CONTADOR] [float] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BANCASEG] ADD CONSTRAINT [PK_BANCASEG_1__12] PRIMARY KEY CLUSTERED  ([CONTADOR]) ON [PRIMARY]
GO
