CREATE TABLE [dbo].[Info_Adic_Producto_Hogar]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipInfAdi] [smallint] NOT NULL,
[ValInfAdi] [float] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Info_Adic_Producto_Hogar] ADD CONSTRAINT [PK_Info_Adic_Producto_Hogar] PRIMARY KEY NONCLUSTERED  ([CodPro], [CodTipInfAdi]) ON [PRIMARY]
GO
