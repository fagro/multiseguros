CREATE TABLE [dbo].[Amparos]
(
[CodAmp] [int] NOT NULL IDENTITY(1, 1),
[NomAmp] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipRam] [smallint] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Amparos] ADD CONSTRAINT [PK_Amparos_1__13] PRIMARY KEY CLUSTERED  ([CodAmp]) ON [PRIMARY]
GO
