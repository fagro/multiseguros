CREATE TABLE [dbo].[Tasas]
(
[Id] [int] NOT NULL,
[CodigoProducto] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TasaMillar] [float] NULL,
[Impuesto] [float] NULL
) ON [PRIMARY]
GO
