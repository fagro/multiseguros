CREATE TABLE [dbo].[Tipos_Nacionalidad]
(
[CodTipNacionalidad] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomTipNacionalidad] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Nacionalidad] ADD CONSTRAINT [PK___14__11] PRIMARY KEY CLUSTERED  ([CodTipNacionalidad]) ON [PRIMARY]
GO
