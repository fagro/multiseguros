CREATE TABLE [dbo].[BeneficiariosCertificado]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodTipDocIde] [smallint] NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Parentesco] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PorCentaje] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BeneficiariosCertificado] ADD CONSTRAINT [PK___1__10] PRIMARY KEY CLUSTERED  ([CodPro], [CodCer], [ConCer], [CodTipDocIde], [NumDocIde]) ON [PRIMARY]
GO
