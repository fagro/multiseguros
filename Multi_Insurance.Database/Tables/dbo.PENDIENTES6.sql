CREATE TABLE [dbo].[PENDIENTES6]
(
[CodPro] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [int] NULL,
[ConCer] [smallint] NULL,
[FecRec] [datetime] NULL,
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValRec] [float] NULL,
[NumRecEnv] [smallint] NULL,
[MonOpc] [float] NULL,
[FecIniPerCob] [datetime] NULL,
[FecFinPerCob] [datetime] NULL,
[CodTipMedPag] [smallint] NULL,
[CodTipEstRec] [smallint] NULL,
[CodTipForPag] [smallint] NULL,
[BanEnv] [bit] NOT NULL,
[FecEnvRec] [datetime] NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Altura] [smallint] NULL,
[Ano] [smallint] NULL,
[FecExpCer] [datetime] NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
