CREATE TABLE [dbo].[reclamaciones]
(
[CodTipDocIde] [smallint] NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [int] NULL,
[ConCer] [smallint] NULL,
[FecPresen] [datetime] NULL,
[CodEstado] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipDocIdePres] [smallint] NULL,
[NumDocIdePres] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cerificado] [bit] NOT NULL,
[Cedula] [bit] NOT NULL,
[Formulario] [bit] NOT NULL,
[Estadocuenta] [bit] NOT NULL,
[Confirmaciontel] [bit] NOT NULL,
[Cofirmadopor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
