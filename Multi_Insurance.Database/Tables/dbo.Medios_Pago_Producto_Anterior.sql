CREATE TABLE [dbo].[Medios_Pago_Producto_Anterior]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[FecUltMod] [datetime] NULL,
[CodTipDigCheq] [int] NULL
) ON [PRIMARY]
GO
