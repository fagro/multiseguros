CREATE TABLE [dbo].[cartasbien]
(
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [int] NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValPriCer] [float] NULL,
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecExpCer] [datetime] NULL,
[fecexpcer1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecDigCer] [datetime] NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipEstCer] [smallint] NULL,
[FecUltMod] [datetime] NULL,
[FecTerCer1] [varchar] (98) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipOpc] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cartasbien] ADD CONSTRAINT [PK_cartasbien_1__11] PRIMARY KEY CLUSTERED  ([CodPro], [CodCer], [NumDocIde]) ON [PRIMARY]
GO
