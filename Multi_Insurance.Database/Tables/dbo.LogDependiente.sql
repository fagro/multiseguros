CREATE TABLE [dbo].[LogDependiente]
(
[CodigoProducto] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCertificado] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Apellido] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Consecutivo] [int] NULL,
[Porciento] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Parentesco] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipoIdentidad] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumDocIdentificacion] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Direccion] [varchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Telefono] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Movil] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoCiudad] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltimaModificacion] [datetime] NULL,
[FechaNacimiento] [datetime] NULL,
[CodTipoSexo] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cuenta] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoEmpleado] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Oficina] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Id] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogDependiente] ADD CONSTRAINT [PK_LogDependiente] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
