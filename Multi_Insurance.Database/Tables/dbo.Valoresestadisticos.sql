CREATE TABLE [dbo].[Valoresestadisticos]
(
[CodInd] [float] NULL,
[FecIni] [datetime] NULL,
[FecFin] [datetime] NULL,
[PrimaAnual] [float] NULL,
[ValorAsegurado] [float] NULL,
[RecaudoAcumulado] [float] NULL
) ON [PRIMARY]
GO
