CREATE TABLE [dbo].[FormasPagEq]
(
[CodPro] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipMedPag] [float] NOT NULL,
[CodTipForPag] [float] NOT NULL,
[CodTipForPageq1] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipForPageq2] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipForPageq3] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipForPageq4] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormasPagEq] ADD CONSTRAINT [PK_FormasPagEq_1__16] PRIMARY KEY CLUSTERED  ([CodPro], [CodTipForPag], [CodTipMedPag]) ON [PRIMARY]
GO
