CREATE TABLE [dbo].[tmpRechazos]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [float] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodTipEstRec] [smallint] NULL,
[CodTipForPag] [smallint] NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[CodTipOpc] [smallint] NOT NULL,
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValRec] [float] NULL,
[Minimo] [datetime] NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirCli] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelCli] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecNacCli] [datetime] NULL,
[FecIniPerCob] [datetime] NULL,
[FecFinPerCob] [datetime] NULL,
[NomTipEstRec] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonOpc] [float] NULL,
[FecExpCer] [datetime] NULL,
[tipopo1] [float] NULL
) ON [PRIMARY]
GO
