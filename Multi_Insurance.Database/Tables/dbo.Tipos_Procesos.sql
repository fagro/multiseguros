CREATE TABLE [dbo].[Tipos_Procesos]
(
[codproprocesos] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[formato] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Directorio] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prefijo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FArchivo] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtencionP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediosPago] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Genrecibos] [bit] NOT NULL,
[Gendiscos] [bit] NOT NULL,
[Desproceso] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Procesos] ADD CONSTRAINT [PK_Tipos_Procesos] PRIMARY KEY CLUSTERED  ([codproprocesos], [formato]) ON [PRIMARY]
GO
