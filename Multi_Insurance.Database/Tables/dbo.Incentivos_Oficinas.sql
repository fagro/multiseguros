CREATE TABLE [dbo].[Incentivos_Oficinas]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SumaDeValPriCer] [float] NULL,
[CuentaDeValPriCer] [float] NULL
) ON [PRIMARY]
GO
