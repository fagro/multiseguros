CREATE TABLE [dbo].[ParaFiltro]
(
[ramo] [smallint] NULL,
[codigo] [int] NULL,
[valor] [float] NULL,
[estado] [smallint] NULL
) ON [PRIMARY]
GO
