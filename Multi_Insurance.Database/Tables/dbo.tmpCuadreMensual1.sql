CREATE TABLE [dbo].[tmpCuadreMensual1]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecEnvRec] [datetime] NULL,
[CodTipForPag] [smallint] NULL,
[NomTipForPag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomTipOpc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipEstRec] [smallint] NULL,
[CuentaDeCodCer] [int] NULL,
[SumaDeValRec] [float] NULL,
[SumaDeMonOpc] [float] NULL
) ON [PRIMARY]
GO
