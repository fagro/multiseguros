CREATE TABLE [dbo].[Tipos_Indexacion]
(
[TipoIndexacionId] [smallint] NOT NULL IDENTITY(1, 1),
[NomTipInd] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [DF_TiposIndexacion_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Indexacion] ADD CONSTRAINT [PK__Tipos_In__E4DA96E4B3D4A1EE] PRIMARY KEY CLUSTERED  ([TipoIndexacionId]) ON [PRIMARY]
GO
