CREATE TABLE [dbo].[Productos_Usuario]
(
[CodUsu] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodPro] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Productos_Usuario] ADD CONSTRAINT [PK_Ususarios_Producto] PRIMARY KEY NONCLUSTERED  ([CodUsu], [CodPro]) ON [PRIMARY]
GO
