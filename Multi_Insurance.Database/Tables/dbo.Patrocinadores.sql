CREATE TABLE [dbo].[Patrocinadores]
(
[NitPat] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomPat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirPat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL,
[CodTipEntBan] [smallint] NULL,
[CodBanPat] [smallint] NULL,
[CONTACTO] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELPAT] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patrocinadores] ADD CONSTRAINT [PK_Patrocinadores] PRIMARY KEY NONCLUSTERED  ([NitPat]) ON [PRIMARY]
GO
