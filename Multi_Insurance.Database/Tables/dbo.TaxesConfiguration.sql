CREATE TABLE [dbo].[TaxesConfiguration]
(
[EffectiveDate] [datetime] NOT NULL,
[Taxes] [money] NOT NULL,
[WDate] [datetime] NOT NULL,
[Cuser] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [smallint] NULL
) ON [PRIMARY]
GO
