CREATE TABLE [dbo].[Tipos_Estados_Devoluciones]
(
[CodEstDev] [smallint] NOT NULL,
[NomEstDev] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Estados_Devoluciones] ADD CONSTRAINT [PK_Tipos_Estados_Devoluci1__13] PRIMARY KEY CLUSTERED  ([CodEstDev]) ON [PRIMARY]
GO
