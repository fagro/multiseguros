CREATE TABLE [dbo].[Opcion_Producto]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipOpc] [smallint] NOT NULL,
[MonOpc] [float] NULL,
[FecUltMod] [datetime] NULL,
[Dependiente] [bit] NULL,
[ValidacionVinculado] [bit] NULL,
[declaracionJurada] [varchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Opcion_Producto] ADD CONSTRAINT [PK_Opcion_Producto] PRIMARY KEY CLUSTERED  ([CodPro], [CodTipOpc]) ON [PRIMARY]
GO
