CREATE TABLE [dbo].[Vehiculos]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[ConCer] [smallint] NOT NULL,
[Tipo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Marca] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Modelo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Chasis] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ano] [int] NULL,
[Registro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FechaVencimiento] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Vehiculos] ADD CONSTRAINT [PK_Vehiculos_1__19] PRIMARY KEY CLUSTERED  ([CodPro], [CodCer], [ConCer]) ON [PRIMARY]
GO
