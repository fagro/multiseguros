CREATE TABLE [dbo].[Tipos_Estados_Certificado]
(
[CodTipEstCer] [smallint] NOT NULL,
[NomTipEstCer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Estados_Certificado] ADD CONSTRAINT [PK_Tipos_Estados_Certificado] PRIMARY KEY NONCLUSTERED  ([CodTipEstCer]) ON [PRIMARY]
GO
