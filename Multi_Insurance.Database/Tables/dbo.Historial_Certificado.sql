CREATE TABLE [dbo].[Historial_Certificado]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodTipForPag] [smallint] NULL,
[CodTipMedPag] [smallint] NULL,
[CodTipOpc] [smallint] NULL,
[FecIniHisCer] [datetime] NOT NULL,
[FecUltMod] [datetime] NOT NULL,
[CodTipDocIde] [smallint] NULL,
[NumDocIDe] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumCue] [float] NULL,
[FecFinHisCer] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Historial_Certificado] ADD CONSTRAINT [PK_Historial_Certificado_1__15] PRIMARY KEY CLUSTERED  ([CodPro], [CodCer], [ConCer], [FecUltMod]) ON [PRIMARY]
GO
