CREATE TABLE [dbo].[Vigencias]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CodigoVigencia] [int] NOT NULL,
[Vigencia] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodigoProducto] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Vigencias] ADD CONSTRAINT [PK_VigenciasVehiculos] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
