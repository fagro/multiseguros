CREATE TABLE [dbo].[Amparos_Producto]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodAmp] [int] NOT NULL,
[PorValAmp] [money] NULL,
[IndAmpPri] [bit] NOT NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Amparos_Producto] ADD CONSTRAINT [PK_Amparos_Producto] PRIMARY KEY NONCLUSTERED  ([CodPro], [CodAmp]) ON [PRIMARY]
GO
