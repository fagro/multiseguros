CREATE TABLE [dbo].[DetalleProductos]
(
[CodPro] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipOpc] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descripcion] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dependiente] [bit] NULL,
[ValidacionVinculo] [bit] NULL
) ON [PRIMARY]
GO
