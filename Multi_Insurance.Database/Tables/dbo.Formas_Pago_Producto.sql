CREATE TABLE [dbo].[Formas_Pago_Producto]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipForPag] [smallint] NOT NULL,
[PorRecForPag] [float] NULL,
[fecultmod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Formas_Pago_Producto] ADD CONSTRAINT [PK_Formas_Pago_Producto_1] PRIMARY KEY NONCLUSTERED  ([CodPro], [CodTipForPag]) ON [PRIMARY]
GO
