CREATE TABLE [dbo].[Estadisticaventas]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CuentadeCodcer] [float] NULL,
[SumadeMonOpc] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Estadisticaventas] ADD CONSTRAINT [PK___1__12] PRIMARY KEY CLUSTERED  ([CodPro], [CodOfi]) ON [PRIMARY]
GO
