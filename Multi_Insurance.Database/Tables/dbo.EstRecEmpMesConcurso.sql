CREATE TABLE [dbo].[EstRecEmpMesConcurso]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnoEst] [int] NOT NULL,
[MesEst] [int] NOT NULL,
[Zona] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomZona] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Responsable] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodEmp] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomEmp] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CertRecMes] [int] NULL,
[ValRecMes] [float] NULL,
[ValAsegMes] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EstRecEmpMesConcurso] ADD CONSTRAINT [PK_EstRecMesEmpConcurso] PRIMARY KEY CLUSTERED  ([CodPro], [AnoEst], [MesEst], [CodEmp]) ON [PRIMARY]
GO
