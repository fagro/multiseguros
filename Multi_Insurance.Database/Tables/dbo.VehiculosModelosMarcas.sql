CREATE TABLE [dbo].[VehiculosModelosMarcas]
(
[NombreModelo] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NombreMarca] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
