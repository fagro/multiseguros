CREATE TABLE [dbo].[EspecFormatos]
(
[FORMATO] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ORDEN] [float] NOT NULL,
[TABLA] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAMPO] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LONGITUD] [float] NULL,
[ALINEAR] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELLENO] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[formatear] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[decimales] [float] NULL,
[separador] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIPO] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EspecFormatos] ADD CONSTRAINT [PK_ESPECFORMATOS] PRIMARY KEY CLUSTERED  ([FORMATO], [ORDEN]) ON [PRIMARY]
GO
