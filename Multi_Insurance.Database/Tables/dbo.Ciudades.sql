CREATE TABLE [dbo].[Ciudades]
(
[CodCiu] [int] NOT NULL,
[NomCiu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[coddep] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDICATIVO] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timestamp] [binary] (8) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Ciudades] ADD CONSTRAINT [PK_Ciudades1_1__13] PRIMARY KEY CLUSTERED  ([CodCiu]) ON [PRIMARY]
GO
