CREATE TABLE [dbo].[Tipos_Digito_Chequeo]
(
[CodTipDigCheq] [float] NULL,
[NomTipDigCheq] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nomprogdigcheq] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tiprutdigcheq] [float] NULL
) ON [PRIMARY]
GO
