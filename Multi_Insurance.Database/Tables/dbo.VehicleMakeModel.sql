CREATE TABLE [dbo].[VehicleMakeModel]
(
[MakeCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MakeModel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [tinyint] NULL,
[WDate] [datetime] NULL,
[CUser] [nvarchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCurrency] [tinyint] NULL,
[RCurrency] [money] NULL,
[ARate] [money] NULL,
[VehicleType] [smallint] NULL,
[VRS] [bit] NULL,
[Restricted] [bit] NULL,
[id] [int] NOT NULL,
[Deprecated] [bit] NULL
) ON [PRIMARY]
GO
