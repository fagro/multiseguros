CREATE TABLE [dbo].[Oficinas]
(
[NitPat] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCiu] [int] NULL,
[FaxOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL,
[Categoria] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodDep] [smallint] NULL,
[zona] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZONAASEG] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[meta] [float] NULL,
[nopolizas] [float] NULL,
[valaseg] [float] NULL,
[nrocobradas] [float] NULL,
[valcobrado] [float] NULL,
[metarecaudo] [float] NULL,
[CodProm] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[metapromotor] [float] NULL,
[metaaseg] [float] NULL,
[autorizada] [smallint] NULL,
[unico] [binary] (8) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Oficinas] ADD CONSTRAINT [PK_Oficinas_1] PRIMARY KEY CLUSTERED  ([NitPat], [CodOfi]) ON [PRIMARY]
GO
