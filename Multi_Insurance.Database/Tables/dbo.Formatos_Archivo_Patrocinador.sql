CREATE TABLE [dbo].[Formatos_Archivo_Patrocinador]
(
[CodFor] [float] NOT NULL,
[NitPat] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Formatos_Archivo_Patrocinador] ADD CONSTRAINT [PK_Formatos_Archivo_Patrocinador] PRIMARY KEY NONCLUSTERED  ([CodFor], [NitPat]) ON [PRIMARY]
GO
