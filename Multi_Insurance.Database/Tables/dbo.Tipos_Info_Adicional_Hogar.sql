CREATE TABLE [dbo].[Tipos_Info_Adicional_Hogar]
(
[CodTipInfAdi] [smallint] NOT NULL,
[NomTipInfAdi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Info_Adicional_Hogar] ADD CONSTRAINT [PK_Tipos_Info_Adicional_Hogar] PRIMARY KEY NONCLUSTERED  ([CodTipInfAdi]) ON [PRIMARY]
GO
