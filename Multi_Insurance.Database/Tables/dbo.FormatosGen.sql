CREATE TABLE [dbo].[FormatosGen]
(
[CodFormato] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomFormato] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TablaFormato] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConFormato] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArcSalidaFormato] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormatosGen] ADD CONSTRAINT [PK___2__12] PRIMARY KEY CLUSTERED  ([CodFormato]) ON [PRIMARY]
GO
