CREATE TABLE [dbo].[Incentivos_Empleados]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipDocIde] [smallint] NULL,
[NumDocIDe] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SumaDeValPriCer] [float] NULL,
[CuentaDeValPriCer] [int] NULL
) ON [PRIMARY]
GO
