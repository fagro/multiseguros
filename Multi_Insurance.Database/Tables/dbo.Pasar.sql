CREATE TABLE [dbo].[Pasar]
(
[CodPro] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [float] NULL,
[ConCer] [float] NULL,
[FecRec] [smalldatetime] NULL,
[NumCue] [float] NULL,
[ValRec] [float] NULL,
[NumRecEnv] [float] NULL,
[MonOpc] [float] NULL,
[FecIniPerCob] [smalldatetime] NULL,
[FecFinPerCob] [smalldatetime] NULL,
[CodTipMedPag] [float] NULL,
[CodTipEstRec] [float] NULL,
[CodTipForPag] [float] NULL,
[BanEnv] [float] NULL,
[FecEnvRec] [smalldatetime] NULL,
[NumDocIde] [float] NULL,
[Altura] [float] NULL,
[Ano] [float] NULL,
[FecExpCer] [smalldatetime] NULL,
[CodOfi] [float] NULL,
[FecUltMod] [smalldatetime] NULL,
[unico] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONSECUTIVO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumCueAlterna] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
