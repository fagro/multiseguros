CREATE TABLE [dbo].[Tipos_Incentivos]
(
[CodTipInc] [smallint] NOT NULL,
[NomTipInc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomRepInc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Incentivos] ADD CONSTRAINT [PK_Tipos_Incentivos] PRIMARY KEY NONCLUSTERED  ([CodTipInc]) ON [PRIMARY]
GO
