CREATE TABLE [dbo].[tmpCuadreMensual]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecEnvRec] [datetime] NULL,
[CodTipForPag] [smallint] NULL,
[NomTipForPag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomTipOpc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipEstRec] [smallint] NULL,
[CuentaDeCodCer] [int] NULL,
[SumaDeValRec] [float] NULL,
[SumaDeMonOpc] [float] NULL,
[Edad] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AonN] [float] NULL,
[AonR] [float] NULL,
[AonRT] [float] NULL,
[Valor1] [float] NULL,
[Valor2] [float] NULL
) ON [PRIMARY]
GO
