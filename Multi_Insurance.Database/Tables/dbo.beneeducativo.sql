CREATE TABLE [dbo].[beneeducativo]
(
[codpro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[codcer] [float] NOT NULL,
[concer] [float] NOT NULL,
[CONSECBENE] [float] NOT NULL,
[nombene] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[apebene] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[parentbene] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cursoactualbene] [int] NULL,
[fecfinestudios] [datetime] NULL,
[calendario] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pagindemnizacion] [float] NULL,
[cedulatutor] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nomtutor] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[valrenta] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[beneeducativo] ADD CONSTRAINT [PK_beneeducativo_1__13] PRIMARY KEY CLUSTERED  ([codpro], [codcer], [concer], [CONSECBENE]) ON [PRIMARY]
GO
