CREATE TABLE [dbo].[promedios]
(
[codpro] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fecha] [datetime] NULL,
[edad] [float] NULL,
[prima] [float] NULL,
[asegurado] [float] NULL
) ON [PRIMARY]
GO
