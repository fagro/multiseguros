CREATE TABLE [dbo].[Perfiles]
(
[CodPer] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomPer] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Perfiles] ADD CONSTRAINT [PK_Perfiles] PRIMARY KEY NONCLUSTERED  ([CodPer]) ON [PRIMARY]
GO
