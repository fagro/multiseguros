CREATE TABLE [dbo].[Primas_Amparo]
(
[PrimasAmparoID] [int] NOT NULL IDENTITY(1, 1),
[productID] [int] NOT NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodAmp] [int] NOT NULL,
[CodTipOpc] [smallint] NOT NULL,
[CodTipForPag] [float] NOT NULL,
[EdaDes] [smallint] NOT NULL,
[EdaHas] [smallint] NOT NULL,
[PorPriAmp] [float] NULL,
[FecUltMod] [datetime] NULL CONSTRAINT [DF_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Primas_Amparo] ADD CONSTRAINT [PK__Primas_A__B3D24FC143E8ABBB] PRIMARY KEY CLUSTERED  ([PrimasAmparoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Primas_Amparo] ADD CONSTRAINT [Ak_PrimasAmparo] UNIQUE NONCLUSTERED  ([CodPro], [CodAmp], [CodTipOpc], [CodTipForPag], [EdaDes], [EdaHas]) ON [PRIMARY]
GO
