CREATE TABLE [dbo].[Tipos_Moneda]
(
[TiposMonedaId] [smallint] NOT NULL IDENTITY(1, 1),
[NomTipMon] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[simbolo] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [DF_TiposMoneda] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Moneda] ADD CONSTRAINT [PK__Tipos_Mo__8504ABD9F0BA7D0F] PRIMARY KEY CLUSTERED  ([TiposMonedaId]) ON [PRIMARY]
GO
