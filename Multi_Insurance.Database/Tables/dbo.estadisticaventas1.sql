CREATE TABLE [dbo].[estadisticaventas1]
(
[Fecha] [datetime] NOT NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CuentadeCodcer] [float] NULL,
[SumadeMonOpc] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[estadisticaventas1] ADD CONSTRAINT [PK_estadisticaventas1_1__12] PRIMARY KEY CLUSTERED  ([CodPro], [CodOfi], [Fecha]) ON [PRIMARY]
GO
