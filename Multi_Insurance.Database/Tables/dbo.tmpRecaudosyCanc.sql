CREATE TABLE [dbo].[tmpRecaudosyCanc]
(
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MC] [int] NULL,
[MonOpc] [float] NULL,
[NoCert] [int] NULL,
[Recaudo] [float] NULL,
[Cancelados] [int] NULL,
[Primas] [float] NULL
) ON [PRIMARY]
GO
