CREATE TABLE [dbo].[Tipos_Mercadeo]
(
[CodTipMer] [smallint] NOT NULL,
[NomTipMer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Mercadeo] ADD CONSTRAINT [PK_Tipos_Mercadeo] PRIMARY KEY NONCLUSTERED  ([CodTipMer]) ON [PRIMARY]
GO
