CREATE TABLE [dbo].[Tipos_Estado_Civil]
(
[CodTipEstCivil] [int] NOT NULL,
[NomTipEstCivil] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Estado_Civil] ADD CONSTRAINT [PK___2__10] PRIMARY KEY CLUSTERED  ([CodTipEstCivil]) ON [PRIMARY]
GO
