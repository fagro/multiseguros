CREATE TABLE [dbo].[Dependientes]
(
[Nombre] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Apellido] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumDocIde] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Telefono] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Movil] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Direccion] [varchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[CodPro] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipDocIde] [smallint] NOT NULL,
[CodTipSex] [int] NOT NULL,
[ConCer] [smallint] NULL,
[Consecutivo] [int] NOT NULL,
[FecNacDep] [datetime] NOT NULL,
[FecUltMod] [datetime] NOT NULL,
[Parentesco] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Porciento] [decimal] (3, 2) NULL,
[Incongruencia] [bit] NULL,
[Cuenta] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodigoEmpleado] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCiu] [int] NULL,
[Profesion] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Dependientes] ADD CONSTRAINT [PK_Dependientes_1] PRIMARY KEY CLUSTERED  ([NumDocIde], [Cuenta]) ON [PRIMARY]
GO
