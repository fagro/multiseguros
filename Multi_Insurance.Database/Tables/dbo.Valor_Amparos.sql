CREATE TABLE [dbo].[Valor_Amparos]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodAmp] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipOpc] [smallint] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
