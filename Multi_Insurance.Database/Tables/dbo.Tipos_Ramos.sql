CREATE TABLE [dbo].[Tipos_Ramos]
(
[TiposRamosId] [int] NOT NULL IDENTITY(1, 1),
[CodTipRam] [smallint] NOT NULL,
[NomTipRam] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL CONSTRAINT [DF_tipos_ramos_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Ramos] ADD CONSTRAINT [PK__Tipos_Ra__9E1BB8BD47A6D6F9] PRIMARY KEY CLUSTERED  ([TiposRamosId]) ON [PRIMARY]
GO
