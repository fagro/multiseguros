CREATE TABLE [dbo].[Log_Certificados]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [float] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodTipDocIde] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipDocI11__13] DEFAULT ((0)),
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_LogCertificad_NumDocIde_12__13] DEFAULT ((0)),
[DirInm] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_LogCertificad_CodOfi_1__13] DEFAULT ((0)),
[CodTipDocIdeEmp] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipDocId2__13] DEFAULT ((0)),
[NumDocIdeEmp] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_LogCertificad_NumDocIdeEm9__13] DEFAULT ((0)),
[CodTipEstCer] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipEstCe3__13] DEFAULT ((1)),
[CodTipForPag] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipForPa4__13] DEFAULT ((12)),
[CodTipMedPag] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipMedPa5__13] DEFAULT ((1)),
[CodTipOpc] [smallint] NOT NULL CONSTRAINT [DF_LogCertificad_CodTipOpc_6__13] DEFAULT ((0)),
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_LogCertificad_NumCue_8__13] DEFAULT ((0)),
[ValSobTas] [float] NULL CONSTRAINT [DF_LogCertificados_ValSobTas] DEFAULT ((0)),
[ValPriCer] [float] NOT NULL CONSTRAINT [DF_LogCertificad_ValPriCer_10__13] DEFAULT ((0)),
[NumMet] [float] NULL,
[IndCob] [bit] NOT NULL CONSTRAINT [DF_LogCertificad_IndCob_7__13] DEFAULT ((1)),
[PorDes] [smallint] NULL,
[CodTipMer] [smallint] NULL,
[FecExpCer] [datetime] NULL,
[FecTerCer] [datetime] NULL,
[FecUltMod] [datetime] NULL,
[FecDigCer] [datetime] NULL,
[USUARIO] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fecpaghasta] [datetime] NULL,
[MonOpc] [float] NULL,
[CodHabilPago] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodPagado] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecPagInc] [datetime] NULL,
[VALCONTENIDO] [float] NULL,
[CodMotCan] [smallint] NULL,
[estmatric] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[unico] [binary] (8) NULL,
[observaciones] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CODTIPMOTCAN] [int] NULL,
[fecvto] [datetime] NULL,
[CodTipDocIdeTomador] [smallint] NULL,
[NumDocIdeTomador] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecIniVig] [datetime] NULL,
[FecFinVig] [datetime] NULL,
[CodTipNacionalidad] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Poliza] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipDocIdeGes] [smallint] NULL,
[NumDocIdeGes] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumCueAlterna] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecIngAseg] [datetime] NULL,
[TipoTransaccion] [smallint] NOT NULL,
[FechaTransaccion] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Log_Certificados] ADD CONSTRAINT [PK_Certificados__15] PRIMARY KEY NONCLUSTERED  ([CodPro], [CodCer], [ConCer], [FechaTransaccion]) ON [PRIMARY]
GO
