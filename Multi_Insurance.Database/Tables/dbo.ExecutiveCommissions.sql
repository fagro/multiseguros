CREATE TABLE [dbo].[ExecutiveCommissions]
(
[ExecutiveId] [int] NOT NULL,
[ExecutiveName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExecutivePosition] [int] NULL,
[CommissionPercentage] [money] NULL,
[CommissionDescription] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExecutiveWindowsUser] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateModified] [datetime] NULL,
[Status] [tinyint] NULL,
[UserId] [int] NULL,
[EffectiveDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExecutiveCommissions] ADD CONSTRAINT [PK_ExecutiveCommissions] PRIMARY KEY CLUSTERED  ([ExecutiveId]) ON [PRIMARY]
GO
