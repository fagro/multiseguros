CREATE TABLE [dbo].[PruebaArchivoCobro]
(
[Cuenta] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Valor] [int] NULL,
[CodigoRechazo] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
