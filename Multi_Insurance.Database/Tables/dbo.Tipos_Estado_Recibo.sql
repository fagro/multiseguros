CREATE TABLE [dbo].[Tipos_Estado_Recibo]
(
[CodTipEstRec] [smallint] NOT NULL,
[NomTipEstRec] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Estado_Recibo] ADD CONSTRAINT [PK_Tipos_Estado_Recibo] PRIMARY KEY NONCLUSTERED  ([CodTipEstRec]) ON [PRIMARY]
GO
