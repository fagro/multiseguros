CREATE TABLE [dbo].[Historial_Itbis]
(
[FechaIni] [datetime] NOT NULL,
[FechaFin] [datetime] NOT NULL,
[Itbis] [float] NULL CONSTRAINT [DF_Historial_Itbis_Itbis] DEFAULT ((0)),
[FecUltMod] [datetime] NULL CONSTRAINT [DF_Historial_Itbis_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Historial_Itbis] ADD CONSTRAINT [PK_Historial_Itbis] PRIMARY KEY CLUSTERED  ([FechaIni], [FechaFin]) ON [PRIMARY]
GO
