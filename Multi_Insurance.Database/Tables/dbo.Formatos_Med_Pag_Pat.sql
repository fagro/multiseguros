CREATE TABLE [dbo].[Formatos_Med_Pag_Pat]
(
[CodFor] [smallint] NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[ForTipMedPag] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Formatos_Med_Pag_Pat] ADD CONSTRAINT [PK_Formatos_Med_Pag_Pat] PRIMARY KEY NONCLUSTERED  ([CodFor], [CodTipMedPag]) ON [PRIMARY]
GO
