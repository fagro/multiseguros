CREATE TABLE [dbo].[Tipos_Estados_Facturas]
(
[CodTipEstFac] [smallint] NOT NULL,
[NomTipEstFac] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Estados_Facturas] ADD CONSTRAINT [PK_Tipos_Estados_Facturas] PRIMARY KEY NONCLUSTERED  ([CodTipEstFac]) ON [PRIMARY]
GO
