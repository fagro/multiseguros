CREATE TABLE [dbo].[TiposVehiculos]
(
[CodTipo] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomTipo] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TiposVehiculos] ADD CONSTRAINT [PK___2__19] PRIMARY KEY CLUSTERED  ([CodTipo]) ON [PRIMARY]
GO
