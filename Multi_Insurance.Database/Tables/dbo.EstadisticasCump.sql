CREATE TABLE [dbo].[EstadisticasCump]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnoEst] [int] NOT NULL,
[MesEst] [int] NOT NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MetaCerMen] [int] NULL,
[VentasCerMen] [int] NULL,
[CumpVtasMes] [float] NULL,
[MetaCerAnual] [int] NULL,
[VentasCerAnual] [int] NULL,
[CumpVtasAnual] [float] NULL,
[MetaRecaudoMes] [float] NULL,
[ValRecaudoMes] [float] NULL,
[CumpRecaudoMes] [float] NULL,
[MetaRecaudoAnual] [float] NULL,
[ValRecaudoAnual] [float] NULL,
[CumpRecaudoAnual] [float] NULL,
[MetaValAsegMes] [float] NULL,
[ValAsegMes] [float] NULL,
[CumpValAsegMes] [float] NULL,
[MetaValAsegAnual] [float] NULL,
[ValAsegAnual] [float] NULL,
[CumpValAsegAnual] [float] NULL,
[Zona] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZonaAseg] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EstadisticasCump] ADD CONSTRAINT [PK_EstadisticasCump] PRIMARY KEY CLUSTERED  ([CodPro], [AnoEst], [MesEst], [CodOfi]) ON [PRIMARY]
GO
