CREATE TABLE [dbo].[tmpRechazosCall]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FecEnvRec] [datetime] NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Oficina] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [float] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodTipForPag] [smallint] NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[CodTipOpc] [smallint] NOT NULL,
[ValPriCer] [float] NOT NULL,
[NumCue] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Nombre] [varchar] (91) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirCli] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelCli] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomCiu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipEstRec] [smallint] NULL,
[NomTipEstRec] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonOpc] [float] NULL,
[FecExpCer] [datetime] NULL,
[Deuda] [float] NULL,
[Recibos] [int] NULL
) ON [PRIMARY]
GO
