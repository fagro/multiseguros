CREATE TABLE [dbo].[Tipos_Motivo_Devolucion]
(
[CodTipMotdev] [float] NOT NULL,
[NomTipMotdev] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Motivo_Devolucion] ADD CONSTRAINT [PK_Tipos_Motivo_Devolucio1__13] PRIMARY KEY CLUSTERED  ([CodTipMotdev]) ON [PRIMARY]
GO
