CREATE TABLE [dbo].[EspecFormatosRechazo]
(
[CodFormato] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Campo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Inicio] [int] NULL,
[Longitud] [int] NULL,
[CampoOrigen] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EspecFormatosRechazo] ADD CONSTRAINT [PK___5__12] PRIMARY KEY CLUSTERED  ([CodFormato], [Campo]) ON [PRIMARY]
GO
