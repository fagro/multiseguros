CREATE TABLE [dbo].[MarcasVehiculos]
(
[IdMarca] [int] NOT NULL IDENTITY(1, 1),
[NombreMarca] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MarcasVehiculos] ADD CONSTRAINT [PK_MarcasVehiculos] PRIMARY KEY CLUSTERED  ([IdMarca]) ON [PRIMARY]
GO
