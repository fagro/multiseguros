CREATE TABLE [dbo].[IncentivosWeb]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Codigo] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_IncentivosWeb_Codigo] DEFAULT ((0)),
[CodCer] [float] NOT NULL,
[ConCer] [smallint] NOT NULL,
[AnoP] [smallint] NOT NULL,
[MesP] [smallint] NOT NULL,
[CodTipForPag] [smallint] NOT NULL CONSTRAINT [DF_IncentivosWeb_CodTipForPag] DEFAULT ((12)),
[CodTipOpc] [smallint] NOT NULL CONSTRAINT [DF_IncentivosWeb_CodTipOpc] DEFAULT ((0)),
[CodTipEstCer] [smallint] NOT NULL CONSTRAINT [DF_IncentivosWeb_CodTipEstCer] DEFAULT ((1)),
[IncenMes] [float] NULL CONSTRAINT [DF_IncentivosWeb_Incenmes] DEFAULT ((0)),
[RecaudoMes] [float] NULL CONSTRAINT [DF_IncentivosWeb_RecaudoMes] DEFAULT ((0)),
[IncenTotal] [float] NULL CONSTRAINT [DF_IncentivosWeb_IncenTotal] DEFAULT ((0)),
[IncenCobrado] [float] NULL CONSTRAINT [DF_IncentivosWeb_IncenCobrado] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IncentivosWeb] ADD CONSTRAINT [PK_IncentivosWeb] PRIMARY KEY CLUSTERED  ([CodPro], [Codigo], [CodCer], [ConCer], [AnoP], [MesP]) ON [PRIMARY]
GO
