CREATE TABLE [dbo].[prueba]
(
[CodTipDocIde] [smallint] NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomCli] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApeCli] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ape2Cli] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecNacCli] [datetime] NULL,
[DirCli] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelCli] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCiu] [int] NULL,
[CodTipOcu] [smallint] NULL,
[CodTipSex] [smallint] NULL,
[ExaMed] [bit] NOT NULL,
[FecUltMod] [datetime] NULL,
[DIRREC] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELREC] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nohijos] [int] NULL,
[codtipestcivil] [int] NULL,
[email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[telmovil] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipNacionalidad] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
