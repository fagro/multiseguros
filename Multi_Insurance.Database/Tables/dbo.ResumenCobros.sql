CREATE TABLE [dbo].[ResumenCobros]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AnoP] [smallint] NOT NULL,
[MesP] [smallint] NOT NULL,
[CodCer] [float] NOT NULL,
[ConCer] [smallint] NOT NULL,
[CodigoEmp] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ResumenCobros_CodigoEmp] DEFAULT ((0)),
[CodTipForPag] [smallint] NOT NULL CONSTRAINT [DF_ResumenCobros_CodTipForPag] DEFAULT ((12)),
[CodTipOpc] [smallint] NOT NULL CONSTRAINT [DF_ResumenCobros_CodTipOpc] DEFAULT ((0)),
[CodTipEstCer] [smallint] NOT NULL CONSTRAINT [DF_ResumenCobros_CodTipEstCer] DEFAULT ((1)),
[IncenMesAnt] [float] NULL CONSTRAINT [DF_ResumenCobros_IncenMesAnt] DEFAULT ((0)),
[RecaudoMes] [float] NULL CONSTRAINT [DF_ResumenCobros_RecaudoMes] DEFAULT ((0)),
[IncenMes] [float] NULL CONSTRAINT [DF_ResumenCobros_IncenMes] DEFAULT ((0)),
[IncenCobrado] [float] NULL CONSTRAINT [DF_ResumenCobros_IncenCobrado] DEFAULT ((0)),
[IncenTotal] [float] NULL CONSTRAINT [DF_ResumenCobros_IncenTotal] DEFAULT ((0))
) ON [PRIMARY]
GO
