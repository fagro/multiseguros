CREATE TABLE [dbo].[TiposArchivo]
(
[NomTipArchivo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TiposArchivo] ADD CONSTRAINT [PK_TiposArchivo] PRIMARY KEY NONCLUSTERED  ([NomTipArchivo]) ON [PRIMARY]
GO
