CREATE TABLE [dbo].[Registro_Carac_Certi_Hogar]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCer] [int] NULL,
[ConCer] [smallint] NULL,
[CodTipCarHog] [smallint] NULL,
[FecUltMod] [datetime] NULL
) ON [PRIMARY]
GO
