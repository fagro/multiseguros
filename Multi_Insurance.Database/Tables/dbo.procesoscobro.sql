CREATE TABLE [dbo].[procesoscobro]
(
[codpro] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[fecenvrec] [datetime] NULL,
[valenviado] [float] NULL,
[noenviado] [float] NULL,
[valrechazo] [float] NULL,
[norechazo] [float] NULL,
[valcobrado] [float] NULL,
[nocobrado] [float] NULL,
[fecconciliado] [datetime] NULL,
[fecrevisado] [datetime] NULL,
[fecfacturado] [datetime] NULL,
[generadopor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[revisadopor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[facturadopor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
