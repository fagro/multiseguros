CREATE TABLE [dbo].[tmpOficiales]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NumDocIdeEmp] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nombre] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CertNuevos] [float] NULL,
[PrimaN] [float] NULL,
[AnualN] [float] NULL,
[CertCancelados] [float] NULL,
[PrimasC] [float] NULL,
[AnualC] [float] NULL
) ON [PRIMARY]
GO
