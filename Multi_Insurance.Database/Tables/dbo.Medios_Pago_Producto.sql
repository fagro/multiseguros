CREATE TABLE [dbo].[Medios_Pago_Producto]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodTipMedPag] [smallint] NOT NULL,
[FecUltMod] [datetime] NULL,
[CodTipDigCheq] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Medios_Pago_Producto] ADD CONSTRAINT [PK_Medios_Pago_Producto] PRIMARY KEY CLUSTERED  ([CodPro], [CodTipMedPag]) ON [PRIMARY]
GO
