CREATE TABLE [dbo].[Gestores]
(
[GestoresId] [smallint] NOT NULL IDENTITY(1, 1),
[NomGes] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirGes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelGes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [DF_Gestores_FecUlmod] DEFAULT (getdate()),
[FACTURA] [float] NULL,
[LOGOTIPO] [image] NULL,
[COMENTARIO] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Nitr] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tipdev] [smallint] NULL,
[notacredito] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Gestores] ADD CONSTRAINT [PK_Gestores] PRIMARY KEY NONCLUSTERED  ([GestoresId]) ON [PRIMARY]
GO
