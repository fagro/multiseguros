CREATE TABLE [dbo].[Tipos_TransaccionesLog]
(
[CodigoTransaccion] [smallint] NOT NULL,
[DescripcionTransaccion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_TransaccionesLog] ADD CONSTRAINT [PK_Tipos_Transacciones] PRIMARY KEY CLUSTERED  ([CodigoTransaccion]) ON [PRIMARY]
GO
