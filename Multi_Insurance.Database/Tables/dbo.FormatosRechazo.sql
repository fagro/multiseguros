CREATE TABLE [dbo].[FormatosRechazo]
(
[CodFormato] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomFormato] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipoArhivo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Archivo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tabla] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormatosRechazo] ADD CONSTRAINT [PK___4__12] PRIMARY KEY CLUSTERED  ([CodFormato]) ON [PRIMARY]
GO
