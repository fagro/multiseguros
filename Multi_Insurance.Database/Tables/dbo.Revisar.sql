CREATE TABLE [dbo].[Revisar]
(
[cuenta] [float] NULL,
[certificado] [float] NULL,
[MEDIO DE PAGO] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOMBRES] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APELLIDOS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDENTIFICACION] [float] NULL,
[FECHA DE NACIMIENTO] [smalldatetime] NULL,
[EDAD] [float] NULL,
[COBERTURA] [float] NULL,
[PRIMA MENSUAL A/I] [float] NULL,
[PRIMA] [float] NULL
) ON [PRIMARY]
GO
