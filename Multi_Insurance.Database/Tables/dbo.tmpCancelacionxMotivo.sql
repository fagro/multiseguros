CREATE TABLE [dbo].[tmpCancelacionxMotivo]
(
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AT] [int] NULL,
[MT] [int] NULL,
[Motivo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[0] [int] NULL,
[1] [int] NULL,
[2] [int] NULL,
[3] [int] NULL,
[4] [int] NULL,
[5] [int] NULL,
[6] [int] NULL,
[>6] [int] NULL,
[Primas] [float] NULL,
[PAnual] [float] NULL
) ON [PRIMARY]
GO
