CREATE TABLE [dbo].[Tipos_Formatos]
(
[CodFor] [int] NOT NULL,
[DesFor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrdRegMovCon] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TipMedPagFor] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForRegMov] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumCamForRegMov] [smallint] NULL,
[LonForRegMov] [smallint] NULL,
[ForRegCon] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumCamForRegCon] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LonForRegCon] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tipos_Formatos] ADD CONSTRAINT [PK_Tipos_Formatos] PRIMARY KEY NONCLUSTERED  ([CodFor]) ON [PRIMARY]
GO
