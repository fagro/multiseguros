CREATE TABLE [dbo].[Companias_Seguros]
(
[NitComSeg] [int] NOT NULL IDENTITY(1, 1),
[NomComSeg] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DirComSeg] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelComSeg] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxComSeg] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NOT NULL CONSTRAINT [Def_Company_FecUltMod] DEFAULT (getdate()),
[CONTACTO] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Porciento] [smallint] NOT NULL,
[RNC] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Companias_Seguros] ADD CONSTRAINT [PK__Compania__0FBE1369B82BE6FA] PRIMARY KEY CLUSTERED  ([NitComSeg]) ON [PRIMARY]
GO
