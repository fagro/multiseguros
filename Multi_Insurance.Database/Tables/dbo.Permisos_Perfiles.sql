CREATE TABLE [dbo].[Permisos_Perfiles]
(
[CodPer] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCon] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FecUltMod] [datetime] NULL,
[permiso] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Permisos_Perfiles] ADD CONSTRAINT [PK_Permisos_Perfiles] PRIMARY KEY NONCLUSTERED  ([CodPer], [CodCon]) ON [PRIMARY]
GO
