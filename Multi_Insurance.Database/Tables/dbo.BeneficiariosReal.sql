CREATE TABLE [dbo].[BeneficiariosReal]
(
[CodPro] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodCer] [int] NOT NULL,
[ConCer] [smallint] NOT NULL,
[NomBene] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ApeBene] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Consecutivo] [int] NULL,
[PorBene] [decimal] (18, 0) NOT NULL,
[ParentBene] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipDocIde] [smallint] NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DirBene] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TelBene] [varchar] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodCiu] [int] NULL,
[Email] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL CONSTRAINT [DF_BeneficiariosReal_FecUltMod] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BeneficiariosReal] ADD CONSTRAINT [PK_BeneficiariosReal_1__30] PRIMARY KEY CLUSTERED  ([CodPro], [CodCer], [ConCer], [NomBene], [ApeBene]) ON [PRIMARY]
GO
