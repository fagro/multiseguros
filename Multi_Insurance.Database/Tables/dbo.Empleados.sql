CREATE TABLE [dbo].[Empleados]
(
[CodTipDocIde] [smallint] NOT NULL,
[NumDocIde] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomEmp] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApeEmp] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ape2Emp] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FecUltMod] [datetime] NULL,
[CodTipMedPag] [float] NULL,
[NumCue] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meta] [int] NULL,
[FecIngreso] [datetime] NULL,
[FecTraslado] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Empleados] ADD CONSTRAINT [PK_Empleados1_1__15] PRIMARY KEY CLUSTERED  ([CodTipDocIde], [NumDocIde]) ON [PRIMARY]
GO
