CREATE TABLE [dbo].[ParametersControl]
(
[ProductCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KeyWord] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OCode] [int] NOT NULL,
[NumericData] [int] NULL,
[DoubleData] [money] NULL,
[StringData] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BooleanData] [bit] NULL,
[DateData] [datetime] NULL,
[Status] [tinyint] NULL CONSTRAINT [DF_ParametersControl_Status] DEFAULT ((1)),
[CUser] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WDate] [datetime] NULL CONSTRAINT [DF_ParametersControl_WDate] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParametersControl] ADD CONSTRAINT [PK_ParametersControl] PRIMARY KEY NONCLUSTERED  ([KeyWord], [ProductCode], [OCode]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
