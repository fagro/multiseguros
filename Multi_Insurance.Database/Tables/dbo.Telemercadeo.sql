CREATE TABLE [dbo].[Telemercadeo]
(
[CodTel] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomTel] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipRam] [smallint] NULL,
[ObjTel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesTel] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumPerLla] [int] NULL,
[NumTom] [float] NULL,
[MonTel] [float] NULL,
[EdaCli] [int] NULL,
[EdaCer] [int] NULL,
[CodTipMedPag] [smallint] NULL,
[CodCiu] [smallint] NULL,
[CodTipSex] [smallint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
