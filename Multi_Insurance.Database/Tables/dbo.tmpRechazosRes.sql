CREATE TABLE [dbo].[tmpRechazosRes]
(
[CodPro] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NomPro] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodOfi] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NomOfi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodTipEstRec] [smallint] NULL,
[NomTipEstRec] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Recaudos] [float] NULL,
[VrAseg] [float] NULL,
[Recibos] [int] NULL
) ON [PRIMARY]
GO
