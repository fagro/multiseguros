CREATE TABLE [dbo].[Formatos_Controles]
(
[FORMATO] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[POS] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ORDEN] [float] NOT NULL,
[TABLA] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAMPO] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LONGITUD] [float] NULL,
[ALINEAR] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RELLENO] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FORMATEAR] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DECIMALES] [float] NULL,
[SEPARADOR] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIPO] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOREGISTRO] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Formatos_Controles] ADD CONSTRAINT [PK_Controles_Formatos] PRIMARY KEY CLUSTERED  ([FORMATO], [POS], [ORDEN]) ON [PRIMARY]
GO
