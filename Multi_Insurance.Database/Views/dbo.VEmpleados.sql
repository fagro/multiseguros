SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VEmpleados]
AS
SELECT     dbo.Empleados.CodTipDocIde, dbo.Empleados.NumDocIde, dbo.Empleados.CodOfi, dbo.Oficinas.NomOfi, dbo.Empleados.NomEmp, 
                      dbo.Empleados.ApeEmp, dbo.Tipos_Medio_Pago.NomTipMedPag, dbo.Empleados.NumCue
FROM         dbo.Empleados LEFT OUTER JOIN
                      dbo.Tipos_Medio_Pago ON dbo.Empleados.CodTipMedPag = dbo.Tipos_Medio_Pago.CodTipMedPag LEFT OUTER JOIN
                      dbo.Oficinas ON dbo.Empleados.CodOfi = dbo.Oficinas.CodOfi







GO
