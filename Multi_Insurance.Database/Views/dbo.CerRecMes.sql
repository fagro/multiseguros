SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CerRecMes]
AS
SELECT     dbo.Certificados.CodPro, YEAR(dbo.Certificados.FecExpCer) AS AE, MONTH(dbo.Certificados.FecExpCer) AS ME, dbo.Certificados.CodCer, 
                      dbo.Certificados.NumDocIdeEmp, dbo.Certificados.ValPriCer, dbo.Certificados.MonOpc, dbo.Recibos.CodTipEstRec
FROM         dbo.Recibos INNER JOIN
                      dbo.Certificados ON dbo.Recibos.CodPro = dbo.Certificados.CodPro AND dbo.Recibos.CodCer = dbo.Certificados.CodCer AND 
                      dbo.Recibos.ConCer = dbo.Certificados.ConCer AND YEAR(dbo.Recibos.FecEnvRec) = YEAR(dbo.Certificados.FecExpCer) AND 
                      MONTH(dbo.Recibos.FecEnvRec) = MONTH(dbo.Certificados.FecExpCer)
WHERE     (dbo.Recibos.CodTipEstRec = 9)







GO
