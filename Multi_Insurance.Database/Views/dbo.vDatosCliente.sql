SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vDatosCliente]
AS
SELECT     dbo.Clientes.NumDocIde, dbo.Clientes.NomCli, dbo.Clientes.ApeCli, dbo.Clientes.DirCli, dbo.Clientes.FecNacCli, dbo.Tipos_Sexos.NomTipSex, 
                      dbo.Tipos_Estado_Civil.NomTipEstCivil, dbo.Tipos_Ocupacion.NomTipOcu, dbo.Ciudades.NomCiu, dbo.Clientes.TelCli, dbo.Clientes.TELREC, 
                      dbo.Clientes.telmovil, dbo.Clientes.nohijos
FROM         dbo.Clientes INNER JOIN
                      dbo.Tipos_Sexos ON dbo.Clientes.CodTipSex = dbo.Tipos_Sexos.CodTipSex INNER JOIN
                      dbo.Tipos_Estado_Civil ON dbo.Clientes.codtipestcivil = dbo.Tipos_Estado_Civil.CodTipEstCivil INNER JOIN
                      dbo.Tipos_Ocupacion ON dbo.Clientes.CodTipOcu = dbo.Tipos_Ocupacion.CodTipOcu INNER JOIN
                      dbo.Ciudades ON dbo.Clientes.CodCiu = dbo.Ciudades.CodCiu







GO
