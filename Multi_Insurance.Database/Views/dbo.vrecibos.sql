SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  View dbo.vrecibos    Script Date: 11/07/2000 14:29:20 *****
***** Object:  View dbo.vrecibos    Script Date: 19/05/00 19:58:06 *****
*/
CREATE VIEW [dbo].[vrecibos]
AS
SELECT     dbo.Recibos.CodPro, dbo.Recibos.CodCer, dbo.Recibos.ConCer, dbo.Recibos.FecRec, dbo.Recibos.FecRec AS Fecha1, 
                      dbo.Recibos.FecIniPerCob AS Fecini1, dbo.Recibos.FecFinPerCob AS FecFin1, dbo.Recibos.FecEnvRec, dbo.Recibos.FecEnvRec AS FecEnvRec1, 
                      dbo.Tipos_Estado_Recibo.NomTipEstRec, CONVERT(char(10), CONVERT(money, dbo.Recibos.ValRec), 1) AS ValRec
FROM         dbo.Recibos INNER JOIN
                      dbo.Tipos_Estado_Recibo ON dbo.Recibos.CodTipEstRec = dbo.Tipos_Estado_Recibo.CodTipEstRec







GO
