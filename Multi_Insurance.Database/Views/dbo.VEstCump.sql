SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VEstCump]
AS
SELECT     dbo.Productos.NomPro AS Producto, dbo.ZONASPATPRO.NOMZONA AS NZona, dbo.ZONASPATPRO.RESPONSABLE AS Responsable, 
                      dbo.Oficinas.NomOfi AS Oficina, dbo.EstadisticasCump.CodPro, dbo.EstadisticasCump.AnoEst, dbo.EstadisticasCump.MesEst, 
                      dbo.EstadisticasCump.CodOfi, dbo.EstadisticasCump.MetaCerMen, dbo.EstadisticasCump.VentasCerMen, dbo.EstadisticasCump.CumpVtasMes, 
                      dbo.EstadisticasCump.MetaCerAnual, dbo.EstadisticasCump.VentasCerAnual, dbo.EstadisticasCump.CumpVtasAnual, 
                      dbo.EstadisticasCump.MetaRecaudoMes, dbo.EstadisticasCump.ValRecaudoMes, dbo.EstadisticasCump.CumpRecaudoMes, 
                      dbo.EstadisticasCump.MetaRecaudoAnual, dbo.EstadisticasCump.ValRecaudoAnual, dbo.EstadisticasCump.CumpRecaudoAnual, 
                      dbo.EstadisticasCump.MetaValAsegMes, dbo.EstadisticasCump.ValAsegMes, dbo.EstadisticasCump.CumpValAsegMes, 
                      dbo.EstadisticasCump.MetaValAsegAnual, dbo.EstadisticasCump.ValAsegAnual, dbo.EstadisticasCump.CumpValAsegAnual, 
                      dbo.EstadisticasCump.Zona, dbo.EstadisticasCump.ZonaAseg
FROM         dbo.EstadisticasCump INNER JOIN
                      dbo.Productos ON dbo.EstadisticasCump.CodPro = dbo.Productos.CodPro LEFT OUTER JOIN
                      dbo.Oficinas ON dbo.EstadisticasCump.CodOfi = dbo.Oficinas.CodOfi LEFT OUTER JOIN
                      dbo.ZONASPATPRO ON dbo.EstadisticasCump.Zona = dbo.ZONASPATPRO.CODZONA







GO
