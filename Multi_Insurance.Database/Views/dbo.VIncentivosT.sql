SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VIncentivosT]
AS
SELECT     dbo.IncentivosWeb.CodPro, dbo.IncentivosWeb.AnoP, dbo.IncentivosWeb.MesP, dbo.IncentivosWeb.Codigo, CONVERT(char(15), CONVERT(money, 
                      SUM(dbo.Opcion_Producto.MonOpc)), 1) AS MonOpc, CONVERT(char(10), CONVERT(money, SUM(dbo.IncentivosWeb.IncenMes)), 1) AS IncenMes, 
                      CONVERT(char(10), CONVERT(money, SUM(dbo.IncentivosWeb.RecaudoMes)), 1) AS RecaudoMes, CONVERT(char(10), CONVERT(money, 
                      SUM(dbo.IncentivosWeb.IncenTotal)), 1) AS IncenTotal, CONVERT(char(10), CONVERT(money, SUM(dbo.IncentivosWeb.IncenCobrado)), 1) 
                      AS IncenCobrado
FROM         dbo.Tipos_Estados_Certificado INNER JOIN
                      dbo.IncentivosWeb INNER JOIN
                      dbo.Tipos_Forma_Pago ON dbo.IncentivosWeb.CodTipForPag = dbo.Tipos_Forma_Pago.CodTipForPag ON 
                      dbo.Tipos_Estados_Certificado.CodTipEstCer = dbo.IncentivosWeb.CodTipEstCer LEFT OUTER JOIN
                      dbo.Opcion_Producto ON dbo.IncentivosWeb.CodPro = dbo.Opcion_Producto.CodPro AND 
                      dbo.IncentivosWeb.CodTipOpc = dbo.Opcion_Producto.CodTipOpc
GROUP BY dbo.IncentivosWeb.CodPro, dbo.IncentivosWeb.Codigo, dbo.IncentivosWeb.AnoP, dbo.IncentivosWeb.MesP







GO
