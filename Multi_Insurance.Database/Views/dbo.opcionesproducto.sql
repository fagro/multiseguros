SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** Object:  View dbo.opcionesproducto    Script Date: 11/07/2000 14:29:20 *****
*/
CREATE VIEW [dbo].[opcionesproducto]
AS
SELECT     dbo.Opcion_Producto.CodPro, dbo.Opcion_Producto.CodTipOpc, dbo.Tipos_Opcion.NomTipOpc, CONVERT(char(15), CONVERT(money, 
                      dbo.Opcion_Producto.MonOpc), 1) AS MonOpc
FROM         dbo.Opcion_Producto INNER JOIN
                      dbo.Tipos_Opcion ON dbo.Opcion_Producto.CodTipOpc = dbo.Tipos_Opcion.CodTipOpc







GO
