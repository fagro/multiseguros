SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[impcert1] AS SELECT Productos.NomPro, Productos.CodPro, Tipos_Medio_Pago.NomTipMedPag, Certificados.NumCue, Certificados.CodCer, Certificados.ConCer, NomCli + ' ' + ApeCli AS NombreCliente, Clientes.NumDocIde, Clientes.FecNacCli, Clientes.DirCli, Clientes.TelCli, Ciudades.NomCiu, Tipos_Opcion.NomTipOpc, Tipos_Forma_Pago.NomTipForPag, Certificados.MonOpc, Certificados.ValPriCer, Certificados.FecExpCer FROM Certificados, Tipos_Medio_Pago, Clientes, Productos,Ciudades, Opcion_Producto, Tipos_Opcion,Tipos_Forma_Pago 
Where (Certificados.CodTipMedPag = Tipos_Medio_Pago.CodTipMedPag and Certificados.CodTipDocIde = Clientes.CodTipDocIde and Certificados.NumDocIde = Clientes.NumDocIde and Certificados.CodPro = Productos.CodPro and Clientes.CodCiu = Ciudades.CodCiu and Certificados.CodPro = Opcion_Producto.CodPro and Certificados.CodTipOpc = Opcion_Producto.CodTipOpc and Certificados.CodTipOpc = Tipos_Opcion.CodTipOpc and Certificados.CodTipForPag = Tipos_Forma_Pago.CodTipForPag)







GO
