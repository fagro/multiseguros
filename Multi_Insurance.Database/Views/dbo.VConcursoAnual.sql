SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VConcursoAnual]
AS
SELECT     TOP 15 PERCENT dbo.Certificados.CodPro, YEAR(dbo.Recibos.FecEnvRec) AS AR, dbo.Certificados.NumDocIdeEmp AS Codigo, 
                      dbo.Empleados.NomEmp, dbo.Empleados.ApeEmp, dbo.Oficinas.CodOfi, dbo.Oficinas.NomOfi, dbo.ZONASPATPRO.NOMZONA, CONVERT(char(12), 
                      CONVERT(money, SUM(dbo.Recibos.ValRec)), 1) AS VR, dbo.Recibos.CodTipEstRec
FROM         dbo.ZONASPATPRO INNER JOIN
                      dbo.Oficinas ON dbo.ZONASPATPRO.CODZONA = dbo.Oficinas.zona RIGHT OUTER JOIN
                      dbo.Certificados INNER JOIN
                      dbo.Recibos ON dbo.Certificados.CodPro = dbo.Recibos.CodPro AND dbo.Certificados.CodCer = dbo.Recibos.CodCer AND 
                      dbo.Certificados.ConCer = dbo.Recibos.ConCer INNER JOIN
                      dbo.Empleados ON dbo.Certificados.NumDocIdeEmp = dbo.Empleados.NumDocIde ON dbo.Oficinas.CodOfi = dbo.Empleados.CodOfi
WHERE     (dbo.Recibos.CodTipEstRec = 9)
GROUP BY dbo.Certificados.CodPro, YEAR(dbo.Recibos.FecEnvRec), dbo.Recibos.CodTipEstRec, dbo.Certificados.NumDocIdeEmp, dbo.Empleados.NomEmp, 
                      dbo.Empleados.ApeEmp, dbo.Oficinas.NomOfi, dbo.Oficinas.CodOfi, dbo.ZONASPATPRO.NOMZONA
HAVING      (YEAR(dbo.Recibos.FecEnvRec) >= 2004)
ORDER BY YEAR(dbo.Recibos.FecEnvRec), SUM(dbo.Recibos.ValRec) DESC







GO
