SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[EstCertificados]
AS
SELECT     TOP 100 PERCENT CodPro, YEAR(FecExpCer) AS AE, MONTH(FecExpCer) AS ME, CodOfi, COUNT(CodCer) AS Cert, SUM(MonOpc) AS VrAseg, 
                      CodTipEstCer
FROM         dbo.Certificados
WHERE     (CodTipEstCer = 1)
GROUP BY CodPro, YEAR(FecExpCer), MONTH(FecExpCer), CodOfi, CodTipEstCer
HAVING      (CodPro <> 'xx') AND (YEAR(FecExpCer) >= 2004)
ORDER BY YEAR(FecExpCer), MONTH(FecExpCer), CodOfi







GO
