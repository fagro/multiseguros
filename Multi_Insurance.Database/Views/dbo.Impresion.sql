SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Impresion]
AS
SELECT     dbo.Productos.NomPro, dbo.Productos.CodPro, dbo.Tipos_Medio_Pago.NomTipMedPag, dbo.Certificados.NumCue, dbo.Certificados.CodCer, 
                      RTRIM(dbo.Clientes.NomCli) + ' ' + RTRIM(dbo.Clientes.ApeCli) AS NombreCliente, dbo.Clientes.NumDocIde, dbo.Clientes.FecNacCli, 
                      dbo.Clientes.DirCli, dbo.Clientes.TelCli, dbo.Ciudades.NomCiu, dbo.Tipos_Opcion.NomTipOpc, dbo.Tipos_Forma_Pago.NomTipForPag, 
                      dbo.Certificados.MonOpc, dbo.Certificados.ValPriCer, dbo.Certificados.FecExpCer, dbo.BeneficiariosReal.PorBene, 
                      dbo.BeneficiariosReal.NomBene + ' ' + dbo.BeneficiariosReal.ApeBene AS Beneficiario, dbo.PARENTESCOS.NOMPARENT,
         	      RTRIM(dbo.Clientes.NomCli) as NomCli,RTRIM(dbo.Clientes.ApeCli) as ApeCli	
FROM         dbo.Certificados INNER JOIN
                      dbo.Tipos_Medio_Pago ON dbo.Certificados.CodTipMedPag = dbo.Tipos_Medio_Pago.CodTipMedPag INNER JOIN
                      dbo.Clientes ON dbo.Certificados.CodTipDocIde = dbo.Clientes.CodTipDocIde AND dbo.Certificados.NumDocIde = dbo.Clientes.NumDocIde INNER JOIN
                      dbo.Productos ON dbo.Certificados.CodPro = dbo.Productos.CodPro INNER JOIN
                      dbo.Ciudades ON dbo.Clientes.CodCiu = dbo.Ciudades.CodCiu INNER JOIN
                      dbo.Opcion_Producto ON dbo.Certificados.CodPro = dbo.Opcion_Producto.CodPro AND 
                      dbo.Certificados.CodTipOpc = dbo.Opcion_Producto.CodTipOpc INNER JOIN
                      dbo.Tipos_Opcion ON dbo.Certificados.CodTipOpc = dbo.Tipos_Opcion.CodTipOpc INNER JOIN
                      dbo.Tipos_Forma_Pago ON dbo.Certificados.CodTipForPag = dbo.Tipos_Forma_Pago.CodTipForPag LEFT OUTER JOIN
                      dbo.BeneficiariosReal ON dbo.Certificados.CodPro = dbo.BeneficiariosReal.CodPro AND dbo.Certificados.CodCer = dbo.BeneficiariosReal.CodCer AND 
                      dbo.Certificados.ConCer = dbo.BeneficiariosReal.ConCer LEFT OUTER JOIN
                      dbo.PARENTESCOS ON dbo.BeneficiariosReal.ParentBene = dbo.PARENTESCOS.CODPARENT







GO
