SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[View_WOpcionProducto]
AS
SELECT op.CodPro AS	[CodigoProducto],
		tp.CodTipOpc AS [CodigoTipoOpcion],
		tp.NomTipOpc  [NombreTipoOpcion],
		pm.CodTipForPag AS [CodigoTipoFormaDePago],
		tf.NomTipForPag AS [NombreTipoFormaDePago],
		mp.CodTipMedPag AS [CodigoTipoMedioDePago],
		tm.NomTipMedPag AS [NombreTipoMedioDePago],
		tp.InfoAdicional As [InfoAdicional],
		dp.Descripcion As [Mensaje],
		v.CodigoVigencia As [CodigoVigencia],
		v.Vigencia  AS [Vigencia]

FROM Opcion_Producto AS op
		
		INNER JOIN Tipos_Opcion AS tp
		on op.CodTipOpc = tp.CodTipOpc
		
		INNER JOIN Primas_Amparo AS pm
		on pm.CodPro = op.CodPro
		
		INNER JOIN Tipos_Forma_Pago AS tf
		on tf.CodTipForPag = pm.CodTipForPag
		
		INNER JOIN Medios_Pago_Producto As mp
		on mp.CodPro =  op.CodPro
		
		INNER JOIN Tipos_Medio_Pago AS tm
		on tm.CodTipMedPag = mp.CodTipMedPag
		
		INNER JOIN DetalleProductos AS dp
		on  op.CodTipOpc = dp.CodTipOpc
			and op.CodPro = dp.CodPro

		INNER JOIN Vigencias As v
		on v.CodigoProducto = op.CodPro
		
GROUP BY op.CodPro, 
		tp.CodTipOpc, 
		tp.NomTipOpc, 
		pm.CodTipForPag, 
		tf.NomTipForPag,
		mp.CodTipMedPag,
		tm.NomTipMedPag,
		tp.InfoAdicional,
		dp.Descripcion,
		v.CodigoVigencia,
		v.Vigencia







GO
