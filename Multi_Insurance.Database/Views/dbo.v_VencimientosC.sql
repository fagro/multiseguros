SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE  VIEW [dbo].[v_VencimientosC]
AS
SELECT  B.CodPro, B.CodCer, B.ConCer, B.CodTipEstCer, B.CodTipForPag, B.CodTipMedPag, B.CodTipOpc, 
	B.NumCue, B.ValPriCer, B.FecTerCer AS Minimo, 
        DATEPART(m, B.FecTerCer) AS Expr1, B.CodOfi, B.NumDocIde, LTRIM(RTRIM(A.ApeCli)) + ' ' + LTRIM(RTRIM(A.NomCli)) AS Nombre, A.DirCli, 
	A.TelCli, A.FecNacCli, C.NomPro, 
        B.ValPriCer * B.CodTipForPag AS anual, E.NomCiu, D.MonOpc, DATEPART(m, B.FecDigCer) AS mesdesen, 
	DATEPART(m, B.FecExpCer) AS mesexp, 
        B.FecExpCer, B.NumMet, B.NumDocIdeEmp,f.nomofi,B.fecTerCer,datediff(year,a.fecnaccli,getdate())  as Edad, b.Observaciones
FROM    dbo.Clientes A INNER JOIN
        dbo.Certificados B ON A.NumDocIde = B.NumDocIde AND A.CodTipDocIde = B.CodTipDocIde INNER JOIN
        dbo.Productos C ON B.CodPro = C.CodPro INNER JOIN
        dbo.Opcion_Producto D ON B.CodPro = D.CodPro AND B.CodTipOpc = D.CodTipOpc INNER JOIN
        dbo.Ciudades E ON A.CodCiu = E.CodCiu inner join
	dbo.oficinas F on B.codofi = F.codofi







GO
