SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  View dbo.vbeneficiarios    Script Date: 11/07/2000 14:29:20 ******/
CREATE VIEW [dbo].[vbeneficiarios] AS
SELECT BeneficiariosReal.CodPro, BeneficiariosReal.CodCer, BeneficiariosReal.ConCer, BeneficiariosReal.NomBene, BeneficiariosReal.ApeBene, BeneficiariosReal.PorBene, PARENTESCOS.NOMPARENT
FROM BeneficiariosReal LEFT JOIN PARENTESCOS ON BeneficiariosReal.ParentBene = PARENTESCOS.CODPARENT







GO
