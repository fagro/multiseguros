SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vcertificados]
AS
SELECT     RTRIM(dbo.Clientes.NomCli) + ' ' + RTRIM(dbo.Clientes.ApeCli) AS Nombre, dbo.Clientes.nohijos AS Hijos, dbo.Clientes.TelCli, dbo.Clientes.DirCli, 
                      dbo.Ciudades.NomCiu, dbo.Certificados.CodCer, dbo.Productos.NomPro, CONVERT(char(10), CONVERT(money, dbo.Certificados.ValPriCer), 1) 
                      AS ValPriCer, dbo.Certificados.NumDocIde, dbo.Certificados.FecExpCer, dbo.Certificados.NumCue, dbo.Tipos_Forma_Pago.NomTipForPag, 
                      dbo.Tipos_Estados_Certificado.NomTipEstCer, dbo.Tipos_Medio_Pago.NomTipMedPag, dbo.Certificados.CodPro, dbo.Certificados.ConCer, 
                      dbo.Certificados.CodTipDocIde, dbo.Tipos_Opcion.NomTipOpc, CONVERT(char(15), CONVERT(money, dbo.Certificados.MonOpc), 1) AS MonOpc, 
                      dbo.Certificados.CodTipOpc + 1 AS Opcion, dbo.Clientes.FecNacCli, dbo.Tipos_Ocupacion.NomTipOcu, dbo.Oficinas.NomOfi, 
                      dbo.Tipos_Estado_Civil.NomTipEstCivil, dbo.Certificados.MonOpc AS VrAseg, dbo.Certificados.NumDocIdeEmp AS CodEmp
FROM         dbo.Certificados INNER JOIN
                      dbo.Clientes ON dbo.Certificados.NumDocIde = dbo.Clientes.NumDocIde AND dbo.Certificados.CodTipDocIde = dbo.Clientes.CodTipDocIde INNER JOIN
                      dbo.Productos ON dbo.Certificados.CodPro = dbo.Productos.CodPro INNER JOIN
                      dbo.Ciudades ON dbo.Clientes.CodCiu = dbo.Ciudades.CodCiu INNER JOIN
                      dbo.Tipos_Medio_Pago ON dbo.Certificados.CodTipMedPag = dbo.Tipos_Medio_Pago.CodTipMedPag INNER JOIN
                      dbo.Tipos_Estados_Certificado ON dbo.Certificados.CodTipEstCer = dbo.Tipos_Estados_Certificado.CodTipEstCer INNER JOIN
                      dbo.Tipos_Forma_Pago ON dbo.Certificados.CodTipForPag = dbo.Tipos_Forma_Pago.CodTipForPag INNER JOIN
                      dbo.Tipos_Opcion ON dbo.Certificados.CodTipOpc = dbo.Tipos_Opcion.CodTipOpc INNER JOIN
                      dbo.Tipos_Ocupacion ON dbo.Clientes.CodTipOcu = dbo.Tipos_Ocupacion.CodTipOcu LEFT OUTER JOIN
                      dbo.Tipos_Estado_Civil ON dbo.Clientes.codtipestcivil = dbo.Tipos_Estado_Civil.CodTipEstCivil LEFT OUTER JOIN
                      dbo.Oficinas ON dbo.Certificados.CodOfi = dbo.Oficinas.CodOfi







GO
