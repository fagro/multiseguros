SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VConcursoMes]
AS
SELECT     TOP 100 PERCENT dbo.CerRecMes.CodPro, dbo.CerRecMes.AE, dbo.CerRecMes.ME, dbo.ZONASPATPRO.CODZONA, dbo.ZONASPATPRO.NOMZONA, 
                      dbo.Oficinas.CodOfi, dbo.Oficinas.NomOfi, dbo.CerRecMes.NumDocIdeEmp, dbo.Empleados.NomEmp, dbo.Empleados.ApeEmp, CONVERT(char(8), 
                      CONVERT(integer, COUNT(dbo.CerRecMes.CodCer)), 1) AS CertV, CONVERT(char(12), CONVERT(money, SUM(dbo.CerRecMes.ValPriCer)), 1) AS Prima, 
                      CONVERT(char(15), CONVERT(money, SUM(dbo.CerRecMes.MonOpc)), 1) AS VAseg
FROM         dbo.ZONASPATPRO INNER JOIN
                      dbo.Oficinas ON dbo.ZONASPATPRO.CODZONA = dbo.Oficinas.zona RIGHT OUTER JOIN
                      dbo.CerRecMes INNER JOIN
                      dbo.Empleados ON dbo.CerRecMes.NumDocIdeEmp = dbo.Empleados.NumDocIde ON dbo.Oficinas.CodOfi = dbo.Empleados.CodOfi
GROUP BY dbo.CerRecMes.CodPro, dbo.CerRecMes.AE, dbo.CerRecMes.ME, dbo.ZONASPATPRO.CODZONA, dbo.ZONASPATPRO.NOMZONA, 
                      dbo.Oficinas.CodOfi, dbo.CerRecMes.NumDocIdeEmp, dbo.Empleados.NomEmp, dbo.Empleados.ApeEmp, dbo.Oficinas.NomOfi
HAVING      (COUNT(dbo.CerRecMes.CodCer) >= 10)
ORDER BY dbo.CerRecMes.AE, dbo.CerRecMes.ME, dbo.ZONASPATPRO.CODZONA, COUNT(dbo.CerRecMes.CodCer) DESC, SUM(dbo.CerRecMes.MonOpc)







GO
