SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[EstRecaudo]
AS
SELECT     TOP 100 PERCENT dbo.Certificados.CodPro, YEAR(dbo.Recibos.FecEnvRec) AS AR, MONTH(dbo.Recibos.FecEnvRec) AS MR, dbo.Certificados.CodOfi, 
                      'RD$' + CONVERT(char(10), CONVERT(money, SUM(dbo.Recibos.ValRec)), 1) AS VR, SUM(dbo.Recibos.ValRec) AS VRec, 
                      dbo.Recibos.CodTipEstRec
FROM         dbo.Certificados LEFT OUTER JOIN
                      dbo.Recibos ON dbo.Certificados.CodPro = dbo.Recibos.CodPro AND dbo.Certificados.CodCer = dbo.Recibos.CodCer AND 
                      dbo.Certificados.ConCer = dbo.Recibos.ConCer
WHERE     (dbo.Recibos.CodTipEstRec = 9)
GROUP BY dbo.Certificados.CodPro, YEAR(dbo.Recibos.FecEnvRec), MONTH(dbo.Recibos.FecEnvRec), dbo.Certificados.CodOfi, 
                      dbo.Recibos.CodTipEstRec
HAVING      (YEAR(dbo.Recibos.FecEnvRec) >= 2004)
ORDER BY YEAR(dbo.Recibos.FecEnvRec), MONTH(dbo.Recibos.FecEnvRec), dbo.Certificados.CodOfi







GO
