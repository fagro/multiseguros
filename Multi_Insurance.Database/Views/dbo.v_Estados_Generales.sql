SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[v_Estados_Generales]
AS
SELECT     dbo.Tipos_Estado_Recibo.CodTipEstRec AS Est, dbo.Tipos_Estados_Certificado.NomTipEstCer AS Certificados, 
                      dbo.Tipos_Estado_Recibo.NomTipEstRec AS Recibos
FROM         dbo.Tipos_Estado_Recibo LEFT OUTER JOIN
                      dbo.Tipos_Estados_Certificado ON dbo.Tipos_Estado_Recibo.CodTipEstRec = dbo.Tipos_Estados_Certificado.CodTipEstCer







GO
