SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  VIEW [dbo].[v_Vencimientos]
AS
SELECT     B.CodPro, B.CodCer, B.ConCer, B.CodTipEstCer, B.CodTipForPag, B.CodTipMedPag, B.CodTipOpc, B.NumCue, B.ValPriCer, B.FecExpCer AS Minimo, 
                      DATEPART([month], B.FecExpCer) AS Expr1, B.CodOfi, B.NumDocIde, LTRIM(RTRIM(A.ApeCli)) + ' ' + LTRIM(RTRIM(A.NomCli)) AS Nombre, A.DirCli, 
                      A.TelCli, A.FecNacCli, C.NomPro, B.ValPriCer * B.CodTipForPag AS anual, E.NomCiu, D.MonOpc, DATEPART([month], B.FecDigCer) AS mesdesen, 
                      DATEPART([month], B.FecExpCer) AS mesexp, B.FecDigCer, B.NumMet, F.NomOfi, B.USUARIO,datediff(year,a.fecnaccli,getdate())  as Edad
FROM         dbo.Clientes A INNER JOIN
                      dbo.Certificados B ON A.NumDocIde = B.NumDocIde AND A.CodTipDocIde = B.CodTipDocIde INNER JOIN
                      dbo.Productos C ON B.CodPro = C.CodPro INNER JOIN
                      dbo.Opcion_Producto D ON B.CodPro = D.CodPro AND B.CodTipOpc = D.CodTipOpc INNER JOIN
                      dbo.Ciudades E ON A.CodCiu = E.CodCiu LEFT OUTER JOIN
                      dbo.Oficinas F ON B.CodOfi = F.CodOfi







GO
