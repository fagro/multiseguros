SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  View dbo.carbien    Script Date: 11/07/2000 14:29:20 ******/
CREATE VIEW [dbo].[carbien] AS
SELECT cartasbien.CodOfi, cartasbien.CodCer, cartasbien.NumDocIde,
       cartasbien.ValPriCer, cartasbien.NumCue, cartasbien.FecExpCer,
       FecExpCer1,
       cartasbien.FecDigCer, Opcion_Producto.MonOpc, 
       Nomcli + ' ' + ApeCli + ' ' + Ape2Cli AS NombreCliente, 
       Clientes.DirCli, Clientes.TelCli, cartasbien.CodPro, 
       Productos.NomPro, cartasbien.CodTipEstCer, cartasbien.FecUltMod,
       FecTerCer1, 
       cartasbien.CodTipOpc, Ciudades.Nomciu
FROM   cartasbien

INNER JOIN clientes
	ON	cartasbien.NumDocIde = Clientes.NumDocIde
		AND cartasbien.NumDocIde = Clientes.NumDocIde
	
INNER JOIN Opcion_Producto
	ON	cartasbien.CodPro = Opcion_Producto.CodPro
        AND cartasbien.CodTipOpc = Opcion_Producto.CodTipOpc
        
INNER JOIN Productos
	ON cartasbien.CodPro = Productos.CodPro
	
LEFT JOIN Ciudades 
	ON clientes.codciu = ciudades.codciu

/*ORDER BY Certificados.CodOfi*/







GO
