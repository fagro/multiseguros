SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VRecMensualEmp]
AS
SELECT     TOP 100 PERCENT dbo.Certificados.CodPro, YEAR(dbo.Certificados.FecExpCer) AS AE, MONTH(dbo.Certificados.FecExpCer) AS ME, dbo.Oficinas.zona, 
                      dbo.VEmpleados.CodOfi, dbo.VEmpleados.NomOfi, dbo.Certificados.NumDocIdeEmp AS CodEmp, dbo.Certificados.CodCer, dbo.Recibos.ValRec, 
                      dbo.Certificados.MonOpc, dbo.VEmpleados.NomEmp, dbo.VEmpleados.NomTipMedPag, dbo.VEmpleados.NumCue, dbo.ZONASPATPRO.NOMZONA, 
                      dbo.ZONASPATPRO.RESPONSABLE
FROM         dbo.Oficinas LEFT OUTER JOIN
                      dbo.ZONASPATPRO ON dbo.Oficinas.zona = dbo.ZONASPATPRO.CODZONA AND 
                      dbo.Oficinas.NitPat = dbo.ZONASPATPRO.NITPATPRO RIGHT OUTER JOIN
                      dbo.VEmpleados ON dbo.Oficinas.CodOfi = dbo.VEmpleados.CodOfi RIGHT OUTER JOIN
                      dbo.Recibos INNER JOIN
                      dbo.Certificados ON dbo.Recibos.CodPro = dbo.Certificados.CodPro AND dbo.Recibos.CodCer = dbo.Certificados.CodCer AND 
                      dbo.Recibos.ConCer = dbo.Certificados.ConCer AND YEAR(dbo.Recibos.FecEnvRec) = YEAR(dbo.Certificados.FecExpCer) AND 
                      MONTH(dbo.Recibos.FecEnvRec) = MONTH(dbo.Certificados.FecExpCer) ON dbo.VEmpleados.NumDocIde = dbo.Certificados.NumDocIdeEmp
WHERE     (dbo.Recibos.CodTipEstRec = 9)
ORDER BY YEAR(dbo.Certificados.FecExpCer), MONTH(dbo.Certificados.FecExpCer), dbo.Oficinas.zona, dbo.VEmpleados.CodOfi







GO
