SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VOficinasZonas]
AS
SELECT     TOP 100 PERCENT dbo.Productos.CodPro, dbo.Productos.NomPro, dbo.Oficinas.*, dbo.ZONASPATPRO.NOMZONA, 
                      dbo.ZONASPATPRO.RESPONSABLE
FROM         dbo.Productos INNER JOIN
                      dbo.ZONASPATPRO ON dbo.Productos.NitPatPro = dbo.ZONASPATPRO.NITPATPRO INNER JOIN
                      dbo.Oficinas ON dbo.Productos.NitPatPro = dbo.Oficinas.NitPat AND dbo.ZONASPATPRO.CODZONA = dbo.Oficinas.zona
ORDER BY dbo.Productos.CodPro, dbo.Oficinas.CodOfi







GO
