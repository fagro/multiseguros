SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/****** Object:  View dbo.estadocuenta1    Script Date: 11/07/2000 14:29:20 ******/
CREATE VIEW [dbo].[estadocuenta1] AS 
select 
  certificados.*, 
  clientes.Nomcli,
  clientes.apecli,
  clientes.ape2cli,
  clientes.dircli,
  clientes.telcli,
  clientes.codciu,
  clientes.fecnaccli,
  clientes.codtipsex,
  recibos.fecrec,
  recibos.valrec,
  recibos.fecenvrec,
  recibos.codtipestrec,
  recibos.fecinipercob,
  recibos.fecfinpercob,
  oficinas.nomofi,
  productos.nompro,
  tipos_estados_certificado.nomtipestcer,
  Tipos_forma_pago.nomtipforpag,
  tipos_medio_pago.nomtipmedpag,
  tipos_estado_recibo.nomtipestrec 
FROM certificados
INNER JOIN clientes
	ON	clientes.codtipdocide = certificados.codtipdocide
		AND clientes.codtipdocide = certificados.codtipdocide 
		AND clientes.numdocide = certificados.numdocide
	
INNER JOIN recibos
	ON	certificados.codpro = recibos.codpro
		AND certificados.codcer = recibos.codcer
		AND certificados.concer = recibos.concer
	
LEFT JOIN oficinas
	ON certificados.codofi = oficinas.codofi
	
LEFT JOIN productos
	ON certificados.codpro = productos.codpro
	
LEFT JOIN tipos_estados_certificado
	ON certificados.codtipestcer = tipos_estados_certificado.codtipestcer
	
LEFT JOIN Tipos_forma_pago
	ON certificados.codtipforpag = tipos_forma_pago.codtipforpag
	
LEFT JOIN tipos_medio_pago
	ON certificados.codtipmedpag = tipos_medio_pago.codtipmedpag
	
LEFT JOIN tipos_estado_recibo
	ON recibos.codtipestrec = tipos_estado_recibo.codtipestrec







GO
