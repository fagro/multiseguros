SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vbeneficiariosc]
AS
SELECT     dbo.BeneficiariosReal.CodPro, dbo.BeneficiariosReal.CodCer, dbo.BeneficiariosReal.ConCer, dbo.BeneficiariosReal.NomBene, 
                      dbo.BeneficiariosReal.ApeBene, dbo.BeneficiariosReal.PorBene, dbo.PARENTESCOS.NOMPARENT
FROM         dbo.BeneficiariosReal LEFT OUTER JOIN
                      dbo.PARENTESCOS ON dbo.BeneficiariosReal.ParentBene = dbo.PARENTESCOS.CODPARENT







GO
