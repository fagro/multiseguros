﻿using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class InsuranceCompany
    {

        public short Id { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "RNC")]
        public string Rnc { get; set; }

        [Display(Name = "Dirección")]
        public string Address { get; set; }

        [Display(Name = "Porciento")]
        public short Percentage { get; set; }

        [Display(Name = "Telefono")]
        public string Phone { get; set; }

        [Display(Name = "Contacto")]
        public string Contact { get; set; }

    }
}
