﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class BranchType
    {
        public int Id { get; set; }

        [Display(Name = "Código tipo de ramo")]
        public short Code { get; set; }

        [Display(Name = "Nombre del ramo")]
        public string Name { get; set; }

        public DateTime Modified { get; set; }
    }
}
