﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Dependent
    {      

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Display(Name = "Identificacion")]
        public string IdNumber { get; set; }

        [Display(Name = "Género")]
        public Int16 Gender { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        public DateTime? Birthday { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Display(Name = "Celular")]
        public string Cellphone { get; set; }

        [Display(Name = "Parentesco")]
        public int IdRelationship { get; set; }

        [Display(Name = "Porcentaje")]
        public short Percentage { get; set; }

        public string Email { get; set; }

        [Display(Name = "IdPóliza")]
        public int IdPolicy { get; set; }

        [Display(Name = "IdProducto")]
        public string IdProduct { get; set; }

        public DateTime Modified { get; set; }

    }
}
