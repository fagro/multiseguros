﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Policy
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Display(Name = "Plan")]
        public int PlanId { get; set; }

        [Display(Name = "Forma de Pagos")]
        public int MethodOfPayment { get; set; }

        [Display(Name = "Medio de pago")]
        public short MeansOfPayment { get; set; }

        [Display(Name = "Valor poliza")]
        public decimal PolicyValue { get; set; }

        [Display (Name = "Valor Opción")]
        public decimal ValueOptions { get; set; }

        [Display(Name = "Número de cuenta/Préstamo")]
        public string AccountNumber { get; set; }
        
        [Display(Name = "Prima")]
        public decimal PolicyPremiumValue { get; set; }
        public short Status { get; set; }
        public Client Client { get; set; }
        public IEnumerable<Beneficiary> Beneficiaries { get; set; } 
        public DateTime Created { get; set; }
        public DateTime Modified { get { return DateTime.Now.Date; } }
        public string UserName { get; set; }



    }
}

