﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class ShelterType
    {
        public int Id { get; set; }
        [Display(Name = "Nombre Amparo")]
        public string ShelterName { get; set; }
        [Display(Name = "Ramo")]
        public short BranchCode  { get; set; }
        [Display(Name = "Fecha Modificación")]
        public DateTime DateModified { get; set; }

    }
}
