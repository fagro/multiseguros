﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Reflection;
using System.Security.AccessControl;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Product
    {
        [Display(Name = "Producto")]
        public int Id { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Codigo producto")]
        public string ProductCode { get; set; }

        //DropDownValues
        [Display(Name = "Aseguradora")]
        public short InsuranceCompanyId { get; set; }
        [Display(Name = "Tipo Moneda")]
        public short CurrencyRateId { get; set; }
        [Display(Name = "Ramo")]
        public short BranchId { get; set; }
        [Display(Name = "Operador")]
        public short RepresentativeId { get; set; }
        [Display(Name = "Tipo Indexación")]
        public short TypeIndexingId { get; set; }
        [Display(Name = "Tipo Mercadeo")]
        public short MarketingId { get; set; }

        //values
        [Display(Name = "Contributivo")]
        public bool Contributive { get; set; }
        [Display(Name = "Amparos Optativos")]
        public bool OptativeShelter { get; set; }
        [Display(Name = "ITBIS")]
        public bool Itbis { get; set; }
        [Display(Name = "Expedir")]
        public bool Emit { get; set; }

        [Display(Name = "Día Corte")]
        [Required]
        public string CutDay { get; set; }

        [Display(Name = "Fecha Inicial Venta")]
        public DateTime InitSaleDay { get; set; }

        [Display(Name = "Comision Promotor Nuevas")]
        public decimal NewPromotorCommission { get; set; }

        [Display(Name = "Comision Promotor Renov")]
        public decimal RenewPromotorCommission { get; set; }

        [Display(Name = "Comision Banco Nuevas")]
        public decimal NewBankCommission { get; set; }

        [Display(Name = "Comision Banco Renov")]
        public decimal RenewBankCommission { get; set; }

        [Display(Name = "Descuento Hombres")]
        public decimal MenDiscount { get; set; }

        [Display(Name = "Comision Mujeres")]
        public decimal WomenDiscount { get; set; }

        [Display(Name = "Porcentaje Incentivo nuevas")]
        public decimal NewSalerIncentive { get; set; }

        [Display(Name = "Porcentaje Incentivo renovadas")]
        public decimal RenewSalerIncentive { get; set; }

        [Display(Name = "Fecha Modificación")]
        public DateTime Modified { get; set; }
    }
}
