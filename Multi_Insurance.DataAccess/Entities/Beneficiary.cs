﻿using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Beneficiary
    {
        public int Id { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Display(Name = "Parentesco")]
        public int IdRelationship { get; set; }

        [Display(Name = "Porcentage")]
        public short Percentage { get; set; }
    }
}
