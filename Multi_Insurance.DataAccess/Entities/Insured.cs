﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Insured
    {
        [Display(Name = "Identificación")]
        public long Id { get; set; }

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Display(Name = "Género")]
        public char Gender { get; set; }

        [Display(Name = "Fecha nacimiento")]
        public DateTime? Birthday { get; set; }

        [Display(Name = "Teléfono")]
        public string Phone { get; set; }

        [Display(Name = "Celular")]
        public string Cellphone { get; set; }

        [Display(Name = "Empresa")]
        public string Empresa { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }


        [Display(Name = "Ciudad")]
        public int? CityId { get; set; }

        [Display(Name = "Sector")]
        public string Sector { get; set; }

        [Display(Name = "Calle")]
        public string Street { get; set; }

        [Display(Name = "Profesión")]
        public string Profession { get; set; }


        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}
