﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class MethodOfPaymentOptions
    {
        [Display(Name = "Forma de pago")]
        public short MethodOfPaymentOptionsCode { get; set; }

        [Display(Name = "Nombre")]
        public string MethodOfPaymentOptionName { get; set; }

        [Display(Name = "Divisor")]
        public short MethodOfPaymentOptionDivisor { get; set; }

        [Display(Name = "Ultima Modificación")]
        public DateTime LastModified { get; set; }

    }
}
