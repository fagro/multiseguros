﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class MeansOfPaymentOptions
    {
        [Display(Name = "Medio de pago")]
        public short MeansOfPaymentCode { get; set; }

        [Display(Name = "Nombre")]
        public string MeansOfPaymentName { get; set; }

        public DateTime Modified { get; set; }

    }
}
