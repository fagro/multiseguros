﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class MeansOfPayment
    {
        [Display(Name = "Código Producto")]
        public string ProductCode { get; set; }

        [Display(Name = "Código Medio de pago")]
        public short MeansOfPaymenOptionCode { get; set; }
        public DateTime Modified { get; set; }

        public short CheckedDigicCode { get; set; }

    }
}
