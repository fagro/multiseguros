﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class ProductOptions
    {
        
        [Display(Name = "plan")]
        public short PlanCode { get; set; }

        [Display(Name = "Monto de plan")]
        public decimal ValueOptions { get; set; }
        public DateTime Modified { get; set; }

        [Display(Name = "Declaración Jurada")]
        public string Declaration { get; set; }

        public bool Dependant { get; set; }

        public bool ValidationAssociated { get; set; }

    }
}
