﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Representative
    {
        [Display(Name = "Id")]
        public short RepresentativeId { get; set; }
        [Display(Name = "Nombre del gestor")]
        public string Name { get; set; }
        [Display(Name = "Dirección")]
        public string Addresss { get; set; }
        [Display(Name = "Teléfono")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Fecha Modificación")]
        public DateTime Modified { get; set; }
        [Display(Name = "Factura")]
        public decimal Invoice { get; set; }

    }
}
