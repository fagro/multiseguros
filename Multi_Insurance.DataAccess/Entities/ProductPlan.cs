﻿using System.ComponentModel.DataAnnotations;
using System;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class ProductPlan 
    {
        public int ProductId { get; set; }
        [Display(Name = "Plan")]
        public string PlanName { get; set; }
        public short ProductTypeOptionId { get; set; }
        [Display(Name="Fecha modificación")]
        public DateTime Modified { get; set; }

    }
}
