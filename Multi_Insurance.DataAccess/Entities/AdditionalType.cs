﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class AdditionalType
    {
        [Display(Name = "Info Adicional")]
        public short AdditionalTypeInfoId { get; set; }
        [Display(Name = "Valor Info Adicional")]
        public decimal AdditionalValue { get; set; }
        public DateTime Modified { get; set; }

    }
}
