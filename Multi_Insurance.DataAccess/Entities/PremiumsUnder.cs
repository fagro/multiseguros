﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class PremiumsUnder
    {
        [Display(Name = "Codigo Producto")]
        public int ProductId { get; set; }

        public int  Id { get; set; }

        [Display(Name = "Código del plan")]
        public short ProductPlancode { get; set; }

        [Display(Name = "Codigo Forma de pago")]
        public short MethodOfPaymentCode { get; set; }

        [Display(Name = "Edad desde")]
        public short AgeFrom { get; set; }

        [Display(Name = "Edad Hasta")]
        public short AgeTo { get; set; }

        [Display(Name = "Valor de Prima")]
        public decimal PremiumsUnderValue { get; set; }

        public DateTime Modified { get; set; }

        [Display(Name = "Amparo")]
        public short ShelterCode { get; set; }

    }
}
