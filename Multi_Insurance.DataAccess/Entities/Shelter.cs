﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class Shelter
    {
        public string ProductCode { get; set; }
        [Display(Name = "Amparo")]
        public short ShelterTypeId { get; set; }
        [Display(Name = "Valor amparo")]
        public Decimal ShelterValue { get; set; }
        public bool IndAmpPri { get; set; }

    }
}
