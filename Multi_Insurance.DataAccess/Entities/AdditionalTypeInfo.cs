﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class AdditionalTypeInfo
    {
        public int Id { get; set; }

        [Display(Name = "Código info adicional")]
        public short Code { get; set; }

        [Display(Name = "Nombre info adicional")]
        public string Name { get; set; }

        [Display(Name = "Fecha Modificación")]
        public DateTime Modified { get; set; }
    }
}
