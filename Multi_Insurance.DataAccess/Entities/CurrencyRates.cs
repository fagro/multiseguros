﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multi_Insurance.DataAccessLayer.Entities
{
    public class CurrencyRates
    {
        public short CurrencyRatesId { get; set; }
        [Display(Name = "Nombre")]        
        public string Name { get; set; }
        [Display(Name = "Símbolo")]
        public string Symbol { get; set; }
        public DateTime Modified { get; set; }

    }
}
