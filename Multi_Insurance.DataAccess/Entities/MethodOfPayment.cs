﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Multi_Insurance.DataAccessLayer.Entities
{
   public class MethodOfPayment
    {
       [Display(Name = "Código Producto")]
       public string ProductCode { get; set; }

       [Display(Name = "Código Forma de pago")]
       public short MethodOfpaymentCode { get; set; }

       public short MethodOfPaymentRec { get; set; }

       public DateTime Modified { get; set; }
    }
}
