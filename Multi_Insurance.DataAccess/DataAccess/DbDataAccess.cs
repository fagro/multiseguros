﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.DataAccessLayer.DataAccess
{
    public class DbDataAccess
    {
        public DbConnection DbConnection;

        public DbDataAccess()
        {
            DbConnection = new DbConnection();
        }

        #region Policy Creation
        public bool AddPolicy(Policy policy)
        {
            var conn = DbConnection.SqlConnection;
            var cmd = new SqlCommand
            {
                CommandText = "usp_CreatePolicy",
                CommandType = CommandType.StoredProcedure,
                Connection = conn
            };
            policy.UserName = "carl";
            cmd.Parameters.AddWithValue("@CodPro", "2-102-495");
            cmd.Parameters.AddWithValue("@CodTipDocId", 1);
            cmd.Parameters.AddWithValue("@NumDocIde", policy.Client.Id.ToString());
            cmd.Parameters.AddWithValue("@CodTipDocIdeEmp", 90);
            cmd.Parameters.AddWithValue("@CodTipForPag", policy.MethodOfPayment);
            cmd.Parameters.AddWithValue("@CodTipMedPag", policy.MeansOfPayment);
            cmd.Parameters.AddWithValue("@CodTipOpc", policy.PlanId);
            cmd.Parameters.AddWithValue("@NumCue", "00002222");
            cmd.Parameters.AddWithValue("@ValPriCer", policy.PolicyPremiumValue);
            cmd.Parameters.AddWithValue("@userName", policy.UserName);
            //cmd.Parameters.AddWithValue("@TvpBeneficiaryList", "")
            var confir = cmd.ExecuteScalar() as int?;

            return confir != 0;
        }
        #endregion

        #region Product Parameters

        public int AddInsuranceCompany(InsuranceCompany insuranceCompany)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = insuranceCompany.Id,
                    ParameterName = "@Id"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = insuranceCompany.Name,
                    ParameterName = "@Name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10,
                    Value = insuranceCompany.Phone,
                    ParameterName = "@Phone"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 120,
                    Value = insuranceCompany.Address,
                    ParameterName = "@Address"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 30,
                    Value = insuranceCompany.Contact,
                    ParameterName = "@Contact"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = insuranceCompany.Percentage,
                    ParameterName = "@percent"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 12,
                    Value = insuranceCompany.Rnc,
                    ParameterName = "@rnc"
                },
                //@rnc
            };

            var data = CreateTo("usp_CreateInsuranceCompany", sqlParameters);
            var result = data != null ? (int)data : 0;
            return result;

        }

        public int DeleteInsuranceCompany(int id)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = id, 
                    ParameterName = "@Id"
                }
            };

            var data = CreateTo("usp_deleteInsuranceCompany", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;

        }
        public short AddMethodOfPaymentOption(MethodOfPaymentOptions paymentOptions)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = paymentOptions.MethodOfPaymentOptionsCode,
                    ParameterName = "@PaymentMethodCode"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = paymentOptions.MethodOfPaymentOptionName,
                    ParameterName = "@Name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Float,
                    Value = paymentOptions.MethodOfPaymentOptionDivisor,
                    ParameterName = "@Div"
                },
            };
            var data = CreateTo("usp_CreateTiposFormaDePagoOpcion", sqlParameters);
            var result = (data != null) ? (short)data : (short)0;
            return result;

        }


        public int DeleteMethodOfPaymentOption(int id)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = id,
                    ParameterName = "@id"
                }
            };
            var data = CreateTo("DeleteMethodOfPaymentOption", sqlParameteres);
            var result = data != null ? (short)data : (short)0;
            return result;
        }
        public short AddMeansOfPaymentOption(MeansOfPaymentOptions meansOfPayment)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = meansOfPayment.MeansOfPaymentName,
                    ParameterName = "@Name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = meansOfPayment.MeansOfPaymentCode,
                    ParameterName = "@Id"
                }
            };

            var data = CreateTo("usp_CreateTiposMedioDePago", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public short AddBrachType(BranchType branchType)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = branchType.Id, 
                    ParameterName = "@Id"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = branchType.Code, 
                    ParameterName = "@Code"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = branchType.Name,
                    ParameterName = "@Name"
                }
            };

            var data = CreateTo("usp_CreateTipoRamo", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public short AddRepresentative(Representative representative)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = representative.RepresentativeId,
                    ParameterName = "@Id"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 60,
                    Value = representative.Name,
                    ParameterName = "@name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 120,
                    Value = representative.Addresss,
                    ParameterName = "@address"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10,
                    Value = representative.PhoneNumber,
                    ParameterName = "@phone"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Decimal,
                    Value = representative.Invoice,
                    ParameterName = "@invoice"
                }
            };

            var data = CreateTo("usp_CreateGestores", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public short AddTypesIndexing(TypesIndexing typesIndexing)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = typesIndexing.TypeIndexingId,
                    ParameterName = "@id"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25,
                    Value = typesIndexing.Name,
                    ParameterName = "@Name"
                }
            };
            var data = CreateTo("usp_CreateTiposIndexacion", sqlParameteres);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public DataTable GetTypesIndexing(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id",
                }
            };
            return GetDataFrom("usp_GetTiposIndexacion", sqlParameters);
        }

        public DataTable GetCompanyList(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id",
                }
            };
            return GetDataFrom("usp_GetCompanyList", sqlParameters);
        }

        public DataTable GetBranchType(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id",
                }
            };
            return GetDataFrom("usp_GetBranchType", sqlParameters);
        }

        public DataTable GetRepresentative(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id",
                }
            };
            return GetDataFrom("usp_GetRepresentative", sqlParameters);
        }

        public short AddCurrencyRates(CurrencyRates currencyRates)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25,
                    Value = currencyRates.Name,
                    ParameterName = "@name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 4,
                    Value = currencyRates.Symbol,
                    ParameterName = "@symbol"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = currencyRates.CurrencyRatesId,
                    ParameterName = "@id"
                }
            };

            var data = CreateTo("usp_CreateTiposMoneda", sqlParameteres);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public DataTable GetCurrencyRates(short id = 0)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id"
                }
            };
            return GetDataFrom("usp_GetTiposMoneda", sqlParameteres);
        }

        #endregion

        #region Product Creation

        public int AddProductPlan(ProductPlan productPlan)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50,
                    Value = productPlan.PlanName,
                    ParameterName = "@Name"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = productPlan.ProductId,
                    ParameterName = "@Id"
                }
            };

            var data = CreateTo("usp_CreateTiposOpcion", sqlParameters);
            var result = data != null ? (int)data : 0;
            return result;
        }

        public int DeleteProductPlan(int id)
        {
            var sqlParameters = new List<SqlParameter>
            {   
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = id,
                    ParameterName = "@Id"
                }
            };

            var data = CreateTo("DeleteProductPlanOption", sqlParameters);
            var result = data != null ? (int)data : 0;
            return result;
        }

        public short AddMethodOfPayment(MethodOfPayment methodOfPayment)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = methodOfPayment.ProductCode,
                    ParameterName = "@productCode"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = methodOfPayment.MethodOfpaymentCode,
                    ParameterName = "@methodOfPaymentCode"
                }
            };
            var data = CreateTo("usp_CreateMethodOfPayment", sqlParameters);
            var result = (data != null) ? (short)data : (short)0;
            return result;
        }

        public short AddMeansOfPayment(MeansOfPayment meansOfPayment)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = meansOfPayment.MeansOfPaymenOptionCode,
                    ParameterName = "@productCode",
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = meansOfPayment.MeansOfPaymenOptionCode,
                    ParameterName = "@meansOfPaymentCode"
                }
            };
            var data = CreateTo("usp_CreateMeansOfPayment", sqlParameters);
            var result = (data != null) ? (short)data : (short)0;
            return result;
        }

        public short AddProductBasicData(Product product)
        {
            var sqlParameters = new List<SqlParameter>
              {
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Int,
                      Value = product.Id,
                      ParameterName = "@id"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.VarChar,
                      Size = 50,
                      Value = product.Name,
                      ParameterName = "@Name"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.VarChar, 
                      Size = 40,
                      Value = product.ProductCode,
                      ParameterName = "@productCode"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = product.InsuranceCompanyId,
                      ParameterName = "@companyId"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = 1,
                      ParameterName = "@patrocinador"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = product.CurrencyRateId,
                      ParameterName = "@currency"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = product.BranchId,
                      ParameterName = "@branchId"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = product.RepresentativeId,
                      ParameterName = "@representative"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = product.TypeIndexingId,
                      ParameterName = "@typeIndexId"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.SmallInt,
                      Value = 1,
                      ParameterName = "@MarketingType"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Bit,
                      Value = product.Contributive,
                      ParameterName = "@contributive"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Bit,
                      Value = product.OptativeShelter,
                      ParameterName = "@optativeShelter"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Bit,
                      Value = product.Itbis,
                      ParameterName = "@Itbis"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Bit,
                      Value = product.Emit,
                      ParameterName = "@emit"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.VarChar,
                      Size = 15,
                      Value = product.CutDay,
                      ParameterName = "@cutDay"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.VarChar,
                      Size = 10,
                      Value = product.InitSaleDay.ToString("yyyyMMdd"),
                      ParameterName = "@initSaleDay"
                  },
                   new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.NewPromotorCommission,
                      ParameterName = "@NewPromotorCommission"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.RenewPromotorCommission,
                      ParameterName = "@RenewPromotorCommission"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.NewBankCommission,
                      ParameterName = "@NewBankCommission"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.RenewBankCommission,
                      ParameterName = "@RenewBankCommission"
                  },
                   new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.MenDiscount,
                      ParameterName = "@MenDiscount"
                  },
                   new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.WomenDiscount,
                      ParameterName = "@WomenDiscount"
                  },
                   new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.NewSalerIncentive,
                      ParameterName = "@NewSalerIncentive"
                  },
                  new SqlParameter
                  {
                      SqlDbType = SqlDbType.Decimal,
                      Value = product.RenewSalerIncentive,
                      ParameterName = "@RenewSalerIncentive"
                  }
              };

            var data = CreateTo("usp_CreateProductBasicData", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public short AddProductPayPlan(Dictionary<string, string> model)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = short.Parse(model["productCode"]),
                    ParameterName = "@planId"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Money,
                    Value = decimal.Parse(model["ValueOptions"]),
                    ParameterName = "@valueOptions"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = short.Parse(model["MethodOfPaymentOptionsId"]),
                    ParameterName = "@MethodOfPaymentOptionsId"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = short.Parse(model["MeansOfPaymentOptionsId"]),
                    ParameterName = "@MeansOfPaymentOptionsId"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = int.Parse(model["productId"]),
                    ParameterName = "@productId"
                }
            };

            var data = CreateTo("usp_CreateProductPaymentPlans", sqlParameters);
            var result = (data != null) ? (short)data : (short)0;
            return result;
        }

        public short AddProductShelterAndCondition(Dictionary<string, string> model)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = short.Parse(model["ProductId"]),
                    ParameterName = "@productId"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Money,
                    Value = decimal.Parse(model["ShelterValue"]),
                    ParameterName = "@shelterValue"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = short.Parse(model["ShelterId"]),
                    ParameterName = "@shelterId"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Decimal,
                    Value = decimal.Parse(model["AdditionalInfoValue"]),
                    ParameterName = "@additionalInfoValue"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = short.Parse(model["AdditionalTypeId"]),
                    ParameterName = "@additionalTypeId"
                }
            };
            var data = CreateTo("usp_CreateShelterAndConditions", sqlParameters);
            var result = (data != null) ? (short)data : (short)0;
            return result;
        }

        public int AddPremiumsUnder(PremiumsUnder premiumsUnder)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = premiumsUnder.ProductId,
                    ParameterName = "@productId"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = premiumsUnder.ShelterCode,
                    ParameterName = "@shelterCode"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = premiumsUnder.ProductPlancode,
                    ParameterName = "@productPlan"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = premiumsUnder.MethodOfPaymentCode,
                    ParameterName = "@methodOfPayment"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = premiumsUnder.AgeFrom,
                    ParameterName = "@ageFrom"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = premiumsUnder.AgeTo,
                    ParameterName = "@ageTo"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Decimal,
                    Value = premiumsUnder.PremiumsUnderValue,
                    ParameterName = "@premiumUnderValue"
                }
            };

            var data = CreateTo("Usp_CreatePremiumUnder", sqlParameters);
            var result = data != null ? (short)data : (short)0;

            return result;
        }

        //public short AddProductOptions(ProductOptions productOptions)
        //{
        //    var sqlParameters = new List<SqlParameter>
        //    {
        //        new SqlParameter
        //        {
        //            SqlDbType = SqlDbType.VarChar,
        //            Size = 15,
        //            Value = productOptions.ProductCode,
        //            ParameterName = "@ProductCode"
        //        },
        //        new SqlParameter
        //        {
        //            SqlDbType = SqlDbType.SmallInt,
        //            Value = productOptions.PlanCode,
        //            ParameterName = "@PlanCode"
        //        },
        //        new SqlParameter
        //        {
        //            SqlDbType = SqlDbType.Decimal,
        //            Value = productOptions.ValueOptions,
        //            ParameterName = "@MonOpc"

        //        },
        //        new SqlParameter
        //        {
        //            SqlDbType = SqlDbType.VarChar,
        //            Size = 800,
        //            Value = productOptions.Declaration,
        //            ParameterName = "@declaration"

        //        }                

        //    };

        //    var data = CreateTo("usp_CreateProductOption", sqlParameters);
        //    var result = data != null ? (short)data : (short)0;
        //    return result;
        //}

        public short AddShelterType(ShelterType shelterType)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = shelterType.Id, 
                    ParameterName = "@Id"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = shelterType.BranchCode, 
                    ParameterName = "@branchCode"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20,
                    Value = shelterType.ShelterName,
                    ParameterName = "@Name"
                }
            };

            var data = CreateTo("usp_CreateShelterType", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        public short AddAdditionalTypeInfo(AdditionalTypeInfo additionalTypeInfo)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = additionalTypeInfo.Id, 
                    ParameterName = "@Id"
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = additionalTypeInfo.Code, 
                    ParameterName = "@code"
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 49,
                    Value = additionalTypeInfo.Name, 
                    ParameterName = "@name"
                }
            };

            var data = CreateTo("usp_CreateAdditionalTypeInfo", sqlParameters);
            var result = data != null ? (short)data : (short)0;
            return result;
        }

        #endregion




        public DataTable GetProductsPlan(string productCode)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = productCode,
                    ParameterName = "@productCode"
                }
            };

            return GetDataFrom("usp_GetProductsPlan", sqlParameteres);
        }

        public DataTable GetProductsPlanOptions(short id)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id"
                }
            };

            return GetDataFrom("usp_GetProductsPlanOptions", sqlParameteres);
        }


        public DataTable GetPolicyProductsPlan(int productCode)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = productCode,
                    ParameterName = "@productId"
                }
            };

            return GetDataFrom("usp_GetPolicyProductPlan", sqlParameteres);
        }


        public DataTable GetProductBasicData(int id = 0)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = id,
                    ParameterName = "@id"
                }
            };

            return GetDataFrom("usp_GetProductBasicData", sqlParameteres);
        }

        public DataTable GetMethodOfPaymentOptions(string productCode = null)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    Value = productCode,
                    ParameterName = "@productCode"
                }
            };
            return GetDataFrom("usp_GetMethodOfPaymentByProductCode", sqlParameteres);
        }
        
        public DataTable GetPolicyMethodOfPaymentOptions(int productId = 0)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = productId,
                    ParameterName = "@productId"
                }
            };
            return GetDataFrom("usp_GetPolicyMethodOfPayment", sqlParameteres);
        }

        public DataTable GetProduct(int productId = 0)
        {
            IEnumerable<SqlParameter> sqlParameters = null;
            if (productId > 0)
            {
                sqlParameters = new List<SqlParameter>
                {
                    new SqlParameter
                    {
                        SqlDbType = SqlDbType.Int,
                        ParameterName = "@ProductId",
                        SqlValue = productId
                    }
                };
            }
            return GetDataFrom("usp_GetProduct", sqlParameters);
        }

        public DataTable GetMeansOfPaymentOptions(short id = 0)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    Value = id,
                    ParameterName = "@id"
                }
            };
            return GetDataFrom("usp_GetMeansOfPaymentOptions", sqlParameteres);
        }

        public DataTable GetPolicyMeansOfPaymentOptions(int id = 0)
        {
            var sqlParameteres = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.Int,
                    Value = id,
                    ParameterName = "@productId"
                }
            };
            return GetDataFrom("usp_GetPolicyMeansOfPaymentOptions", sqlParameteres);
        }

        public DataTable GetProductOptionsByProductCode(string productCode)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    ParameterName = "@productCode",
                    SqlValue = productCode
                }
            };

            return GetDataFrom("usp_GetProductOptions", sqlParameters);
        }

        public DataTable GetProductOptionsByPlanCode(short planCode, string productCode)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 5,
                    ParameterName = "@planCode",
                    SqlValue = planCode
                },
                 new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    ParameterName = "@ProductCode",
                    SqlValue = productCode
                }
            };

            return GetDataFrom("usp_GetProductOptionsByPlanCode", sqlParameters);
        }

        public DataTable GetPremiumUnders(short plancode, string productCode, short methodOfPaymentCode)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    ParameterName = "@codTipOpc",
                    SqlValue = plancode
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15,
                    ParameterName = "@codPro",
                    SqlValue = productCode
                },
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    ParameterName = "@codTipForPag",
                    SqlValue = methodOfPaymentCode
                }
            };

            return GetDataFrom("usp_GetPremiumUnders", sqlParameters);
        }

        public DataTable GetShelter(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    ParameterName = "@id",
                    SqlValue = id
                }
            };
            return GetDataFrom("usp_GetShelter", sqlParameters);
        }

        public DataTable GetAdditionalTypeInfo(short id = 0)
        {
            var sqlParameters = new List<SqlParameter>
            {
                new SqlParameter
                {
                    SqlDbType = SqlDbType.SmallInt,
                    ParameterName = "@id",
                    SqlValue = id
                }
            };
            return GetDataFrom("usp_GetAdditionalTypeInfo", sqlParameters);
        }




        /// <summary>
        /// Call stored procedure and returns a datatable.
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="parameters"></param>
        /// <returns>returns a DataTable</returns>
        private DataTable GetDataFrom(string storedProcedure, IEnumerable<SqlParameter> parameters = null)
        {
            var conn = DbConnection.SqlConnection;
            var cmd = new SqlCommand
            {
                CommandText = storedProcedure,
                CommandType = CommandType.StoredProcedure,
                Connection = conn
            };

            if (parameters != null)
            {
                foreach (var sqlParameter in parameters)
                {
                    cmd.Parameters.Add(sqlParameter);
                }
            }

            var data = new DataTable(storedProcedure);
            var rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            data.Load(rdr);
            rdr.Close();
            return data;
        }

        /// <summary>
        /// Insert an entity in sql server and returns the Id of the entity. 
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private int? CreateTo(string storedProcedure, IEnumerable<SqlParameter> parameters)
        {
            var conn = DbConnection.SqlConnection;
            var cmd = new SqlCommand
            {
                CommandText = storedProcedure,
                CommandType = CommandType.StoredProcedure,
                Connection = conn
            };

            foreach (var sqlParameter in parameters)
            {
                cmd.Parameters.Add(sqlParameter);
            }
            var id = cmd.ExecuteNonQuery();

            conn.Close();
            return id;
        }


    }
}
