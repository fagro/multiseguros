﻿using System.Configuration;
using System.Data.SqlClient;

namespace Multi_Insurance.DataAccessLayer.DataAccess
{
   public class DbConnection
   {
       #region Instance
       private SqlConnection _conn;
       #endregion

       #region Behavior
       /// <summary>
       /// Returns an open Sql connection
       /// </summary>
       public SqlConnection SqlConnection
       {
           get
           {
               var stringConn = ConfigurationManager.ConnectionStrings["Multi_InsuranceDB"].ConnectionString;
               _conn = new SqlConnection(stringConn);
               _conn.Open();
               return _conn;
           }
       }

       public void Close()
       {
           if (_conn != null)
                _conn.Close();
       }
       #endregion
   }
}
