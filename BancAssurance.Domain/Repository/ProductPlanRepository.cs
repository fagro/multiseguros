﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;


namespace Multi_Insurance.Domain.Repository
{
    public class ProductPlanRepository : IProductPlanRepository
    {
        private DbDataAccess db;

        public ProductPlanRepository()
        {
            db = new DbDataAccess();
        }
        public IEnumerable<ProductPlan> GetProductsPlan(string productCode = null )
        {
            var data = db.GetProductsPlan(productCode);
            IList<ProductPlan> productPlans = new List<ProductPlan>();

            foreach (DataRow item in data.Rows)
            {
                productPlans.Add(new ProductPlan
                {
                    ProductId = int.Parse(item["productId"].ToString()),
                    PlanName = item["PlanName"].ToString()
                });
            }

            return productPlans;
        }

        public IEnumerable<ProductPlan> GetProductsPlanOptions(short productCode = 0)
        {
            var data = db.GetProductsPlanOptions(productCode);
            IList<ProductPlan> productPlans = new List<ProductPlan>();

            foreach (DataRow item in data.Rows)
            {
                productPlans.Add(new ProductPlan
                {
                    ProductId = int.Parse(item["CodTipOpc"].ToString()),
                    PlanName = item["NomTipOpc"].ToString(),
                    Modified = DateTime.Parse(item["FecUltMod"].ToString()) 
                });
            }

            return productPlans;
        }

        public int AddProductPlan(ProductPlan productPlan)
        {
            return db.AddProductPlan(productPlan);
        }

         public IEnumerable<ProductPlan> GetPolicyProductsPlan(int productCode)
        {
            var data = db.GetPolicyProductsPlan(productCode);
            IList<ProductPlan> productPlans = new List<ProductPlan>();

            foreach (DataRow item in data.Rows)
            {
                productPlans.Add(new ProductPlan
                {
                    ProductId = int.Parse(item["productId"].ToString()),
                    PlanName = item["PlanName"].ToString()
                });
            }

            return productPlans;
        }
        
        public int DeleteProductPlan(int id)
         {
             return db.DeleteProductPlan(id);
         }
    }
}
