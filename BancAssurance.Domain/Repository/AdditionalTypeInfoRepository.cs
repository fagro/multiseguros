﻿using System;
using System.Collections.Generic;
using System.Data;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class AdditionalTypeInfoRepository : IAdditionalTypeInfoRepository
    {
        private readonly DbDataAccess _db;

        public AdditionalTypeInfoRepository()
        {
                _db = new DbDataAccess();
        }

        public IEnumerable<AdditionalTypeInfo> GetAdditionalTypeInfos(short id = 0)
        {
            var data = _db.GetAdditionalTypeInfo(id);
            var additionalList = new List<AdditionalTypeInfo>();
            foreach (DataRow row in data.Rows)
            {
                additionalList.Add
                    (
                        new AdditionalTypeInfo
                        {
                            Id = int.Parse(row["idTipInfAdi"].ToString()),
                            Code = short.Parse(row["CodTipInfAdi"].ToString()),
                            Name = row["NomTipInfAdi"].ToString(),
                            Modified = DateTime.Parse(row["FecUltMod"].ToString()) 
                        }
                    );
            }
            return additionalList;
        }

        public short AddAdditionalTypeInfo(AdditionalTypeInfo additionalTypeInfo)
        {
            return _db.AddAdditionalTypeInfo(additionalTypeInfo);
        }
    }
}
