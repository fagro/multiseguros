﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class TypesIndexingRepository : ITypesIndexingRepository
    {

        private DbDataAccess _db;

        public TypesIndexingRepository()
        {
            _db = new DbDataAccess();
        }
        public short AddTypesIndexing(TypesIndexing typesIndexing)
        {
            return _db.AddTypesIndexing(typesIndexing);
        }

        public IEnumerable<TypesIndexing> GetTypesIndexings(short id = 0)
        {
                var data = _db.GetTypesIndexing(id);
                IList<TypesIndexing> typesIndexings = new List<TypesIndexing>();

                foreach (DataRow item in data.Rows)
                {
                    typesIndexings.Add(new TypesIndexing
                    {
                        TypeIndexingId = short.Parse(item["TipoIndexacionId"].ToString()),
                        Name = item["NomTipInd"].ToString(),
                        Modified = DateTime.Parse(item["FecUltMod"].ToString()) 
                    });
                }
                return typesIndexings;
            
        }
    }
}
