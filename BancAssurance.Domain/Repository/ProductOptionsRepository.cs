﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;
namespace Multi_Insurance.Domain.Repository
{
    public class ProductOptionsRepository : IProductOptionsRepository
    {
        private readonly DbDataAccess _db;

        public ProductOptionsRepository()
        {
            _db = new DbDataAccess();
        }
        public IEnumerable<ProductOptions> GetProductOptionsByProductCode(string productCode)
        {
            var data = _db.GetProductOptionsByProductCode(productCode);
            IList<ProductOptions> productOptions = new List<ProductOptions>();

            foreach (DataRow item in data.Rows)
            {
                productOptions.Add(new ProductOptions
                {
                    PlanCode = short.Parse(item["planCode"].ToString()),
                    ValueOptions = decimal.Parse(item["PremiumValue"].ToString()) 
                });
            }

            return productOptions;
        }

        public short AddProductOptions(ProductOptions productOptions)
        {
            return 0;
        }

        public IEnumerable<ProductOptions> GetProductOptionsByPlanCode(short planCode, string productCode)
        {
            var data = _db.GetProductOptionsByPlanCode(planCode, productCode);
            IList<ProductOptions> productOptions = new List<ProductOptions>();

            foreach (DataRow item in data.Rows)
            {
                productOptions.Add(new ProductOptions
                {
                    PlanCode = short.Parse(item["PlanCode"].ToString()),
                    ValueOptions = decimal.Parse(item["PremiumValue"].ToString())
                });
            }

            return productOptions;
        }
    }
}
