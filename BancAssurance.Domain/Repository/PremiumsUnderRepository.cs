﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class PremiumsUnderRepository : IPremiumsUnderRepository
    {
        private DbDataAccess db;

        public PremiumsUnderRepository()
        {
            db = new DbDataAccess();
        }

        public IEnumerable<PremiumsUnder> GetPremiumsUnders { get; private set; }

        public int AddPremiumsUnder(PremiumsUnder premiumsUnder)
        {
            return db.AddPremiumsUnder(premiumsUnder);
        }
    }
}
