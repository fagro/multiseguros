﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class BeneficiaryRepository : IBeneficiaryRepository
    {
        private Beneficiary _beneficiary;

        public BeneficiaryRepository(Beneficiary beneficiary)
        {
            this._beneficiary = beneficiary;
            GetBeneficiaries = new List<Beneficiary>();
        }

        public IList<Beneficiary> GetBeneficiaries { get; private set; }

        public bool AddBeneficiary(Beneficiary mbeneficiary)
        {
            GetBeneficiaries.Add(mbeneficiary);
            return GetBeneficiaries.Count > 0;
        }
        public bool UpdateBeneficiary(Beneficiary mbeneficiary)
        {
            throw new NotImplementedException();
        }
        public bool DeleteBeneficiary(int id)
        {
            throw new NotImplementedException();
        }
    }
}
