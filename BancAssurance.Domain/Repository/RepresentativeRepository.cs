﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class RepresentativeRepository : IRepresentativeRepository
    {
        private DbDataAccess _db;

        public RepresentativeRepository()
        {
            _db = new DbDataAccess();
        }
        public short AddRepresentative(Representative representative)
        {
            return _db.AddRepresentative(representative);
        }

        public IEnumerable<Representative> GetRepresentatives(short id = 0)
        {

            var data = _db.GetRepresentative();
            IList<Representative> representatives = new List<Representative>();

            foreach (DataRow item in data.Rows)
            {
                representatives.Add(new Representative
                {
                    RepresentativeId = short.Parse(item["GestoresId"].ToString()),
                    Name = item["name"].ToString(),
                    Addresss = item["address"].ToString(),
                    Modified = DateTime.Parse(item["modified"].ToString()),
                    Invoice = decimal.Parse(item["invoice"].ToString()),
                    PhoneNumber = item["phoneNumber"].ToString()
                });
            }

            return representatives;

        }
    }
}
