﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class DependentRepository : IDependentRepository
    {
        private Dependent _dependent;

        public DependentRepository (Dependent dependent)
        {
            this._dependent = dependent;
            GetDependents = new List<Dependent>();
        }

        public IList<Dependent> GetDependents { get; private set; }

        public bool AddDependent(Dependent mDependent)
        {
            GetDependents.Add(mDependent);
            return GetDependents.Count > 0;       
        }

        public bool UpdateDependent(Dependent mDependent)
        {
            throw new NotImplementedException();        
        }

        public bool DeleteDependent(string IdProduct, int IdPolicy, string IdNumber)
        {
            throw new NotImplementedException();        
        }

    }

}
