﻿using System;
using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;
using System.Data;

namespace Multi_Insurance.Domain.Repository
{
    public class ShelterRepository : IShelterRepository
    {
        private DbDataAccess db;
        public ShelterRepository()
        {
            db = new DbDataAccess();
        }

        public IEnumerable<ShelterType> GetShelters(short id = 0)
        {

            var data = db.GetShelter(id);
            var shelterList = new List<ShelterType>();
            foreach (DataRow row in data.Rows)
            {
                shelterList.Add
                    (
                        new ShelterType
                        {
                            Id = short.Parse(row["CodAmp"].ToString()),
                            ShelterName = row["NomAmp"].ToString(),
                            BranchCode = short.Parse(row["CodTipRam"].ToString()),
                            DateModified = DateTime.Parse(row["FecUltMod"].ToString())
                        }
                    );
            }
            return shelterList;


        }
        public short AddShelter(ShelterType shelterType)
        {
           return  db.AddShelterType(shelterType);
        }
    }
}
