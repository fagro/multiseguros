﻿using System;
using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class PolicyRepository : IPolicyRepository
    {
        private Policy _policy;
        private DbDataAccess _dataAccess;
        public Policy Policy { get { return _policy; } }
        public PolicyRepository(Policy policy, Beneficiary beneficiary, DbDataAccess dataAccess) 
        {
            _policy = policy;
            var data = new List<Beneficiary> {beneficiary};
            _policy.Beneficiaries = data;
            _dataAccess = dataAccess;
        }
        public bool AddPolicy(Policy mPolicy)
        {
            return _dataAccess.AddPolicy(mPolicy);
        }
        public Policy FindPolicy(int id)
        {
            throw new NotImplementedException();
        }
       public bool DeletePolicy(int id)
        {
            throw new NotImplementedException();
        }
        public bool UpdatePolicy(int id)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<Beneficiary> GetBeneficiaries
        {
            get { return Policy.Beneficiaries; }
        }
    }
}
