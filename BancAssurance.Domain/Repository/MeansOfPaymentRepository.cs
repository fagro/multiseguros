﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;
namespace Multi_Insurance.Domain.Repository
{
    public class MeansOfPaymentRepository : IMeansOfPaymentRepository
    {
        private DbDataAccess db;

        public MeansOfPaymentRepository()
        {
            db = new DbDataAccess();
            
        }
        public IEnumerable<MeansOfPaymentOptions> GetMeansOfPaymentOptions(short id = 0)
        {

            var data = db.GetMeansOfPaymentOptions(id);
            IList<MeansOfPaymentOptions> meansOfPaymentOptions = new List<MeansOfPaymentOptions>();

            foreach (DataRow item in data.Rows)
            {
                meansOfPaymentOptions.Add(new MeansOfPaymentOptions
                {
                    MeansOfPaymentName = item["MeansOfPaymentOptionName"].ToString(),
                    MeansOfPaymentCode = short.Parse(item["MeansOfPaymenOptiontCode"].ToString()),
                    Modified = DateTime.Parse(item["LastModification"].ToString())
                });
            }

            return meansOfPaymentOptions;
         }

        public short AddMeansOfPaymentOption(MeansOfPaymentOptions meansOfPayment)
        {
            return db.AddMeansOfPaymentOption(meansOfPayment);
        }

        public short AddMeansOfPayment(MeansOfPayment meansOfPayment)
        {
            return db.AddMeansOfPayment(meansOfPayment);
        }

        public IEnumerable<MeansOfPaymentOptions> GetPolicyMeansOfPaymentOptions(int id = 0)
        {
            var data = db.GetPolicyMeansOfPaymentOptions(id);
            IList<MeansOfPaymentOptions> meansOfPaymentOptions = new List<MeansOfPaymentOptions>();

            foreach (DataRow item in data.Rows)
            {
                meansOfPaymentOptions.Add(new MeansOfPaymentOptions
                {
                    MeansOfPaymentName = item["MeansOfPaymentOptionName"].ToString(),
                    MeansOfPaymentCode = short.Parse(item["MeansOfPaymenOptiontCode"].ToString()),
                    Modified = DateTime.Parse(item["LastModification"].ToString())
                });
            }
            return meansOfPaymentOptions;
        }
    }
}
