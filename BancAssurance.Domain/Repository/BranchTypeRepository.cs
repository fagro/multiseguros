﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class BranchTypeRepository : IBranchTypeRepository
    {
        private DbDataAccess db;

        public BranchTypeRepository()
        {
            db = new DbDataAccess();
        }

        public short AddBranchType(BranchType branchType)
        {
            return db.AddBrachType(branchType);
        }


        public IEnumerable<BranchType> GetBranchTypes(short id =0)
        {
            var data = db.GetBranchType(id);
            IList<BranchType> brancList = new List<BranchType>();

            foreach (DataRow item in data.Rows)
            {
                brancList.Add(new BranchType
                {
                    Id = short.Parse(item["TiposRamosId"].ToString()),
                    Code = short.Parse(item["CodTipRam"].ToString()),
                    Name = item["NomTipRam"].ToString(),
                    Modified = DateTime.Parse(item["FecUltMod"].ToString())
                });
            }
            return brancList;
        }
    }
}
