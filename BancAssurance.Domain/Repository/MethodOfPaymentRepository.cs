﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;
namespace Multi_Insurance.Domain.Repository
{
    public class MethodOfPaymentRepository : IMethodOfPaymentRepository
    {
        private DbDataAccess db;

        public MethodOfPaymentRepository()
        {
            db = new DbDataAccess();
        }
        public IEnumerable<MethodOfPaymentOptions> GetMethodOfPaymentOptions(string productCode = null)
        {
            var data = db.GetMethodOfPaymentOptions(productCode);
            IList<MethodOfPaymentOptions> methodOfPaymentsOptions = new List<MethodOfPaymentOptions>();

            foreach (DataRow item in data.Rows)
            {
                methodOfPaymentsOptions.Add(new MethodOfPaymentOptions
                {
                    MethodOfPaymentOptionName = item["MethodOfPaymentOptionName"].ToString(),
                    MethodOfPaymentOptionsCode = short.Parse(item["MethodOfpaymentOptionCode"].ToString()),
                    MethodOfPaymentOptionDivisor = short.Parse(item["MethodOfPaymentOptionDivisor"].ToString()),
                    LastModified = DateTime.Parse(item["LastModified"].ToString())
                });
            }

            return methodOfPaymentsOptions;
        }

        public short AddMethodOfPaymentOption(MethodOfPaymentOptions paymentOptions)
        {
            return db.AddMethodOfPaymentOption(paymentOptions);
        }

        public short AddMethodPayment(MethodOfPayment methodOfPayment)
        {
            return db.AddMethodOfPayment(methodOfPayment);
        }

        public IEnumerable<MethodOfPaymentOptions> GetPolicyMethodOfPaymentOptions(int id = 0)
        {
             var data = db.GetPolicyMethodOfPaymentOptions(id);
            IList<MethodOfPaymentOptions> methodOfPaymentsOptions = new List<MethodOfPaymentOptions>();

            foreach (DataRow item in data.Rows)
            {
                methodOfPaymentsOptions.Add(new MethodOfPaymentOptions
                {
                    MethodOfPaymentOptionName = item["MethodOfPaymentOptionName"].ToString(),
                    MethodOfPaymentOptionsCode = short.Parse(item["MethodOfpaymentOptionCode"].ToString()),
                    MethodOfPaymentOptionDivisor = short.Parse(item["MethodOfPaymentOptionDivisor"].ToString()),
                    LastModified = DateTime.Parse(item["LastModified"].ToString())
                });
            }
            return methodOfPaymentsOptions;
        }


        public int DeleteMethodOfPaymentOption(int id)
        {
            return db.DeleteMethodOfPaymentOption(id);
        }
        
    }
}
