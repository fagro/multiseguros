﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class ProductRepository : IProductRepository
    {
        private DbDataAccess _db;

        public ProductRepository()
        {
            _db = new DbDataAccess();
        }

        public short AddProductBasicData(Product product)
        {
            return _db.AddProductBasicData(product);
        }

        public IEnumerable<Product> GetProducts(int productId = 0)
        {
            var data = _db.GetProductBasicData(productId);
            IList<Product> products = new List<Product>();

            foreach (DataRow item in data.Rows)
            {
                products.Add(new Product
                {
                    Id = int.Parse(item["IdProductos"].ToString()),
                    Name = item["NomPro"].ToString(),
                    ProductCode = item["CodPro"].ToString(),
                    InsuranceCompanyId = !string.IsNullOrEmpty(item["NitComSeg"].ToString()) ? short.Parse(item["NitComSeg"].ToString()) : (short)0,
                    CurrencyRateId = !string.IsNullOrEmpty(item["CodTipMon"].ToString()) ? short.Parse(item["CodTipMon"].ToString()) : (short)0,
                    BranchId = !string.IsNullOrEmpty(item["CodTipRam"].ToString()) ? short.Parse(item["CodTipRam"].ToString()) : (short) 0,
                    RepresentativeId = !string.IsNullOrEmpty(item["NitGes"].ToString()) ? short.Parse(item["NitGes"].ToString()) : (short) 0,
                    TypeIndexingId = !string.IsNullOrEmpty(item["CodTipInd"].ToString()) ? short.Parse(item["CodTipInd"].ToString()) : (short)0,
                    MarketingId = !string.IsNullOrEmpty(item["CodTipMer"].ToString()) ? short.Parse(item["CodTipMer"].ToString()) : (short) 0,
                    Contributive = !string.IsNullOrEmpty(item["IndProCon"].ToString()) && bool.Parse(item["IndProCon"].ToString()),
                    OptativeShelter = !string.IsNullOrEmpty(item["IndAmpSel"].ToString()) && bool.Parse(item["IndAmpSel"].ToString()),
                    Itbis = !string.IsNullOrEmpty(item["iva"].ToString()) && bool.Parse(item["iva"].ToString()),
                    Emit = !string.IsNullOrEmpty(item["expedir"].ToString()) && bool.Parse(item["expedir"].ToString()),
                    CutDay = !string.IsNullOrEmpty(item["FecCorPro"].ToString()) ? item["FecCorPro"].ToString() : "",
                    InitSaleDay = !string.IsNullOrEmpty(item["FecIniVen"].ToString()) ? DateTime.Parse(item["FecIniVen"].ToString()): new DateTime(),
                    NewPromotorCommission = !string.IsNullOrEmpty(item["PorComPro"].ToString()) ? decimal.Parse(item["PorComPro"].ToString()) : 0,
                    RenewPromotorCommission = !string.IsNullOrEmpty(item["porcompror"].ToString()) ? decimal.Parse(item["porcompror"].ToString()) : 0,
                    NewBankCommission = !string.IsNullOrEmpty(item["honocobn"].ToString()) ? decimal.Parse(item["honocobn"].ToString()) : 0,
                    RenewBankCommission = !string.IsNullOrEmpty(item["honocobr"].ToString())  ? decimal.Parse(item["honocobr"].ToString()) :0,
                    MenDiscount =  !string.IsNullOrEmpty(item["medadm"].ToString()) ?decimal.Parse(item["medadm"].ToString()) : 0,
                    WomenDiscount = !string.IsNullOrEmpty(item["medadf"].ToString()) ? decimal.Parse(item["medadf"].ToString()) : 0,
                    NewSalerIncentive = !string.IsNullOrEmpty(item["PORINCENT"].ToString()) ? decimal.Parse(item["PORINCENT"].ToString()) : 0,
                    RenewSalerIncentive = !string.IsNullOrEmpty(item["PORINCENTR"].ToString()) ? decimal.Parse(item["PORINCENTR"].ToString()) : 0,
                    Modified = !string.IsNullOrEmpty(item["FecUltMod"].ToString()) ? DateTime.Parse(item["FecUltMod"].ToString()) : DateTime.Now

                });
            }

            return products;

        }

        public short AddProductPayPlan(Dictionary<string, string> model)
        {
            return _db.AddProductPayPlan(model);
        }

        public short AddShelterAndCondition(Dictionary<string, string> model)
        {
            return _db.AddProductShelterAndCondition(model);
        }
    }
}
