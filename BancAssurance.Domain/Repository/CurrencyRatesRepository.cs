﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class CurrencyRatesRepository : ICurrencyRatesRepository
    {
        private DbDataAccess _db;

        public CurrencyRatesRepository()
        {
            _db = new DbDataAccess();
        }

        public short AddCurrencyRate(CurrencyRates currencyRates)
        {
            return _db.AddCurrencyRates(currencyRates);
        }

        public IEnumerable<CurrencyRates> GetCurrencyRates(short id = 0)
        {
            
                var data = _db.GetCurrencyRates(id);
                IList<CurrencyRates> currencyRates = new List<CurrencyRates>();

                foreach (DataRow item in data.Rows)
                {
                    currencyRates.Add(new CurrencyRates
                    {
                        CurrencyRatesId = short.Parse(item["TiposMonedaId"].ToString()),
                        Name = item["NomTipMon"].ToString(),
                        Symbol = item["simbolo"].ToString(),
                        Modified = DateTime.Parse(item["FecUltMod"].ToString())
                    });
                }
                return currencyRates;
            
        }
    }
}
