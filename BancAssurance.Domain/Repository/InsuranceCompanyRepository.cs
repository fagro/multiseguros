﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Multi_Insurance.DataAccessLayer.DataAccess;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;

namespace Multi_Insurance.Domain.Repository
{
    public class InsuranceCompanyRepository : IInsuranceCompanyRepository
    {
        private DbDataAccess db;

        public InsuranceCompanyRepository()
        {
            db = new DbDataAccess();
        }

        public int AddCompany(InsuranceCompany insuranceCompany)
        {
            return db.AddInsuranceCompany(insuranceCompany);
        }

        public IEnumerable<InsuranceCompany> GetInsuranceCompanies(short id = 0)
        {
            var data = db.GetCompanyList(id);
            IList<InsuranceCompany> companies = new List<InsuranceCompany>();

            foreach (DataRow item in data.Rows)
            {
                companies.Add(new InsuranceCompany
                {
                    Id = short.Parse(item["NitComSeg"].ToString()),
                    Name = item["NomComSeg"].ToString(),
                    Phone = item["TelComSeg"].ToString(),
                    Address = item["DirComSeg"].ToString(),
                    Contact = item["CONTACTO"].ToString(),
                    Percentage = short.Parse(item["Porciento"].ToString()),
                    Rnc = item["RNC"].ToString()
                });
            }
            return companies;
        }


        public int DeleteInsuranceCompany(int id)
        {
            return db.DeleteInsuranceCompany(id);
        }

    }
}
