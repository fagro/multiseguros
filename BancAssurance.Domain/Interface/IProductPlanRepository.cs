﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;
namespace Multi_Insurance.Domain.Interface
{
    public interface IProductPlanRepository
    {
        IEnumerable<ProductPlan> GetProductsPlan(string productCode = null);
        int AddProductPlan(ProductPlan productPlan);
        IEnumerable<ProductPlan> GetPolicyProductsPlan(int productCode);
        IEnumerable<ProductPlan> GetProductsPlanOptions(short productCode = 0);
        int DeleteProductPlan(int id);
    }
}

