﻿using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IPolicyRepository
    {
        Policy Policy { get; }
        bool AddPolicy(Policy mPolicy);
        Policy FindPolicy(int id);
        bool DeletePolicy(int id);
        bool UpdatePolicy(int id);
        IEnumerable<Beneficiary> GetBeneficiaries { get; }
    }
}
