﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;
namespace Multi_Insurance.Domain.Interface
{
    public interface IMeansOfPaymentRepository
    {
        IEnumerable<MeansOfPaymentOptions> GetMeansOfPaymentOptions(short id = 0);
        IEnumerable<MeansOfPaymentOptions> GetPolicyMeansOfPaymentOptions(int id = 0);
        short AddMeansOfPaymentOption(MeansOfPaymentOptions meansOfPayment);

        short AddMeansOfPayment(MeansOfPayment meansOfPayment);
    }
}
