﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IRepresentativeRepository
    {
        short AddRepresentative(Representative representative);
        IEnumerable<Representative> GetRepresentatives(short id = 0);
    }
}
