﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IDependentRepository
    {
        IList<Dependent> GetDependents { get; }
        bool AddDependent(Dependent mDependent);
        bool UpdateDependent(Dependent mDependent);
        bool DeleteDependent(string IdProduct, int IdPolicy, string IdNumber);
        
    }

}
