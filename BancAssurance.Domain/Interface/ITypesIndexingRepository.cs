﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface ITypesIndexingRepository
    {
        short AddTypesIndexing(TypesIndexing typesIndexing);
        IEnumerable<TypesIndexing> GetTypesIndexings(short id = 0);
    }
}
