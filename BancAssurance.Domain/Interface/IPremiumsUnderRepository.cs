﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IPremiumsUnderRepository
    {
        IEnumerable<PremiumsUnder> GetPremiumsUnders { get; }
        int AddPremiumsUnder(PremiumsUnder premiumsUnder);
    }
}
