﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
   public interface IBranchTypeRepository
   {
       short AddBranchType(BranchType branchType);
       IEnumerable<BranchType> GetBranchTypes(short id = 0);
   }
}
