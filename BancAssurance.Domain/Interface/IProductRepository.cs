﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IProductRepository
    {
        short AddProductBasicData(Product product);
        IEnumerable<Product> GetProducts(int productId = 0);
        short AddProductPayPlan(Dictionary<string, string> model);
        short AddShelterAndCondition(Dictionary<string, string> model);
    }
}
