﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IInsuranceCompanyRepository
    {
        int AddCompany(InsuranceCompany insuranceCompany);
        IEnumerable<InsuranceCompany> GetInsuranceCompanies(short id = 0);
        int DeleteInsuranceCompany(int id = 0);
    }
}
