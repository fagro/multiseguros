﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;
namespace Multi_Insurance.Domain.Interface
{
    public interface IMethodOfPaymentRepository
    {
        IEnumerable<MethodOfPaymentOptions> GetMethodOfPaymentOptions(string productCode = null);
        IEnumerable<MethodOfPaymentOptions> GetPolicyMethodOfPaymentOptions(int id = 0);
        short AddMethodOfPaymentOption(MethodOfPaymentOptions paymentOptions);
        int DeleteMethodOfPaymentOption(int id);

        short AddMethodPayment(MethodOfPayment methodOfPayment);
    }
}
