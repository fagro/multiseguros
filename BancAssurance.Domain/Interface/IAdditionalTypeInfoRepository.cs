﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IAdditionalTypeInfoRepository
    {
        IEnumerable<AdditionalTypeInfo> GetAdditionalTypeInfos(short id = 0);
        short AddAdditionalTypeInfo(AdditionalTypeInfo additionalTypeInfo);
    }
}
