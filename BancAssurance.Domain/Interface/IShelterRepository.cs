﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IShelterRepository
    {
        IEnumerable<ShelterType> GetShelters(short id = 0);
        short AddShelter(ShelterType shelterType);
    }
}
