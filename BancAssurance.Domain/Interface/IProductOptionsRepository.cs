﻿using System.Collections.Generic;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IProductOptionsRepository
    {
        IEnumerable<ProductOptions> GetProductOptionsByProductCode(string productCode);
        IEnumerable<ProductOptions> GetProductOptionsByPlanCode(short planCode, string productCode);
        short AddProductOptions(ProductOptions productOptions);
    }
}
