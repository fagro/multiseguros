﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.Domain.Interface
{
    public interface IBeneficiaryRepository
    {
        IList<Beneficiary> GetBeneficiaries { get; }
        bool AddBeneficiary(Beneficiary mbeneficiary);
        bool UpdateBeneficiary(Beneficiary mbeneficiary);
        bool DeleteBeneficiary (int id);
        }
}
