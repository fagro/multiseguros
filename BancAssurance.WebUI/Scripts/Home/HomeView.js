﻿
$('#btnChooseProduct').click(function () {


    var code = $('#ddlProductToChoose').val();
    var name = $('#ddlProductToChoose option[value="' + code + '"]').text();
    var product = new Object();
    product.Name = name;
    product.ProductCode = code;
    var myData = JSON.stringify(product);
    if (product.ProductCode === "") {
        $('#alertsMessage').append('<div class="alert alert-dismissable alert-danger text-center"> <button type="button"' +
            '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4>Mensaje</h4>' +
            '<p> Oh oh! <strong>No hay productos</strong> seleccionados</p> </div>');
        createAutoClosingAlert('#alertsMessage', 2000);
    } else {

        $.ajax({
            type: 'POST',
            url: '/Home/ChooseProductToUse',
            contentType: 'application/json',
            dataType: 'json',
            data: myData,
            success:
               function (data) {
                   $('#alertsMessage').append('<div class="alert alert-dismissable alert-success text-center"> <button type="button"' +
                    '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '<h4>Mensaje</h4>' +
                    '<p>Las pólizas <strong>utilizaran</strong> automaticamente este producto</p> </div>');
                   createAutoClosingAlert('#alertsMessage', 2000);
               },
            error: function (data) {
                $('#alertsMessage').append('<div class="alert alert-dismissable alert-danger text-center"> <button type="button"' +
                    '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                    '<h4>Oh!!! Ocurrio un Error en servidor :( </h4>' +
                    '<p>Intente de nuevo, disculpe el inconveniente </p> </div>');
                createAutoClosingAlertDanger('#alertsMessage', 4000);
            }
        });
    }

});

function createAutoClosingAlert(selector, delay) {

    window.setTimeout(function () {
        $(selector).fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 3000);

    $('#alerts').append("<div id='alertsMessage'></div>");
}


function createAutoClosingAlertDanger(selector, delay) {

    window.setTimeout(function () {
        $(selector).fadeTo(500, 0).slideUp(800, function () {
            $(this).remove();
        });
    }, 5000);

    $('#alerts').append("<div id='alertsMessage'></div>");
}




//$('#createPolicyLink').click(function() {

//    if ($('#ddlProductToChoose').val() != null) {

//        $('#createPolicyLink').click();
//        console.log();
//    } else {
//        $('#alertsMessage').append('<div class="alert alert-dismissable alert-danger text-center"> <button type="button"' +
//                '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
//                '<h4>Mensaje :( </h4>' +
//                '<p>Debe seleccionar un producto para crear pólizas </p> </div>');
//        createAutoClosingAlertDanger('#alertsMessage', 3000);
//    }
//});
