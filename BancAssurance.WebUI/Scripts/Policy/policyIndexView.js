﻿
function onSuccessClient(data) {
    
    console.log('madeItPolicyCLient');
        $('#listPolicy').removeClass("disabled");
        $('#clientList').addClass("disabled");

        $('#policyNext').attr('href', '#policyData');
        $('#clientNext').attr('href', '#');
    
        $('#policyNext').click();
        $('#policyDataScroll').click();

}

function OnSuccessPolicyData(data) {

    console.log('madeItPolicyData');
    
        $('#listBeneficiary').removeClass("disabled");
        $('#listPolicy').addClass("disabled");
        
        $('#beneficiaryNext').attr('href', '#beneficiary');
        $('#policyNext').attr('href', '#');
        
        $('#beneficiaryNext').click();
    }

function onSuccessBeneficiary(data) {
    
    
}


$('#addBeneficiary').click(function () {


    var beneficiaryName = $('#beneficiaryName').val();
    var beneficiaryLastName = $('#beneficiaryLastName').val();
    var beneficiaryRelationship = $('#beneficiaryRelationship').val();
    var beneficiaryPercentage = $('#beneficiaryPercentage').val();
    var percentage = $('.percentageBene').val();
    
    if (beneficiaryPercentage < 100 ) {
        
    }
    if (beneficiaryName != "" &&  beneficiaryLastName !=  "" && beneficiaryRelationship != "") {
        
        $('#beneficiaryTable > tbody:last').append('<tr>' +
                                                    '<td>' + beneficiaryName + '</td>' +
                                                    '<td>' + beneficiaryLastName + '</td>' +
                                                    '<td>' + beneficiaryRelationship + '</td> ' +
                                                    '<td class= "percentageBene">' + beneficiaryPercentage + '</td> ' +
                                                '</tr>');
        $('#beneficiaryName').val("");
        $('#beneficiaryLastName').val("");
        $('#beneficiaryRelationship').val("");
        $('#beneficiaryPercentage').val("");
        
        $('#createPolicyButton').removeClass('disabled');
    }                           
});


$('#createPolicyButton').click(function () {

    var beneficiaryList = $('#beneficiaryTable').tableToJSON();
    beneficiaryList = JSON.stringify(beneficiaryList);
    $.ajax({
        type: 'POST',
        url: ' /Policy/CreatePolicy',
        contentType: 'application/json',
        data: beneficiaryList,
        success: function(data) {
            if (data)
                $('#alerts').append('<div class="alert alert-success fade in text-center"> <button type="button"' +
                '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                     '<h4>Poliza creada con exito </h4>' +
                                        '<p>Codigo de poliza:'+data+'</p> </div>');
        },
        error: function(data) {
            $('#alerts').append('<div class="alert alert-danger fade in text-center"> <button type="button"' +
                '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                                     '<h4>Oh!!! Ocurrio un Error en servidor :( </h4>' +
                                        '<p>Intente de nuevo, disculpe el inconveniente </p> </div>' );
        }      
    });
    $('.alert').alert();
});

$("#ddlProductPlans").change(function () {
    var planCode = parseInt($("#ddlProductPlans").val());
    if (!isNaN(planCode)) {
        var ddOptionValue = $("#ddlValueOptions");
        ddOptionValue.empty();
        ddOptionValue.append($("<option></option>").val("").html("Seleccione un valor asegurado"));
        
        $.ajax({
            type: "POST",
            url: '/Policy/GetPremiumValuesByPlanCode',
            data: { planCode: planCode },
            dataType : "json",
            success: function (data) {
               
                $.each(data, function (i, val) {
                    ddOptionValue.append($("<option></option>").val(val.PlanCode).html(val.ValueOptions));
                });
            },
            error: function(data) {
                onFailureAlertMessage(data);
            } 
        });
    }
   
});



function onFailureAlertMessage(data) {

    $('#alerts').append('<div class="alert alert-danger fade in text-center"> <button type="button"' +
              '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                   '<h4>Oh!!! Ocurrio un Error al procesar su solicitud :( </h4>' +
                                      '<p>Intente de nuevo con otros valores, disculpe el inconveniente </p> </div>');
    createAutoClosingAlertDanger('#alerts', 2000);
}


function createAutoClosingAlertDanger(selector, delay) {

    window.setTimeout(function () {
        $(selector).fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 5000);
}