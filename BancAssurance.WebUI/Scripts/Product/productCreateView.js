﻿

function onSuccessAlertMessage(data) {

    $('#alerts').append('<div class="alert alert-success fade in text-center"> <button type="button"' +
            '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4> Mensaje</h4>' +
            '<p> Operación completa</p> </div>');
    
    

    createAutoClosingAlert('#alerts', 2000);

}

function onFailureAlertMessage(data) {
    
    $('#alerts').append('<div class="alert alert-danger fade in text-center"> <button type="button"' +
              '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                   '<h4>Oh!!! Ocurrio un Error al procesar su solicitud :( </h4>' +
                                      '<p>Intente de nuevo con otros valores, disculpe el inconveniente'+data+' </p> </div>');
    createAutoClosingAlertDanger('#alerts', 2000);
}


function onSuccessAlertInfoMessage(data) {

    $('#alerts').append('<div class="alert alert-info fade in text-center"> <button type="button"' +
               '                    class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                                    '<h4> Mensaje' + +'</h4>' +
                                       '<p> Agregado con éxito :) </p> </div>');
    createAutoClosingAlert('#alerts', 2000);

}


function createAutoClosingAlert(selector, delay) {
   
    window.setTimeout(function () {
        $(selector).fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 3000);
}


function createAutoClosingAlertDanger(selector, delay) {

    window.setTimeout(function () {
        $(selector).fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 5000);
}


$("#idCompany").change(function () {

        $.ajax({
            type: 'POST',
            url: ' /Product/GetCompanyById',
            contentType: 'application/json',
            data: {id: $('idCompany').val()},
            dataType: 'json',
            success: function(data) {
                $('#InsuranceCompany_Name').val(data.name);
                $('#InsuranceCompany_Rnc').val(data.Rnc);
                $('#InsuranceCompany_Address').val(data.Address);
                $('#InsuranceCompany_Percentage').val(data.Percentage);
                $('#InsuranceCompany_Phone').val(data.Phone);
                $('#InsuranceCompany_Contact').val(data.Contact);
                console.log(JSON.parse(data));
                var result = JSON.parse(data);
            }
        });
    });
    
