﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Multi_Insurance.DataAccessLayer.Entities;
using Multi_Insurance.Domain.Interface;
using Multi_Insurance.WebUI.Models;

namespace Multi_Insurance.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepository _productRepository;
        private ProductViewModel productViewModel;
        private HomeViewModel _homeViewModel;
        public HomeController(IProductRepository repository)
        {
            _productRepository = repository;
            productViewModel = new ProductViewModel();
            _homeViewModel = new HomeViewModel();
        }
        //
        // GET: /Home/
        public ActionResult Index()
        {
            //set the project name 
            ViewBag.Title = "MultiSeguros";
            ViewBag.AppName = "Multiseguros - Inicio";
            
            _homeViewModel.ProductSelectListItems = _productRepository.GetProducts(0).ToSelectListItems(0);
            if (_homeViewModel.ProductSelectListItems != null)
                return View(_homeViewModel);
            _homeViewModel.ProductSelectListItems = new List<SelectListItem>();
            return View(_homeViewModel);
        }


        public JsonResult ChooseProductToUse(HomeViewModel myData)
        {
            Session["ProductChosen"] = myData;
            return Json(bool.TrueString);
        }
	}
}