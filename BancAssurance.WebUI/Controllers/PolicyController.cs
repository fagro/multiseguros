﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Multi_Insurance.Domain.Interface;
using Multi_Insurance.WebUI.Models;
using System.Collections.Generic;


namespace Multi_Insurance.WebUI.Controllers
{
    public class PolicyController : Controller
    {
        #region Repositories Instances

        private IPolicyRepository policyRepository;
        private IProductPlanRepository _productPlanRepositoryRepository;
        private IMethodOfPaymentRepository _methodOfPaymentRepository;
        private IMeansOfPaymentRepository _meansOfPaymentRepository;
        private IProductOptionsRepository _productOptionsRepository;
        #endregion

        #region Constructor
        public PolicyController(IPolicyRepository policyRepository, IProductPlanRepository productPlanRepositoryRepository, 
                                IMethodOfPaymentRepository methodOfPaymentRepository, IMeansOfPaymentRepository meansOfPaymentRepository,
                                IProductOptionsRepository productOptionsRepository)
        {
            this.policyRepository = policyRepository;
            _productPlanRepositoryRepository = productPlanRepositoryRepository;
            _methodOfPaymentRepository = methodOfPaymentRepository;
            _meansOfPaymentRepository = meansOfPaymentRepository;
            _productOptionsRepository = productOptionsRepository;

        }
        #endregion

        #region Action Methods
        // GET: /Policy/
        public ActionResult Index()
        {
            ViewBag.Product = "Vida";
            return View();
        }


        public ViewResult Search()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Search(int id)
        {
            return View();
        }

        //
        // GET: /Policy/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Policy/Create
        public ActionResult Create()
        {
            var homeModel = (Session["ProductChosen"] as HomeViewModel);
            if (homeModel == null)
                return RedirectToAction("Index", "Home");
            else
            {
                var model = CreatePolicyModel();
                return View(model);   
            } 
        }

        public PolicyViewModel CreatePolicyModel()
        {
            var model = new PolicyViewModel();
            var homeModel = (Session["ProductChosen"] as HomeViewModel);

            if (homeModel != null)
            {
                ViewBag.ProductName = homeModel.Name;
                
            }

            model.Beneficiaries = policyRepository.GetBeneficiaries;
            model.ProductPlansSelectListItems = _productPlanRepositoryRepository.GetPolicyProductsPlan(int.Parse(homeModel.ProductCode)).ToSelectListItems();
            model.MethodOfPaymentOptionsSelectListItems =
                _methodOfPaymentRepository.GetPolicyMethodOfPaymentOptions(int.Parse(homeModel.ProductCode)).ToSelectListItems();
            model.MeansOfPaymentSelectListItemss =
                _meansOfPaymentRepository.GetPolicyMeansOfPaymentOptions(int.Parse(homeModel.ProductCode)).ToSelectListItems();
            model.ProductOptionSelectListItems = _productOptionsRepository.GetProductOptionsByProductCode(homeModel.ProductCode).ToSelectListItems(-1);
            return model;
        }
        //
        // POST: /Policy/CreateClient

        public ActionResult CreatePolicyClient(PolicyViewModel policy)
        {
            if (!ModelState.IsValid)
                return PartialView(policy);

            if (!Request.IsAjaxRequest()) return PartialView();

            Session["Client"] = policy.Client;
            
            return PartialView();
        }


        //POST: /Insured/Create
        public ActionResult CreatePolicyInsured(PolicyViewModel viewModel)
        {
            return PartialView();
        }
    

        // POST: /Policy/CreatePolicyData
        public ActionResult CreatePolicyData(PolicyViewModel policy)
        {
            if (!ModelState.IsValid)
                return PartialView(policy);
            
            if (!Request.IsAjaxRequest())
                return PartialView();

            Session["Policy"] = policy;
            var model = CreatePolicyModel();
            return PartialView(model);
        }


      

        //
        // GET: /Policy/Edit/5
        public ActionResult Edit(int id)
        {
            //TODO: Get the policy to edit, create the policy object and pass it to the view.
            return View();
        }

        //
        // POST: /Policy/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Policy/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Policy/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult MassivePolicyGeneration()
        {
            var homeModel = (Session["ProductChosen"] as HomeViewModel);
            if (homeModel == null)
                return RedirectToAction("Index", "Home");

            ViewBag.ProductName = homeModel.Name;
            return View();
        }
        #endregion 

        #region Ajax Actions Methods

        [HttpPost]
        public ActionResult CreatePolicy(PolicyViewModel beneficiaryList)
        {
            var clientData = Session["Client"] as PolicyViewModel;
            var policy = Session["Policy"] as PolicyViewModel;
            if (clientData != null && policy != null)
            {
                policy.Client = clientData.Client;
                policy.Beneficiaries = beneficiaryList.Beneficiaries;

                //var data = policyRepository.AddPolicy(policy);
            }
        
            //if (data)
            //    return RedirectToAction("Create");

            return View(beneficiaryList);
        }


        [HttpPost]
        public JsonResult GetPolicyPremiumValue(PolicyViewModel policyViewModel)
        {
            var product = (Session["ProductChosen"] as HomeViewModel);
            if (product == null) return Json("error");
            //var data = _productOptionsRepository.GetProductOptionsByProductCode(product.ProductCode).ToSelectListItems();

            return Json(product);
        }

        [HttpPost]
        public JsonResult GetPremiumValuesByPlanCode(string planCode = "")
        {

            var homeModel = (Session["ProductChosen"] as HomeViewModel);
            if (homeModel == null)
                return new JsonResult
                {
                    Data = "El producto no ha sido seleccionado",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            if (string.IsNullOrEmpty(planCode))
                return new JsonResult
                {
                    Data = "An Error occur",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            if (Request.IsAjaxRequest())
            {
                return new JsonResult
                {
                    Data = _productOptionsRepository.GetProductOptionsByPlanCode(short.Parse(planCode), homeModel.ProductCode),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return new JsonResult
            {
                Data = "An Error occur",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        #endregion

    }
}


//TODO: In order to validade policies is required that the insurnace company validate the policy that this app produce. 