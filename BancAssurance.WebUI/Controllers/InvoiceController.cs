﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Multi_Insurance.WebUI.Models;

namespace Multi_Insurance.WebUI.Controllers
{
    public class InvoiceController : Controller
    {
        //
        // GET: /Invoice/
        public ActionResult GenerateInvoice()
        {
            var homeModel = (Session["ProductChosen"] as HomeViewModel);
            if (homeModel == null)
                return RedirectToAction("Index", "Home");

            ViewBag.ProductName = homeModel.Name;
            return View();
        }


        [HttpPost]
        public ActionResult GenerateInvoices()
        {
            return View();
        }

        public ActionResult GenerateCharge()
        {
            var homeModel = (Session["ProductChosen"] as HomeViewModel);
            if (homeModel == null)
                return RedirectToAction("Index", "Home");

            ViewBag.ProductName = homeModel.Name;
            return View();
        }

        [HttpPost]
        public ActionResult GenerateCharges()
        {
            return View();
        }
        
	}
}