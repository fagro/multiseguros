﻿using System.Linq;
using System.Net.Configuration;
using System.Web.Mvc;
using Multi_Insurance.Domain.Interface;
using Multi_Insurance.WebUI.Models;

namespace Multi_Insurance.WebUI.Controllers
{
    public class ProductController : Controller
    {
        #region Instances
        private IInsuranceCompanyRepository _insuranceCompanyRepository;
        private IProductPlanRepository _productPlanRepository;
        private IMethodOfPaymentRepository _methodOfPaymentRepository;
        private IMeansOfPaymentRepository _meansOfPaymentRepository;
        private IBranchTypeRepository _branchTypeRepository;
        private IProductRepository _productRepository;
        private ProductViewModel productViewModel;
        private IProductOptionsRepository _productOptionsRepository;
        private IShelterRepository _shelterRepository;
        private IPremiumsUnderRepository _premiumsUnderRepository;
        private IRepresentativeRepository _representativeRepository;
        private ITypesIndexingRepository _typesIndexingRepository;
        private ICurrencyRatesRepository _currencyRatesRepository;
        private IAdditionalTypeInfoRepository _additionalTypeInfoRepository;
        #endregion

        #region Constructor

        public ProductController(IInsuranceCompanyRepository insuranceCompanyRepository,
                                  IProductPlanRepository productPlanRepository,
                                  IMethodOfPaymentRepository methodOfPaymentRepository,
                                  IMeansOfPaymentRepository meansOfPayment,
                                  IBranchTypeRepository branchTypeRepository,
                                  IProductRepository productRepository,
                                  IProductOptionsRepository productOptionsRepository,
                                  IShelterRepository shelterRepository,
                                  IPremiumsUnderRepository premiumsUnderRepository,
                                  IRepresentativeRepository representativeRepository,
                                  ITypesIndexingRepository typesIndexingRepository,
                                  ICurrencyRatesRepository currencyRatesRepository,
                                  IAdditionalTypeInfoRepository additionalTypeInfoRepository)
        {
            _insuranceCompanyRepository = insuranceCompanyRepository;
            _productPlanRepository = productPlanRepository;
            _methodOfPaymentRepository = methodOfPaymentRepository;
            _meansOfPaymentRepository = meansOfPayment;
            _branchTypeRepository = branchTypeRepository;
            _productRepository = productRepository;
            _productOptionsRepository = productOptionsRepository;
            _shelterRepository = shelterRepository;
            _premiumsUnderRepository = premiumsUnderRepository;
            _representativeRepository = representativeRepository;
            _typesIndexingRepository = typesIndexingRepository;
            _currencyRatesRepository = currencyRatesRepository;
            _additionalTypeInfoRepository = additionalTypeInfoRepository;
            productViewModel = new ProductViewModel();
        }

        #endregion


        public ActionResult Index()
        {
            return View(productViewModel);
        }

        #region Product Administration

        #region Company CRUD

        [HttpGet]
        public ActionResult ListOfInsuranceCompanies()
        {
            var companies = _insuranceCompanyRepository.GetInsuranceCompanies();
            return View(companies);
        }


        [HttpGet]
        public ActionResult CreateCompany()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DetailsCompany(short id)
        {
            var model = new ProductViewModel();
            var data = _insuranceCompanyRepository.GetInsuranceCompanies(id);
            foreach (var item in data)
            {
                model.InsuranceCompany = item;
            }
            return View(model);

        }


        [HttpPost]
        public ActionResult CreateCompany(ProductViewModel company)
        {
            if (!ModelState.IsValid) return PartialView(company);
            if (!Request.IsAjaxRequest()) return PartialView();

            var result = _insuranceCompanyRepository.AddCompany(company.InsuranceCompany);
            if (result == -1)
            {
                return View();
            }
            return View(company);
        }

        [HttpGet]
        public ActionResult EditCompany(short id)
        {
            var model = new ProductViewModel();
            var data = _insuranceCompanyRepository.GetInsuranceCompanies(id);
            foreach (var item in data)
            {
                model.InsuranceCompany = item;
            }
            return View(model);

        }

        [HttpPost]
        public ActionResult EditCompany(ProductViewModel company)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _insuranceCompanyRepository.AddCompany(company.InsuranceCompany);
            if (result == -1)
            {
                return View();
            }
            return View(company);
        }

        [HttpDelete]
        public ActionResult EditCompany(ProductViewModel company, int id)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _insuranceCompanyRepository.DeleteInsuranceCompany(company.InsuranceCompany.Id);
            if (result == -1)
            {
                return RedirectToAction("ListOfInsuranceCompanies");
            }
            return View();
        }
        #endregion

        #region Product Plans CRUD
        [HttpGet]
        public ActionResult ListOfProductPlan()
        {
            var data = _productPlanRepository.GetProductsPlan();
            return View(data);
        }

        [HttpGet]
        public ActionResult CreateProductPlan()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProductPlan(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return View(productViewModelParams);
            if (!Request.IsAjaxRequest()) return View();

            var result = _productPlanRepository.AddProductPlan(productViewModelParams.ProductPlan);
            return result == -1 ? View() : View(productViewModelParams);
        }

        [HttpGet]
        public ActionResult EditProductPlan(short id)
        {
            var model = new ProductViewModel();
            var data = _productPlanRepository.GetProductsPlanOptions(id);
            foreach (var item in data)
            {
                model.ProductPlan = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditProductPlan(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return View(productViewModelParams);
            if (!Request.IsAjaxRequest()) return View();

            var result = _productPlanRepository.AddProductPlan(productViewModelParams.ProductPlan);
            return result == -1 ? View() : View(productViewModelParams);
        }

        [HttpDelete]
        public ActionResult EditProductPlan(ProductViewModel productViewModelParams, int ProductId)
        {
            if (!ModelState.IsValid) return View(productViewModelParams);
            if (!Request.IsAjaxRequest()) return View();

            var result = _productPlanRepository.DeleteProductPlan(productViewModelParams.ProductPlan.ProductTypeOptionId);
            if (result == -1)
            {
                RedirectToAction("ListOfProductPlan");
            }
            return View(productViewModelParams);
        }


        [HttpGet]
        public ActionResult DetailsProductPlan(short id)
        {
            var model = new ProductViewModel();
            var data = _productPlanRepository.GetProductsPlanOptions(id);
            foreach (var item in data)
            {
                model.ProductPlan = item;
            }
            return View(model);

        }
        #endregion

        #region Product Method of payment CRUD

        [HttpGet]
        public ActionResult ListOfProductMethodOfPaymentOptions()
        {
            var methodOfPayment = _methodOfPaymentRepository.GetMethodOfPaymentOptions();
            return View(methodOfPayment);
        }

        [HttpGet]
        public ActionResult CreateProductMethodOfPaymentOption()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProductMethodOfPaymentOption(ProductViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            if (!Request.IsAjaxRequest()) return View();
            var result = _methodOfPaymentRepository.AddMethodOfPaymentOption(model.MethodOfPaymentOptions);
            return result == -1 ? View() : View(model);

        }


        [HttpGet]
        public ActionResult EditProductMethodOfPaymentOption(short id)
        {
            var model = new ProductViewModel();
            var data = _methodOfPaymentRepository.GetMethodOfPaymentOptions(id.ToString());
            foreach (var item in data)
            {
                model.MethodOfPaymentOptions = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditProductMethodOfPaymentOption(ProductViewModel model)
        {
            if (!ModelState.IsValid) return View(productViewModel);
            if (!Request.IsAjaxRequest()) return View();
            var result = _methodOfPaymentRepository.AddMethodOfPaymentOption(model.MethodOfPaymentOptions);
            return result == -1 ? View() : View(model);
        }

        [HttpDelete]
        public ActionResult EditProductMethodOfPaymentOption(ProductViewModel model, short MethodOfPaymentOptionsCode)
        {
            if (!ModelState.IsValid) return View(productViewModel);
            if (!Request.IsAjaxRequest()) return View();
            var result = _methodOfPaymentRepository.DeleteMethodOfPaymentOption(model.MethodOfPaymentOptions.MethodOfPaymentOptionsCode);
            if (result == -1)
            {
                return RedirectToAction("ListOfProductMethodOfPaymentOptions");
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult DetailsProductMethodOfPaymentOption(short id)
        {
            var model = new ProductViewModel();
            var data = _methodOfPaymentRepository.GetMethodOfPaymentOptions(id.ToString());
            foreach (var item in data)
            {
                model.MethodOfPaymentOptions = item;
            }
            return View(model);
        }

        #endregion

        #region Product Means of payment CRUD

        [HttpGet]
        public ActionResult ListOfProductMeansOfPaymentOptions()
        {
            var methodOfPayment = _meansOfPaymentRepository.GetMeansOfPaymentOptions();
            return View(methodOfPayment);
        }

        [HttpGet]
        public ActionResult CreateProductMeansOfPaymentOption()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProductMeansOfPaymentOption(ProductViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            if (!Request.IsAjaxRequest()) return View();

            var result = _meansOfPaymentRepository.AddMeansOfPaymentOption(model.MeansOfPaymentOptions);
            return result == -1 ? View() : View(model);
        }


        [HttpGet]
        public ActionResult EditProductMeansfPaymentOption(short id)
        {
            var model = new ProductViewModel();
            var data = _meansOfPaymentRepository.GetMeansOfPaymentOptions(id);
            foreach (var item in data)
            {
                model.MeansOfPaymentOptions = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditProductMeansfPaymentOption(ProductViewModel model)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _meansOfPaymentRepository.AddMeansOfPaymentOption(model.MeansOfPaymentOptions);
            if (result == -1)
            {
                return View();
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult DetailsProductMeansOfPaymentOption(short id)
        {
            var model = new ProductViewModel();
            var data = _meansOfPaymentRepository.GetMeansOfPaymentOptions(id);
            foreach (var item in data)
            {
                model.MeansOfPaymentOptions = item;
            }
            return View(model);
        }

        #endregion

        #region Product BranchTypes CRUD

        [HttpGet]
        public ActionResult ListOfBranchTypes()
        {
            var branch = _branchTypeRepository.GetBranchTypes();
            return View(branch);
        }

        [HttpGet]
        public ActionResult CreateBranchType()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateBranchType(ProductViewModel branchModel)
        {
            if (!ModelState.IsValid) return View(branchModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _branchTypeRepository.AddBranchType(branchModel.BranchType);
            return result == -1 ? View() : View(branchModel);
        }


        [HttpGet]
        public ActionResult EditBranchType(short id)
        {
            var model = new ProductViewModel();
            var data = _branchTypeRepository.GetBranchTypes(id);
            foreach (var item in data)
            {
                model.BranchType = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditBranchType(ProductViewModel branchModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _branchTypeRepository.AddBranchType(branchModel.BranchType);
            if (result == -1)
            {
                return View();
            }
            return View(branchModel);
        }


        [HttpGet]
        public ActionResult DetailsBranchType(short id)
        {
            var model = new ProductViewModel();
            var data = _branchTypeRepository.GetBranchTypes(id);
            foreach (var item in data)
            {
                model.BranchType = item;
            }
            return View(model);
        }
        #endregion

        #region Product Representative CRUD

        [HttpGet]
        public ActionResult ListOfRepresentatives()
        {
            var representative = _representativeRepository.GetRepresentatives();
            return View(representative);
        }

        [HttpGet]
        public ActionResult CreateRepresentative()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateRepresentative(ProductViewModel representativeModel)
        {
            if (!ModelState.IsValid) return View(representativeModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _representativeRepository.AddRepresentative(representativeModel.Representative);
            return result == -1 ? View() : View(representativeModel);
        }


        [HttpGet]
        public ActionResult EditRepresentative(short id)
        {
            var model = new ProductViewModel();
            var data = _representativeRepository.GetRepresentatives(id);
            foreach (var item in data)
            {
                model.Representative = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditRepresentative(ProductViewModel representativeModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _representativeRepository.AddRepresentative(representativeModel.Representative);
            if (result == -1)
            {
                return View();
            }
            return View(representativeModel);
        }


        [HttpGet]
        public ActionResult DetailsRepresentative(short id)
        {
            var model = new ProductViewModel();
            var data = _representativeRepository.GetRepresentatives(id);
            foreach (var item in data)
            {
                model.Representative = item;
            }
            return View(model);
        }
        #endregion

        #region Product ProductTypesIndexing CRUD

        [HttpGet]
        public ActionResult ListOfTypesIndexing()
        {
            var typeIndexing = _typesIndexingRepository.GetTypesIndexings();
            return View(typeIndexing);
        }

        [HttpGet]
        public ActionResult CreateTypesIndexing()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateTypesIndexing(ProductViewModel typeIndexingModel)
        {
            if (!ModelState.IsValid) return View(typeIndexingModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _typesIndexingRepository.AddTypesIndexing(typeIndexingModel.TypesIndexing);
            return result == -1 ? View() : View(typeIndexingModel);
        }


        [HttpGet]
        public ActionResult EditTypesIndexing(short id)
        {
            var model = new ProductViewModel();
            var data = _typesIndexingRepository.GetTypesIndexings(id);
            foreach (var item in data)
            {
                model.TypesIndexing = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditTypesIndexing(ProductViewModel typeIndexingModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _typesIndexingRepository.AddTypesIndexing(typeIndexingModel.TypesIndexing);
            if (result == -1)
            {
                return View();
            }
            return View(typeIndexingModel);
        }


        [HttpGet]
        public ActionResult DetailsTypesIndexing(short id)
        {
            var model = new ProductViewModel();
            var data = _typesIndexingRepository.GetTypesIndexings(id);
            foreach (var item in data)
            {
                model.TypesIndexing = item;
            }
            return View(model);
        }
        #endregion

        #region Product CurrencyRates CRUD

        [HttpGet]
        public ActionResult ListOfCurrencyRates()
        {
            var currency = _currencyRatesRepository.GetCurrencyRates();
            return View(currency);
        }

        [HttpGet]
        public ActionResult CreateCurrencyRate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateCurrencyRate(ProductViewModel currencyModel)
        {
            if (!ModelState.IsValid) return View(currencyModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _currencyRatesRepository.AddCurrencyRate(currencyModel.CurrencyRates);
            return result == -1 ? View() : View(currencyModel);
        }


        [HttpGet]
        public ActionResult EditCurrencyRate(short id)
        {
            var model = new ProductViewModel();
            var data = _currencyRatesRepository.GetCurrencyRates(id);
            foreach (var item in data)
            {
                model.CurrencyRates = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditCurrencyRate(ProductViewModel currencyModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _currencyRatesRepository.AddCurrencyRate(currencyModel.CurrencyRates);
            if (result == -1)
            {
                return View();
            }
            return View(currencyModel);
        }


        [HttpGet]
        public ActionResult DetailsCurrencyRate(short id)
        {
            var model = new ProductViewModel();
            var data = _currencyRatesRepository.GetCurrencyRates(id);
            foreach (var item in data)
            {
                model.CurrencyRates = item;
            }
            return View(model);
        }

        #endregion

        #region Product ShelterType CRUD

        [HttpGet]
        public ActionResult ListOfShelterTypes()
        {
            var shelterType = _shelterRepository.GetShelters();
            return View(shelterType);
        }

        [HttpGet]
        public ActionResult CreateShelterType()
        {
            productViewModel.BranchTypesSelectListItems = _branchTypeRepository.GetBranchTypes().ToSelectListItems();
            return View(productViewModel);
        }

        [HttpPost]
        public ActionResult CreateShelterType(ProductViewModel shelterTypeModel)
        {
            InitializeProduct();
            if (!ModelState.IsValid) return View(shelterTypeModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _shelterRepository.AddShelter(shelterTypeModel.ShelterType);
            return result == -1 ? View(productViewModel) : View(shelterTypeModel);
        }


        [HttpGet]
        public ActionResult EditShelterType(short id)
        {
            var model = new ProductViewModel();
            var data = _shelterRepository.GetShelters(id);
            foreach (var item in data)
            {
                model.ShelterType = item;
                model.BranchTypesSelectListItems = _branchTypeRepository.GetBranchTypes().ToSelectListItems();
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditShelterType(ProductViewModel shelterTypeModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _shelterRepository.AddShelter(shelterTypeModel.ShelterType);
            if (result == -1)
            {
                InitializeProduct();
                return View(productViewModel);
            }

            return View(shelterTypeModel);
        }


        [HttpGet]
        public ActionResult DetailsShelterType(short id)
        {
            var model = new ProductViewModel();
            var data = _shelterRepository.GetShelters(id);

            foreach (var item in data)
            {
                model.ShelterType = item;
                model.BranchType = _branchTypeRepository.GetBranchTypes(item.BranchCode).First();
            }
            return View(model);
        }
        #endregion

        #region Product AdditionalTypeInfo

        [HttpGet]
        public ActionResult ListOfAdditionalTypeInfos()
        {
            var additionalTypeInfo = _additionalTypeInfoRepository.GetAdditionalTypeInfos();
            return View(additionalTypeInfo);
        }

        [HttpGet]
        public ActionResult CreateAdditionalTypeInfo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAdditionalTypeInfo(ProductViewModel additionalTypeInfoModel)
        {
            if (!ModelState.IsValid) return View(additionalTypeInfoModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _additionalTypeInfoRepository.AddAdditionalTypeInfo(additionalTypeInfoModel.AdditionalTypeInfo);
            return result == -1 ? View() : View(additionalTypeInfoModel);
        }


        [HttpGet]
        public ActionResult EditAdditionalTypeInfo(short id)
        {
            var model = new ProductViewModel();
            var data = _additionalTypeInfoRepository.GetAdditionalTypeInfos(id);
            foreach (var item in data)
            {
                model.AdditionalTypeInfo = item;
            }
            return View(model);
        }


        [HttpPost]
        public ActionResult EditAdditionalTypeInfo(ProductViewModel additionalTypeInfoModel)
        {
            if (!ModelState.IsValid) return View(additionalTypeInfoModel);
            if (!Request.IsAjaxRequest()) return View();

            var result = _additionalTypeInfoRepository.AddAdditionalTypeInfo(additionalTypeInfoModel.AdditionalTypeInfo);
            return result == -1 ? View() : View(additionalTypeInfoModel);
        }


        [HttpGet]
        public ActionResult DetailsAdditionalTypeInfo(short id)
        {
            var model = new ProductViewModel();
            var data = _additionalTypeInfoRepository.GetAdditionalTypeInfos(id);
            foreach (var item in data)
            {
                model.AdditionalTypeInfo = item;
            }
            return View(model);
        }
        #endregion

        #region Product Basic Data

        [HttpGet]
        public ActionResult ListOfProductBasicData()
        {
            var productBasic = _productRepository.GetProducts();
            return View(productBasic);
        }

        [HttpGet]
        public ActionResult EditProductBasicData(short id)
        {
            InitializeProduct();
            var data = _productRepository.GetProducts(id); ;
            foreach (var item in data)
            {
                productViewModel.Product = item;
            }
            return View(productViewModel);

        }

        [HttpGet]
        public ActionResult DetailProductBasicData(short id)
        {
            InitializeProduct();
            var data = _productRepository.GetProducts(id); ;
            foreach (var item in data)
            {

                productViewModel.Product = item;
                productViewModel.InsuranceCompanies = _insuranceCompanyRepository.GetInsuranceCompanies(item.InsuranceCompanyId).ToSelectListItems();
                productViewModel.CurrencyRatesSelectListItems = _currencyRatesRepository.GetCurrencyRates(item.CurrencyRateId).ToSelectListItems();
                productViewModel.ShelterSelectListItems = _shelterRepository.GetShelters(item.BranchId).ToSelectListItems();
                productViewModel.RepresentativeSelectListItems = _representativeRepository.GetRepresentatives(item.RepresentativeId).ToSelectListItems();
                productViewModel.TypeIndexingSelectListItems = _typesIndexingRepository.GetTypesIndexings(item.TypeIndexingId).ToSelectListItems();
            }
            return View(productViewModel);
        }


        [HttpPost]
        public ActionResult EditProductBasicData(ProductViewModel productBasicModel)
        {
            if (!ModelState.IsValid) return View();
            if (!Request.IsAjaxRequest()) return View();
            var result = _productRepository.AddProductBasicData(productBasicModel.Product);
            InitializeProduct();
            if (result == -1)
            {
                return View(productViewModel);
            }
            return View(productBasicModel);
        }

        #endregion

        #endregion

        #region Product Creation

        public ActionResult CreateProduct()
        {
            InitializeProduct();

            return View(productViewModel);
        }

        public ActionResult CreateProductBasic(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return PartialView(productViewModelParams);
            if (!Request.IsAjaxRequest()) return PartialView();

            var result = _productRepository.AddProductBasicData(productViewModelParams.Product);
            InitializeProduct();
            return result == -1 ? PartialView(productViewModel) : PartialView(productViewModelParams);
        }

        public ActionResult CreateProductPaymentPlan(ProductViewModel model)
        {
            if (!ModelState.IsValid) return PartialView(model);
            if (!Request.IsAjaxRequest()) return PartialView();

            var productPayPlanModel = model.MapProductPayPlan(model);
            var result = _productRepository.AddProductPayPlan(productPayPlanModel);
            InitializeProduct();
            return result == -1 ? PartialView(productViewModel) : PartialView(model);
        }

        public ActionResult CreateProductShelterAndCondition(ProductViewModel model)
        {
            if (!ModelState.IsValid) return PartialView(model);
            if (!Request.IsAjaxRequest()) return PartialView();

            var productShelterAndCondition = model.MapProductShelterAndCondition(model);
            var result = _productRepository.AddShelterAndCondition(productShelterAndCondition);
            InitializeProduct();
            return result == -1 ? PartialView(productViewModel) : PartialView(model);
        }

        public ActionResult CreateProductPremiumUnder(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return PartialView(productViewModelParams);
            if (!Request.IsAjaxRequest()) return PartialView();

            var result = _premiumsUnderRepository.AddPremiumsUnder(productViewModelParams.PremiumsUnder);
            InitializeProduct();
            return result == -1 ? PartialView(productViewModel) : PartialView(productViewModelParams);
        }


        //Initialize Product values
        private void InitializeProduct()
        {
            productViewModel.ProductSelectListItems = _productRepository.GetProducts().ToSelectListItems(0);

            productViewModel.MethodOfPaymentSelectListItems =
                _methodOfPaymentRepository.GetMethodOfPaymentOptions().ToSelectListItems();

            productViewModel.MeansOfPaymentSelectListItems =
                _meansOfPaymentRepository.GetMeansOfPaymentOptions().ToSelectListItems();

            productViewModel.InsuranceCompanies =
                _insuranceCompanyRepository.GetInsuranceCompanies().ToSelectListItems();

            productViewModel.ProductPlanSelectItems = _productPlanRepository.GetProductsPlan().ToSelectListItems();
            productViewModel.ShelterSelectListItems = _shelterRepository.GetShelters().ToSelectListItems();
            productViewModel.BranchTypesSelectListItems = _branchTypeRepository.GetBranchTypes().ToSelectListItems();
            productViewModel.CurrencyRatesSelectListItems =
                _currencyRatesRepository.GetCurrencyRates().ToSelectListItems();

            productViewModel.RepresentativeSelectListItems =
                _representativeRepository.GetRepresentatives().ToSelectListItems();
            productViewModel.TypeIndexingSelectListItems =
                _typesIndexingRepository.GetTypesIndexings().ToSelectListItems();

            productViewModel.AdditionalTypeInfosSelectListItems =
                _additionalTypeInfoRepository.GetAdditionalTypeInfos().ToSelectListItems();

        }

        #endregion

        #region Ajax Product Creation

        public ActionResult CreateProductMethodOfPayment(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return PartialView(productViewModel);
            if (!Request.IsAjaxRequest()) return PartialView();


            var result = _methodOfPaymentRepository.AddMethodPayment(productViewModelParams.MethodOfPayment);
            return result > 0 ? PartialView() : PartialView(productViewModel);
        }

        public ActionResult CreateProductMeansOfPayment(ProductViewModel productViewModelParams)
        {
            if (!ModelState.IsValid) return PartialView(productViewModelParams);
            if (!Request.IsAjaxRequest()) return PartialView();

            var result = _meansOfPaymentRepository.AddMeansOfPayment(productViewModelParams.MeansOfPayment);
            return result > 0 ? PartialView() : PartialView(productViewModelParams);
        }

        //public ActionResult CreateProductOptions(ProductViewModel productViewModelParams)
        //{
        //    if (!ModelState.IsValid) return PartialView(productViewModelParams);
        //    if (!Request.IsAjaxRequest()) return PartialView();

        //    var result = _productOptionsRepository.AddProductOptions(productViewModelParams.ProductOptions);
        //    return result > 0 ? PartialView() : PartialView(productViewModelParams);
        //}


        #endregion


    }
}
