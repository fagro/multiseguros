﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.WebUI.Models
{
    public class MassivePolicyGeneratorViewModel
    {
        public Product Product { get; set; }

        [Display(Name = "Subir Archivo")]
        public HttpPostedFileBase FileUpload { get; set; }
    }
}