﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Multi_Insurance.WebUI.Models
{
    public class HomeViewModel
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public IEnumerable<SelectListItem> ProductSelectListItems { get; set; }
    }
}