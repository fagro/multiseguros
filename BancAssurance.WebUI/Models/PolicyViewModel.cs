﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.WebUI.Models
{
    public class PolicyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ProductPlan ProductPlan { get; set; }
        public MethodOfPaymentOptions MethodOfPaymentOptions { get; set; }
        public MethodOfPayment MethodOfPayments { get; set; }
               
        //TODO: Account numbers by means of payment
        public ProductOptions ProductOptions { get; set; }

        public short Status { get; set; }
        //Client Values
        public Client Client { get; set; }

        //Policy Values
        public Policy Policy { get; set; }
        public MeansOfPaymentOptions MeansOfPaymentOptions { get; set; }
        
        public DateTime Created { get; set; }
        public DateTime Modified { get { return DateTime.Now.Date; } }
        public string UserName { get; set; }



        //Insured
        public Insured Insured { get; set; }


        //Values for policy
        [Display(Name = "Plan")]
        public short PlanCode { get; set; }
        public IEnumerable<SelectListItem> ProductPlansSelectListItems { get; set; }

        [Display(Name = "Opciones de producto")]
        public short ValueOptions { get; set; }
        public IEnumerable<SelectListItem> ProductOptionSelectListItems { get; set; }

        [Display(Name = "Número de cuenta")]
        public string AccountNumber { get; set; }

        [Display(Name = "Prima")]
        public decimal PremiumsUnder { get; set; }
        public IEnumerable<SelectListItem> PremiumsUnderSelectListItems { get; set; }

        [Display(Name = "Forma de pago")]
        public short MethodOfPayment { get; set; }
        public IEnumerable<SelectListItem> MethodOfPaymentOptionsSelectListItems { get; set; }

        [Display(Name = "Medio de pago")]
        public short MeansOfPayment { get; set; }
        public IEnumerable<SelectListItem> MeansOfPaymentSelectListItemss { get; set; }

        public IEnumerable<Beneficiary> Beneficiaries { get; set; }

        public IEnumerable<Dependent> Dependents { get; set; }
    }
}