﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.WebUI.Models
{
    public static class ViewModelMapper
    {
        
        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ProductPlan> productPlans
           )
        {

            var items = from s in productPlans
                orderby s.PlanName
                select
                    new SelectListItem
                    {
                        Text = s.PlanName,
                        Value = s.ProductId.ToString()
                    };
            return items;
        }

      public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<MethodOfPaymentOptions>  methodOfPaymentOptions)
        {

            var items = from s in methodOfPaymentOptions
                        orderby s.MethodOfPaymentOptionName
                        select
                            new SelectListItem
                            {                                
                                Text = s.MethodOfPaymentOptionName,
                                Value = s.MethodOfPaymentOptionsCode.ToString()
                            };
            return items;
        }


        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<MeansOfPaymentOptions> meansOfPaymentOptions)
        {

            var items = from s in meansOfPaymentOptions
                        orderby s.MeansOfPaymentName
                        select
                            new SelectListItem
                            {
                                Text = s.MeansOfPaymentName,
                                Value = s.MeansOfPaymentCode.ToString()
                            };
            return items;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ProductOptions> productOptions,
       int planCode)
        {
            var items = from s in productOptions
                        orderby s.ValueOptions
                        select
                            new SelectListItem
                            {
                                Selected = (s.PlanCode == planCode),
                                Text = s.ValueOptions.ToString(),
                                Value = s.PlanCode.ToString() 
                            };
            return items;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<PremiumsUnder> premiumsUnders)
        {

            var items = from s in premiumsUnders
                        orderby s.PremiumsUnderValue
                        select
                            new SelectListItem
                            {
                                Text = s.PremiumsUnderValue.ToString(),
                                Value = s.Id.ToString()
                            };
            return items;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Product> products, int selectedId)
        {
            var items = from p in products
                orderby p.Id
                select new SelectListItem
                {
                    Selected = (p.Id == selectedId),
                    Text = p.Name,
                    Value = p.Id.ToString()
                };

            return items;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<ShelterType> shelters)
        {
            return
                shelters.OrderBy(a => a.Id)
                    .Select(a => new SelectListItem {Text = a.ShelterName, Value = a.Id.ToString()});
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<InsuranceCompany> companies)
        {
            return companies.OrderBy(a => a.Name).Select(a => new SelectListItem {Text = a.Name, Value = a.Id.ToString()});
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<BranchType> branchTypes)
        {
            return
                branchTypes.OrderBy(a => a.Id).Select(a => new SelectListItem {Text = a.Name, Value = a.Id.ToString()});
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<CurrencyRates> currencyRates)
        {
            return
                currencyRates.OrderBy(a => a.CurrencyRatesId).Select(a => new SelectListItem { Text = a.Name, Value = a.CurrencyRatesId.ToString() });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Representative> representatives)
        {
            return
                representatives.OrderBy(a => a.RepresentativeId).Select(a => new SelectListItem { Text = a.Name, Value = a.RepresentativeId.ToString() });
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<TypesIndexing> typesIndexings)
        {
            return
                typesIndexings.OrderBy(a => a.TypeIndexingId).Select(a => new SelectListItem { Text = a.Name, Value = a.TypeIndexingId.ToString() });
        }

            public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<AdditionalTypeInfo> additionalTypeInfos)
        {
            return
                additionalTypeInfos.OrderBy(a => a.Id).Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() });
        }
    }
}