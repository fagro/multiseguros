﻿using System.Collections.Generic;
using System.Web.Mvc;
using Multi_Insurance.DataAccessLayer.Entities;

namespace Multi_Insurance.WebUI.Models
{
    public class ProductViewModel
    {
        public InsuranceCompany InsuranceCompany { get; set; }
        public IEnumerable<SelectListItem> InsuranceCompanies { get; set; }
        public Product Product { get; set; }
        public IEnumerable<SelectListItem> ProductSelectListItems { get; set; }
        public ProductPlan ProductPlan { get; set; }
        public IEnumerable<SelectListItem> ProductPlanSelectItems { get; set; }
        public ProductOptions ProductOptions { get; set; }
        public MethodOfPayment MethodOfPayment { get; set; }
        public IEnumerable<SelectListItem> MethodOfPaymentSelectListItems { get; set; } 
        public MethodOfPaymentOptions MethodOfPaymentOptions { get; set; }
        public MeansOfPayment MeansOfPayment { get; set; }
        public IEnumerable<SelectListItem> MeansOfPaymentSelectListItems { get; set; }
        public MeansOfPaymentOptions MeansOfPaymentOptions { get; set; }
        public PremiumsUnder PremiumsUnder { get; set; }
        public IEnumerable<SelectList> PremiumsUnderSelectItems { get; set; }
        
        public ShelterType ShelterType { get; set; }
        public Shelter Shelter { get; set; }
        public IEnumerable<SelectListItem> ShelterSelectListItems { get; set; }

        public Representative Representative { get; set; }
        public IEnumerable<SelectListItem> RepresentativeSelectListItems { get; set; }

        public TypesIndexing TypesIndexing { get; set; }
        public IEnumerable<SelectListItem> TypeIndexingSelectListItems { get; set; }
        
        
        public CurrencyRates CurrencyRates { get; set; }
        public IEnumerable<SelectListItem> CurrencyRatesSelectListItems { get; set; }


        public BranchType BranchType { get; set; }
        public IEnumerable<SelectListItem>  BranchTypesSelectListItems { get; set; }

        public AdditionalTypeInfo AdditionalTypeInfo { get; set; }
        public IEnumerable<SelectListItem> AdditionalTypeInfosSelectListItems { get; set; }
        public AdditionalType AdditionalType { get; set; }

        public Dictionary<string, string> MapProductPayPlan(ProductViewModel model)
        {
            var result = new Dictionary<string, string>
            {
                {"productCode", model.ProductOptions.PlanCode.ToString()},
                {"ValueOptions", model.ProductOptions.ValueOptions.ToString()},
                {"MethodOfPaymentOptionsId", model.MethodOfPaymentOptions.MethodOfPaymentOptionsCode.ToString()},
                {"MeansOfPaymentOptionsId", model.MeansOfPaymentOptions.MeansOfPaymentCode.ToString()},
                {"productId", model.Product.Id.ToString()}
            };
            return result;
        }

        public Dictionary<string, string> MapProductShelterAndCondition(ProductViewModel model)
        {
            var result = new Dictionary<string, string>
            {
                {"ProductId", model.Product.Id.ToString()},
                {"ShelterValue", model.Shelter.ShelterValue.ToString()},
                {"ShelterId", model.Shelter.ShelterTypeId.ToString()},
                {"AdditionalInfoValue", model.AdditionalType.AdditionalValue.ToString()},
                {"AdditionalTypeId", model.AdditionalType.AdditionalTypeInfoId.ToString()}
            };
            return result;
        }
     
    }
}